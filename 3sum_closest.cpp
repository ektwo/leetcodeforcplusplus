#include <limits.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <queue>


using namespace std;


class Solution {

public:
    vector<int> twoSum(vector<int>& nums, int target) {
        vector<int> ans;

        map<int, int> m;
        for (int i = 0; i < nums.size(); ++i) {
            if (m.find(nums[i]) != m.end()) {
                ans.push_back(m[nums[i]]);
                ans.push_back(i);
                return ans;
            }else {
                m[target - nums[i]] = i;
            }
            
        }

        return ans;
    }

    int threeSumClosest(vector<int>& nums, int target) {
        int ans = 0;

        int diff = INT_MAX;

        sort(nums.begin(), nums.end());

        for (int i = 0; i < nums.size() - 2; ++i) {
            int left = i + 1;
            int right = nums.size() - 1;
            while (left < right) {
                int sum = nums[i] + nums[left] + nums[right];
                int new_diff = abs(sum - target);
                if (new_diff < diff) {
                    diff = new_diff;
                    ans = sum;
                }

                if (sum < target) ++left;
                else --right;
            }
        }


        return ans;
    }
};

int main()
{
    Solution solution;
    {
        vector<int> nums = { -1,2,1,-4 };
        cout << solution.threeSumClosest(nums, 1) << endl;
    }

    {
        vector<int> nums = { 0,0,0 };
        cout << solution.threeSumClosest(nums, 1) << endl;
    }

    return 0;
}
