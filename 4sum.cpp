#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>

using namespace std;

void print_2d_int(vector<vector<int>> nums)
{
    cout << "{";
    for (int i = 0; i < nums.size(); ++i) {
        cout << "{";
        for (int j = 0; j < nums[0].size(); ++j) {
            cout << nums[i][j] << ",";
        }
        cout << "}," << endl;
    }
    cout << "}," << endl;
}
class Solution {
public:
    vector<vector<int>> fourSum(vector<int>& nums, int target) {
        vector<vector<int>> ans;
        sort(nums.begin(), nums.end());

        for (int k = 0; k < nums.size(); ++k) {
            if (nums[k] > target && nums[k] >= 0) break; // pruning
            if (k > 0 && nums[k] == nums[k-1]) continue; // deduplicate

            for (int i=k+1; i<nums.size();++i) {
                if (nums[k]+nums[i] > target && nums[k]+nums[i]>=0) break;
                if (i>k+1 && nums[i]==nums[i-1]) continue;

                int left = i+1;
                int right = nums.size()-1;
                while (right > left) {
                    if ((long)nums[k]+nums[i]+nums[left]+nums[right]>target) --right;
                    else if ((long)nums[k]+nums[i]+nums[left]+nums[right]<target) ++left;
                    else {
                        ans.push_back({nums[k],nums[i],nums[left],nums[right]});
                        while (right > left && nums[right] == nums[right - 1]) right--;
                        while (right > left && nums[left] == nums[left + 1]) left++;
                        ++left;
                        --right;
                    }
                }

            }
        }
        return ans;
    }
};

int main()
{
    Solution solution;
    {
        vector<int> nums = {1,0,-1,0,-2,2};
        int target = 0;
        vector<vector<int>> ans = solution.fourSum(nums, target);
        print_2d_int(ans);
    }
    {
        vector<int> nums = {2,2,2,2,2};
        int target = 8;
        vector<vector<int>> ans = solution.fourSum(nums, target);
        print_2d_int(ans);
    }

    return 0;
}
