#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>

using namespace std;


class Solution {
public:
    int fourSumCount(vector<int>& nums1, vector<int>& nums2, vector<int>& nums3, vector<int>& nums4) {
        int ans = 0;
        unordered_map<int, int> m;
        for (int i = 0; i < nums1.size(); ++i) {
            for (int j = 0; j < nums2.size(); ++ j) {
                m[nums1[i]+nums2[j]]++;
            }
        }
        for (const auto&c : nums3) {
            for (const auto &d : nums4) {
                if (m.find(0 - (c+d)) != m.end()) {
                    ans += m[0-(c+d)];
                    
                }
            }
        }
        return ans;
    }
};

int main()
{
    Solution solution;
    {
        vector<int> nums1 = {1,2};
        vector<int> nums2 = {-2,-1};
        vector<int> nums3 = {-1,2};
        vector<int> nums4 = {0,2};
        int ans = solution.fourSumCount(nums1, nums2, nums3, nums4);
        cout << ans << endl;
    }
    {
        vector<int> nums1 = {0};
        vector<int> nums2 = {0};
        vector<int> nums3 = {0};
        vector<int> nums4 = {0};
        int ans = solution.fourSumCount(nums1, nums2, nums3, nums4);
        cout << ans << endl;
    }

    return 0;
}
