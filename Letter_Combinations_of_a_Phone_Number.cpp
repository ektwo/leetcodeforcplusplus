#include <iostream>
#include <vector>

using namespace std;

class Solution
{
  public:
    vector<string> letterCombinations(string digits)
    {
        vector<string> ret = {};
        int n;
        static const vector<string> chLists = {"", "", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"};

        if (digits.empty())
            return ret;

        ret.push_back("");

        for (int i = 0; i < digits.size(); ++i)
        { //2,3
            n = digits[i] - '0';
            if ((n < 0) || (n > 9))
                break;
            const string &candidate = chLists[n];
            if (candidate.empty())
                continue;

            vector<string> tmp;
            //for(int j = 0 ; j < candidate.size() ; ++j) {
            for (int k = 0; k < ret.size(); ++k)
            { //0,1,2,0,1,2 = a-b-c, d-e-f
                for (int j = 0; j < chLists[n].size(); ++j)
                {
                    //cout << " ret[" << k << "]=" << ret[k].c_str() << endl;
                    tmp.push_back(ret[k] + chLists[n][j]); //a, b, c
                    //tmp.push_back(ret[j] + chLists[n][j]); //a, b, c
                    //for (int x = 0; x < tmp.size(); ++x)
                    //    cout << " x=" << tmp[x] << endl; //a, b, c
                }
            }
            ret.swap(tmp);
        }

        return ret;
    }
};

int main()
{
    Solution s;
    //string str("23");
    string str("");
    vector<string> ret = s.letterCombinations(str);

    for (int i = 0; i < ret.size(); ++i)
    {
        cout << "i=" << i << "ret=" << ret[i] << endl;
    }

    return 0;
}