#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>


using namespace std;

//Definition for singly-linked list.
 struct ListNode {
      int val;
      ListNode *next;
      ListNode() : val(0), next(nullptr) {}
      ListNode(int x) : val(x), next(nullptr) {}
      ListNode(int x, ListNode *next) : val(x), next(next) {}
 };



//class Solution {
//public:
//    ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {
//        ListNode* l1_tmp = l1, *l1_end;
//        ListNode* l2_tmp = l2, * l2_end;
//        int l1_len = 0, l2_len = 0, max_len;
//        int l1_num = 0, l2_num = 0, total_val = 0, total_sum = 0;
//
//        {
//            while (l1_tmp) {
//                l1_len++;
//                l1_tmp = l1_tmp->next;
//            }
//            cout << "l1_len=" << l1_len << endl;
//            l1_end = l1_tmp;
//
//            while (l2_tmp) {
//                l2_len++;
//                l2_tmp = l2_tmp->next;
//            }
//            cout << "l2_len=" << l2_len << endl;
//            l2_end = l2_tmp;
//        }
//
//        int l1_len2 = l1_len;
//        int l2_len2 = l2_len;
//        l1_tmp = l1;
//        l2_tmp = l2;
//        l1_num = 0;
//        l2_num = 0;
//
//        //total_val = l1_num + l2_num;
//        //total_sum = total_val;
//        //cout << "total_val=" << total_val << endl;
//        int digits = 0;
//        //while (total_sum) {
//        //    total_sum /= 10;
//        //    ++digits;
//        //}
//        //cout << "digits=" << digits << endl;
//
//        l1_tmp = l1;
//        l2_tmp = l2;
//        ////while (l1_tmp) {
//        ////    l1_len++;
//        ////    l1_tmp = l1_tmp->next;
//        ////}
//        ////
//
//        max_len = (l1_len > l2_len) ? (l1_len) : (l2_len);
//        cout << "max_len=" << max_len << endl;
//
//        //ListNode *ret = new ListNode[max_len];
//        int sum = 0;
//        int carry = 0;
//        int ret_num = 0;
//        ListNode **ret = new ListNode*[max_len + 1];
//        ListNode* prev = NULL;
//        while (l1_tmp || l2_tmp) {
//            sum = ((l1_tmp) ? (l1_tmp->val) : (0)) + ((l2_tmp) ? (l2_tmp->val) : 0);
//            sum += carry;
//            cout << "l1_tmp->val=" << l1_tmp->val << endl;
//            cout << "l2_tmp->val=" << l2_tmp->val << endl;
//            cout << "sum=" << sum << endl;
//            if (sum > 9) {
//                sum -= 10;
//                ret[ret_num] = new ListNode(sum);
//                carry = 1;
//                cout << "1 sum=" << sum << endl;
//                cout << "1 carry=" << carry << endl;
//            }
//            else {
//                ret[ret_num] = new ListNode(sum);
//                carry = 0;
//                cout << "2 sum=" << sum << endl;
//                cout << "2 carry=" << carry << endl;
//            }
//            
//            if (!prev) prev = ret[0];
//            else prev->next = ret[ret_num];
//            prev = ret[ret_num];
//
//            if (l1_tmp) l1_tmp = l1_tmp->next;
//            if (l2_tmp) l2_tmp = l2_tmp->next;
//            ++ret_num;
//        }
//
//        ListNode* t = ret[0];
//        while (t) {
//            cout << t->val << endl;
//            t = t->next;
//        }
//        return ret[0];
//    }
//};


 //class Solution {
 //public:
 //    ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {
 //       ListNode* l1_tmp = l1;
 //       ListNode* l2_tmp = l2;
 //       int l1_len = 0, l2_len = 0, max_len;

 //       while (l1_tmp) {
 //           l1_len++;
 //           l1_tmp = l1_tmp->next;
 //       }

 //       while (l2_tmp) {
 //           l2_len++;
 //           l2_tmp = l2_tmp->next;
 //       }

 //       l1_tmp = l1;
 //       l2_tmp = l2;
 //       //max_len = (l1_len > l2_len) ? (l1_len) : (l2_len);

 //       int sum = 0;
 //       int carry = 0;
 //       int ret_num = 0;
 //       //ListNode** ret = new ListNode * [max_len + 1];
 //       ListNode* ret = (l1_len > l2_len) ? l1 : l2;
 //       ListNode *head = ret;
 //       ListNode* newl = NULL;
 //       while (l1_tmp || l2_tmp || (carry == 1)) {
 //           if (carry == 1 && !l1_tmp && !l2_tmp) {
 //               ListNode* newl = new ListNode(1);
 //               ret->next = newl;
 //           }
 //           sum = ((l1_tmp) ? (l1_tmp->val) : (0)) + ((l2_tmp) ? (l2_tmp->val) : 0);
 //           sum += carry;
 //           if (sum > 9) {
 //               sum -= 10;
 //               ret->val = sum;
 //               //ret[ret_num] = new ListNode(sum);
 //               carry = 1;
 //           }
 //           else {
 //               ret->val = sum;
 //               //ret[ret_num] = new ListNode(sum);
 //               carry = 0;
 //           }

 //           //if (!prev) prev = ret[0];
 //           //else prev->next = ret[ret_num];
 //           //prev = ret[ret_num];
 //           ret = ret->next;

 //           if (l1_tmp) l1_tmp = l1_tmp->next;
 //           if (l2_tmp) l2_tmp = l2_tmp->next;
 //           //++ret_num;
 //       }

 //       return head;// ret[0];
 //    }
 //};

class Solution {
public:
    ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {
        ListNode* l1_tmp = l1;
        ListNode* l2_tmp = l2;
        int l1_len = 0, l2_len = 0;

        while (l1_tmp) {
            l1_len++;
            l1_tmp = l1_tmp->next;
        }

        while (l2_tmp) {
            l2_len++;
            l2_tmp = l2_tmp->next;
        }
        l1_tmp = l1;
        l2_tmp = l2;

        int carry = 0;
        int cnt = 0;
        int max_len = (l1_len > l2_len) ? (l1_len) : (l2_len);

        ListNode* ret = (l1_len > l2_len) ? l1 : l2;
        ListNode* head = ret;
        while (cnt++ < max_len) {
            int sum = ((l1_tmp) ? (l1_tmp->val) : (0)) + ((l2_tmp) ? (l2_tmp->val) : 0) + carry;
            cout << sum << endl;
            if (sum > 9) {
                sum -= 10;
                carry = 1;
            } else             
                carry = 0;
            ret->val = sum;

            if (l1_tmp) l1_tmp = l1_tmp->next;
            if (l2_tmp) l2_tmp = l2_tmp->next;
            if (ret->next) ret = ret->next;
        }
        if (carry == 1)
            ret->next = new ListNode(1);

        return head;
    }
};

int main()
{
    Solution s;
    {
        ListNode** lna = new ListNode * [7];
        ListNode** lnb = new ListNode * [4];

        lna[6] = new ListNode(9, nullptr);
        lna[5] = new ListNode(9, lna[6]);
        lna[4] = new ListNode(9, lna[5]);
        lna[3] = new ListNode(9, lna[4]);
        lna[2] = new ListNode(9, lna[3]);
        lna[1] = new ListNode(9, lna[2]);
        lna[0] = new ListNode(9, lna[1]);
        lnb[3] = new ListNode(9, nullptr);
        lnb[2] = new ListNode(9, lnb[3]);
        lnb[1] = new ListNode(9, lnb[2]);
        lnb[0] = new ListNode(9, lnb[1]);

        ListNode *ret = s.addTwoNumbers(lna[0], lnb[0]);
        ListNode* ret_tmp = ret;
        cout << "[";
        while (ret_tmp) {
            if (!ret_tmp->next)
                cout << ret_tmp->val;
            else
                cout << ret_tmp->val << ",";
            
            ret_tmp = ret_tmp->next;
        }
        cout << "]" << endl;


        //while (ret) {
        //    ret_tmp = ret;
        //    if (ret->next) {
        //        ret = ret->next;
        //        delete ret_tmp;
        //    }
        //}
    }
}