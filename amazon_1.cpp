#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <cmath>

using namespace std;


class Solution {
public:

	int amazon_1(vector<int>& nums, int channels) {
        int ans = 0;
        int sum = 0;
        int n = nums.size();
        for (auto &num : nums) sum += num;
        cout << "sum=" << sum << endl;
        if (channels == 1) {
            ans = ceil(sum / n);
        }
        else {
            //ans = ceil(channels * (sum / (float)n));
            ans = ceil((channels * sum) / (float)n);
            cout << "(channels * sum) / (float)n=" << (channels * sum) / (float)n << endl;
            cout << "ceil((channels * sum) / (float)n)=" << ceil((channels * sum) / (float)n)  << endl;
        }

        return ans;
	}
};

int main()
{
	Solution solution;

	{
		vector<int> nums = { 1,2,3,4,5 };
        int channels = 1;
		cout << solution.amazon_1(nums, channels) << endl;
	}
	{
		vector<int> nums = { 1,2,3,4,6 };
        int channels = 2;
		cout << solution.amazon_1(nums, channels) << endl;
	}
	{
		vector<int> nums = { 1,2,3,4,5 };
        int channels = 3;
		cout << solution.amazon_1(nums, channels) << endl;
	}
	return 0;
}