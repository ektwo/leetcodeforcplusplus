#include <iostream>   
#include<string>
#include<algorithm>
#include<vector>
using namespace std;
void splitStr2Vec(string s, vector<string>& buf);
int main()
{
	string s = "John/Ammy\rDue/Sam";
	vector<string> buf;
	splitStr2Vec(s, buf);
	for (string tmp : buf)
		cout << tmp << endl;
}

void splitStr2Vec(string s, vector<string>& buf)
{
	int current = 0; //最初由 0 的位置開始找
	int next;
	while (1)
	{
		next = s.find_first_of("/\r", current);
		if (next != current)
		{
			string tmp = s.substr(current, next - current);
			if (tmp.size() != 0) //忽略空字串
				buf.push_back(tmp);
		}
		if (next == string::npos) break;
		current = next + 1; //下次由 next + 1 的位置開始找起。
	}
}