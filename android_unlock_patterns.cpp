#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>


using namespace std;


//
//class Solution {
//public:
//
//	void dfs(vector<vector<char>>& board, int x, int y, unordered_set<string>& result, Trie* trie, string s) {
//		const int n = board.size();
//		const int m = board[0].size();
//		if (x < 0 || y < 0 || y == n || x == m) return;
//
//		char c = board[y][x];
//		if (c == '$') return;
//
//		Trie* child = trie->children[c - 'a'];
//		if (!child) return;
//
//		board[y][x] = '$';
//
//			string tmp_s = s + c;
//			if (child->isEndOfword) result.insert(tmp_s);
//			//if (i < board.size() - 1) 
//				dfs(board, x + 1, y, result, child, tmp_s);
//				//if (i > 0) 
//				dfs(board, x - 1, y, result, child, tmp_s);
//			//if (j < board[0].size() - 1) 
//				dfs(board, y, y + 1, result, child, tmp_s);
//			//if (j > 0) 
//				dfs(board, y, y - 1, result, child, tmp_s);
//
//		board[y][x] = c;
//	}
//
//	vector<string> findWords(vector<vector<char>>& board, vector<string>& words) {
//		if (words.size() == 0) return {};
//		Trie trie;
//		for (auto& w : words)
//			trie.insert(w);
//
//		unordered_set<string> result;
//
//		for (int i = 0; i < board.size(); ++i) {
//			for (int j = 0; j < board[0].size(); ++j) {
//				dfs(board, i, j, result, &trie, "");
//			}
//		}
//
//		vector<string> result_v(result.begin(), result.end());
//
//		return result_v;
//	}
//};

class TrieNode {
public:
    vector<TrieNode*> child;
    bool isEndOfword;
    string str;
    TrieNode() : child(26, nullptr), isEndOfword(false), str("") {};
    ~TrieNode() {
        for (auto c : child) delete c;
    }
};

class Trie {
public:
    TrieNode* root;
    Trie() : root(new TrieNode()) {};

    void insert(string word) {
        TrieNode* p = root;
        for (char c : word) {
            int i = c - 'a';
            if (!p->child[i])
                p->child[i] = new TrieNode();
            p = p->child[i];
        }
        p->isEndOfword = true;
        p->str = word;
    }
};

class Solution {
public:
    int numberOfPatterns(int m, int n) {
        int res = 0;
        vector<bool> visited(10, false);
        vector<vector<int>> jumps(10, vector<int>(10, 0));
        jumps[1][3] = jumps[3][1] = 2;
        jumps[4][6] = jumps[6][4] = 5;
        jumps[7][9] = jumps[9][7] = 8;
        jumps[1][7] = jumps[7][1] = 4;
        jumps[2][8] = jumps[8][2] = 5;
        jumps[3][9] = jumps[9][3] = 6;
        jumps[1][9] = jumps[9][1] = jumps[3][7] = jumps[7][3] = 5;
        res += helper(1, 1, m, n, jumps, visited, 0) * 4;
        cout << res << endl;
        res += helper(2, 1, m, n, jumps, visited, 0) * 4;
        cout << res << endl;
        res += helper(5, 1, m, n, jumps, visited, 0);
        return res;
    }
    int helper(int num, int len, int m, int n, vector<vector<int>>& jump, vector<bool>& visited, int res) {

        if (len >= m) res++;        //1>=1,res=1
        len++;                      //len=2
        if (len > n) return res;    //len>1,return=1

        //num=1,len=1,res=0,m=1,n=1
        visited[num] = true;//visited[0]=0,[1]=1,[2]=0,[3]=0,[4]=0,[5]=0,[6]=0,[7]=0,[8]=0,[9]=0
        //next==1~9
        for (int next = 1; next <= 9; ++next) {
            int jumpNext = jump[num][next];
            // (!visited[1   ] && (visited[0       ] || jumpNext == 0))//jump[1][1]==0==jumpNext
            if (!visited[next] && (visited[jumpNext] || jumpNext == 0))
                // (!visited[2   ] && (visited[0       ] || jumpNext == 0))//jump[1][2]==0==jumpNext
                res = helper(next, len, m, n, jump, visited, res); //help(2, 2, 1, m,n,jump,visited)

                //if (len >=m) res++; // res=2
                //len++; //len=3
                //if (3>1) return 2;


        }
        visited[num] = false;

        return res;
    }
};

int main()
{
    Solution solution;
    {
        cout << solution.numberOfPatterns(1, 2) << endl;
    }

    return 0;
}
