#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <queue>


using namespace std;



class Solution {

public:
    bool backspaceCompare(string s, string t) {
        
        int s_idx = 0;
        int t_idx = 0;

        for (int i = 0; i < s.length(); ++i) {
            if (s[i] != '#') {
                s[s_idx++] = s[i];
            }
            else {
                if (s_idx) {
                    s[--s_idx] = '/0';
                }
            }
            for (int i = 0; i < s_idx; ++i) {
                cout << "3 i=" << i << endl;
                cout << "3 s[i] =" << s[i]  << endl;
                cout << "3 t[i] =" << t[i]  << endl;
                if (s[i] != t[i]) return false;
            }
        }

        for (int i = 0; i < t.length(); ++i) {
            if (t[i] != '#') {
                t[t_idx++] = t[i];
            }
            else {
                if (t_idx)
                    t[--t_idx] = '/0';
            }
            for (int i = 0; i < s_idx; ++i) {
                cout << "3 i=" << i << endl;
                cout << "3 s[i] =" << s[i]  << endl;
                cout << "3 t[i] =" << t[i]  << endl;
                if (s[i] != t[i]) return false;
            }
        }

        if (s_idx != t_idx) return false;

        for (int i = 0; i < s_idx; ++i) {
            cout << "3 i=" << i << endl;
            cout << "3 s[i] =" << s[i]  << endl;
            cout << "3 t[i] =" << t[i]  << endl;
            if (s[i] != t[i]) return false;
        }

        return true;
    }
};

int main()
{
    Solution solution;
    // {
    //     string s = "ab#c";
    //     string t = "ad#c";
    //     cout << solution.backspaceCompare(s, t) << endl;
    // }
    // {
    //     string s = "ab##";
    //     string t = "c#d#";
    //     cout << solution.backspaceCompare(s, t) << endl;
    // }
    // {
    //     string s = "a#c";
    //     string t = "b";
    //     cout << solution.backspaceCompare(s, t) << endl;
    // }
    // {
    //     string s = "a##c";
    //     string t = "#a#c";
    //     cout << solution.backspaceCompare(s, t) << endl;
    // }
    // {
    //     string s = "gtc#uz#";
    //     string t = "gtc#uz##";
    //     cout << solution.backspaceCompare(s, t) << endl;
    // }
    {
        string s = "aaa###a";
        string t = "aaaa###a";
        cout << solution.backspaceCompare(s, t) << endl;
    }

    return 0;
}
