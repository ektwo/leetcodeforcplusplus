#ifndef __BASE_ALGORITHM_H__
#define __BASE_ALGORITHM_H__

#include "stdint.h "
#include "system_time.h"

class CBaseAlgorithm
{
  protected:
    int64_t m_startTime;
    int64_t m_endTime;

  public:
    CBaseAlgorithm() : m_startTime(0), m_endTime(0)
    {
    }
    ~CBaseAlgorithm()
    {
    }
};

#endif