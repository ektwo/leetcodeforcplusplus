#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>


using namespace std;


class Solution {
public:
	void expand(vector<int>& prices, int left, int right, int& best_left, int& best_right) {
		bool right_flag = false;
		while (left >= 0 && right < prices.size() && prices[left] < prices[right]) {
		//	right_flag = true;
			cout << "right=" << right << endl;
			cout << "prices[" << left << "] = " << prices[left] << " prices[" << right << "] = " << prices[right] << " prices[" << best_left << "] = " << prices[best_left] << " prices[" << best_right << "] = " << prices[best_right] << endl;
			if ((prices[right] - prices[left]) > (prices[best_right] - prices[best_left])) {
				best_left = left;
				best_right = right;
			}
			++right;
		}
		//if (right_flag)
		//	--right;



	}
	int maxProfit(vector<int>& prices) {
		int best_left = 0;
		int best_right = 0;

		for (int i = 0; i < prices.size() - 1; i++) {
			cout << "i=" << i << endl;
			expand(prices, i, i + 1, best_left, best_right);
		}

		return prices[best_right] - prices[best_left];
	}
};

int main()
{
	Solution solution;
	{
		vector<int> v = { 7,1,5,3,6,4 };
		cout << solution.maxProfit(v) << endl;
	}
	{
		vector<int> v = { 7,6,4,3,1 };
		cout << solution.maxProfit(v) << endl;
	}
	//{
	//	vector<int> v = { 5,4,-1,7,8 };
	//	cout << solution.maxSubArray(v) << endl;
	//}
	return 0;
}