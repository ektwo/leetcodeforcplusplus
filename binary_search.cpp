#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <limits>


using namespace std;


class Solution {
public:

    int search(vector<int>& nums, int target) {
        int left = 0;
        int right = nums.size() - 1;
        while (left <= right) {
            int mid = left + (right - left) / 2;
            if (nums[mid] == target) return mid;
            else if (nums[mid] < target) left = mid + 1;
            else right = mid - 1;
        }
        return -1;
    }
};

int main()
{
    Solution solution;
    {
        vector<int> nums = { -1,0,3,5,9,12};
        int target = 9;
        int ans = solution.search(nums, target);
        cout << ans << endl;
    }
    cout << endl;
    {
        vector<int> nums = { -1,0,3,5,9,12};
        int target = 2;
        int ans = solution.search(nums, target);
        cout << ans << endl;
    }

    return 0;
}