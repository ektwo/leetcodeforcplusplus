#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>


using namespace std;


struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};


class Solution {
public:
    int total = INT_MIN;
    TreeNode* real_root = NULL;
    vector<int> total_array;

    int maxPathSum(TreeNode* root) {
        if (!root) return INT_MIN;
        if (!real_root) real_root = root;
        int tmp_c_val = INT_MIN, c_val = INT_MIN, l_val = INT_MIN, r_val = INT_MIN;
        if (root->left) l_val = maxPathSum(root->left);
        if (root->right) r_val = maxPathSum(root->right);

        if ((l_val >= 0) && (r_val >= 0)) {
            tmp_c_val = max(root->val + l_val + r_val, 0);
            total_array.push_back(tmp_c_val);

            tmp_c_val = max(root->val + l_val, root->val + r_val);
            c_val = tmp_c_val;
        }
        else if (l_val >= 0) {
            tmp_c_val = max(root->val + l_val, 0);
            c_val = tmp_c_val;
        }
        else if (r_val >= 0) {
            tmp_c_val = max(root->val + r_val, 0);
            c_val = tmp_c_val;
        }
        else {
            tmp_c_val = root->val;
            c_val = tmp_c_val;
        }

        if (c_val > total) {
            total = c_val;
        }

        if (root == real_root) {
            for (int i = 0; i < total_array.size(); ++i) {
                if (total_array[i] > total)
                    total = total_array[i];
            }
            return total;
        }
        else
            return c_val;
    }
};


int main()
{
#if 1
    {
        Solution solution;
        TreeNode root(-1);
        cout << solution.maxPathSum(&root) << endl;
    }
    {
        Solution solution;
        TreeNode l(-2);
        TreeNode r(-3);
        TreeNode root(-1, &l, &r);
        cout << solution.maxPathSum(&root) << endl;
    }
    {
        Solution solution;
        TreeNode lr(4);
        TreeNode l(2, NULL, &lr);
        TreeNode r(3);
        TreeNode root(-6, &l, &r);
        cout << solution.maxPathSum(&root) << endl;
    }
    {
        Solution solution;
        TreeNode ll(4);
        TreeNode lr(4);
        TreeNode l(2, &ll, &lr);
        TreeNode r(3);
        TreeNode root(1, &l, &r);
        cout << solution.maxPathSum(&root) << endl;
    }
    {
        Solution solution;
        TreeNode l(2);
        TreeNode r(3);
        TreeNode root(1, &l, &r);
        cout << solution.maxPathSum(&root) << endl;
    }
    {
        Solution solution;
        TreeNode l(9);
        TreeNode rl(15);
        TreeNode rr(7);
        TreeNode r(20, &rl, &rr);
        TreeNode root(-10, &l, &r);
        cout << solution.maxPathSum(&root) << endl;
    }
    {
        Solution solution;
        TreeNode lll(0);
        TreeNode llr(1);
        TreeNode ll(1, &lll, &llr);
        TreeNode lrl(-1);
        TreeNode lrr(0);
        TreeNode lr(2, &lrl, &lrr);
        TreeNode l(0, &ll, &lr);

        TreeNode rll(-1);
        TreeNode rlr(0);
        TreeNode rl(0, &rll, &rlr);
        TreeNode rrl(1);
        TreeNode rrr(0);
        TreeNode rr(-1, &rrl, &rrr);
        TreeNode r(1, &rl, &rr);
        TreeNode root(1, &l, &r);
        cout << solution.maxPathSum(&root) << endl;
    }
    {
        Solution solution;
        TreeNode l(9);
        TreeNode rl(5);
        TreeNode rr(9);
        TreeNode r(-6, &rl, &rr);
        TreeNode root(8, &l, &r);
        cout << solution.maxPathSum(&root) << endl;
    }
#endif
    {
        Solution solution;
        TreeNode lr(-9);
        TreeNode l(8, NULL, &lr);


        TreeNode rllrr(2);
        TreeNode rllr(-9, NULL, &rllrr);
        TreeNode rll(-3, NULL, &rllr);
        TreeNode rl(0, &rll, NULL);
        TreeNode r(2, &rl, NULL);
        TreeNode root(-1, &l, &r);
        cout << solution.maxPathSum(&root) << endl;
    }
    return 0;
}
