#include <iostream>
#include <vector>
#include <string>
#include <queue>

using namespace std;


struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};


void print_1d_int(vector<int> nums)
{

        cout << "{";
        for (int j = 0; j < nums.size(); ++j) {
            cout << nums[j] << ",";
        }
        cout << "}," << endl;

}

void print_2d_int(vector<vector<int>> nums)
{
    cout << "{";
    for (int i = 0; i < nums.size(); ++i) {
        cout << "{";
        for (int j = 0; j < nums[0].size(); ++j) {
            cout << nums[i][j] << ",";
        }
        cout << "}," << endl;
    }
    cout << "}," << endl;
}

class Solution
{
private:
    int node_nums;
    int tree_height;
    TreeNode *m_head;
public:
    void reset() {
        node_nums = 0;
        tree_height = 0;
        m_head = nullptr;
    }

    TreeNode* get_head() {
        return m_head;
    }

    TreeNode* push(TreeNode *root, int val, bool left, bool right)
    {
        TreeNode* temp = new TreeNode(val);
        cout << "push val=" << val << " m_head=" << m_head << endl;
        cout << "push val=" << val << " root=" << root << endl;
        if (!m_head) {
            m_head = temp;
        }

        if (!root)
            root = m_head;

        if (root && left)
            root->left = temp;
        else if (root && right)
            root->right = temp;

        return temp;
    }
    void _inorderTraversal(TreeNode* root, vector<int> &ans) {
        if (!root) return;
        _inorderTraversal(root->left, ans);
        ans.push_back(root->val);
        _inorderTraversal(root->right, ans);
    }
    vector<int> inorderTraversal(TreeNode* root) {

        vector<int> ans;

        _inorderTraversal(root, ans);

        return ans;
    }
    vector<int> rightSideView(TreeNode* root) {
        if (!root) return {};
        vector<int> ans;

        queue<TreeNode*> q;
        q.push(root);
        while (!q.empty()) {
            //vector<int> tmp_solu;
            for (int i = q.size(); i > 0; --i) {
                TreeNode *tmp = q.front(); q.pop();
                if (i == 1)
                ans.push_back(tmp->val);

                if (tmp->left) q.push(tmp->left);
                if (tmp->right) q.push(tmp->right);
            }
            //ans.push_back(tmp_solu);
        }

        return ans;
    }

};

int main()
{
    Solution solution;
    solution.reset();
    {
        TreeNode *root = solution.push(nullptr, 1, false, false);
        TreeNode *l = solution.push(root, 2, true, false);
        TreeNode *r = solution.push(root, 3, false, true);
        TreeNode *lr = solution.push(r, 5, false, true);
        TreeNode *rr = solution.push(r, 4, false, true);

        vector<int> ans = solution.rightSideView(solution.get_head());
        print_1d_int(ans);
    }
    cout << endl;
    solution.reset();    
    {
        TreeNode *root = solution.push(nullptr, 1, false, false);
        TreeNode *r = solution.push(root, 3, false, true);

        vector<int> ans = solution.rightSideView(solution.get_head());
        print_1d_int(ans);
    }
    cout << endl;
    solution.reset();    
    {
        vector<int> ans = solution.rightSideView(nullptr);
        print_1d_int(ans);
    }

    return 0;
}
