#include <limits.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <queue>


using namespace std;


struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};
 

class Solution {

private:
    TreeNode *m_head;
    bool _isValidBST(TreeNode* root, long min, long max) {
        if (!root) return true;
        cout << "root=" << root << endl;
        cout << "root->val=" << root->val << endl;
        cout << "root->left=" << root->left << " min=" << min << " max=" << max << endl;
        cout << "root->right=" << root->right << " min=" << min << " max=" << max << endl;
        if (root->val <= min) return false;
        if (root->val >= max) return false;
        
        return _isValidBST(root->left, min, root->val) && _isValidBST(root->right, root->val, max);
    }

    bool _isSymmetric(TreeNode *left, TreeNode *right) {
        if (!left && !right) return true;
        if (left && !right) return false;
        if (!left && right) return false;
        if (left->val != right->val) return false;
        return _isSymmetric(left->left, right->right) && _isSymmetric(left->right, right->left);
    }

public:
    void reset() {
        m_head = nullptr;
    }

    TreeNode* get_head() {
        return m_head;
    }

    TreeNode* push(TreeNode *root, int val, bool left, bool right)
    {
        TreeNode* temp = new TreeNode(val);
        cout << "push val=" << val << " m_head=" << m_head << endl;
        cout << "push val=" << val << " root=" << root << endl;
        if (!m_head) {
            m_head = temp;
        }

        if (!root)
            root = m_head;

        if (root && left)
            root->left = temp;
        else if (root && right)
            root->right = temp;

        return temp;
    }

    bool isValidBST(TreeNode* root) {
        return _isValidBST(root, LONG_MIN, LONG_MAX);

    }

    bool isSymmetric(TreeNode* root) {
        if (!root) return true;
        return _isSymmetric(root->left, root->right);
    }

    vector<vector<int>> levelOrder(TreeNode* root) {
        if (!root) return {};
        vector<vector<int>> ans;
        queue<TreeNode*> q;
        q.push(root);


        while (!q.empty()) {
            vector<int> nodes_val;
            for (int i = q.size(); i > 0; --i) {
                TreeNode *node = q.front();
                q.pop();
                nodes_val.push_back(node->val);

                if (node->left) q.push(node->left);
                if (node->right) q.push(node->right);
            }

            ans.push_back(nodes_val);
        }

        return ans;
    }
    vector<vector<int>> zigzagLevelOrder(TreeNode* root) {
        if (!root) return {};

        int level = 1;
        vector<vector<int>> ans;
        queue<TreeNode*> q;
        q.push(root);

        while (!q.empty()) {
            vector<int> nodes_val;
            for (int i = q.size(); i > 0; --i) {
                TreeNode *node = q.front();
                q.pop();
                nodes_val.push_back(node->val);
                if (node->left) q.push(node->left);
                if (node->right) q.push(node->right);
            }

            if (level % 2 == 0)
                reverse(nodes_val.begin(), nodes_val.end());
            ans.push_back(nodes_val);
            ++level;
        }

        return ans;
    }
};
//          1
//     2          2
//  4    5     5     4
// 8 9 10 11 11 10 9   8
int main()
{
    Solution solution;

    solution.reset();
    {
        TreeNode *root = solution.push(nullptr, 3, false, false);
        TreeNode *l = solution.push(root, 9, true, false);
        TreeNode *r = solution.push(root, 20, false, true);
        TreeNode *rl = solution.push(r, 15, true, false);
        TreeNode *rr = solution.push(r, 7, false, true);

        vector<vector<int>> ans = solution.zigzagLevelOrder(solution.get_head());

        cout << "[";
        for (int i = 0; i < ans.size(); ++i) {
            cout << "[";
            for (auto &x : ans[i]) {
                cout << x << ",";
            }
            cout << "]";
        }
        cout << "]" << endl;
    }
    solution.reset();
    {
        TreeNode *root = solution.push(nullptr, 1, false, false);

        vector<vector<int>> ans = solution.zigzagLevelOrder(solution.get_head());

        cout << "[";
        for (int i = 0; i < ans.size(); ++i) {
            cout << "[";
            for (auto &x : ans[i]) {
                cout << x << ",";
            }
            cout << "]";
        }
        cout << "]" << endl;
    }
    solution.reset();
    {
        TreeNode *root = nullptr;

        vector<vector<int>> ans = solution.zigzagLevelOrder(solution.get_head());

        cout << "[";
        for (int i = 0; i < ans.size(); ++i) {
            cout << "[";
            for (auto &x : ans[i]) {
                cout << x << ",";
            }
            cout << "]";
        }
        cout << "]" << endl;
    }
    solution.reset();
    {
        TreeNode *root = solution.push(nullptr, 1, false, false);
        TreeNode *l = solution.push(root, 2, true, false);
        TreeNode *r = solution.push(root, 3, false, true);
        TreeNode *ll = solution.push(l, 4, true, false);
        TreeNode *rr = solution.push(r, 5, false, true);

        vector<vector<int>> ans = solution.zigzagLevelOrder(solution.get_head());

        cout << "[";
        for (int i = 0; i < ans.size(); ++i) {
            cout << "[";
            for (auto &x : ans[i]) {
                cout << x << ",";
            }
            cout << "]";
        }
        cout << "]" << endl;
    }
    return 0;
}
