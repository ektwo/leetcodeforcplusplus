#include <iostream>
#include <vector>
using namespace std;

template <typename T>
void printVector(vector<T> v)
{
    for (size_t i = 0; i < v.size(); i++)
    {
        cout << v[i] << " ";
    }
    cout << endl;
}

template <typename T>
void BubbleSort(vector<T> &v)
{
#if 1
    bool bSwap = true;
    while (bSwap)
    {
        bSwap = false;
        for (size_t i = 0; i < v.size() - 1; i++)
        {
            if (v[i] > v[i + 1])
            {
                swap(v[i], v[i + 1]);
                bSwap = true;
            }
        }
    }
#else
    for (int i = 0; i < v.size(); ++i)
    {
        for (int j = 0; j < v.size() - 1; ++j)
        {
            if (array[j] > array[j + 1])
            {
                tmp = array[j + 1];
                array[j + 1] = array[j];
                array[j] = tmp;
            }
        }
    }
#endif
}

int main()
{
    vector<int> a = {3, 2, 6, 1};

    printVector(a);

    BubbleSort(a);

    printVector(a);
}