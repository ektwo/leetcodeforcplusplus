quick_sort_parition
const int pivot_pos = right;
const int pivot = nums[pivot_pos];
int i = left - 1;
int j = left;
for (; j < right; ++j) {
    if (nums[j] > pivot) continue;
    ++i;
    swap(nums[i], nums[j]);
}
swap(nums[pivot_pos], nums[i+1]);image.png
return i+1;

int climbStairs(int n) {
if (n <= 1) return 1;
vector<int> dp(n);
/* fibonacci */
dp[0] = 1; dp[1] = 2;
for (int i = 2; i < n; ++i) {
    dp[i] = dp[i - 1] + dp[i - 2];
}
return dp.back();}

int minSubArrayLen(int target, vector<int>& nums) {
int n = nums.size(); //sliding window
int win_l = 0;
int win_sum = 0;
int MAX_VAL = numeric_limits<int>::max();
int min_win_size = MAX_VAL;
for (int win_r = 0; win_r < n; ++win_r) {
win_sum += nums[win_r];
while (win_sum >= target) {
min_win_size = min(min_win_size,win_r-win_l+1);
win_sum -= nums[win_l];
win_l++;}}
return (min_win_size == MAX_VAL)?(0):(min_win_size);}

vector<int> twoSum(vector<int>& nums, int target) {
if(nums.size() < 2) return {};
int l = 0, r = nums.size()-1;
while(l <= r) { //two pointer+cumulative sum
if(nums[l] + nums[r] == target) {return {l+1, r+1};}
else if(nums[l] + nums[r] > target) r--;
else if(nums[l] + nums[r] < target) l++;
}return {};}

int majorityElement(vector<int>& nums) {
int count1 = 0, candidate1 = 0;
for (int i = 0; i < nums.size(); ++i) {
int num = nums[i];//moore voting
if      (num == candidate1) ++count1;
else if (0 == count1) 
{candidate1 = nums[i]; ++count1; }
else    --count1; }
return candidate1; }

void rotate(vector<int>& nums, int k) {
if ((k %= nums.size()) == 0) return;
int n = nums.size();//
reverse(nums.begin(),nums.begin()+n-k);
reverse(nums.begin()+n-k,nums.end());
reverse(nums.begin(),nums.end());}

vector<int> nextGreaterElement(
vector<int>& nums1,vector<int>& nums2){
unordered_map<int, int> table;
for(int i = 0; i < nums1.size(); ++i) {
table[ nums1[i] ] = i;}
vector<int> ans(nums1.size(), -1);
stack<int> st;
for(int i = 0; i < nums2.size(); ++i) {
while(!st.empty() and st.top()<nums2[i])
{int cur = st.top();st.pop();
if(table.count(cur)) {
ans[ table[cur] ] = nums2[i];}}
st.push(nums2[i]);}return ans;}

ListNode* middleNode(ListNode* head) {
ListNode *slow = head, *fast = head;
while(fast->next!=nullptr && 
fast->next->next!=nullptr){
slow = slow->next;//slow & fast
fast = fast->next->next;}
return (fast->next==nullptr)?slow:slow->next;}

missingNumber(vector<int>& nums){
int ans = 0, n = nums.size();
for(int i = 0; i <= n; ++i) {
ans ^= i;} //xor bitwise
for(int idx = 0; idx < n; ++idx){
ans ^= nums[idx];} return ans;

void wallsAndGates(vector<vector<int>>& rooms) {image.png
int m = rooms.size();
int n = rooms[0].size();
// Prepare for BFS
int GATE = 0, WALL = -1, ROOM = numeric_limits<int>::max();
queue<pair<int,int>> q;
vector<pair<int,int>> dirs{{0, -1}, {-1, 0}, {0, 1}, {1, 0}};
// Put all gates into queue
for(int i = 0; i < m; ++i) {
for(int j = 0; j < n; ++j) {
if(rooms[i][j] == GATE) q.push({i,j});}}
// Start BFS
int steps = 0;
while(!q.empty()) {                        
for(int sz = q.size()-1; sz >= 0; --sz) {
pair<int, int> cur = q.front();
q.pop();
if(rooms[cur.first][cur.second] == ROOM) {
rooms[cur.first][cur.second] = steps;}
if(rooms[cur.first][cur.second] != WALL) {
// Traverse neighbor
for(int i = 0; i < dirs.size(); ++i) {
cur.first += dirs[i].first;
cur.second += dirs[i].second;
if(isValid(cur, m, n) && rooms[cur.first][cur.second] == ROOM) {
q.push(cur); }
cur.first -= dirs[i].first;
cur.second -= dirs[i].second;}}}
++steps;}}
private:
bool isValid(const pair<int, int>& cur,const int& m,const int& n)
{return cur.first>=0&&cur.first<m&&cur.second>=0&&cur.second<n;}


int numIslands(vector<vector<char>>& grid) {
int m = grid.size(), n = m ? grid[0].size() : 0, islands = 0;
for (int i = 0; i < m; i++) {
for (int j = 0; j < n; j++) {
if (grid[i][j] == '1') {
islands++;
dfs(grid, m, n, i, j);}}}
return islands;}
private:
void dfs(vector<vector<char>>& grid, const int& m, const int& n, 
const int& i, const int& j) {
if(grid[i][j] == '0') return;
grid[i][j] = '0';
if(i - 1 >= 0) { dfs(grid, m, n, i-1, j); }
if(i + 1 < m)  { dfs(grid, m, n, i+1, j); }
if(j - 1 >= 0) { dfs(grid, m, n, i, j-1); }
if(j + 1 < n)  { dfs(grid, m, n, i, j+1); }}


MedianFinder() {}
void addNum(int num) {
if(maxHeap.empty() or num <= maxHeap.top()) {
maxHeap.push(num);} //two heaps
else {minHeap.push(num);}
if( maxHeap.size() > minHeap.size() + 1) {
minHeap.push(maxHeap.top());
maxHeap.pop();}
else if( minHeap.size() > maxHeap.size() ) {
maxHeap.push(minHeap.top());
minHeap.pop();}}
double findMedian() {
if(maxHeap.size() == minHeap.size()) {
return maxHeap.top() / 2.0 + minHeap.top() / 2.0;}
return maxHeap.top();}
private:
priority_queue<int,vector<int>,greater<int>> minHeap;
priority_queue<int> maxHeap; 
// keep one more element in maxHeap if there are odd 
// number of elements

int findKthLargest(vector<int>& nums, int k) {
priority_queue<int,vector<int>,greater<int>> minHeap;
for(auto& n: nums) {//top 'k' element
minHeap.push(n);
if(minHeap.size() > k) {
minHeap.pop();}}
return minHeap.top();}


vector<vector<int>> merge(vector<vector<int>>& intervals) {
vector<vector<int>> ans;
if (intervals.size() < 2) return intervals;
sort(intervals.begin(), intervals.end(), 
[](const vector<int>&a, const vector<int>&b) {return a[0] < b[0];});
ans.push_back(intervals[0]);
for (int i = 1; i < intervals.size(); ++i) {
if (ans.back()[1]<intervals[i][0]) {
ans.push_back(intervals[i]);}
else {
ans.back()[0]=min(ans.back()[0], intervals[i][0]);
ans.back()[1]=max(ans.back()[1], intervals[i][1]);} }return ans;}

vector<int> findDisappearedNumbers(vector<int>& nums) 
{int src = 0; int n = nums.size();
while (src < n) {
int dst = nums[src] - 1;
if (dst < n && nums[src] != nums[dst])
swap(nums[src], nums[dst]);
else ++src; } //cyclic sort
vector<int> ans;
for (int i = 0; i < nums.size(); ++i) {
if (i != (nums[i] - 1))
ans.push_back(i + 1);
} return ans; }


bool sequenceReconstruction(vector<int>& org, vector<vector<int>>& seqs)
{ // Init the graph
unordered_map<int, int> inDegree;
unordered_map<int, vector<int>> graph;
for(auto& seq: seqs) {
for(auto& num: seq) {
    inDegree[num] = 0;
    graph[num] = {};}}
// Build the graph
for(auto& seq: seqs) {
    if(seq.empty()) {
    continue;}
    for(int i = 0; i < seq.size() - 1; ++i) {
        int parent = seq[i];
        int child = seq[i + 1];
        ++inDegree[child];
        graph[parent].push_back(child);}}     
    if(inDegree.size() != org.size()) return false;
// Find sources
    queue<int> sources;
    for(auto& p: inDegree) {
        if(p.second == 0) { sources.push(p.first); } }
// Start topological sort
vector<int> order;
while(!sources.empty()) {
    // If there are more than one choice at this time
    if(sources.size() > 1) { return false; }
    if(org[order.size()] != sources.front()) { return false; }
    int cur = sources.front();
    sources.pop();
    order.push_back(cur);
    for(auto& child: graph[cur]) {
        if(--inDegree[child] == 0) { sources.push(child);}
    }}
    return order.size() == org.size();


vector<vector<int>> insert(vector<vector<int>>& intervals, vector<int>& newInterval)
{vector<vector<int>> ans; //insert interval/merge interval
int cur = 0; //determine if sb. or sth. can attend/occupy resources
for (int i = 0; i < intervals.size(); ++i) {
if (intervals[i][1] < newInterval[0]) { image.png
ans.push_back(intervals[i]);
++cur; }
else if (intervals[i][0] > newInterval[1]) {
ans.push_back(intervals[i]);
} else { //merge all overlapping intervals
newInterval[0] = min(intervals[i][0], newInterval[0]);
newInterval[1] = max(intervals[i][1], newInterval[1]);
}} ans.insert(ans.begin() + cur, newInterval);

void _merge(vector<int> &v, int front, int mid, int end) {
vector<int> sub_l=vector<int>(v.begin()+front,v.begin()+mid+1);
vector<int> sub_r=vector<int>(v.begin()+mid+1,v.begin()+end+1);
sub_l.insert(sub_l.end(), numeric_limits<int>::max());
sub_r.insert(sub_r.end(), numeric_limits<int>::max());
int idx_l = 0, idx_r = 0;
for (int i = front; i <= end; ++i) {
if (sub_l[idx_l] < sub_r[idx_r]) v[i] = sub_l[idx_l++];
else                             v[i] = sub_r[idx_r++];}}
void merge_sort(vector<int> &v, int front, int end) {
if (front >= end) return;
int mid = front + (end - front) / 2;
merge_sort(v, front, mid);
merge_sort(v, mid+1, end);
_merge(v, front, mid, end);}

int partition(vector<int>& nums, int leftmostIndex, int rightmostIndex)
{ const int pivot_pos = rightmostIndex;
const int pivot = nums[pivot_pos];
int i = leftmostIndex - 1;
int j = leftmostIndex;
for (; j < rightmostIndex; ++j) {
if (nums[j] > pivot) continue;
++i;swap(nums[i], nums[j]);}
swap(nums[pivot_pos], nums[i+1]);
return i+1;}
void quick_sort(vector<int>& nums, int leftmostIndex, int rightmostIndex)
{ if (leftmostIndex < rightmostIndex) {
int pivot_pos = partition(nums, leftmostIndex, rightmostIndex);
quick_sort(nums, leftmostIndex, pivot_pos - 1);                  
quick_sort(nums, pivot_pos + 1, rightmostIndex);} }
int findKthLargest(vector<int>& nums, int k) {
quick_sort(nums, 0, nums.size() - 1);
return nums[nums.size() - k];
}


bool wordBreak(string s, vector<string>& wordDict) {
unordered_set<string> wordset(wordDict.begin(),wordDict.end());
vector<bool> visited(s.length(), false);
queue<int> q {{0}};
while (!q.empty()) {
int start = q.front(); q.pop();
if (!visited[start]) {
for (int i = start; i <= s.length(); ++i) {
if (0 != wordset.count(s.substr(start, i-start))) {
q.push(i);if (i == s.length()) return true;}}
visited[start] = 1;}}return false;}


int networkDelayTime(vector<vector<int>>& times, int N, int K) {
int res = 0; //dijkstra
vector<vector<int>> edges(101, vector<int>(101, -1));
queue<int> q{{K}};
vector<int> dist(N + 1, INT_MAX);
dist[K] = 0;
for (auto e : times) edges[e[0]][e[1]] = e[2];
while (!q.empty()) {
unordered_set<int> visited;
for (int i = q.size(); i > 0; --i) {
int u = q.front(); q.pop();
for (int v = 1; v <= 100; ++v) {
if (edges[u][v] != -1 && dist[u] + edges[u][v] < dist[v]) {
if (!visited.count(v)) {
visited.insert(v);
q.push(v);}
dist[v] = dist[u] + edges[u][v];}}}}
for (int i = 1; i <= N; ++i) {res = max(res, dist[i]);}
return res == INT_MAX ? -1 : res;}


void dfs(int& sum,TreeNode* root,int hgt) {
if(!root) return ;
if(hgt>len) { len = hgt ; sum=0; }
if(root->left) dfs(sum,root->left,hgt+1);
if(root->right) dfs(sum,root->right,hgt+1);
if(len==hgt) sum+=root->val ;
}
int deepestLeavesSum(TreeNode* root) {
int sum = 0 ;
dfs(sum,root,0);
return sum ; }



void _inorderTraversal(TreeNode* root, vector<int> &ans) {
if (!root) return;
_inorderTraversal(root->left, ans);
ans.push_back(root->val);
_inorderTraversal(root->right, ans);}
vector<int> inorderTraversal(TreeNode* root) {
vector<int> ans;
_inorderTraversal(root, ans);
return ans;}

vector<vector<int>> levelOrder(TreeNode* root) {
if (!root) return {};
vector<vector<int>> ans;
queue<TreeNode*> q;
q.push(root);
while (!q.empty()) {
vector<int> tmp_solu;
for (int i = q.size(); i > 0; --i) {
TreeNode *tmp = q.front(); q.pop();
tmp_solu.push_back(tmp->val);
if (tmp->left) q.push(tmp->left);
if (tmp->right) q.push(tmp->right);}
ans.push_back(tmp_solu);}return ans;}

vector<int> rightSideView(TreeNode* root) {
if (!root) return {};
vector<int> ans;
queue<TreeNode*> q;
q.push(root);
while (!q.empty()) {
for (int i = q.size(); i > 0; --i) {
TreeNode *tmp = q.front(); q.pop();
if (i == 1)ans.push_back(tmp->val);
if (tmp->left) q.push(tmp->left);
if (tmp->right) q.push(tmp->right);}}
return ans;}

TreeNode* invertTree(TreeNode* root) {
if (!root) return nullptr;
queue<TreeNode*> q;
q.push(root);
while (!q.empty()) {
TreeNode *node = q.front(); q.pop();
TreeNode *tmp = node->right;
node->right = node->left;
node->left = tmp;
if (node->left) q.push(node->left);
if (node->right) q.push(node->right);}
return root;}


vector<vector<int>> dirs{{0,-1},{-1,0},{0,1},{1,0}};
bool hasPath(vector<vector<int>>& maze, 
vector<int>& start, vector<int>& destination) 
{int m = maze.size(), n = maze[0].size();
return helper(maze, start[0],start[1],destination[0],destination[1]);
} //dfs, maze
bool helper(vector<vector<int>>& maze, int i, int j, int di, int dj){
if (i == di && j == dj) return true;
bool res = false;
int m = maze.size(), n = maze[0].size();
maze[i][j] = -1;
for (auto dir : dirs) {
int x = i, y = j;
while (x >= 0 && x < m && y >= 0 && y < n && maze[x][y] != 1) {
x += dir[0]; y += dir[1];}
x -= dir[0]; y -= dir[1];
if (maze[x][y] != -1) {res |= helper(maze, x, y, di, dj);}
}return res;}


bool validTree(int n, vector<pair<int, int>>& edges) {
vector<int> roots(n, -1); //union find
for (auto a : edges) {
int x = find(roots, a.first), y=find(roots, a.second);
if (x == y) return false;
roots[x] = y;}
return edges.size() == n - 1;}
int find(vector<int> &roots, int i) {
while (roots[i] != -1) i = roots[i];
return i;}


int longestCommonSubsequence(string text1, string text2) {
int ans = 0; //lcs
int m = text1.size();
int n = text2.size();
vector<vector<int>> dp(m+1, vector<int>(n+1));
for (int i = 1; i <= m; ++i) {
for (int j = 1; j <= n; ++j) {
if (text1[i-1] == text2[j-1]) { dp[i][j] = dp[i-1][j-1] + 1;}
else dp[i][j] = max(dp[i-1][j], dp[i][j-1]);
ans = max(ans, dp[i][j]); } } return ans;//dp[m][n]; }





int solveKnapsack(const vector<int> &profits, const vector<int> &weights, int capacity) {
// basic checks
if (capacity <= 0 || profits.empty() || weights.size() != profits.size()) {return 0;}
int n = profits.size();
vector<vector<int>> dp(n, vector<int>(capacity + 1));
// populate the capacity=0 columns, with '0' capacity we have '0' profit
for (int i = 0; i < n; i++) { dp[i][0] = 0;}
// if we have only one weight, we will take it if it is not more than the capacity
for (int c = 0; c <= capacity; c++) { if (weights[0] <= c) { dp[0][c] = profits[0];}}
// process all sub-arrays for all the capacities
for (int i = 1; i < n; i++) {
for (int c = 1; c <= capacity; c++) {
int profit1 = 0, profit2 = 0;
// include the item, if it is not more than the capacity
if (weights[i] <= c) { profit1 = profits[i] + dp[i - 1][c - weights[i]]; }
profit2 = dp[i - 1][c]; // exclude the item
dp[i][c] = max(profit1, profit2); // take maximum
}}
// maximum profit will be at the bottom-right corner.
return dp[n - 1][capacity]; }};