#include <limits.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <queue>


using namespace std;


bool isBadVersion(int version)
{
    return false;
}

class Solution {

public:

    int climbStairs(int n) {
        if (n <= 1) return 1;
        vector<int> dp(n);
        /* fibonacci */
        dp[0] = 1; dp[1] = 2;
        for (int i = 2; i < n; ++i) {
            dp[i] = dp[i - 1] + dp[i - 2];
        }
        return dp.back();
    }
};

int main()
{
    Solution solution;

    {
        int n = 2;
        int ans = solution.climbStairs(n);
        cout << "solution.climbStairs=" << ans << endl;
    }
cout << "next=" << endl;
    {
        int n = 3;
        int ans = solution.climbStairs(n);
        cout << "solution.climbStairs=" << ans << endl;
    }
    return 0;
}
