#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>


using namespace std;


class Solution {
public:
	void expand(vector<int>& nums, int left, int right, int& min_product, int& max_product, int& best_product) {


		while (left >= 0 && right < nums.size()) {

			if (nums[right] > 0) {
				max_product = max(max_product * nums[right], nums[right]);
				min_product = min(min_product * nums[right], nums[right]);
			}
			else if (nums[right] == 0) {
				max_product = max(max_product * nums[right], nums[right]);
				min_product = min(min_product * nums[right], nums[right]);
			}
			else {
				int tmp_max_product = max_product;
				max_product = max(min_product * nums[right], nums[right]);
				min_product = min(tmp_max_product * nums[right], nums[right]);
			}
			best_product = max(best_product, max_product);

			++right;
		}

	}

	int maxProduct(vector<int>& nums) {
		int best_product = nums[0];
		int min_product = best_product;
		int max_product = best_product;
		int i = 1;
		//for (i = 1; i < nums.size(); i++) {
		expand(nums, i, i, min_product, max_product, best_product);
		//}

		return best_product;
	}
	int coinChange(vector<int>& coins, int amount) {
		if (amount == 0) return 0;

		//int small_coin_price = INT_MAX;
		//for (auto& coin : coins) {
		//	if (coin < small_coin_price)
		//		small_coin_price = coin;
		//}

		//if (amount < small_coin_price) {
		//	return 0;
		//}

		int* combinations = new int[amount + 1];
		int* combination_cnt = new int[amount + 1];
		for (int i = 0; i <= amount; ++i) {
			combinations[i] = 0;
			combination_cnt[i] = amount + 1;
		}

		combinations[0] = 1;
		combination_cnt[0] = 0;

		for (auto& coin : coins) {
			for (int i = 0; i <= amount; ++i) {
				if (i >= coin) {
					combinations[i] += combinations[i - coin];
				}
			}
		}

		for (auto& coin : coins) {
			for (int i = coin; i <= amount; ++i) {
				combination_cnt[i] = min(combination_cnt[i], combination_cnt[i - coin] + 1);
			}
		}
		return (combination_cnt[amount] == amount + 1) ? (-1) : (combination_cnt[amount]);
	}
};

int main()
{
	Solution solution;

	//{
	//	vector<int> v = { 1,2,5 };
	//	cout << solution.coinChange(v, 11) << endl;
	//}
	{
		vector<int> v = {2 };
		cout << solution.coinChange(v, 3) << endl;
	}
	return 0;
}