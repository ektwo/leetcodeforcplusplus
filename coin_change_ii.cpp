#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>


using namespace std;


class Solution {
public:

	int coinChange(int amount, vector<int>& coins) {
        int n = coins.size();
        vector<vector<int>> dp(n+1, vector<int>(amount+1, 0));
        dp[0][0] = 1; 
        for (int i = 1; i <= n; ++i) {
            int val = coins[i-1];
            for (int j = 0; j <= amount; ++j) {
                for (int k = 0; k * val <= j; ++k) {
                    dp[i][j] += dp[i-1][j-k*val];
                }
            }
        }
        return dp[n][amount];
	}
};

int main()
{
	Solution solution;

	{
        int amount = 5;
		vector<int> coins = { 1,2,5 };
		cout << solution.coinChange(amount, coins) << endl;
	}
	{
        int amount = 3;
		vector<int> coins = { 2 };
		cout << solution.coinChange(amount, coins) << endl;
	}
	{
        int amount = 10;
		vector<int> coins = { 10 };
		cout << solution.coinChange(amount, coins) << endl;
	}
	return 0;
}