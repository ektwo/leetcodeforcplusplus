#include <limits.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <queue>


using namespace std;

template<class T>
void print_1d(const T& nums)
{
    cout << "{";
    for (int j = 0; j < nums.size(); ++j) {
        cout << nums[j] << ",";
    }
    cout << "}," << endl;
}
template<class T>
void print_2d(const T& nums)
{
    cout << "{";
    for (int i = 0; i < nums.size(); ++i) {
        cout << "{";
        for (int j = 0; j < nums[i].size(); ++j) {
            cout << nums[i][j] << ",";
        }
        cout << "}," << endl;
    }
    cout << "}," << endl;
}
class Solution {
private:
    int m_cur_target;
    int m_end_target;
    vector<vector<int>> ans;
    vector<vector<int>> dirs = {
        { 0, -1}, { -1, 0}, { 0, 1}, { 1, 0}
    };

    void init() {
        m_cur_target = 0;
        m_end_target = 0;
    }

    bool is_end_condition() {
        if (m_end_target == m_cur_target) return true;
        return false;
    } 

    bool is_branch_can_be_pruned(vector<int>& candidates, int candidate_start, int candiadate_end, int i) {
        if (i > candidate_start && (candidates[i-1] == candidates[i])) return true;
        return false;
    }

    bool combinationSum2_dfs(vector<int>& candidates, int candidate_start, int target, vector<int> &tmp_solu) {

        bool ret = false;

        //cout << "target=" << target << endl;
        //cout << "m_cur_target=" << m_cur_target << endl;
        //cout << "m_end_target=" << m_end_target << endl;
        m_cur_target = target;
        if (is_end_condition()) { 
            /* do something */
            ans.push_back(tmp_solu);
            return true;
        }

        /* ignore possible solution at this step that do not satisfy the problem constraints */
        if (target > m_end_target) { return false; }

        /* traverse all executable branch path */
        const int candidates_end = candidates.size();
        for (int i = candidate_start; i < candidates_end; ++i)
        {
            /* prune, check if it matches for pruning */
            if (is_branch_can_be_pruned(candidates, candidate_start, candidates_end, i)) continue;

            /* update state variables */
            tmp_solu.push_back(candidates[i]);

            /* recursively execute the logic of the next step */
            //for (auto &dir : dirs)
            {
                bool ret = combinationSum2_dfs(candidates, i + 1, target + candidates[i], tmp_solu);
            }

            /* reset state variables */
            tmp_solu.pop_back();
        }

        return false;
    }
public:

    vector<vector<int>> combinationSum2(vector<int>& candidates, int target) {



        vector<int> tmp_solu;

        init();
        m_cur_target = 0;
        m_end_target = target;
sort(candidates.begin(), candidates.end());

        //for (int i = 0; i < n; ++i)
        {
            //for (int j = 0; j < m; ++j)
            {
                combinationSum2_dfs(candidates, 0, 0, tmp_solu);
                //if (ans) return ans;
            }
        }

        return ans;
    }
    void combinationSum3_backtracking(int k, int n, int start_idx, int sum, vector<int>& path, vector<vector<int>> &ans) {
        //end condition
        if (path.size() == k) {
            //存放结果
            if (n==sum)
                ans.push_back(path);
            return;
        }
        for (int i=start_idx;i<=9;++i) {
            //handle node
            sum+=i;
            path.push_back(i);
            combinationSum3_backtracking(k,n,i+1,sum,path,ans);
            //backtracking
            sum-=i;
            path.pop_back();

        }        
    }
    vector<vector<int>> combinationSum3(int k, int n) {
        vector<vector<int>> ans;
        vector<int> path;
        int sum=0;
        combinationSum3_backtracking(k,n,1,sum,path,ans);

        return ans;
    }
};

int main()
{
    Solution solution;

    {
        int k=3;
        int n=7;
        vector<vector<int>> ans = solution.combinationSum3(k,n);
        print_2d(ans);
    }
    {
        int k=3;
        int n=9;
        vector<vector<int>> ans = solution.combinationSum3(k,n);
        print_2d(ans);
    }
    {
        int k=4;
        int n=1;
        vector<vector<int>> ans = solution.combinationSum3(k,n);
        print_2d(ans);
    }

    return 0;
}
