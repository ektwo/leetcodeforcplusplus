#include <limits.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <queue>


using namespace std;


class Solution {
private:
    vector<int> parse_version(const string &str) {
        vector<int> ans;
        int tmp_revision = 0;

        for (auto &c : str) {
            if (c == '.') {
                ans.push_back(tmp_revision);
                tmp_revision = 0;
            }
            else {
                tmp_revision = tmp_revision * 10 + (c - '0');
            }
        }

        ans.push_back(tmp_revision);

        return ans;
    }
public:
    int compareVersion(string version1, string version2) {
        const auto &v1 = parse_version(version1);
        const auto &v2 = parse_version(version2);
        int v1_idx = 0, v2_idx = 0;

        while (v1_idx < v1.size() || v2_idx < v2.size()) {
            //if (v1[])
            int revision1 = v1_idx < v1.size() ? v1[v1_idx++] : 0;
            int revision2 = v2_idx < v2.size() ? v2[v2_idx++] : 0;
            if (revision1 < revision2) return -1;
            else if (revision1 > revision2) return 1;
            //if (revision1 == revision2) continue;
        }

        return 0;
    }

};

int main()
{
    Solution solution;
    {
        string version1 = "1.01";
        string version2 = "1.001";
        cout << solution.compareVersion(version1, version2) << endl;
    }

    {
        string version1 = "1.0";
        string version2 = "1.0.0";
        cout << solution.compareVersion(version1, version2) << endl;
    }

    {
        string version1 = "0.1";
        string version2 = "1.1";
        cout << solution.compareVersion(version1, version2) << endl;
    }

    return 0;
}
