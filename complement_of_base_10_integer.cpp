#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <queue>


using namespace std;


class Solution {
public:
    int bitwiseComplement(int n) {
        int bm = 1;
        while (bm < n) {
            bm = (bm <<1)+1;
        }
        return bm ^n;
    }
};