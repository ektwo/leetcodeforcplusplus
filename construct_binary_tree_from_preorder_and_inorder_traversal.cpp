#include <iostream>
#include <vector>
#include <string>


using namespace std;


struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};


void print_1d_int(vector<int> nums)
{

        cout << "{";
        for (int j = 0; j < nums.size(); ++j) {
            cout << nums[j] << ",";
        }
        cout << "}," << endl;

}

void print_2d_int(vector<vector<int>> nums)
{
    cout << "{";
    for (int i = 0; i < nums.size(); ++i) {
        cout << "{";
        for (int j = 0; j < nums[0].size(); ++j) {
            cout << nums[i][j] << ",";
        }
        cout << "}," << endl;
    }
    cout << "}," << endl;
}

void print_tree_preorder(TreeNode *root)
{
    if (root) {
        cout << root->val << ",";
        if (root->left) print_tree_preorder(root->left);
        if (root->right) print_tree_preorder(root->right);
    }
    else
        cout << "null,";
}

class Solution
{
private:
    int node_nums;
    int tree_height;
    TreeNode *m_head;
public:
    void reset() {
        node_nums = 0;
        tree_height = 0;
        m_head = nullptr;
    }

    TreeNode* get_head() {
        return m_head;
    }

    TreeNode* push(TreeNode *root, int val, bool left, bool right)
    {
        TreeNode* temp = new TreeNode(val);
        cout << "push val=" << val << " m_head=" << m_head << endl;
        cout << "push val=" << val << " root=" << root << endl;
        if (!m_head) {
            m_head = temp;
        }

        if (!root)
            root = m_head;

        if (root && left)
            root->left = temp;
        else if (root && right)
            root->right = temp;

        return temp;
    }
    void _inorderTraversal(TreeNode* root, vector<int> &ans) {
        if (!root) return;
        _inorderTraversal(root->left, ans);
        ans.push_back(root->val);
        _inorderTraversal(root->right, ans);
    }
    vector<int> inorderTraversal(TreeNode* root) {

        vector<int> ans;

        _inorderTraversal(root, ans);

        return ans;
    }
    TreeNode* _buildTree(vector<int>& preorder, int pre_left, int pre_right, vector<int>& inorder, int i_left, int i_right) {
        if ((pre_left > pre_right) || (i_left > i_right)) return nullptr;
        TreeNode *root = new TreeNode(preorder[pre_left]);
        
        int inorder_root_idx = -1;
        for (int i = i_left; i <= i_right; ++i) {
            if (preorder[pre_left] == inorder[i]) {
                inorder_root_idx = i;
                break;
            }
        }

        int left_sub_tree_len = inorder_root_idx - i_left;
        int right_sub_tree_len = i_right - inorder_root_idx;

        root->left = _buildTree(preorder, pre_left+1, pre_left+left_sub_tree_len, inorder, i_left, inorder_root_idx-1);
        root->right = _buildTree(preorder, pre_left+1+left_sub_tree_len, pre_right, inorder, inorder_root_idx+1, i_right);

        return root;
    }

    TreeNode* buildTree(vector<int>& preorder, vector<int>& inorder) {
        return _buildTree(preorder, 0, preorder.size()-1, inorder, 0, inorder.size()-1);

    }
};

int main()
{
    Solution solution;
    solution.reset();
    {
        vector<int> preorder = { 1, 2, 4, 3, 5, 7, 8, 6};
        vector<int> inorder = { 4, 2, 1, 7, 5, 8, 3, 6};
        TreeNode *root = solution.buildTree(preorder,inorder);
        print_tree_preorder(root);
    }
    cout << endl;
    solution.reset();
    {
        vector<int> preorder = { 3,9,20,15,7};
        vector<int> inorder = { 9,3,15,20,7};
        TreeNode *root = solution.buildTree(preorder,inorder);
        print_tree_preorder(root);
    }
    cout << endl;
    solution.reset();    
    {
        vector<int> preorder = {-1};
        vector<int> inorder = { -1};
        TreeNode *root = solution.buildTree(preorder,inorder);
        print_tree_preorder(root);
    }

    return 0;
}
