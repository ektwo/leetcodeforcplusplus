#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <algorithm>

using namespace std;
//class Solution
//{
//public:
//    //A:[ [2 9 10], [3 7 15], [5 12 12], [15 20 10], [19 24 8] ]
//    //A1:[[0,2,3],[2,5,3]]
//    //A2:[[2,9,10],[9,12,15]]
//    //ALast:[[1,2,1],[1,2,2],[1,2,3]]
//    //B:[ [2 10], [3 15], [7 12], [12 0], [15 10], [20 8], [24, 0] ]
//    vector<pair<int, int>> getSkyline(vector<vector<int>>& buildings)
//    {
//        map<int, vector<int>> m_map;
//        for (vector<int> v : buildings)
//        {
//            m_map[v[0]].push_back(v[2]);
//            m_map[v[1]].push_back(-v[2]);
//        }
//
//        //temprary high point container
//        multiset<int> m_multiset;
//        m_multiset.insert(0); //set a default point value : ground
//        vector<pair<int, int>> res;
//        for (auto itMap = m_map.begin(); itMap != m_map.end(); ++itMap)
//        {
//            //Q:[ [2 9 10], [3 7 15], [5 12 12], [15 20 10], [19 24 8] ]
//            //A:[ [2 10], [3 15], [7 12], [12 0], [15 10], [20 8], [24, 0] ].
//            cout << " first=[" << (*itMap).first << endl; //[2 [3 [5 [7 [9 [12 [15
//            //", " << (*it).second << "]" << endl;
//            for (int height : itMap->second)
//            {
//                cout << " " << height << "]" << endl; // 10] 15] 12] -15] -10] -12] 10]
//                if (height > 0)
//                {
//                    m_multiset.insert(height); //10 15 12 . 10
//                    cout << "insert" << endl;
//                }
//                else
//                {
//                    m_multiset.erase(m_multiset.find(-height)); //-15->10 X 12, -10 -> X X 12 -> X X X
//                    cout << "erase" << endl;
//                }
//            }
//
//            if (res.empty())
//            {
//                cout << "empty" << endl;
//                res.push_back({ itMap->first, *m_multiset.rbegin() }); //{2, 10}
//            }
//            else
//            {
//                cout << "*m_multiset.rbegin()=" << *m_multiset.rbegin() << endl; //15 15 12 12 0 10
//                cout << "res.back().second=" << res.back().second << endl;       //10 15 12 12 0
//                if (*m_multiset.rbegin() != res.back().second)                   // still hava a higher line on working.
//                {
//                    cout << "push_back {" << itMap->first << ", " << *m_multiset.rbegin() << "}" << endl;
//                    //{3, 15} {7 12} {12 0}
//                    res.push_back({ itMap->first, *m_multiset.rbegin() });
//                }
//            }
//        }
//        return res;
//    }
//};

class Solution {
public:
    int maxArea(vector<int>& height) {
        int size = height.size();
        cout << "size= " << size << endl;
        int ret = 0, tmp_max = 0;
        int left = 0;
        int right = size - 1;
        //for (; left < right; )
        while (left < right)
        {
            int length = right - left;
            int left_height = height.at(left);
            int right_height = height.at(right);
            cout << "left= " << left << " right= " << right << endl;
            cout << "left_height= " << left_height << " right_height= " << right_height << endl;
            int low_height;
            if (right_height > left_height) {
                low_height = left_height;
                left++;
            }
            else {
                low_height = right_height;
                right--;
            }
            //int low_height = (right_height > left_height) ? (left_height) : (right_height);
            tmp_max = low_height * length;
            if (ret < tmp_max)
                ret = tmp_max;
            cout << "ret=" << ret << endl;
        }

        return ret;
    }
};

int main()
{

#if 1
    //static const int X = 5;
    //vector<vector<int>> buildings(X, vector<int>(3, 1));
    ////buildings = {{0, 3, 3}, {1, 5, 3}, {2, 4, 3}, {3, 7, 3}};
    //buildings = { {2, 9, 10}, {3, 7, 15}, {5, 12, 12}, {15, 20, 10}, {19, 24, 8} };
#else
    static const int X = 10000;
    vector<vector<int>> buildings(X, vector<int>(3, 1));
    //buildings[0] = {2, 9, 10};
    //[ [2 9 10], [3 7 15], [5 12 12], [15 20 10], [19 24 8] ]
    //buildings = {{2, 9, 10}, {3, 7, 15}, {5, 12, 12}, {15, 20, 10}, {19, 24, 8}};
    //[ [1 2 1], [1 2 2], [1, 2, 3] ]
    //buildings = {{1, 2, 1}, {1, 2, 2}, {1, 2, 3}};
#endif

    Solution s;
    {
        vector<int> data = { 1,8,6,2,5,4,8,3,7 };
        cout << "ret= " << s.maxArea(data) << endl;
        //vector<pair<int, int>> ret = s.getSkyline(buildings);
        //for (int i = 0; i < ret.size(); i++)
        //{
        //    cout << "i=" << i << " ret[i].first=" << ret[i].first << " ret[i].second=" << ret[i].second << endl;
        //}
    }

    return 0;
}
//#endif