
#include <limits.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <queue>
#include <limits>
#include <math.h>



using namespace std;

template<class T>
void print_1d(const T& nums)
{
    cout << "{";
    for (int j = 0; j < nums.size(); ++j) {
        cout << nums[j] << ",";
    }
    cout << "}," << endl;
}
template<class T>
void print_2d(const T& nums)
{
    cout << "{";
    for (int i = 0; i < nums.size(); ++i) {
        cout << "{";
        for (int j = 0; j < nums[i].size(); ++j) {
            cout << nums[i][j] << ",";
        }
        cout << "}," << endl;
    }
    cout << "}," << endl;
}


class Solution {
public:
    int findMaxK(vector<int>& nums) {
        int ans=-1;

        sort(nums.begin(), nums.end());

        int left=0;
        int right=nums.size()-1;
        //for (int right=nums.size()-1;right>0;--right) {
        while (right > left) {
            if (nums[right] == -nums[left]) {
                return nums[right];
            }
            else if (nums[right] > -nums[left]) {
                --right;
            }
            else
                ++left;
        }

        return ans;
    }
    int revert_digits(int num) {
        int ans=0;
        int i=0;
        while (num) {
            //cout << "pow(10,i)=" << pow(10,i) << endl;
            //ans+=(num%10)*(pow(10,i));
            ans=ans*10+(num%10);
            num/=10;
            ++i;
        }
        return ans;
    }
    int countDistinctIntegers(vector<int>& nums) {
        int ans=1;
        // cout << revert_digits(12345)<<endl;
        // cout << revert_digits(6789)<<endl;
        int n=nums.size();
        for (int i=0;i<n;++i) {
            nums.push_back(revert_digits(nums[i]));
        }
        sort(nums.begin(), nums.end());
        int slow=0;
        print_1d(nums);
        for (int fast=1;fast<nums.size();++fast) {
            cout << "nums["<<slow<<"]="<<nums[slow]<<endl;
            cout << "nums["<<fast<<"]="<<nums[fast]<<endl;
            if (nums[slow]!=nums[fast]) {
                ++ans;
                slow=fast;
            }
            else {

            }
        }
        print_1d(nums);
        return ans;
    }
    bool sumOfNumberAndReverse(int num) {
        if (num==0) return true;
        bool ans=false;
        //int num_div2=ceil(num/2.0);
        for (int i=1;i<num;++i) {
            if (i+revert_digits(i)==num) return true;
        }
        //cout << num_div2 << endl;
        return ans;
    }

    long long countSubarrays(vector<int>& nums, int minK, int maxK) {
        long res = 0;
        bool minFound = false, maxFound = false;
        int start = 0, minStart = 0, maxStart = 0;
        for (int i = 0; i < nums.size(); i++) {
            int num = nums[i];
            if (num < minK || num > maxK) {
                minFound = false;
                maxFound = false;
                start = i+1;
            }
            if (num == minK) {
                minFound = true;
                minStart = i;
            }
            if (num == maxK) {
                maxFound = true;
                maxStart = i;
            }
            if (minFound && maxFound) {
                res += (min(minStart, maxStart) - start + 1);
            }
        }
        return res;
    }
};


int main()
{
    //6204
    Solution solution;
    {
        vector<int> nums = {-1,2,-3,3};
        
        int ans = solution.findMaxK(nums);
        cout << "ans=" << ans << endl;
    }
    cout << endl;
    {
        vector<int> nums = {-1,10,6,7,-7,1};
        
        int ans = solution.findMaxK(nums);
        cout << "ans=" << ans << endl;
    }
    cout << endl;
    {
        vector<int> nums = {-10,8,6,7,-2,-3};
        
        int ans = solution.findMaxK(nums);
        cout << "ans=" << ans << endl;
    }
    cout << endl;
    //6205

    {
        vector<int> nums = {1,13,10,12,31};
        
        int ans = solution.countDistinctIntegers(nums);
        cout << "ans=" << ans << endl;
    }
    cout << endl;
    {
        vector<int> nums = {2,2,2};
        
        int ans = solution.countDistinctIntegers(nums);
        cout << "ans=" << ans << endl;
    }
    cout << endl;

    //6219
    {
        int num=443;
        bool ans = solution.sumOfNumberAndReverse(num);
        cout << "ans=" << ans << endl;
    }
    cout << endl;
    {
        int num=63;
        bool ans = solution.sumOfNumberAndReverse(num);
        cout << "ans=" << ans << endl;
    }
    cout << endl;
    {
        int num=181;
        bool ans = solution.sumOfNumberAndReverse(num);
        cout << "ans=" << ans << endl;
    }
    cout << endl;


    //6207
    {
        vector<int> nums={1,3,5,2,7,5};
        int minK=1;
        int maxK=5;
        auto ans = solution.countSubarrays(nums,minK,maxK);
        cout << "ans=" << ans << endl;
    }
    cout << endl;
    {
        vector<int> nums={1,1,1,1};
        int minK=1;
        int maxK=1;
        auto ans = solution.countSubarrays(nums,minK,maxK);
        cout << "ans=" << ans << endl;
    }
    cout << endl;
    {
        vector<int> nums={35054,398719,945315,945315,820417,945315,35054,945315,171832,945315,35054,109750,790964,441974,552913};
        int minK=35054;
        int maxK=945315;
        auto ans = solution.countSubarrays(nums,minK,maxK);
        cout << "ans=" << ans << endl;
    }
    return 0;
}
