
#include <limits.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <queue>
#include <limits>



using namespace std;

template<class T>
void print_1d(const T& nums)
{
    cout << "{";
    for (int j = 0; j < nums.size(); ++j) {
        cout << nums[j] << ",";
    }
    cout << "}," << endl;
}
template<class T>
void print_2d(const T& nums)
{
    cout << "{";
    for (int i = 0; i < nums.size(); ++i) {
        cout << "{";
        for (int j = 0; j < nums[i].size(); ++j) {
            cout << nums[i][j] << ",";
        }
        cout << "}," << endl;
    }
    cout << "}," << endl;
}


class Solution {

public:
    bool haveConflict(vector<string>& event1, vector<string>& event2) {
        bool ans = false;

        sort(event1.begin(),event1.end());
        sort(event2.begin(),event2.end());
        int start_hour_1=0,start_min_1=0,end_hour_1=0,end_min_1=0;
        int start_hour_2=0,start_min_2=0,end_hour_2=0,end_min_2=0;

        for (int i=0;i<event1.size();++i) {
            if (i==0) {
            size_t pos=event1[i].find(":");
            string s_begin = event1[i].substr(0,pos);
            start_hour_1=stoi(s_begin);
            cout << "start_hour_1=" << start_hour_1 << endl;
            string s_end = event1[i].substr(pos+1);
            start_min_1=stoi(s_end,nullptr,10);
            cout << "start_min_1=" << start_min_1 << endl;
            }
            if (i==1) {
            size_t pos=event1[i].find(":");
            string s_begin = event1[i].substr(0,pos);
            end_hour_1=stoi(s_begin);
            cout << "end_hour_1=" << end_hour_1 << endl;
            string s_end = event1[i].substr(pos+1,2);
            end_min_1=stoi(s_end);
            cout << "end_min_1=" << end_min_1 << endl;
            }
        }
        for (int i=0;i<event2.size();++i) {
            if (i==0) {
            size_t pos=event2[i].find(":");
            string s_begin = event2[i].substr(0,pos);
            start_hour_2=stoi(s_begin);
            //cout << "start_hour_2=" << start_hour_2 << endl;
            string s_end = event2[i].substr(pos+1);
            start_min_2=stoi(s_end);
            //cout << "start_min_2=" << start_min_2 << endl;
            }
            if (i==1) {
            size_t pos=event2[i].find(":");
            string s_begin = event2[i].substr(0,pos);
            end_hour_2=stoi(s_begin);
            //cout << "end_hour_2=" << end_hour_2 << endl;
            string s_end = event2[i].substr(pos+1);
            end_min_2=stoi(s_end);
            //cout << "end_min_2=" << end_min_2 << endl;
            }
        }

        if ((start_hour_2>=start_hour_1) && ((end_hour_1>start_hour_2) || ((end_hour_1==start_hour_2)&&(end_min_1>=start_min_2))))
            ans=true;
        else if ((start_hour_1>start_hour_2) && ((end_hour_2>start_hour_1) || ((end_hour_2==start_hour_1)&&(end_min_2>=start_min_1))))
            ans=true;

        return ans;
    }
    int calc_gcd(int a, int b) {
        if (b==0) return a;
        return calc_gcd(b,a%b);
    }
    int subarrayGCD(vector<int>& nums, int k) {
        int ans=0;

        for (int i=0;i<nums.size();++i) {
            int currgcd=0;
            for (int j=i;j<nums.size();++j) {
                currgcd=calc_gcd(currgcd,nums[j]);
                ans+=(currgcd==k)?1:0;
            }
        }
        
        return ans;
    }
};
int main()
{
    Solution solution;
    {
        vector<string> event1 = { "01:15","02:00"};
        vector<string> event2 = { "02:00","03:00"};
        bool ans = solution.haveConflict(event1, event2);
        cout << ans << endl;
    }
    cout << endl;
    {
        vector<string> event1 = { "01:00","02:00"};
        vector<string> event2 = { "01:20","03:00"};
        bool ans = solution.haveConflict(event1, event2);
        cout << ans << endl;
    }
    cout << endl;
    {
        vector<string> event1 = { "10:00","11:00"};
        vector<string> event2 = { "14:00","15:00"};
        bool ans = solution.haveConflict(event1, event2);
        cout << ans << endl;
    }
    cout << endl;
    {
        vector<string> event1 = { "14:13","22:08"};
        vector<string> event2 = { "02:40","08:08"};
        bool ans = solution.haveConflict(event1, event2);
        cout << ans << endl;
    }
    cout << endl;
    {
        vector<int> nums = {9,3,1,2,6,3};
        int k=3;
        auto ans = solution.subarrayGCD(nums,k);
        cout << ans << endl;
    }
    cout << endl;
    {
        vector<int> nums = {4};
        int k=7;
        auto ans = solution.subarrayGCD(nums,k);
        cout << ans << endl;
    }
    cout << endl;
    {
        vector<int> nums = {5};
        int k=1;
        auto ans = solution.subarrayGCD(nums,k);
        cout << ans << endl;
    }
    return 0;
}
