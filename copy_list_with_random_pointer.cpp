#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>


using namespace std;



// Definition for a Node.
class Node {
public:
    int val;
    Node* next;
    Node* random;
    Node(int _val) {
        val = _val;
        next = NULL;
        random = NULL;
    }
    Node(int _val, Node *_next) {
        val = _val;
        next = _next;
        random = NULL;
    }

};

int get_node_random_index(Node* head, Node *target)
{
    static int a = 0;
    int ret = -1;
    if (target) {
        int idx = 0;
        while (head) {
            //cout << "head val=" << head->val << endl;
            //cout << "head =" << head << endl;
            //cout << "head next=" << head->next << endl;
            //cout << "head random=" << head->random << endl;
            if (target->random == head) {
                ret = idx;
                break;
            }
            ++idx;
            head = head->next;
        }
    }
    return ret;
}

class Solution {
public:
    Node* copyRandomList(Node* head) {
        if (head == NULL) return NULL;
        Node* tmp_head = head;
        int size = 0;
        while (tmp_head) {
            tmp_head = tmp_head->next;
            ++size;
        }
        tmp_head = head;

        int curr = 0;
        int random_index = -1;
        Node** node_list = new Node * [size];
        while (tmp_head) {
            node_list[curr] = new Node(tmp_head->val);
            tmp_head = tmp_head->next;
            curr++;
        }
        curr = 0;
        tmp_head = head;

        while (tmp_head->next) {
            node_list[curr]->next = node_list[curr + 1];
            random_index = get_node_random_index(head, tmp_head);
            if (random_index == -1)
                node_list[curr]->random = NULL;
            else
                node_list[curr]->random = node_list[random_index];
            curr++;
            tmp_head = tmp_head->next;
        }
        random_index = get_node_random_index(head, tmp_head);
        if (random_index == -1)
            node_list[curr]->random = (-1 == get_node_random_index(head, tmp_head)) ?�@NULL : node_list[random_index];
        else
            node_list[curr]->random = node_list[random_index];

        return node_list[0];// head;
    }

};

int main()
{
    Solution s;
    {
        const int num = 5;
        Node** lna = new Node * [num];

        lna[4] = new Node(1, NULL);
        lna[3] = new Node(10, lna[4]);
        lna[2] = new Node(11, lna[3]);
        lna[1] = new Node(13, lna[2]);
        lna[0] = new Node(7, lna[1]);
        lna[0]->random = NULL;
        lna[1]->random = lna[0];
        lna[2]->random = lna[4];
        lna[3]->random = lna[2];
        lna[4]->random = lna[0];

        cout << "lna[0]=" << lna[0] << endl;
        cout << "lna[1]=" << lna[1] << endl;
        cout << "lna[2]=" << lna[2] << endl;
        cout << "lna[3]=" << lna[3] << endl;
        cout << "lna[4]=" << lna[4] << endl;
        cout << "lna[0].next=" << lna[0]->next << endl;
        cout << "lna[1].next=" << lna[1]->next << endl;
        cout << "lna[2].next=" << lna[2]->next << endl;
        cout << "lna[3].next=" << lna[3]->next << endl;
        cout << "lna[4].next=" << lna[4]->next << endl;
        cout << "lna[0].random=" << lna[0]->random << endl;
        cout << "lna[1].random=" << lna[1]->random << endl;
        cout << "lna[2].random=" << lna[2]->random << endl;
        cout << "lna[3].random=" << lna[3]->random << endl;
        cout << "lna[4].random=" << lna[4]->random << endl;

        Node* ret = s.copyRandomList(lna[0]);
        cout << "www val=" << ret->val << endl;
        cout << "www =" << ret << endl;
        cout << "www next =" << ret->next << endl;
        cout << "www random=" << ret->random << endl;
        int random = -1;
        cout << "[";
        if (ret) {
            Node* ret_head = ret;
            while (ret) {
                random = get_node_random_index(ret_head, ret);
                if (random == -1)
                    cout << "[" << ret->val << "," << "null" << "]";
                else
                    cout << "[" << ret->val << "," << random << "]";
                if (ret->next)
                    cout << ",";
                ret = ret->next;
            }
        }
        cout << "]" << endl;

        if (lna) {
            for (int i = 0; i < num; ++i) {
                delete lna[i];
            }
            delete lna;
        }
    }
    {
        const int num = 2;
        Node** lna = new Node * [num];

        lna[0] = new Node(1);
        lna[1] = new Node(2);
        lna[0]->next = lna[1];
        lna[1]->next = NULL;
        lna[0]->random = lna[1];
        lna[1]->random = lna[1];

        Node* ret = s.copyRandomList(lna[0]);

        int random = -1;
        cout << "[";
        if (ret) {
            Node* ret_head = ret;
            while (ret) {
                random = get_node_random_index(ret_head, ret);
                if (random == -1)
                    cout << "[" << ret->val << "," << "null" << "]";
                else
                    cout << "[" << ret->val << "," << random << "]";
                if (ret->next)
                    cout << ",";
                ret = ret->next;
            }
        }
        cout << "]" << endl;

        if (lna) {
            for (int i = 0; i < num; ++i) {
                delete lna[i];
            }
            delete lna;
        }
    }
    {
        const int num = 3;
        Node** lna = new Node * [num];

        lna[0] = new Node(3);
        lna[1] = new Node(3);
        lna[2] = new Node(3);
        lna[0]->next = lna[1];
        lna[1]->next = lna[2];
        lna[2]->next = NULL;
        lna[0]->random = NULL;
        lna[1]->random = lna[0];
        lna[2]->random = NULL;

        Node* ret = s.copyRandomList(lna[0]);

        int random = -1;
        cout << "[";
        if (ret) {
            Node* ret_head = ret;
            while (ret) {
                random = get_node_random_index(ret_head, ret);
                if (random == -1)
                    cout << "[" << ret->val << "," << "null" << "]";
                else
                    cout << "[" << ret->val << "," << random << "]";
                if (ret->next)
                    cout << ",";
                ret = ret->next;
            }
        }
        cout << "]" << endl;

        if (lna) {
            for (int i = 0; i < num; ++i) {
                delete lna[i];
            }
            delete lna;
        }
    }
    {
        const int num = 0;

        Node* ret = s.copyRandomList(NULL);

        int random = -1;
        cout << "[";

        cout << "]" << endl;


    }
    return 0;
}