#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>


using namespace std;

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};
 
class Solution {

private:

public:

	int countNodes(TreeNode* root) {

		if (!root) return 0;
		int left_cnt=0, right_cnt=0;
		TreeNode *l_node=root->left;
		TreeNode *r_node=root->right;
		while (l_node) { l_node=l_node->left; ++left_cnt; }
		while (r_node) { r_node=r_node->right; ++right_cnt; }
		if (left_cnt==right_cnt) {
			return ((2<<left_cnt)-1);
		}
		return countNodes(root->left) + countNodes(root->right) + 1;
	}
};

int main()
{
	int a=2<<0;
cout << a<<endl;
	return 0;
}
