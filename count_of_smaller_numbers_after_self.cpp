#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>


using namespace std;

/* Fenwick Tree / Binary Indexed Tree */
class FenwickTree {
public:
    FenwickTree(int n) : sums_(n + 1, 0) {}
    void update(int x, int delta) {
        for (int i = x; i < sums_.size(); i += lowbit(i)) {
            sums_[i] += delta;
        }
    }
    int query(int x) const
    {
        int sum = 0;
        for (int i = x; i > 0; i -= lowbit(i)) {
            sum += sums_[i];
        }
        return sum;
    }
    void modify()
    {
    }

private:
    static inline int lowbit(int x) { return x & (-x); }
    vector<int> sums_;
};

class Solution {
public:
    vector<int> countSmaller(vector<int>& nums) {
        set<int> sorted(nums.begin(), nums.end());
        unordered_map<int, int> ranks;
        int rank = 0;
        for (const int num : sorted)
            ranks[num] = ++rank;

        vector<int> ans;
        FenwickTree tree(ranks.size());

        for (int i = nums.size() - 1; i >= 0; --i) {
            ans.push_back(tree.query(ranks[nums[i]] - 1));

            tree.update(ranks[nums[i]], 1);
        }

        std::reverse(ans.begin(), ans.end());

        return ans;
    }
};

int main()
{
    Solution s;
    {
        vector<int> v1{ 5, 2, 6, 1 };
        vector<int> ret = s.countSmaller(v1);
        cout << "[";
        if (ret.size() > 0) {
            for (int i = 0; i < ret.size(); ++i)
                if (i < (ret.size() - 1))
                    cout << ret[i] << ",";
                else
                    cout << ret[i];
        }
        cout << "]" << endl;
    }
    {
        vector<int> v1{ -1 };
        vector<int> ret = s.countSmaller(v1);
        cout << "[";
        if (ret.size() > 0) {
            for (int i = 0; i < ret.size(); ++i)
                if (i < (ret.size() - 1))
                    cout << ret[i] << ",";
                else
                    cout << ret[i];
        }
        cout << "]" << endl;
    }
    {
        vector<int> v1{ -1, -1 };
        vector<int> ret = s.countSmaller(v1);
        cout << "[";
        if (ret.size() > 0) {
            for (int i = 0; i < ret.size(); ++i)
                if (i < (ret.size() - 1))
                    cout << ret[i] << ",";
                else
                    cout << ret[i];
        }
        cout << "]" << endl;
    }
    {
        vector<int> v1{ 9, 4, 6, 7, 8, 1, 1, 2 };
        vector<int> ret = s.countSmaller(v1);
        cout << "[";
        if (ret.size() > 0) {
            for (int i = 0; i < ret.size(); ++i)
                if (i < (ret.size() - 1))
                    cout << ret[i] << ",";
                else
                    cout << ret[i];
        }
        cout << "]" << endl;
    }

    return 0;
}
