#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>


using namespace std;



struct TreeNode {
     int val;
     TreeNode *left;
     TreeNode *right;
     TreeNode() : val(0), left(nullptr), right(nullptr) {}
     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};


class Solution {
public:
	int countNodes(TreeNode* root) {

	}
	bool dfs(unordered_map<int, vector<int>>& adjacency_list, int vertex,
		unordered_map<int, int>& visited_st, vector<int>& ans) {
		cout << "visited_st[" << vertex << "]=" << visited_st[vertex] << endl;
		if (visited_st[vertex] == 1) return true;	// in stack or queue
		if (visited_st[vertex] == 2) return false;	// processed

		visited_st[vertex] = 1;
		for (auto& next_vertex : adjacency_list[vertex]) {
			cout << "visited_st[" << vertex << "]=" << visited_st[next_vertex] << " next_vertex=" << next_vertex << endl;
			if (dfs(adjacency_list, next_vertex, visited_st, ans)) {
				return true;
			}
		}
		visited_st[vertex] = 2; // processed

		ans.push_back(vertex);

		return false;
	}

	bool canFinish(int numCourses, vector<vector<int>>& prerequisites) {
		vector<int> ans;

		int vertices = numCourses;
		unordered_map<int, vector<int>> adjacency_list;
		unordered_map<int, int> visited_st;
		vector<int> indegree(numCourses, 0);

		for (const auto& p : prerequisites) {
			adjacency_list[p[1]].push_back(p[0]);
			indegree[p[0]]++;
		}

		// 0: not processed, 1: in queue or stack, 2: processed
		for (int i = 0; i < numCourses; ++i) {
			visited_st[i] = 0; //not processed
		}

		for (vector<int>::iterator col = indegree.begin(); col != indegree.end(); col++) {
			cout << "DDD=" << *col << endl;
		}


#if 1
		queue<int> q;
		for (int idx = 0; idx < vertices; ++idx) {
			if (indegree[idx] == 0) { // select the vertex with no indegree
				q.push(idx);
			}
		}

		while (!q.empty()) {
			int v = q.front();
			ans.push_back(v);
			q.pop();
			for (auto& next_vertex : adjacency_list[v]) {
				--indegree[next_vertex];
				if (indegree[next_vertex] == 0)
					q.push(next_vertex);
			}
		}
#else
		for (int idx = 0; idx < vertices; ++idx) {
			if (indegree[idx] == 0) { // select the vertex with no indegree
				if (dfs(adjacency_list, idx, visited_st, ans)) {
					return vector<int>();
				}
			}
		}
		std::reverse(ans.begin(), ans.end());
#endif
		if (ans.size() < numCourses) return false;

		return true;
	}
};

int main()
{
	Solution solution;
	{
		vector<vector<int>> prerequisites{
			{1,0},
		};
		vector<int> ret = solution.canFinish(2, prerequisites);
		cout << "ret=" << ret << endl;
	}
	{
		vector<vector<int>> prerequisites{
			{1,0},
			{2,0},
			{3,1},
			{3,2}
		};

		vector<int> ret = solution.findOrder(4, prerequisites);
		cout << "ret=" << ret << endl;
	}
	{
		vector<vector<int>> prerequisites{

		};

		vector<int> ret = solution.findOrder(1, prerequisites);
		cout << "ret=" << ret << endl;
	}
	return 0;
}
