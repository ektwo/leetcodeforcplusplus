#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <stack>


using namespace std;


class Solution {
public:
    int g_total_possible_pwd;

    bool dfs(const int n, const int k, unordered_set<string> &visited, string &ans) {
        
        if (visited.size() == g_total_possible_pwd) return true;

        //prifix of current node.
        string node = ans.substr(ans.length() - n + 1, n - 1);
        for (char c = '0'; c < '0' + k; ++c) {
            node.push_back(c);
            if (!visited.count(node)) {
                ans.push_back(c);
                visited.insert(node);
                //return true as this path have answer.
                if (dfs(n, k, visited, ans)) return true;
                //no answer, need backtracking
                visited.erase(node);
                ans.pop_back();
            }
            node.pop_back();
        }
        return false;
    }

    string crackSafe(int n, int k) {

        g_total_possible_pwd = pow(k, n);

        string ans(n, '0'); // first password

        unordered_set<string> visited{ ans };
        if (dfs(n, k, visited, ans))
            return ans;
        return "";
    }
};

int main()
{

    return 0;
}
