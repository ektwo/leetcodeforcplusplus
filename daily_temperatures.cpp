#include <limits.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <queue>
#include <stack>


using namespace std;

void print_1d_int(vector<int> nums)
{
    cout << "{";
    for (int j = 0; j < nums.size(); ++j) {
        cout << nums[j] << ",";
    }
    cout << "}," << endl;
}
class Solution {
public:
    vector<int> dailyTemperatures(vector<int>& temperatures) {

        vector<int> ans;
        unordered_map<int, int> m;
        stack<int> st;

        for (int i = 0; i < temperatures.size(); ++i) {
            int num = temperatures[i];
            while (!st.empty()) {
                int stacked_idx = st.top();
                int stacked_num = temperatures[stacked_idx];
                if (stacked_num < num) { m[stacked_idx] = i; st.pop(); continue; }
                break;
            }
            st.push(i);
        }
        for (int i = 0; i < temperatures.size(); ++i) {
            if ((m[i] - i) > 0) {
                ans.push_back(m[i] - i);
            }
            else
                ans.push_back(0);
        }
        return ans;
    }
};

int main()
{
    Solution solution;

    {
        vector<int> temperatures = { 73,74,75,71,69,72,76,73};
        vector<int> ans = solution.dailyTemperatures(temperatures);
        print_1d_int(ans);
    }
cout << "next=" << endl;
    {
        vector<int> temperatures = { 30,40,50,60};
        vector<int> ans = solution.dailyTemperatures(temperatures);
        print_1d_int(ans);
    }
cout << "next=" << endl;
    {
        vector<int> temperatures = { 30,60,90};
        vector<int> ans = solution.dailyTemperatures(temperatures);
        print_1d_int(ans);
    }
    return 0;
}
