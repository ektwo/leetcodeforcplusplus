#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <stack>


using namespace std;


class Solution {
public:
	string decodeString(string s) {
		
		string ans("");
		
		int sector_nums = 0;
		stack<pair<int, string>> stack_pair;

		for (int i = 0; i < s.size(); ++i) {
			if (s[i] >= '0' && s[i] <= '9') {
				sector_nums *= 10;
				sector_nums += s[i] - '0';
			}
			else if (s[i] == '[') {
				stack_pair.push(make_pair(sector_nums, ans));
				sector_nums = 0;
				ans = "";
			}
			else if (s[i] == ']') {
				int nums = stack_pair.top().first;
				string s = stack_pair.top().second;
				stack_pair.pop();
				for (int n = 0; n < nums; ++n) s += ans;
				ans = s;
			}
			else {
				ans += s[i];
			}
		}

		return ans;
	}
};

int main()
{
	Solution solution;
	{
		string str("3[a]2[bc]");
		cout << solution.decodeString(str) << endl;
	}
	{
		string str("3[a2[c]]");
		cout << solution.decodeString(str) << endl;
	}
	{
		string str("2[abc]3[cd]ef");
		cout << solution.decodeString(str) << endl;
	}
	return 0;
}
