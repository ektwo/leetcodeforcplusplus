#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <limits>


using namespace std;

struct ListNode {
    int val;
    ListNode *next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
};

void print_1d_int(vector<int> nums)
{
    cout << "{";
    for (int j = 0; j < nums.size(); ++j) {
        cout << nums[j] << ",";
    }
    cout << "}," << endl;
}
void print_list(ListNode *root)
{
    if (root) {
        cout << root->val << ",";
        if (root->next) print_list(root->next);
    }
    else
        cout << endl;
}

ListNode* create_list_nodes(vector<int> nums) {
    if (nums.size() <= 0) return nullptr;
    ListNode *head = new ListNode(-1);
    ListNode *prev = head, *curr = nullptr;
    for (auto &num : nums) {
        curr = new ListNode(num);
        prev->next = curr;
        prev = curr;
    }
    return head->next;
}

class Solution {

private:

public:
    void deleteNode(ListNode* node) {
        ListNode *next = node->next;
        node->val = next->val;
        node->next = next->next;
        delete next;
    }

};

int main()
{

    return 0;
}