#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <stack>


using namespace std;



 struct TreeNode {
     int val;
     TreeNode *left;
     TreeNode *right;
     TreeNode() : val(0), left(nullptr), right(nullptr) {}
     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 };

class Solution {
public:
    int total = INT_MIN;
    TreeNode* real_root = NULL;
    vector<int> total_array;
    int max_path = INT_MIN;
    vector<int> path_array;

    int diameterOfBinaryTree(TreeNode* root) {
        if (!root) return INT_MIN;
        if (!real_root) real_root = root;
        int c_val = INT_MIN, l_val = INT_MIN, r_val = INT_MIN;
        int len_val = INT_MIN;
        if (root->left) l_val = diameterOfBinaryTree(root->left);
        if (root->right) r_val = diameterOfBinaryTree(root->right);
        cout << "root_val=" << root->val << " l_val=" << l_val << " r_val=" << r_val << endl;
        if ((l_val >= 0) && (r_val >= 0)) {
            total_array.push_back(max(root->val + l_val + r_val, 0));
            path_array.push_back(max(2 + l_val + r_val, 0));
            c_val = max(root->val + l_val, root->val + r_val);
            len_val = max(l_val + 1, r_val + 1);
            cout << "A len_val=" << len_val << endl;
        }
        else if (l_val >= 0) {
            c_val = max(root->val + l_val, 0);
            len_val = max(l_val, 0) + 1;
            cout << "B len_val=" << len_val << endl;
        }
        else if (r_val >= 0) {
            c_val = max(root->val + r_val, 0);
            len_val = max(r_val, 0) + 1;
            cout << "C len_val=" << len_val << endl;
        }
        else {
            c_val = root->val;
            len_val = 0;
            cout << "D len_val=" << len_val << endl;
        }

        //if (c_val > total) {
        //    total = c_val;
        //}
        cout << "E max_path=" << max_path << endl;
        if (len_val > max_path) {
            max_path = len_val;
        }

        if (root == real_root) {
            //for (int i = 0; i < total_array.size(); ++i) {
            //    if (total_array[i] > total)
            //        total = total_array[i];
            //}
            //return total;
            for (int i = 0; i < path_array.size(); ++i) {
                if (path_array[i] > max_path)
                    max_path = path_array[i];
            }
            return max_path;
        }
        else
            //return c_val;
            return len_val;
    }
};

int main()
{
    {
        Solution solution;
        TreeNode r(3);
        TreeNode ll(4);
        TreeNode lr(5);
        TreeNode l(2, &ll, &lr);
        TreeNode root(1, &l, &r);
        cout << solution.diameterOfBinaryTree(&root) << endl;
    }
    {
        Solution solution;
        TreeNode l(2);
        TreeNode root(1, &l, nullptr);
        cout << solution.diameterOfBinaryTree(&root) << endl;
    }
    {
        Solution solution;
        TreeNode ll(1);
        TreeNode l(3, &ll, nullptr);
        TreeNode root(2, &l, nullptr);
        cout << solution.diameterOfBinaryTree(&root) << endl;
    }
	return 0;
}