#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <stack>


using namespace std;


class Solution {
public:
    unordered_set<string> vis;
    unordered_map<string, unordered_map<string, double>> e;

    bool dfs(const string& begin, const string& end, double& ret) {
        if (vis.count(begin)) return false;
        vis.insert(begin); //a,b
        for (auto &[k, v] : e[begin]) { // [k=b,v=2] k=a,v=1/2
            if (k == end) { ret = v; return true; } // end=c
            else if (dfs(k, end, ret)) { ret *= v; return true; } //dfs(b,c)
            else { //c 
            }
        }
        return false;
    }
    vector<double> calcEquation(vector<vector<string>>& equations, vector<double>& values, vector<vector<string>>& queries) {
        vector<double> ret;

        for (int i = 0; i < equations.size(); ++i) {
            auto& q = equations[i];
            e[q[0]][q[1]] = values[i];
            e[q[1]][q[0]] = 1 / values[i];
        }

        for (auto& q : queries) {
            vis.clear();
            ret.push_back(1);
            if (!dfs(q[0], q[1], ret.back())) ret.back() = -1;
        }

        return ret;
    }
};

int main()
{
	Solution solution;
    vector<vector<string>> equations = {
        {"a","b" },
        {"b","c" }
    };
    vector<double> values = {
        {2.0,3.0 }
    };
    vector<vector<string>> queries = {
        {"a","c"},{"b","a"},{"a","e"},{"a","a"},{"x","x"}
    };

    cout << " { ";
    vector<double> ans = solution.calcEquation(equations, values, queries);
    for (int i = 0; i < ans.size(); ++i) {
        if (i != ans.size() - 1)
            cout << ans[i] << ",";
        else
            cout << ans[i];
    }
    cout << " } ";

	return 0;
}
