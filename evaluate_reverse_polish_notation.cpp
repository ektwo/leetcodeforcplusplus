#include <iostream>
#include <stack>
#include <algorithm>
using namespace std;


class Solution
{
private:
    bool is_operator(string str) {
        return (str=="+"||str=="-"||str=="*"||str=="/");
    }
    int add(long long v1, long long v2) {
        return v1+v2;
    }
    int sub(long long v1, long long v2) {
        return v1-v2;
    }
    int mul(long long v1, long long v2) {
        return v1*v2;
    }
    int div(long long v1, long long v2) {
        return v1/v2;
    }
    int calc(string str, long long v1, long long v2) {
        if (str=="+") return add(v1,v2);
        else if (str=="-") return sub(v1,v2);
        else if (str=="*") return mul(v1,v2);
        else if (str=="/") return div(v1,v2);
        else return 0;
    }
public:
    int evalRPN(vector<string>& tokens) {
        if (tokens.size()==1) return stoi(tokens[0]);
        long long ans=0;
        stack<long long> st;

        long long tmp1=0;
        long long tmp2=0;
        for (int i = 0; i < tokens.size();++i) {

            if (is_operator(tokens[i]) && i >= 2) {
                if (!st.empty()) {
                    tmp2=st.top(); st.pop();
                }
                if (!st.empty()) {
                    tmp1=st.top(); st.pop();
                }
                ans=calc(tokens[i],tmp1,tmp2);
                //cout << "ans=" << ans << endl;
                st.push(ans);
            }
            else
                st.push(stoi(tokens[i]));
        }
        return (int)ans;
    }
};

int main()
{
    Solution solution;
    {
        vector<string> tokens={"2","1","+","3","*"};
        int ans = solution.evalRPN(tokens);
        cout << ans << endl;
    }
    {
        vector<string> tokens={"4","13","5","/","+"};
        int ans = solution.evalRPN(tokens);
        cout << ans << endl;
    }
    {
        vector<string> tokens={"10","6","9","3","+","-11","*","/","*","17","+","5","+"};
        int ans = solution.evalRPN(tokens);
        cout << ans << endl;
    }

    return 0;
}