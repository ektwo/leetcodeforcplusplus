#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>


using namespace std;



class Solution {
private:
    bool is_stretchy(const string& S, const string& word) {

        int j = 0;
        for (int i = 0; i < S.length(); ++i) {
            if (j < word.length() && S[i] == word[j]) ++j; // char is matched
            // the previous one and the previous of previous are same to current one.
            else if (i > 1 && (S[i] == S[i - 1]) && (S[i - 1] == S[i - 2])) {
                cout << "11 S[" << i << "]=" << S[i] << " word[" << j << "]=" << word[j] << endl;
                continue;
            }
            // the previous one and the next one are same to current one.
            else if (i > 0 && ((i + 1) < S.length()) && (S[i] == S[i - 1]) && (S[i] == S[i + 1])) {
                cout << "22 S[" << i << "]=" << S[i] << " word[" << j << "]=" << word[j] << endl;
                continue;
            }
            else return false;
        }

        return j == word.length();
    }

public:
    int expressiveWords(string S, vector<string>& words) {

        int ans = 0;

        if (S.empty()) return 0;

        for (auto &word : words) {
            if (is_stretchy(S, word)) {
                ++ans;
            }
        }

        return ans;

    }
};

int main()
{
    Solution s;
    {
        string str = "heeellooo";
        vector<string> words = { "hello", "hi", "helo" };
        cout << s.expressiveWords(str, words) << endl;
    }
    {
        string str = "zzzzzyyyyy";
        vector<string> words = { "zzyy","zy","zyy" };
        cout << s.expressiveWords(str, words) << endl;
    }

    return 0;
}
