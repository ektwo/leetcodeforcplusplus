#include <limits.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <queue>


using namespace std;


class Solution {

public:

    int fib(int n) {
    #if 1
        if (n<=1) return n;
        return fib(n - 1) + fib(n-2);
    #else
        if (n<=1) return n;
        vector<int> dp(n+1);

        /* fibonacci */
        dp[0] = 0; dp[1] = 1;
        for (int i = 2; i <= n; ++i) {
            dp[i] = dp[i - 1] + dp[i - 2];
        }

        return dp.back();
    #endif
    }
};

int main()
{
    Solution solution;

    {
        int n = 2;
        int ans = solution.fib(n);
        cout << "solution.fib=" << ans << endl;
    }
cout << "next=" << endl;
    {
        int n = 3;
        int ans = solution.fib(n);
        cout << "solution.fib=" << ans << endl;
    }
cout << "next=" << endl;
    {
        int n = 4;
        int ans = solution.fib(n);
        cout << "solution.fib=" << ans << endl;
    }
    return 0;
}
