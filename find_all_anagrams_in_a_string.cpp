#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <queue>


using namespace std;


void print_1d_int(vector<int> nums)
{

        cout << "{";
        for (int j = 0; j < nums.size(); ++j) {
            cout << nums[j] << ",";
        }
        cout << "}," << endl;

}


class Solution {

public:

    vector<int> findAnagrams(string s, string p) {
        if (s.length() < p.length()) return {};
        vector<int> ans;
        int n = s.length();
        int windows_size = p.length();
        vector<int> p_ch_freq(26, 0);
        vector<int> window(26, 0);

        for (int right = 0; right < windows_size; ++right) {
            p_ch_freq[p[right]-'a']++;
            window[s[right]-'a']++;
        }
        if (window == p_ch_freq) ans.push_back(0);

        int left = 0;
        for (int right = windows_size; right < n; ++right) {
            window[s[right]-'a']++;
            window[s[left ]-'a']--;
            //if (valid())
                ++left;
            if (window == p_ch_freq) ans.push_back(left);
        }


        return ans;
    }

};

int main()
{
    Solution solution;
    {
        string s = "cbaebabacd";
        string p = "abc";
        vector<int> ans = solution.findAnagrams(s, p);
        print_1d_int(ans);
    }
    {
        string s = "abab";
        string p = "ab";
        vector<int> ans = solution.findAnagrams(s, p);
        print_1d_int(ans);
    }
    return 0;
}
