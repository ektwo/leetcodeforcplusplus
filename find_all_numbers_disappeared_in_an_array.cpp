#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <queue>


using namespace std;

/*
2 4 5 8
2 3 4 5 8 -> 4
2 3 4 5 5 8 -> 5
2 3 4 5 5 8 10 -> 5
2 3 4 5 5 8 9 10 -> 8
2 3 4 4 5 5 8 9 10 -> 8
*/

class Solution {

public:

    vector<int> findDisappearedNumbers(vector<int>& nums) {
        int src = 0;
        int n = nums.size();
        while (src < n) {
            int dst = nums[src] - 1;
            if (dst < n && nums[src] != nums[dst])
                swap(nums[src], nums[dst]);
            else
                ++src;
        }

        vector<int> ans;
        for (int i = 0; i < nums.size(); ++i) {
            if (i != (nums[i] - 1))
                ans.push_back(i + 1);
        }

        return ans;
    }

};

int main()
{
    Solution s;
    {
        vector<int> nums = { 4,3,2,7,8,2,3,1};
        vector<int> ans = s.findDisappearedNumbers(nums);
        for (int i = 0; i < ans.size(); ++i)
        cout << ans[i] << "," << endl;
    }
    {
        vector<int> nums = { 1,1};
        vector<int> ans = s.findDisappearedNumbers(nums);
        for (int i = 0; i < ans.size(); ++i)
        cout << ans[i] << "," << endl;
    }
    return 0;
}
