#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>


using namespace std;



class Solution {

#if 1
public:

    string findReplaceString(string s, vector<int>& indices, vector<string>& sources, vector<string>& targets) {
        string ans = "";

        int indices_idx = 0;
        int idx_offset = 0;
        int max_idx = 0, max_tmp_val = 0;

        for (int j = 0; j < indices.size(); ++j) {
            if (max_tmp_val < indices[j]) {
                max_tmp_val = indices[j];
                max_idx = j;
            }
        }

        for (int i = 0; i < s.length(); ++i) {
            bool found = false;
            bool found1 = false;
            for (int j = 0; j < indices.size(); ++j) {
                if (i == indices[j]) {
                    idx_offset = sources[j].length();

                    if (0 == s.substr(i, idx_offset).compare(sources[j])) {
                        ans += targets[j];
                        i += sources[j].length() - 1;
                        found = true;
                        break;
                    }
                    found1 = true;
                }
            }
            if (!found) {
                ans.append(1, s[i]);

            }
            else if (found1) {
                ans.append(1, s[i]);

            }
        }

        return ans;
    }
    #else
public:

    string findReplaceString(string s, vector<int>& indices, vector<string>& sources, vector<string>& targets) {
        string ans = "";

        int indices_idx = 0;
        int idx_offset = 0;
        int max_idx = 0, max_tmp_val = 0;

        for (int j = 0; j < indices.size(); ++j) {
            if (max_tmp_val < indices[j]) {
                max_tmp_val = indices[j];
                max_idx = j;
            }
        }

        for (int i = 0; i < s.length(); ++i) {
            bool found = false;

            cout << "A i=" << i << " ans=" << ans << endl;

            bool found1 = false;
            for (int j = 0; j < indices.size(); ++j) {
                if (i == indices[j]) {
                    if (j != max_idx)//(j + 1) < indices.size())
                        idx_offset = sources[j].length();//indices[j + 1] - i;
                    else
                        idx_offset = sources[j].length();//s.length() - indices[j];

                    cout << "0 i=" << i << " j=" << j << " idx_offset=" << idx_offset << endl;
                    cout << "0 sources[j]=" << sources[j] << " s.substr(i, idx_offset))=" << s.substr(i, idx_offset) << endl;

                    if (0 == s.substr(i, idx_offset).compare(sources[j])) {
                        ans += targets[j];
                        cout << "B i=" << i << " ans=" << ans << endl;
                        i += sources[j].length() - 1;
                        found = true;
                        break;
                    }
                    else if (s.substr(i, idx_offset).find(sources[j]) != std::string::npos) {
                        ans += targets[j];
                        cout << "C i=" << i << " ans=" << ans << endl;
                        i += sources[j].length() - 1;
                        found = true;
                        break;
                    }
                    found1 = true;
                }
            }
            if (!found) {
                ans.append(1, s[i]);
                cout << "D i=" << i << " ans=" << ans << endl;
            }
            else if (found1) {
                ans.append(1, s[i]);
                cout << "E i=" << i << " ans=" << ans << endl;
            }
        }

        return ans;
    }
    #endif
};

int main()
{
    Solution s;
    {
        string str = "abcd";
        vector<int> indices = { 0, 2 };
        vector<string> sources = { "a", "cd"};
        vector<string> targets = { "eee", "ffff"};
        cout << s.findReplaceString(str, indices, sources, targets) << endl;
    }
    {
        string str = "abcd";
        vector<int> indices = { 0, 2 };
        vector<string> sources = { "ab", "ec"};
        vector<string> targets = { "eee", "ffff"};
        cout << s.findReplaceString(str, indices, sources, targets) << endl;
    }
    {
        string str = "vmokgggqzp";
        vector<int> indices = { 3,5,1 };
        vector<string> sources = { "kg","ggq","mo"};
        vector<string> targets = { "s","so","bfr"};
        cout << s.findReplaceString(str, indices, sources, targets) << endl;
    }
    {
        string str = "jjievdtjfb";
        vector<int> indices = { 4, 6, 1 };
        vector<string> sources = { "md","tjgb","jf"};
        vector<string> targets = { "foe","oov","e"};
        cout << s.findReplaceString(str, indices, sources, targets) << endl;
    }
    return 0;
}
