#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <limits>


using namespace std;


void print_1d_string(vector<string> nums)
{
    cout << "{";
    for (int j = 0; j < nums.size(); ++j) {
        cout << nums[j] << ",";
    }
    cout << "}," << endl;
}

class Solution {
public:
    vector<string> commonChars(vector<string>& words) {
        vector<string> ans;
        int n = words.size();
        vector<vector<int>> map(n, vector<int>(26,0));

        for (int i = 0; i <n; ++i) {
            for (int j = 0; j < words[i].size(); ++j) {
                map[i][words[i][j]-'a']++;
            }
        }

        for (int j = 0; j < 26; ++j) {
            int num = numeric_limits<int>::max();
            for (int x = 0; x < n; ++x)
                num = min(num, (int)(map[x][j]));
            if (num > 0) {
                for (int i = 0; i < num; ++i) {
                    char ch = (char)('a'+j);
                    string str(1, ch);
                    ans.push_back(str);
                }
            }
        }
        return ans;
    }
};

int main()
{
    Solution solution;
    {
        vector<string> words = {"bella","label","roller"};
        vector<string> ans = solution.commonChars(words);
        print_1d_string(ans);
    }
    {
        vector<string> words = {"cool","lock","cook"};
        vector<string> ans = solution.commonChars(words);
        print_1d_string(ans);
    }
    {
        vector<string> words = {"acabcddd","bcbdbcbd","baddbadb","cbdddcac","aacbcccd","ccccddda","cababaab","addcaccd"};
        vector<string> ans = solution.commonChars(words);
        print_1d_string(ans);
    }

    return 0;
}
