#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>


using namespace std;



class Solution {
public:
    vector<int> searchRange(vector<int>& nums, int target) {
        if (nums.size() == 0) return { -1, -1 };
        vector<int> ret;
        ret.push_back(findStartingIndex(nums, target));
        ret.push_back(findEndingIndex(nums, target));
        return ret;
    }

    int findStartingIndex(vector<int>& nums, int target) {
        if (nums.size() == 0) return -1;

        int return_idx = -1;
        int start_idx = 0;
        int end_idx = nums.size() - 1;

        while (start_idx <= end_idx) {
            int midpoint = start_idx + (end_idx - start_idx) / 2;
            if (nums[midpoint] >= target) {
                end_idx = midpoint - 1;
            }
            else {
                start_idx = midpoint + 1;
            }

            if (nums[midpoint] == target) return_idx = midpoint;
        }

        return return_idx;
    }
    int findEndingIndex(vector<int>& nums, int target) {
        if (nums.size() == 0) return -1;

        int return_idx = -1;
        int start_idx = 0;
        int end_idx = nums.size() - 1;

        while (start_idx <= end_idx) {
            int midpoint = start_idx + (end_idx - start_idx) / 2;
            if (nums[midpoint] <= target) {
                start_idx = midpoint + 1;
            }
            else {
                end_idx = midpoint - 1;
            }

            if (nums[midpoint] == target) return_idx = midpoint;
        }

        return return_idx;
    }
};



int main()
{
    Solution s;
    {
        vector<int> v{ 5, 7, 7, 8, 8, 10 };
        vector<int> ret = s.searchRange(v, 8);
        cout << "[";
        if (ret.size() > 0) {
        for (int i = 0; i < ret.size(); ++i)
            if (i < (ret.size() - 1))
                cout << ret[i] << ",";
            else
                cout << ret[i];
        }
        cout << "]" << endl;
    }
    {
        vector<int> v{ 5, 7, 7, 8, 8, 10 };
        vector<int> ret = s.searchRange(v, 6);
        cout << "[";
        if (ret.size() > 0) {
            for (int i = 0; i < ret.size(); ++i)
                if (i < (ret.size() - 1))
                    cout << ret[i] << ",";
                else
                    cout << ret[i];
        }
        cout << "]" << endl;
    }
    {
        vector<int> v{ };
        vector<int> ret = s.searchRange(v, 0);
        cout << "[";
        if (ret.size() > 0) {
            for (int i = 0; i < ret.size(); ++i)
                if (i < (ret.size() - 1))
                    cout << ret[i] << ",";
                else
                    cout << ret[i];
        }
        cout << "]" << endl;
    }

    return 0;
}
