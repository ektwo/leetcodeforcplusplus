#include <limits.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <queue>


using namespace std;


class Solution {

public:

    vector<int> findClosestElements(vector<int>& arr, int k, int x) {

        int left = 0;
        int right = arr.size() - k;

        while (left < right) {
            int mid = left + (right - left) / 2;
            if (x - arr[mid] > arr[mid + k] - x) left = mid + 1;
            else right = mid;
        }

        return vector<int>(arr.begin() + left, arr.begin() + left + k);


    }
};

int main()
{
    Solution solution;

    {
        vector<int> arr = { 1,2,3,4,5};
        int k = 4;
        int x = 3;
        vector<int> ans = solution.findClosestElements(arr, k, x);
        for (int i = 0; i < ans.size(); ++i) {
            cout << ans[i] << "," << endl;
        }
    }
cout << "next" << endl;
    {
        vector<int> arr = { 1,2,3,4,5};
        int k = 4;
        int x = -1;
        vector<int> ans = solution.findClosestElements(arr, k, x);
        for (int i = 0; i < ans.size(); ++i) {
            cout << ans[i] << "," << endl;
        }
    }

    return 0;
}
