#include <limits.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <queue>
#include <limits>


using namespace std;

template<class T>
void print_1d(const T& nums)
{
    cout << "{";
    for (int j = 0; j < nums.size(); ++j) {
        cout << nums[j] << ",";
    }
    cout << "}," << endl;
}
template<class T>
void print_2d(const T& nums)
{
    cout << "{";
    for (int i = 0; i < nums.size(); ++i) {
        cout << "{";
        for (int j = 0; j < nums[i].size(); ++j) {
            cout << nums[i][j] << ",";
        }
        cout << "}," << endl;
    }
    cout << "}," << endl;
}

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};
class Node {
public:
    int val;
    vector<Node*> children;

    Node() {}

    Node(int _val) {
        val = _val;
    }

    Node(int _val, vector<Node*> _children) {
        val = _val;
        children = _children;
    }
};
Node* newNode(int key)
{
    Node* temp = new Node;
    temp->val = key;
    return temp;
}
class Solution {

private:
    int node_nums;
    int tree_height;
    TreeNode *m_head;
    bool _isValidBST(TreeNode* root, long min, long max) {
        if (!root) return true;
        cout << "root=" << root << endl;
        cout << "root->val=" << root->val << endl;
        cout << "root->left=" << root->left << " min=" << min << " max=" << max << endl;
        cout << "root->right=" << root->right << " min=" << min << " max=" << max << endl;
        if (root->val <= min) return false;
        if (root->val >= max) return false;
        
        return _isValidBST(root->left, min, root->val) && _isValidBST(root->right, root->val, max);
    }

    bool _isSymmetric(TreeNode *left, TreeNode *right) {
        if (!left && !right) return true;
        if (left && !right) return false;
        if (!left && right) return false;
        if (left->val != right->val) return false;
        return _isSymmetric(left->left, right->right) && _isSymmetric(left->right, right->left);
    }

public:
    void reset() {
        node_nums = 0;
        tree_height = 0;
        m_head = nullptr;
    }

    TreeNode* get_head() {
        return m_head;
    }

    TreeNode* push(TreeNode *root, int val, bool left, bool right)
    {
        TreeNode* temp = new TreeNode(val);
        //cout << "push val=" << val << " m_head=" << m_head << endl;
        //cout << "push val=" << val << " root=" << root << endl;
        if (!m_head) {
            m_head = temp;
        }

        if (!root)
            root = m_head;

        if (root && left)
            root->left = temp;
        else if (root && right)
            root->right = temp;

        return temp;
    }

    bool isSymmetric(TreeNode* root) {
        if (!root) return true;
        return _isSymmetric(root->left, root->right);
    }

    vector<vector<int>> levelOrder(TreeNode* root) {
        if (!root) return {};
        vector<vector<int>> ans;

        queue<TreeNode*> q;
        q.push(root);
        while (!q.empty()) {
            vector<int> tmp_solu;
            for (int i = q.size(); i > 0; --i) {
                TreeNode *tmp = q.front(); q.pop();
                if (!tmp) break;
                tmp_solu.push_back(tmp->val);

                if (tmp->left) q.push(tmp->left);
                if (tmp->right) q.push(tmp->right);
            }
            ans.push_back(tmp_solu);
        }


        return ans;
    }

    vector<vector<int>> levelOrderBottom(TreeNode* root) {
        if (!root) return {};

        vector<vector<int>> ans;
        queue<TreeNode*> q;
        q.push(root);
        //ans.push_back({root->val});

        while (!q.empty()) {
            vector<int> tmp;
            int size=q.size();
            for (int i=0;i<size;++i) {
                TreeNode* node=q.front(); q.pop();
                if (!node) break;
                tmp.push_back(node->val);
                if (node->left) q.push(node->left);
                if (node->right) q.push(node->right);
            }
            ans.push_back(tmp);
        }
        reverse(ans.begin(), ans.end());
        return ans;
    }
    vector<double> averageOfLevels(TreeNode* root) {
        vector<double> ans;
        if (!root) return {};

        queue<TreeNode*> q;
        q.push(root);

        while (!q.empty()) {
            int size=q.size();
            double total=0,average=0;
            for (int i=size;i>0;--i) {
                TreeNode *node=q.front(); q.pop();
                total+=node->val;
                if (node->left) q.push(node->left);
                if (node->right) q.push(node->right);
            }
            ans.push_back(total/size);
        }
        return ans;
    }
    vector<vector<int>> levelOrder(Node* root) {
        vector<vector<int> > ans;
        if (!root) return {};

        queue<Node*> q;
        q.push(root);

        while (!q.empty()) {
            int size=q.size();
            vector<int> tmp_solu;
            for (int i=size;i>0;--i) {
                Node *tmp=q.front();q.pop();
                tmp_solu.push_back(tmp->val);
                for (auto &child: tmp->children) {
                    q.push(child);
                }
                // while (child) {
                    
                // }
            }
            ans.push_back(tmp_solu);
        }
 
        return ans;
    }    
    vector<int> largestValues(TreeNode* root) {
        if (!root) return {};
        vector<int> ans;

        queue<TreeNode*> q;
        q.push(root);
        while (!q.empty()) {
            int max_val=numeric_limits<int>::min();
            int size=q.size();
            for (int i=size;i>0;--i) {
                TreeNode *node=q.front();q.pop();
                if (!node) break;
                max_val=max(max_val,node->val);
                if (node->left) q.push(node->left);
                if (node->right) q.push(node->right);
            }
            ans.push_back(max_val);
        }

        return ans;
    }
};

int main()
{
    Solution solution;

    solution.reset();
    {
        TreeNode *root = solution.push(nullptr, 1, false, false);
        TreeNode *l = solution.push(root, 3, true, false);
        TreeNode *r = solution.push(root, 2, false, true);
        TreeNode *ll = solution.push(l, 5, true, false);
        TreeNode *lr = solution.push(l, 3, false, true);
        TreeNode *rr = solution.push(r, 9, false, true);

        vector<int> ans = solution.largestValues(solution.get_head());
        print_1d(ans);
    }
    solution.reset();
    {
        TreeNode *root = solution.push(nullptr, 1, false, false);
        TreeNode *l = solution.push(root, 2, true, false);
        TreeNode *r = solution.push(root, 3, false, true);

        vector<int> ans = solution.largestValues(solution.get_head());
        print_1d(ans);
    }

    return 0;
}
