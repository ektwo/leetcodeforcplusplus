#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <queue>


using namespace std;

/*
2 4 5 8
2 3 4 5 8 -> 4
2 3 4 5 5 8 -> 5
2 3 4 5 5 8 10 -> 5
2 3 4 5 5 8 9 10 -> 8
2 3 4 4 5 5 8 9 10 -> 8
*/

class KthLargest {
private:
    int K;
    priority_queue<int, vector<int>, greater<int>> q;

public:

    KthLargest(int k, vector<int>& nums) {
        K = k;
        for (int i = 0; i < nums.size(); ++i) {
            q.push(nums[i]);
            if (q.size() > K) {
                q.pop();
            }
        }
    }
    
    int add(int val) {
        q.push(val);
        if (q.size() > K)
            q.pop();
        return q.top();
    }
};

int main()
{
    {
        int k = 3;
        vector<int> nums = { 4, 5, 8, 2};
        KthLargest s(k, nums);
        cout << "add=" << s.add(3) << endl;
        cout << "add=" << s.add(5) << endl;
        cout << "add=" << s.add(10) << endl;
        cout << "add=" << s.add(9) << endl;
        cout << "add=" << s.add(4) << endl;
    }

    return 0;
}
