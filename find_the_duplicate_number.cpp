#include <iostream>
#include <vector>
#include <string>

using namespace std;

#define SOLUTION_SLOW_FAST          0
#define SOLUTION_BIT_MANIPULATION   1
#define SOLUTION                    SOLUTION_BIT_MANIPULATION
class Solution
{

public:
    int findDuplicate(vector<int>& nums) {
    #if SOLUTION == SOLUTION_SLOW_FAST
        int slow = 0, fast = 0;
        while (true) {
            slow = nums[slow];
            fast = nums[nums[fast]];
            if (slow == fast) break;
        }

        int t = 0;
        while (true) {
            slow = nums[slow];
            t = nums[t];
            if (t == slow) break;
        }

        return slow;
    #elif SOLUTION == SOLUTION_BIT_MANIPULATION

        int ans = 0;
        int cnt1 = 0, cnt2 = 0;
        int n = nums.size();
        for (int i = 0; i < 32; ++i) {
            cnt1 = cnt2 = 0;
            int bit = (1 << i);
            for (int j = 0; j < n; ++j) {
                if ((j & bit) > 0) ++cnt1;
                if ((nums[j] & bit) > 0) ++cnt2;
            }
            if (cnt2 > cnt1) ans += bit;
        }
        return ans;        
    #endif
    }
};

int main()
{
    Solution solution;
    {
        vector<int> nums = {1,3,4,2,2};
        int ans = solution.findDuplicate(nums);
        cout << ans << endl;
    }
    cout << endl;
    {
        vector<int> nums = {3,1,3,4,2};
        int ans = solution.findDuplicate(nums);
        cout << ans << endl;
    }

    return 0;
}
