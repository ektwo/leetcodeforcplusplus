#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>


using namespace std;

class Solution {
public:
    void getNext (const string& pat, vector<int>& next){

        int prefix=-1;
        int suffix=0;
        next[suffix++]=prefix;
        for (;suffix<pat.size();suffix++){
            while (prefix>=0&&pat[prefix+1]!=pat[suffix]) prefix=next[prefix];
            if (pat[prefix+1]==pat[suffix])++prefix;
            next[suffix]=prefix;
        }
    }
    int strStr(string haystack, string needle) {
        int ans = -1;
        int M=haystack.length();
        int N=needle.length();
        if (N == 0) return 0;

        vector<int> next(N,0);
        getNext(needle, next);
        
        int prefix=-1;
        int suffix=0;
        for (;suffix<M;++suffix){
            while (prefix>=0&&needle[prefix+1]!=haystack[suffix]) prefix=next[prefix];
            if (needle[prefix+1]==haystack[suffix]) ++prefix;
            if (prefix==N-1)return suffix-N+1;
        }

        return ans;
    }
};

int main()
{
    Solution solution;
    {
    string haystack = "sadbutsad";
    string needle = "sad";
    int ans = solution.strStr(haystack,needle);
    cout << ans << endl;
    }
    {
    string haystack = "leetcode";
    string needle = "leeto";
    int ans = solution.strStr(haystack,needle);
    cout << ans << endl;
    }
	return 0;

}