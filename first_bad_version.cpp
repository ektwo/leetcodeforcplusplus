#include <limits.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <queue>


using namespace std;


bool isBadVersion(int version)
{
    return false;
}

class Solution {

public:

    vector<int> findClosestElements(vector<int>& arr, int k, int x) {

        int left = 0;
        int right = arr.size() - k;

        while (left < right) {
            int mid = left + (right - left) / 2;
            if (x - arr[mid] > arr[mid + k] - x) left = mid + 1;
            else right = mid;
        }

        return vector<int>(arr.begin() + left, arr.begin() + left + k);


    }
    int firstBadVersion(int n) {
        int left = 1;
        int right = n;
        while (left < right) {
            int mid = left + (right - left) / 2;
            bool result = isBadVersion(mid);
            if (!result) {
                left = mid + 1;
            }
            else {
                right = mid;
            }
        }
        return left;
    }
};

int main()
{
    Solution solution;

    {
        int ans = solution.firstBadVersion(5);
        cout << "solution.firstBadVersion=" << ans << endl;
    }
cout << "next" << endl;
    {
        int ans = solution.firstBadVersion(1);
        cout << "solution.firstBadVersion=" << ans << endl;
    }

    return 0;
}
