#include <limits.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <queue>


using namespace std;


class Solution {

public:
    int firstUniqChar(string s) {
        int res = 0 ;

        unordered_map<char, int> m;
        for ( int i = 0 ; i < s.length(); ++ i) {
            m[s[i]]++;
        }

        for ( int i = 0 ; i < s.length(); ++ i) {
            if (m[s[i]] == 1) return i;
        }

        return -1;
    }

};

int main()
{
    Solution solution;
    {
        string s = "leetcode";
        cout << solution.firstUniqChar(s) << endl;
    }

    {
        string s = "loveleetcode";
        cout << solution.firstUniqChar(s) << endl;
    }

    {
        string s = "aabb";
        cout << solution.firstUniqChar(s) << endl;
    }

    return 0;
}
