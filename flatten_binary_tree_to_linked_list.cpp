#include <iostream>
#include <vector>
#include <string>


using namespace std;


struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};


void print_1d_int(vector<int> nums)
{

        cout << "{";
        for (int j = 0; j < nums.size(); ++j) {
            cout << nums[j] << ",";
        }
        cout << "}," << endl;

}

void print_2d_int(vector<vector<int>> nums)
{
    cout << "{";
    for (int i = 0; i < nums.size(); ++i) {
        cout << "{";
        for (int j = 0; j < nums[0].size(); ++j) {
            cout << nums[i][j] << ",";
        }
        cout << "}," << endl;
    }
    cout << "}," << endl;
}

void print_tree_preorder(TreeNode *root)
{
    if (root) {
        cout << root->val << ",";
        if (root->left) print_tree_preorder(root->left);
        if (root->right) print_tree_preorder(root->right);
    }
    else
        cout << "null,";
}

class Solution
{
private:
    int node_nums;
    int tree_height;
    TreeNode *m_head;
public:
    void reset() {
        node_nums = 0;
        tree_height = 0;
        m_head = nullptr;
    }

    TreeNode* get_head() {
        return m_head;
    }

    TreeNode* push(TreeNode *root, int val, bool left, bool right)
    {
        TreeNode* temp = new TreeNode(val);
        cout << "push val=" << val << " m_head=" << m_head << endl;
        cout << "push val=" << val << " root=" << root << endl;
        if (!m_head) {
            m_head = temp;
        }

        if (!root)
            root = m_head;

        if (root && left)
            root->left = temp;
        else if (root && right)
            root->right = temp;

        return temp;
    }
    void _inorderTraversal(TreeNode* root, vector<int> &ans) {
        if (!root) return;
        _inorderTraversal(root->left, ans);
        ans.push_back(root->val);
        _inorderTraversal(root->right, ans);
    }
    vector<int> inorderTraversal(TreeNode* root) {

        vector<int> ans;

        _inorderTraversal(root, ans);

        return ans;
    }
    TreeNode* _buildTree(vector<int>& inorder, int i_left, int i_right, vector<int>& postorder, int post_left, int post_right) {
        if ((i_left > i_right) || (post_left > post_right)) return nullptr;
        TreeNode *root = new TreeNode(postorder[post_right]);
        
        int inorder_root_idx = -1;
        for (int i = i_left; i <= i_right; ++i) {
            if (postorder[post_right] == inorder[i]) {
                inorder_root_idx = i;
                break;
            }
        }

        int left_sub_tree_len = inorder_root_idx - i_left;
        int right_sub_tree_len = i_right - inorder_root_idx;

        root->left = _buildTree(inorder,  i_left, inorder_root_idx-1, postorder, post_left, post_left+left_sub_tree_len-1);
        root->right = _buildTree(inorder, inorder_root_idx+1, i_right, postorder, post_left+left_sub_tree_len, post_right-1);

        return root;
    }

     TreeNode* buildTree(vector<int>& inorder, vector<int>& postorder) {
        return _buildTree(inorder, 0, inorder.size()-1, postorder, 0, postorder.size()-1);

    }

    void flatten(TreeNode* root) {
        if (!root) return ;
        if (root->left) flatten(root->left);
        if (root->right) flatten(root->right);

        TreeNode *old_right_parent = root->right;
        TreeNode *new_right_parent = root->left;
        root->right = new_right_parent;        
        root->left = nullptr;
        while (root->right) root = root->right;
        root->right = old_right_parent;
    }

};

int main()
{
    Solution solution;
    solution.reset();
    {
        TreeNode *root = solution.push(nullptr, 1, false, false);
        TreeNode *l = solution.push(root, 2, true, false);
        TreeNode *r = solution.push(root, 5, false, true);
        TreeNode *ll = solution.push(l, 3, true, false);
        TreeNode *lr = solution.push(l, 4, false, true);
        TreeNode *rr = solution.push(r, 6, false, true);
        solution.flatten(root);
        print_tree_preorder(root);
    }
    cout << endl;
    solution.reset();
    {
        solution.flatten(nullptr);
        print_tree_preorder(nullptr);
    }
    cout << endl;
    solution.reset();    
    {
        TreeNode *root = solution.push(nullptr, 0, false, false);
        solution.flatten(root);
        print_tree_preorder(root);
    }

    return 0;
}
