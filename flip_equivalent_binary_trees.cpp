#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>


using namespace std;


struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};
 
class Solution {
public:
	bool flipEquiv(TreeNode* root1, TreeNode* root2) {
		if (!root1 && !root2) return true;
		if (!root1 || !root2) return false;
		if ((root1) && (root2) && (root1->val != root2->val)) return false;

		bool check_1 = (flipEquiv(root1->left, root2->left) && flipEquiv(root1->right, root2->right));
		bool check_2 = (flipEquiv(root1->left, root2->right) && flipEquiv(root1->right, root2->left));

		return (check_1 || check_2);
	}
};

int main()
{
	Solution solution;
	{
		TreeNode ll(4);
		TreeNode lrl(7);
		TreeNode lrr(8);
		TreeNode lr(5, &lrl, &lrr);
		TreeNode l(2, &ll, &lr);

		TreeNode rl(6);
		TreeNode r(3, &rl, nullptr);

		TreeNode root1(1, &l, &r);

		TreeNode b_rl(4);
		TreeNode b_rrr(7);
		TreeNode b_rrl(8);
		TreeNode b_rr(5, &b_rrl, &b_rrr);
		TreeNode b_r(2, &b_rl, &b_rr);

		TreeNode b_lr(6);
		TreeNode b_l(3, nullptr, &b_lr);

		TreeNode root2(1, &b_l, &b_r);

		cout << solution.flipEquiv(&root1, &root2) << endl;
	}
	{
		cout << solution.flipEquiv(nullptr, nullptr) << endl;

	}
	{
		TreeNode root2(1);	
		cout << solution.flipEquiv(nullptr, &root2) << endl;
	}
	return 0;
}