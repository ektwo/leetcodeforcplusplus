#include <limits.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <queue>


using namespace std;


class Solution {
private:
    void set_color(vector<vector<int>>& image, int sr, int sc, int color) {
        image[sr][sc] = color;
    }
public:
    vector<vector<int>> floodFill(vector<vector<int>>& image, int sr, int sc, int newcolor) {

        int m = image.size();
        int n = image[0].size();
        int color = image[sr][sc];
        if(color==newcolor) return image;

        vector<vector<int>> ans = image;
        vector<vector<int>> dirs {{ 0, -1 }, { -1, 0 }, { 0, 1}, { 1, 0}};
        queue<pair<int, int>> q;
        q.push(make_pair(sr, sc));

        while (!q.empty()) {
            vector<int> nodes_val;
            for (int i = q.size(); i > 0; --i) {
                auto tmp = q.front(); q.pop();
                set_color(ans, tmp.first, tmp.second, newcolor);

                for (auto &dir : dirs) {
                    int new_x = tmp.first + dir[0];
                    int new_y = tmp.second + dir[1];
                    if ((new_x < 0) || (new_x >= m) || (new_y < 0) || (new_y >= n) || (ans[new_x][new_y] != color)) continue;
                    q.push({new_x, new_y});
                }
            }
        }

        return ans;
    }
};

int main()
{
    Solution solution;

    {
        vector<vector<int>> image = {{1, 1, 1}, {1,1,0}, {1,0,1}};
        vector<vector<int>> ans = solution.floodFill(image, 1, 1, 2);
        cout << " { ";
        for (int i = 0; i < ans.size(); ++i) {
            cout << " { ";
            for (int j = 0; j < ans[0].size(); ++j) {
                //if (i != ans.size() - 1)
                    cout << ans[i][j] << ",";
                //else
                //    cout << ans[i][j];
            }
            cout << " }, ";
        }
        cout << " }, " << endl;
    }
    {
        vector<vector<int>> image = {{0, 0, 0}, {0,0,0}};
        vector<vector<int>> ans = solution.floodFill(image, 0, 0, 0);
        cout << " { ";
        for (int i = 0; i < ans.size(); ++i) {
            cout << " { ";
            for (int j = 0; j < ans[0].size(); ++j) {
                //if (i != ans.size() - 1)
                    cout << ans[i][j] << ",";
                //else
                //    cout << ans[i][j];
            }
            cout << " }, ";
        }
        cout << " }, " << endl;
    }
    return 0;
}
