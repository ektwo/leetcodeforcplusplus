#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <limits>


using namespace std;


void print_1d_int(vector<int> nums)
{
    cout << "{";
    for (int j = 0; j < nums.size(); ++j) {
        cout << nums[j] << ",";
    }
    cout << "}," << endl;
}


class Solution {
public:
    int totalFruit(vector<int>& fruits) {
        int ans = 0;
        int GROUP_MAX = 2;
        int n = fruits.size();

        unordered_map<int, int> fruit_group;

        int win_start = 0;
        for (int win_end = 0; win_end < n; ++win_end) {
            fruit_group[fruits[win_end]]++;
            if (fruit_group.size() >  GROUP_MAX) {
                if (--fruit_group[fruits[win_start]] == 0) {
                    fruit_group.erase(fruits[win_start]);
                }
                ++win_start;
            }
            ans = max(ans, win_end - win_start + 1);
        }

        return ans;
    }
};

int main()
{
    Solution solution;
    {
        vector<int> fruits = {1,2,1};
        int ans = solution.totalFruit(fruits);
        cout << ans << endl;
    }
    cout << endl;
    {
        vector<int> fruits = {0,1,2,2};
        int ans = solution.totalFruit(fruits);
        cout << ans << endl;
    }
    cout << endl;
    {
        vector<int> fruits = {1,2,3,2,2};
        int ans = solution.totalFruit(fruits);
        cout << ans << endl;
    }
    return 0;
}