#ifndef __GENERATE_ARRAY_H__
#define __GENERATE_ARRAY_H__

#include <iostream>
#include <string.h> //memcpy
#include "system_time.h"

class CGenerateArray
{
  public:
    size_t m_arraySize;
    int *m_pArray;
    int m_itemsInArray;

  public:
    CGenerateArray(size_t arraySize) : m_arraySize(arraySize)
    {
        m_pArray = new int[m_arraySize];
        for (int i = 0; i < arraySize; ++i)
            m_pArray[i] = 0;
    }

    ~CGenerateArray()
    {
        if (m_pArray)
        {
            delete[] m_pArray;
        }
    }

    void GenerateRandomNumberArray()
    {
        srand(time(NULL));
        for (int i = 0; i < m_arraySize; ++i)
        {
            m_pArray[i] = (int)(rand() % m_arraySize);
        }

        m_itemsInArray = m_arraySize - 1;
    }

    int *GenerateMutualNumberArray()
    {
        bool bFound = false;
        int j, tmpValue;
        srand(time(NULL));
        m_pArray[0] = (int)(rand() % m_arraySize);
        for (int i = 1; i < m_arraySize; ++i)
        {
            while (1)
            {
                bFound = false;
                tmpValue = (int)(rand() % m_arraySize);
                for (j = 0; j < i; ++j)
                {
                    if (tmpValue == m_pArray[j])
                    {
                        bFound = true;
                        break;
                    }
                }
                //std::cout << "i=" << i << ", j=" << j << std::endl;
                if (!bFound)
                {
                    m_pArray[i] = tmpValue;
                    break;
                }
            }
        }

        m_itemsInArray = m_arraySize - 1;
    }
};

#endif