#include <iostream>
#include <vector>
#include <string>

using namespace std;

class Solution
{
public:
    void helper(int n, int open_num, int close_num, string str, vector<string> &ret)
    {
        if (str.length() == (n << 1)) {
            ret.push_back(str); return;
        }

        if (open_num < n) {
            helper(n, open_num + 1, close_num, str + "(", ret);
        }

        if (close_num < open_num) {
            helper(n, open_num, close_num + 1, str + ")", ret);
        }

    }
    vector<string> generateParenthesis(int n)
    {
        vector<string> ret = {};


        helper(n, 0, 0, "", ret);

        return ret;
    }
};

int main()
{
    Solution s;

    vector<string> ret = s.generateParenthesis(3);

    for (int i = 0; i < ret.size(); ++i)
    {
        cout << "i=" << i << " ret=" << ret[i] << endl;
    }

    return 0;
}