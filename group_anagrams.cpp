#include <limits.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <queue>


using namespace std;


class Solution {

public:
    int strStr(string haystack, string needle) {
        int ans = -1;

        if (needle.size() == 0) return 0;

        int left = 0;
        int needle_size = needle.size();

        for (int right = 0; right < haystack.size(); ++right) {
            
            if (haystack[right] == needle[0] && (right + needle_size <= haystack.size())) {
                left = right;
                bool found = true;
                for (int j = 0; j < needle_size; ++j) {
                    if (haystack[right + j] != needle[j]) { found = false; break;}
                }
                if (found) {
                    ans = left;
                    break;
                }
            }
        }

        return ans;
    }
    vector<vector<string>> groupAnagrams(vector<string>& strs) {
        vector<vector<string>> ans;
        vector<string> hash;
        vector<string>::iterator hash_it;

        for (int i = 0; i < strs.size(); ++i) {
            string tmp = strs[i];
            std::sort(tmp.begin(), tmp.end());
            hash_it = std::find(hash.begin(), hash.end(), tmp);
            if (hash_it != hash.end()) {
                cout << tmp << " same" << endl;
                ans[hash_it - hash.begin()].push_back(strs[i]);
            }
            else {
                cout << tmp << " new" << endl;
                hash.push_back(tmp);
                ans.push_back({strs[i]});
            }
            
        }

        return ans;
    }
};

int main()
{
    Solution solution;
    {
        vector<string> str = { "eat","tea","tan","ate","nat","bat" };
        vector<vector<string>> ans = solution.groupAnagrams(str);
        cout << "[";
        for (int i = 0; i < ans.size(); ++i) {
            cout << "[";
            for (int j = 0; j < ans[i].size(); ++j) {
                cout << "\"" << ans[i][j] << "\",";
            }
            cout << "]," << endl;
        }
        cout << "]" << endl;
    }

    // {
    //     string haystack = "aaaaa";
    //     string needle = "bba";
    //     cout << solution.strStr(haystack, needle) << endl;
    // }

    // {
    //     string haystack = "a";
    //     string needle = "a";
    //     cout << solution.strStr(haystack, needle) << endl;
    // }

    return 0;
}
