#include <limits.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <queue>


using namespace std;

int guess(int num) { return 0;}

class Solution {

public:

    int getMoneyAmount(int n) {
        if (0 == guess(n)) return n;
        int left = 1;
        int right = n;
        while (left < right) {
            int mid = left + (right - left) / 2;
            int guess_num = guess(mid);
            if (0 == guess_num) {
                return mid;
            }
            else if (1 == guess_num) {
                left = mid + 1;
            }
            else if (-1 == guess_num) {
                right = mid;
            }
        }
        return left;
    }
};

int main()
{
    Solution solution;

    {
        int ans = solution.guessNumber(10);
        cout << "ans=" << ans << endl;
        // for (int i = 0; i < ans.size(); ++i) {
        //     cout << ans[i] << "," << endl;
        // }
    }

    {

        int ans = solution.guessNumber(1);
        cout << "ans=" << ans << endl;
        // for (int i = 0; i < ans.size(); ++i) {
        //     cout << ans[i] << "," << endl;
        // }
    }
    {

        int ans = solution.guessNumber(2);
        cout << "ans=" << ans << endl;
        // for (int i = 0; i < ans.size(); ++i) {
        //     cout << ans[i] << "," << endl;
        // }
    }
    return 0;
}
