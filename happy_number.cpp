#include <limits.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <queue>
#include <math.h>


using namespace std;


class Solution {


public:

    bool isHappy(int n) {
        int sum = 0;
        unordered_set<int> us;
        while (true) {
            while (n > 0) {
                sum += pow(n % 10, 2);
                //cout << "sum=" << sum << endl;
                n /= 10;
            }
            if (sum == 1) return true;
            //    cout << "us.count(sum)=" << us.count(sum) << endl;
            if (us.count(sum) > 0) return false;
            n = sum;
            us.insert(sum); sum = 0;

        }
        return false;
    }
};

int main()
{
    Solution solution;

    {
        int n = 19;
        bool ans = solution.isHappy(n);
        cout << ans << endl;
    }

    {
        int n = 2;
        bool ans = solution.isHappy(n);
        cout << ans << endl;
    }

    return 0;
}
