#include "base_algorithm.h"
#include "generate_array.h"
#include "quick_sort.h"

using namespace std;

class CLinearSearch : public CBaseAlgorithm
{
  public:
    CLinearSearch()
    {
    }
    ~CLinearSearch()
    {
    }
    //O(1)
    void AddItemToArray(CGenerateArray *pGenerateArray, int newItem)
    {
        if (pGenerateArray)
        {
            pGenerateArray->m_pArray[pGenerateArray->m_itemsInArray++] = newItem;
        }
    }
    //O(n)
    void LinearSearchForValue(CGenerateArray *pGenerateArray, int value)
    {
        bool bValueInArray = false;
        CGenerateArray *pGArray = pGenerateArray;
        if (!pGArray)
            return;

        std::string str("Result: ");

        m_startTime = CSystemTime::getSystemTime();

        for (int i = 0; i < pGArray->m_arraySize; i++)
        {
            if (pGArray->m_pArray[i] == value)
            {
                bValueInArray = true;
                std::cout << i << ", ";
            }
        }
        std::cout << std::endl;

        m_endTime = CSystemTime::getSystemTime();
        std::cout << "Linear search Took " << (m_endTime - m_startTime) << std::endl;

        if (true == bValueInArray)
            std::cout << "were found:" << str << std::endl;
        else
            std::cout << "value was not found" << std::endl;
    }
};

void TestLinearSearchForValue(size_t items, int value)
{
    CGenerateArray *pGenerateArray = new CGenerateArray(items);
    CLinearSearch *pLinearSearch = new CLinearSearch();
    if ((pGenerateArray) && (pLinearSearch))
    {
        pGenerateArray->GenerateRandomNumberArray();
        pLinearSearch->LinearSearchForValue(pGenerateArray, value);
        delete pLinearSearch;
        delete pGenerateArray;
    }
}

void TestQuickSort(size_t items)
{
    CGenerateArray *pGenerateArray = new CGenerateArray(items);
    CQuickSort *pQuickSort = new CQuickSort();
    if ((pGenerateArray) && (pQuickSort))
    {
        pGenerateArray->GenerateMutualNumberArray();
        pQuickSort->QuickSort(pGenerateArray);
        delete pQuickSort;
        delete pGenerateArray;
    }
}

int main()
{
#if 1
//TestLinearSearchForValue(1000000, 88);
//TestLinearSearchForValue(2000000, 167);
//TestQuicksort(10);
//TestQuicksort(20);

#else

    int arr[] = {1, 3, 2, 7, 6, 9, 4, 8, 0, 5};

    quick_sort(arr, sizeof(arr) / sizeof(int));

#endif
    return 0;
}