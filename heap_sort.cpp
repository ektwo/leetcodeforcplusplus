#include <iostream>

#include "system_time.h"

#include "heap_sort.h"

int main()
{
    static const int k_items = 20;
    CHeapSort *pHeapSort = new CHeapSort(k_items);
    if (pHeapSort)
    {
        pHeapSort->HeapSort();
        delete pHeapSort;
    }

    return 0;
}
