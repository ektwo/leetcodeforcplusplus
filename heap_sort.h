#ifndef __HEAP_SORT_H__
#define __HEAP_SORT_H__

#include <vector>
#include "generate_array.h"
using namespace std;
class Solution
{
  public:
};

class CHeapSort
{
    CGenerateArray *m_pGenerateArray;
    int64_t m_startTime;
    int64_t m_endTime;

  public:
    CHeapSort(size_t arraySize) : m_pGenerateArray(NULL)
    {
        m_pGenerateArray = new CGenerateArray(arraySize);
    }
    ~CHeapSort()
    {
        if (m_pGenerateArray)
        {
            delete m_pGenerateArray;
        }
    }

    int GetLeftChildNodeIdx(int rootIdx)
    {
        return rootIdx * 2 + 1; // 1 : first root
    }
    int GetRightChildNodeIdx(int rootIdx)
    {
        return rootIdx * 2 + 2;
    }

    void MaxHeapify(vector<int> &array, int rootIdx, size_t heapSize)
    {
        int maxIdx = heapSize; //array.size() - 1;
        int largestIdx = rootIdx;
        int rootValue = array[rootIdx];
        int leftIdx = GetLeftChildNodeIdx(rootIdx);
        int rightIdx = GetRightChildNodeIdx(rootIdx);

        if ((leftIdx <= maxIdx) && (array[leftIdx] > rootValue))
            largestIdx = leftIdx;
        if ((rightIdx <= maxIdx) && (array[rightIdx] > array[largestIdx]))
            largestIdx = rightIdx;

        if (largestIdx != rootIdx)
        {
            swap(array[rootIdx], array[largestIdx]);
            MaxHeapify(array, largestIdx, heapSize);
        }
    }
    //O(n)
    void BuildHeap(vector<int> &array)
    {
        // heapsize <- length(A)
        // for i <- floor( length/2 ) downto 1
        //     Heapify(A, i)
        for (int idx = array.size() / 2; idx >= 0; --idx)
            MaxHeapify(array, idx, array.size() - 1);
    }
#if 0
    void HeapSortInternalDebug(vector<int> &array)
    {
        // for i <- length(A) downto 2 //O(1), 1 times
        // do exchange A[1] <-> A[i] //O(1), n-1
        // heapsize <- heapsize -1 //O(1), n-1
        // Heapify(A, 1) //O(logn), n-1
        size_t heapSize = array.size() - 1;
        for (int idx = array.size() - 1; idx >= 1; --idx)
        {
            swap(array[0], array[idx]);
            for (int i = 0; i < array.size(); ++i)
                cout << array[i] << " ";
            cout << "after swap" << endl;

            --heapSize;
            MaxHeapify(array, 0, heapSize);

            for (int i = 0; i < array.size(); ++i)
                cout << array[i] << " ";
            cout << endl;
            cout << "idx=" << idx << endl;
        }
    }
#endif
    void HeapSortInternal(vector<int> &array)
    {
        // for i <- length(A) downto 2 //O(1), 1 times
        // do exchange A[1] <-> A[i] //O(1), n-1
        // heapsize <- heapsize -1 //O(1), n-1
        // Heapify(A, 1) //O(logn), n-1
        size_t heapSize = array.size() - 1;
        for (int idx = array.size() - 1; idx >= 1; --idx)
        {
            swap(array[0], array[idx]);
            --heapSize;
            MaxHeapify(array, 0, heapSize);
        }
    }

    //always O(nlogn)
    void HeapSort()
    {
        if (!m_pGenerateArray)
            return;
        {
            vector<int> nums;
            m_pGenerateArray->GenerateRandomNumberArray();
            {
                int sampleArray[] = {4, 10, 3, 5, 1};
                int *A = sampleArray;
                //int *A = m_pGenerateArray->m_pArray;
                //int len = m_pGenerateArray->m_arraySize;
                int len = 5;

                for (int i = 0; i < len; ++i)
                {
                    nums.push_back(A[i]);
                    cout << A[i] << " ";
                }
                cout << endl;
            }

            m_startTime = CSystemTime::getSystemTime();
            {
                //build max heap(A)
                // init heap structure
                BuildHeap(nums); //O(n)
                // adjust the heap to max-heap
                HeapSortInternal(nums);
            }
            m_endTime = CSystemTime::getSystemTime();
            {
                std::cout << "Heap Sort Took " << (m_endTime - m_startTime) << std::endl;
                std::cout << "after" << std::endl;
                for (int i = 0; i < nums.size(); ++i)
                    cout << nums[i] << " ";
                cout << endl;
            }
        }
    }
};

#endif