#include <limits.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <queue>


using namespace std;


class Solution {

private:
    bool binary_search(vector<int>& nums, int target) {
        int left = 0;
    #if 1
        int right = nums.size();
        while (left < right) {
            int mid = left + (right - left) / 2;
            if (nums[mid] == target) { return true;}
            else if (nums[mid] < target) { left = mid + 1; }
            else right = mid;
        }
    #else
        int right = nums.size() - 1;
        while (left <= right) {
            int mid = left + (right - left) / 2;
            if (nums[mid] == target) { return true;}
            if (nums[mid] < target) { left = mid + 1; }
            else right = mid - 1;
        }
    #endif
        return false;
    }
    int binary_search_leftright(vector<int>& nums, int target, bool got_and_quit, int left, int right) {

        while (left < right) {
            int mid = left + (right - left) / 2;
            if (got_and_quit)
                if (nums[mid] == target) { return mid;}
            if (nums[mid] < target) { left = mid + 1; }
            else right = mid;
        }

        return right;
    }

public:

    vector<int> intersection(vector<int>& nums1, vector<int>& nums2) {
        unordered_set<int> ans;
        sort(nums2.begin(), nums2.end());

        for (auto & a : nums1) {
            if (binary_search(nums2, a)) {
                ans.insert(a);
            }
        }

        return vector<int>(ans.begin(), ans.end());
    }
    vector<int> intersect(vector<int>& nums1, vector<int>& nums2) {
        vector<int> ans;
        sort(nums1.begin(), nums1.end());
        sort(nums2.begin(), nums2.end());

        int left = 0;
        int right = nums2.size();
        for (auto & a : nums1) {

            left = binary_search_leftright(nums2, a, false, left, right);
            if (left >= right) break;
            if (nums2[left] == a) {
                ans.push_back(a);
                ++left;
            }
        }

        return ans;
    }
public:
    int findRadius(vector<int>& houses, vector<int>& heaters) {
        int ans = 0;
        sort(heaters.begin(), heaters.end());

        for (auto &house : houses) {
            int left = 0;
            int right = heaters.size();
            while (left < right) {
                int mid = left + (right - left) / 2;
                if (heaters[mid] < house) left = mid + 1;
                else right = mid;
            }
            right = binary_search_leftright(heaters, house, false, left, right);
            //cout << "left=" << left << " heaters[left]=" << heaters[left] << endl;
            //cout << "right=" << right << " heaters[right]=" << heaters[right] << endl;
            int dist1 = (right == 0) ? (INT_MAX) : (house - heaters[right - 1]);
            int dist2 = (right == heaters.size()) ? (INT_MAX) : (heaters[right] - house);
            ans = max(ans, min(dist1, dist2));
            // if (left >= right) break;
            // if (nums2[left] == a) {
            //     ans.push_back(a);
            //     ++left;
            // }
        }
        return ans;
    }
};

int main()
{
    Solution solution;

    {
        vector<int> houses = { 1, 2, 3};
        vector<int> heaters = { 2};
        int ans = solution.findRadius(houses, heaters);
        cout << "ans=" << ans << endl;
        // for (int i = 0; i < ans.size(); ++i) {
        //     cout << ans[i] << "," << endl;
        // }
    }

    {
        vector<int> houses = { 1, 2, 3, 4};
        vector<int> heaters = { 1, 4};
        int ans = solution.findRadius(houses, heaters);
        cout << "ans=" << ans << endl;
        // for (int i = 0; i < ans.size(); ++i) {
        //     cout << ans[i] << "," << endl;
        // }
    }
    {
        vector<int> houses = { 1, 5};
        vector<int> heaters = { 2};
        int ans = solution.findRadius(houses, heaters);
        cout << "ans=" << ans << endl;
        // for (int i = 0; i < ans.size(); ++i) {
        //     cout << ans[i] << "," << endl;
        // }
    }

    return 0;
}
