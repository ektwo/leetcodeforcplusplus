#include <limits.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <queue>


using namespace std;


class Solution {

public:
    int rob(vector<int>& nums) {
        int n = nums.size();
        if (n == 0) return 0;
        if (n == 1) return nums[0];
        vector < int > dp;
        dp.push_back(nums[0]);
        dp.push_back(max(nums[ 0 ], nums[ 1 ]));
        for (int i = 2; i < n; ++i) {
            dp.push_back(max(dp[i-2]+nums[i], dp[i-1]));
        }
        return dp.back();
    }
};

int main()
{
    Solution solution;

    {
        vector<int> nums = { 1, 2, 3, 1};
        int ans = solution.rob(nums);
        cout << "solution.rob=" << ans << endl;
    }
cout << "next=" << endl;
    {
        vector<int> nums = { 2, 7, 9, 3, 1};
        int ans = solution.rob(nums);
        cout << "solution.rob=" << ans << endl;
    }

    return 0;
}
