#include <limits>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <stack>
#include <queue>
#include <cmath>


using namespace std;


class MyQueue {
private:
    stack<int> in;
    stack<int> out;
public:
    MyQueue() {
        
    }
    
    void push(int x) {
        in.push(x);
    }
    
    int pop() {
        if (out.empty()) {
            while (!in.empty()) {
                int ret=in.top();in.pop();
                out.push(ret);
            }
        }
        else {

        }
        int ans=out.top();
        out.pop();
        return ans;
    }
    
    int peek() {
        if (out.empty()) {
            while (!in.empty()) {
                int ret=in.top();in.pop();
                out.push(ret);
            }
        }
        int ans=out.top();
        //out.pop();
        return ans;
    }
    
    bool empty() {
        return in.empty() && out.empty();
    }
};

/**
 * Your MyQueue object will be instantiated and called as such:
 * MyQueue* obj = new MyQueue();
 * obj->push(x);
 * int param_2 = obj->pop();
 * int param_3 = obj->peek();
 * bool param_4 = obj->empty();
 */

int main()
{


    return 0;
}
