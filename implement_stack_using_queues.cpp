#include <limits>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <stack>
#include <queue>
#include <cmath>


using namespace std;


class MyStack {
    queue<int> q1;
    queue<int> q2;
public:
    MyStack() {
        
    }
    
    void push(int x) {
        int size = q1.size();

            while (size > 0) {
                q2.push(q1.front()); q1.pop();
                --size;
            }
        
        q1.push(x);
    }
    
    int pop() {
        int ret = q1.front();
        q1.pop();

        int size = q2.size();
        while (size > 0) {
            q1.push(q2.front()); q2.pop();
            --size;
        }

         size = q1.size();
        while (size > 1) {
            q2.push(q1.front()); q1.pop();
            --size;
        }
        return ret;
    }
    
    int top() {
        return q1.front();
    }
    
    bool empty() {
        return q1.empty() && q2.empty();
    }
};

/**
 * Your MyQueue object will be instantiated and called as such:
 * MyQueue* obj = new MyQueue();
 * obj->push(x);
 * int param_2 = obj->pop();
 * int param_3 = obj->peek();
 * bool param_4 = obj->empty();
 */

int main()
{


    return 0;
}
