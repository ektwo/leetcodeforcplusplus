#include <limits.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <queue>


using namespace std;


class Solution {

public:
    int strStr(string haystack, string needle) {
        int ans = -1;

        if (needle.size() == 0) return 0;

        int left = 0;
        int needle_size = needle.size();

        for (int right = 0; right < haystack.size(); ++right) {
            
            if (haystack[right] == needle[0] && (right + needle_size <= haystack.size())) {
                left = right;
                bool found = true;
                for (int j = 0; j < needle_size; ++j) {
                    if (haystack[right + j] != needle[j]) { found = false; break;}
                }
                if (found) {
                    ans = left;
                    break;
                }
            }
        }

        return ans;
    }

};

int main()
{
    Solution solution;
    {
        string haystack = "hello";
        string needle = "ll";
        cout << solution.strStr(haystack, needle) << endl;
    }

    {
        string haystack = "aaaaa";
        string needle = "bba";
        cout << solution.strStr(haystack, needle) << endl;
    }

    {
        string haystack = "a";
        string needle = "a";
        cout << solution.strStr(haystack, needle) << endl;
    }

    return 0;
}
