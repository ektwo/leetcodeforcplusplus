#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>


using namespace std;


class Solution {
public:
    vector<vector<int>> insert(vector<vector<int>>& intervals, vector<int>& newInterval) {
    #if 1
        vector<vector<int>> ans;

        int cur = 0;

        for (int i = 0; i < intervals.size(); ++i) {
            if (intervals[i][1] < newInterval[0]) {
                ans.push_back(intervals[i]);
                ++cur;
            }
            else if (intervals[i][0] > newInterval[1]) {
                ans.push_back(intervals[i]);
            }
            else {
                newInterval[0] = min(intervals[i][0], newInterval[0]);
                newInterval[1] = max(intervals[i][1], newInterval[1]);
            }
        }

        ans.insert(ans.begin() + cur, newInterval);

        return ans;
    #else
        vector<vector<int>> ret;
        if (intervals.size() == 0) {
            ret.push_back(newInterval); return ret;
        }
        else if (newInterval.size() == 0) return intervals;

        int i = 0;
        for (; i < intervals.size(); ) {
            if (intervals[i][1] >= newInterval[0])
                break;
            ret.push_back(intervals[i++]);
        }

        for (; i < intervals.size(); ) {
            if (intervals[i][0] > newInterval[1])
                break;

            newInterval[0] = min(newInterval[0], intervals[i][0]);
            newInterval[1] = max(newInterval[1], intervals[i][1]);
            ++i;
        }
        ret.push_back(newInterval);

        for (; i < intervals.size(); ) {
            ret.push_back(intervals[i++]);
        }

        return ret;
    #endif
    }
};

int main()
{
    Solution s;
    {
        vector<int> v11{ 1, 3 };
        vector<int> v12{ 6, 9 };
        vector<vector<int>> v1{ v11, v12 };
        vector<int> v2{ 2, 5 };
        vector<vector<int>> ret = s.insert(v1, v2);
        cout << "[";
        if (ret.size() > 0) {
            //cout << "size=" << ret.size() << endl;
            for (int i = 0; i < ret.size(); ++i) {
                //cout << "size2=" << ret[i].size() << endl;
                cout << "[";
                if (ret[i].size() > 0) {
                    for (int j = 0; j < ret[i].size(); ++j) {
                        if (j < (ret[i].size() - 1))
                            //cout << "1" << ",";
                            cout << ret[i][j] << ",";
                        else
                            //cout << "1";
                            cout << ret[i][j];
                    }
                }
                if (i < (ret.size() - 1))
                    cout << "],";
                else
                    cout << "]";

            }
        }
        cout << "]" << endl;
    }
    {
        vector<int> v11{ 1, 2 };
        vector<int> v12{ 3, 5 };
        vector<int> v13{ 6, 7 };
        vector<int> v14{ 8, 10 };
        vector<int> v15{ 12, 16 };
        vector<vector<int>> v1{ v11, v12, v13, v14, v15 };
        vector<int> v2{ 4, 8 };
        vector<vector<int>> ret = s.insert(v1, v2);
        cout << "[";
        if (ret.size() > 0) {
            //cout << "size=" << ret.size() << endl;
            for (int i = 0; i < ret.size(); ++i) {
                //cout << "size2=" << ret[i].size() << endl;
                cout << "[";
                if (ret[i].size() > 0) {
                    for (int j = 0; j < ret[i].size(); ++j) {
                        if (j < (ret[i].size() - 1))
                            //cout << "1" << ",";
                            cout << ret[i][j] << ",";
                        else
                            //cout << "1";
                            cout << ret[i][j];
                    }
                }
                if (i < (ret.size() - 1))
                    cout << "],";
                else
                    cout << "]";

            }
        }
        cout << "]" << endl;
    }
    {
        vector<vector<int>> v1{  };
        vector<int> v2{ 5, 7 };
        vector<vector<int>> ret = s.insert(v1, v2);
        cout << "[";
        if (ret.size() > 0) {
            //cout << "size=" << ret.size() << endl;
            for (int i = 0; i < ret.size(); ++i) {
                //cout << "size2=" << ret[i].size() << endl;
                cout << "[";
                if (ret[i].size() > 0) {
                    for (int j = 0; j < ret[i].size(); ++j) {
                        if (j < (ret[i].size() - 1))
                            //cout << "1" << ",";
                            cout << ret[i][j] << ",";
                        else
                            //cout << "1";
                            cout << ret[i][j];
                    }
                }
                if (i < (ret.size() - 1))
                    cout << "],";
                else
                    cout << "]";

            }
        }
        cout << "]" << endl;
    }
    {
        vector<int> v11{ 1, 5 };
        vector<vector<int>> v1{ v11 };
        vector<int> v2{ 2, 3 };
        vector<vector<int>> ret = s.insert(v1, v2);
        cout << "[";
        if (ret.size() > 0) {
            //cout << "size=" << ret.size() << endl;
            for (int i = 0; i < ret.size(); ++i) {
                //cout << "size2=" << ret[i].size() << endl;
                cout << "[";
                if (ret[i].size() > 0) {
                    for (int j = 0; j < ret[i].size(); ++j) {
                        if (j < (ret[i].size() - 1))
                            //cout << "1" << ",";
                            cout << ret[i][j] << ",";
                        else
                            //cout << "1";
                            cout << ret[i][j];
                    }
                }
                if (i < (ret.size() - 1))
                    cout << "],";
                else
                    cout << "]";

            }
        }
        cout << "]" << endl;
    }
    {
        vector<int> v11{ 1, 5 };
        vector<vector<int>> v1{ v11 };
        vector<int> v2{ 2, 7 };
        vector<vector<int>> ret = s.insert(v1, v2);
        cout << "[";
        if (ret.size() > 0) {
            //cout << "size=" << ret.size() << endl;
            for (int i = 0; i < ret.size(); ++i) {
                //cout << "size2=" << ret[i].size() << endl;
                cout << "[";
                if (ret[i].size() > 0) {
                    for (int j = 0; j < ret[i].size(); ++j) {
                        if (j < (ret[i].size() - 1))
                            //cout << "1" << ",";
                            cout << ret[i][j] << ",";
                        else
                            //cout << "1";
                            cout << ret[i][j];
                    }
                }
                if (i < (ret.size() - 1))
                    cout << "],";
                else
                    cout << "]";

            }
        }
        cout << "]" << endl;
    }
    return 0;
}
