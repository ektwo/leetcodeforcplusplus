#include <limits>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <queue>


using namespace std;


class Solution {

public:
    string intToRoman(int num) {
        string ans = "";
        // unordered_map<string, int> dict = { 
        //     { "I", 1 }, 
        //     { "IV", 4 }, 
        //     { "V", 5 }, 
        //     { "IX", 9 }, 
        //     { "X", 10 }, 
        //     { "XL", 40 }, 
        //     { "L", 50 }, 
        //     { "XC", 90 }, 
        //     { "C", 100 }, 
        //     { "CD", 400 }, 
        //     { "D", 500 }, 
        //     { "CM", 900 }, 
        //     { "M", 1000 }, 
        //     };

        vector<pair<int, string>> v;
        v.push_back(make_pair(1000, "M"));
        v.push_back(make_pair(900, "CM"));
        v.push_back(make_pair(500, "D"));
        v.push_back(make_pair(400, "CD"));
        v.push_back(make_pair(100, "C"));
        v.push_back(make_pair(90, "XC"));
        v.push_back(make_pair(50, "L"));
        v.push_back(make_pair(40, "XL"));
        v.push_back(make_pair(10, "X"));
        v.push_back(make_pair(9, "IX"));
        v.push_back(make_pair(5, "V"));
        v.push_back(make_pair(4, "IV"));
        v.push_back(make_pair(1, "I"));

        for (int i = 0; i < v.size(); ++i) {
            int count = num / v[i].first;
            num %= v[i].first;
            while (count > 0) {
                ans.append(v[i].second);
                --count;
            }
        }

        return ans;
    }
};

int main()
{
    Solution solution;
    {
        string str = solution.intToRoman(3);
        cout << str << endl;
    }

    {
        string str = solution.intToRoman(58);
        cout << str << endl;
    }

    {
        string str = solution.intToRoman(1994);
        cout << str << endl;
    }

    return 0;
}
