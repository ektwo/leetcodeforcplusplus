#include <limits.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <queue>


using namespace std;


class Solution {

private:
    bool binary_search(vector<int>& nums, int target) {
        int left = 0;
    #if 1
        int right = nums.size();
        while (left < right) {
            int mid = left + (right - left) / 2;
            if (nums[mid] == target) { return true;}
            else if (nums[mid] < target) { left = mid + 1; }
            else right = mid;
        }
    #else
        int right = nums.size() - 1;
        while (left <= right) {
            int mid = left + (right - left) / 2;
            if (nums[mid] == target) { return true;}
            if (nums[mid] < target) { left = mid + 1; }
            else right = mid - 1;
        }
    #endif
        return false;
    }

public:

    vector<int> intersection(vector<int>& nums1, vector<int>& nums2) {
        unordered_set<int> ans;
        sort(nums2.begin(), nums2.end());

        for (auto & a : nums1) {
            if (binary_search(nums2, a)) {
                ans.insert(a);
            }
        }

        return vector<int>(ans.begin(), ans.end());
    }
};

int main()
{
    Solution solution;

    {
        vector<int> nums1 = { 1, 2, 2, 1};
        vector<int> nums2 = { 2, 2};
        vector<int> ans = solution.intersection(nums1, nums2);
        for (int i = 0; i < ans.size(); ++i) {
            cout << ans[i] << "," << endl;
        }
    }

    {
        vector<int> nums1 = { 4, 9, 5};
        vector<int> nums2 = { 9, 4, 9, 8, 4};
        vector<int> ans = solution.intersection(nums1, nums2);
        for (int i = 0; i < ans.size(); ++i) {
            cout << ans[i] << "," << endl;
        }
    }

    return 0;
}
