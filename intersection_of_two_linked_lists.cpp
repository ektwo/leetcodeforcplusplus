#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>


using namespace std;


//Definition for singly-linked list.
struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
    ListNode(int x, ListNode *n) : val(x), next(n) {}
};


class Solution {
private:
    int get_length(ListNode *next) {
        if (!next) return 0;
        int len = 1;
        while (next) {
            next = next->next;
            ++len;
        }
        return len;
    }
public:
    ListNode *getIntersectionNode(ListNode *headA, ListNode * headB) {

        if (!headA||!headB) return nullptr;

        int len_a=get_length(headA);
        int len_b=get_length(headB);
        int offset=abs(len_a-len_b);
        int tmp_len_a=len_a;
        int tmp_len_b=len_b;
        if (len_a>len_b) {
            while (offset-->0) headA=headA->next; 
        }
        else {
            while (offset-->0) headB=headB->next;
        }
        while (len_a-->0) {
            if (headA==headB) return headA;
            headA=headA->next;
            headB=headB->next;
        }

        return nullptr;
    }

};

int main()
{
    Solution s;
    {
        vector<ListNode*> va;
        vector<ListNode*> vb;
        ListNode** lna = new ListNode*[5];
        ListNode** lnb = new ListNode*[6];

        lna[4] = new ListNode(5, NULL);
        lna[3] = new ListNode(4, lna[4]);
        lna[2] = new ListNode(8, lna[3]);
        lna[1] = new ListNode(1, lna[2]);
        lna[0] = new ListNode(4, lna[1]);

        //lnb[5] = new ListNode(5, NULL);
        //lnb[4] = new ListNode(4, lna[5]);
        //lnb[3] = new ListNode(8, lna[2]);
        lnb[2] = new ListNode(1, lna[2]);
        lnb[1] = new ListNode(6, lnb[2]);
        lnb[0] = new ListNode(5, lnb[1]);

        va.push_back(lna[0]);
        va.push_back(lna[1]);
        va.push_back(lna[2]);
        va.push_back(lna[3]);
        va.push_back(lna[4]);

        vb.push_back(lnb[0]);
        vb.push_back(lnb[1]);
        vb.push_back(lnb[2]);
        vb.push_back(lnb[3]);
        //vb.push_back(lnb[2]);
        //vb.push_back(lnb[3]);
        //vb.push_back(lnb[4]);
        //vb.push_back(lnb[5]);

        ListNode *ret = s.getIntersectionNode(va[0], vb[0]);

        while (ret) {
            cout << "val=" << ret->val << endl;
            ret = ret->next;
        }
    }
    {
        ListNode** lna = new ListNode * [3];
        ListNode** lnb = new ListNode * [2];

        lna[2] = new ListNode(4, NULL);
        lna[1] = new ListNode(6, lna[2]);
        lna[0] = new ListNode(2, lna[1]);

        lnb[1] = new ListNode(5, NULL);
        lnb[0] = new ListNode(1, lnb[1]);

        ListNode* ret = s.getIntersectionNode(lna[0], lnb[0]);

        while (ret) {
            cout << "val=" << ret->val << endl;
            ret = ret->next;
        }
    }
    return 0;
}