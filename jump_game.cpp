#include <iostream>
#include <vector>
#include <string>

using namespace std;

class Solution
{
public:
    bool canJump(vector<int>& nums)
    {
        int size = nums.size();
        if (size == 0) return false;

        int max = 0;
        for (int i = 0; i < size; ++i) {
            if (max < i) return false;
            if (max >= size) return true;
            max = std::max(max, nums[i] + i);
        }
        return true;
    }
};

int main()
{
    Solution s;
    {
        vector<int> v = { 1,2,3 };
        cout << s.canJump(v) << endl;
    }
    {
        vector<int> v = { 0,2,3 };
        cout << s.canJump(v) << endl;
    }
    {
        vector<int> v = { 2,3,1,1,4 };
        cout << s.canJump(v) << endl;
    }
    {
        vector<int> v = { 3,2,1,0,4 };
        cout << s.canJump(v) << endl;
    }
    {
        vector<int> v = { 0 };
        cout << s.canJump(v) << endl;
    }
    {
        vector<int> v = { 1 };
        cout << s.canJump(v) << endl;
    }
    {
        vector<int> v = { 2,0 };
        cout << s.canJump(v) << endl;
    }
    {
        vector<int> v = { 0,2,3 };
        cout << s.canJump(v) << endl;
    }
    return 0;
}