#include <iostream>
#include <vector>
#include <string>

using namespace std;

class Solution
{
public:
    int jump(vector<int>& nums) {
    #if 1
        int res = 0, n = nums.size(), last = 0, cur = 0;
        for (int i = 0; i < n - 1; ++i) {
            cur = max(cur, i + nums[i]);
            if (i == last) {
                last = cur;
                ++res;
                if (cur >= n - 1) break;
            }
        }
        return res;
    #else
        if (nums.size() <= 1) return 0;
        if (nums[0]==nums.size()-1) return 1;
        int ans = 1;
        int win_start = 0;
        int win_end = 0;
        int target = nums.size()-1;
        for (; win_end < nums.size(); ++win_end) {
            int max_v = 0;
            int max_idx = 0;
            for (int i = win_start; i < nums[win_end]; ++i) {
                if (nums[i] > max_v) {
                    max_v = nums[i];
                    max_idx = i;
                }
            }
            win_end = win_start + max_idx;
            win_start += win_end;
            ++ans;
            cout << "max_v=" << max_v << endl;
            cout << "max_idx=" << max_idx << endl;
            cout << "win_start=" << win_start << endl;
            cout << "win_end=" << win_end << endl;
            if ((max_v + win_end) == nums.size()-1) return ans;
        }

        return nums.size()-1;
    #endif
    }
};

int main()
{
    Solution s;
    {
        vector<int> nums = { 2, 3, 1, 1, 4 };
        cout << s.jump(nums) << endl;
    }
    {
        vector<int> nums = { 2, 3, 0, 1, 4 };
        cout << s.jump(nums) << endl;
    }
    {
        vector<int> nums = { 1,2 };
        cout << s.jump(nums) << endl;
    }

    return 0;
}