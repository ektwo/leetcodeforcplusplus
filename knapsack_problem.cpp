/*
物品號 重量(c) 價值(w)
i=1 4 5
i=2 7 9
i=3 5 6

ascending order
dp[j]=max(dp[j],dp[j-c[i]]+w[i]
f[0] [1] [2] [3] [4] [5] [6] [7] [8] [9] [10]
  0   0   0   0   0   0   0   0   0   0    0
i=1
dp[j-c[i]], j from 4->10
  0   
      0   
          0   
              0   
                  5   
                      5   
                          5   
                              5  
                                 10   ??? 這裡顯然不對,這時i=1，只能放一件物品,然而沒有一個物品的價值為10的
                                     10    ??? 這裡顯然不對,這時i=1，只能放一件物品,然而沒有一個物品的價值為10的
                                          10 ??? 這裡顯然不對,這時i=1，只能放一件物品,然而沒有一個物品的價值為10的


reverse order
f[0] [1] [2] [3] [4] [5] [6] [7] [8] [9] [10]
  0   0   0   0   0   0   0   0   0   0    0
i=1
dp[j-c[i]], j from 10->4
                                           5 //max(dp[10],dp[6]+w[1])
                                      5 //max(dp[9],dp[5]+w[1])
                                  5 //max(dp[8],dp[4]+w[1])
                              5 //max(dp[7],dp[3]+w[1])
                          5 //max(dp[6],dp[2]+w[1])
                      5 //max(dp[5],dp[1]+w[1])
                  5 //max(dp[4],dp[0]+w[1])
  0   0   0   0   5   5   5   5   5   5    5
i=2
                                           9 //max(dp[10],dp[3]+w[2])
                                      9 //max(dp[9],dp[2]+w[2])
                                  9 //max(dp[8],dp[1]+w[2])
                              9 //max(dp[7],dp[0]+w[2])
  0   0   0   0   5   5   5   9   9   9    9
i=3
                                          11 //max(dp[10],dp[5]+w[3])
                                     11 //max(dp[9],dp[4]+w[3])
                                  9 //max(dp[8],dp[3]+w[3])
                              9 //max(dp[7],dp[2]+w[3])
                          6 //max(dp[6],dp[1]+w[3])
                      6 //max(dp[5],dp[0]+w[3])
  0   0   0   0   5   6   6   9   9  11   11
*/

#include <iostream>
using namespace std;

int  MIN_INT = (~(unsigned(-1)>>1));
int F[100001];
int A[101];
int C[101];


void ZeroOnePack(int cost, int weight, int V)
{
    for (int v=V; v>=cost; --v)
    F[v] = max(F[v], F[v-cost]+weight);
}

void CompletePack(int cost, int weight, int V)
{
    for (int v=cost; v<=V; ++v)
    F[v] = max(F[v], F[v-cost]+weight);
}

void MultiPack(int cost, int weight, int V, int amount)
{
    if (cost*amount>=V) {
        CompletePack(cost, weight, V);
        return;
    }
    int k = 1;
    while (k<amount) {
        ZeroOnePack(cost*k, weight*k, V);
        amount -= k;
        k *= 2;
    }
    ZeroOnePack(cost*amount, weight*amount, V);
}

int main(int argc, char **argv)
{
    int n, m, count;
    while ((cin>>n>>m) && (m+n)) {
    for (int i=1; i<=n; ++i)
    cin>>A[i];
    for (int i=1; i<=n; ++i)
    cin>>C[i];
    count = 0;
    for (int V=1; V<=m; ++V) {
    F[0] = 0;
    for (int i=1; i<=m; ++i)
    F[i] = MIN_INT;
    for (int i=1; i<=n; ++i)
    MultiPack(A[i], A[i], V, C[i]);

    if (F[V]==V)
    ++count;
    }
    cout<<count<<endl;
    }
    system("pause");
    return 0;
}