#include <limits.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <queue>


using namespace std;


bool isBadVersion(int version)
{
    return false;
}

class Solution {

public:

    int minEatingSpeed(vector<int>& piles, int h) {
        long left = 1;
        long right = 1e9;
        while (left < right) {
            long cnt = 0;
            long mid = left + (right - left) / 2;
            for (auto &pile : piles) cnt += (pile + mid - 1) / mid;
            if (cnt > h)
                left = mid + 1;
            else
                right = mid;
        }
        return right;
    }
};

int main()
{
    Solution solution;

    {
        vector<int> piles = { 3, 6, 7, 11};
        int h = 8;
        int ans = solution.minEatingSpeed(piles, h);
        cout << "solution.minEatingSpeed=" << ans << endl;
    }
cout << "next=" << 10e1 << endl;
    {
        vector<int> piles = { 30,11,23,4,20};
        int h = 5;
        int ans = solution.minEatingSpeed(piles, h);
        cout << "solution.minEatingSpeed=" << ans << endl;
    }
cout << "next=" << 10e2 << endl;
    {
        vector<int> piles = { 30,11,23,4,20};
        int h = 6;
        int ans = solution.minEatingSpeed(piles, h);
        cout << "solution.minEatingSpeed=" << ans << endl;
    }
    return 0;
}
