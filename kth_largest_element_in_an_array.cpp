#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>


using namespace std;



class Solution {

private:

    int partition(vector<int>& nums, int leftmostIndex, int rightmostIndex) {

        #if 1
            const int pivot_pos = rightmostIndex;
            const int pivot = nums[pivot_pos];
            int i = leftmostIndex - 1;
            int j = leftmostIndex;
            for (; j < rightmostIndex; ++j) {
                if (nums[j] > pivot) continue;
                ++i;
                swap(nums[i], nums[j]);
            }
            swap(nums[pivot_pos], nums[i+1]);
            return i+1;
        #else
        const int pivot_pos = leftmostIndex;
        const int pivot = nums[pivot_pos];

        int last_pos_that_val_less_than_pivot = leftmostIndex + 1;
        int unpartitioned_idx = leftmostIndex + 1;

        for (; unpartitioned_idx <= rightmostIndex; ++unpartitioned_idx) {
            if (pivot > nums[unpartitioned_idx]) {
                if (last_pos_that_val_less_than_pivot != unpartitioned_idx) {
                    std::swap(nums[last_pos_that_val_less_than_pivot], nums[unpartitioned_idx]);
                }
                ++last_pos_that_val_less_than_pivot;
            }
        }

        int new_pivot_pos = last_pos_that_val_less_than_pivot - 1;
        std::swap(nums[pivot_pos], nums[new_pivot_pos]);

        return new_pivot_pos;
        #endif
    }

    void quick_sort(vector<int>& nums, int leftmostIndex, int rightmostIndex)
    {
        if (leftmostIndex < rightmostIndex) {
            int pivot_pos = partition(nums, leftmostIndex, rightmostIndex);   //find the pivot element such that
            quick_sort(nums, leftmostIndex, pivot_pos - 1);                  //sorts the left side of pivot.
            quick_sort(nums, pivot_pos + 1, rightmostIndex);                 //sorts the right side of pivot.
        }
    }

public:

    int findKthLargest(vector<int>& nums, int k) {
        int ans = 0;

        quick_sort(nums, 0, nums.size() - 1);


        ans = nums[nums.size() - k];

        return ans;
    }
};

int main()
{
    Solution s;
    {
        vector<int> nums = { 3,2,1,5,6,4 };
        cout << s.findKthLargest(nums, 2) << endl;
    }
    {
        vector<int> nums = { 3,2,3,1,2,4,5,5,6};
        cout << s.findKthLargest(nums, 4) << endl;
    }

    return 0;
}
