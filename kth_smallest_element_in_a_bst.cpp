#include <iostream>
#include <vector>
#include <string>
#include <queue>

using namespace std;


struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};


void print_1d_int(vector<int> nums)
{

        cout << "{";
        for (int j = 0; j < nums.size(); ++j) {
            cout << nums[j] << ",";
        }
        cout << "}," << endl;

}

void print_2d_int(vector<vector<int>> nums)
{
    cout << "{";
    for (int i = 0; i < nums.size(); ++i) {
        cout << "{";
        for (int j = 0; j < nums[0].size(); ++j) {
            cout << nums[i][j] << ",";
        }
        cout << "}," << endl;
    }
    cout << "}," << endl;
}

void print_tree_preorder(TreeNode *root)
{
    if (root) {
        cout << root->val << ",";
        if (root->left) print_tree_preorder(root->left);
        if (root->right) print_tree_preorder(root->right);
    }
    else
        cout << "null,";
}

void print_tree_inorder(TreeNode *root)
{
    if (root) {
        if (root->left) print_tree_inorder(root->left);
        cout << root->val << ",";
        if (root->right) print_tree_inorder(root->right);
    }
    else
        cout << "null,";
}


void print_tree_levelorder(TreeNode *root)
{
    if (!root) return ;

    queue<TreeNode*> q;
    q.push(root);
    while (!q.empty()) {
        vector<int> tmp_solu;
        for (int i = q.size(); i > 0; --i) {
            TreeNode *tmp = q.front(); q.pop();
            //tmp_solu.push_back(tmp->val);
            cout << tmp->val << ",";

            if (tmp->left) q.push(tmp->left);
            if (tmp->right) q.push(tmp->right);
        }
        //ans.push_back(tmp_solu);
    }


}

class Solution
{
private:
    int node_nums;
    int tree_height;
    TreeNode *m_head;
public:
    void reset() {
        node_nums = 0;
        tree_height = 0;
        m_head = nullptr;
    }

    TreeNode* get_head() {
        return m_head;
    }

    TreeNode* push(TreeNode *root, int val, bool left, bool right)
    {
        TreeNode* temp = new TreeNode(val);
        //cout << "push val=" << val << " m_head=" << m_head << endl;
        //cout << "push val=" << val << " root=" << root << endl;
        if (!m_head) {
            m_head = temp;
        }

        if (!root)
            root = m_head;

        if (root && left)
            root->left = temp;
        else if (root && right)
            root->right = temp;

        return temp;
    }
    void _inorderTraversal(TreeNode* root, vector<int> &ans) {
        if (!root) return;
        _inorderTraversal(root->left, ans);
        ans.push_back(root->val);
        _inorderTraversal(root->right, ans);
    }
    vector<int> inorderTraversal(TreeNode* root) {

        vector<int> ans;

        _inorderTraversal(root, ans);

        return ans;
    }
    vector<vector<int>> levelOrder(TreeNode* root) {
        if (!root) return {};
        vector<vector<int>> ans;

        queue<TreeNode*> q;
        q.push(root);
        while (!q.empty()) {
            vector<int> tmp_solu;
            for (int i = q.size(); i > 0; --i) {
                TreeNode *tmp = q.front(); q.pop();
                tmp_solu.push_back(tmp->val);

                if (tmp->left) q.push(tmp->left);
                if (tmp->right) q.push(tmp->right);
            }
            ans.push_back(tmp_solu);
        }


        return ans;
    }
    vector<int> rightSideView(TreeNode* root) {
        if (!root) return {};
        vector<int> ans;

        queue<TreeNode*> q;
        q.push(root);
        while (!q.empty()) {
            //vector<int> tmp_solu;
            for (int i = q.size(); i > 0; --i) {
                TreeNode *tmp = q.front(); q.pop();
                if (i == 1)
                ans.push_back(tmp->val);

                if (tmp->left) q.push(tmp->left);
                if (tmp->right) q.push(tmp->right);
            }
            //ans.push_back(tmp_solu);
        }

        return ans;
    }
    TreeNode* invertTree(TreeNode* root) {

        if (!root) return nullptr;

        queue<TreeNode*> q;
        q.push(root);

        while (!q.empty()) {
            TreeNode *node = q.front(); q.pop();
            TreeNode *tmp = node->right;
            node->right = node->left;
            node->left = tmp;
            if (node->left) q.push(node->left);
            if (node->right) q.push(node->right);
        }

        return root;
    }

    int _kthSmallest_cnt(TreeNode *root)
    {
        if (!root) return 0;
        return _kthSmallest_cnt(root->left) + _kthSmallest_cnt(root->right) + 1;
    }

    int _kthSmallest(TreeNode *root, int k, int &cnt)
    {
        int ret = -1;
        if (root) {
            ret = _kthSmallest(root->left, k, cnt);
            if (cnt == k) return ret;
            if (++cnt == k) return root->val;
            return _kthSmallest(root->right, k, cnt);
        }

        return ret;
    }

    int kthSmallest(TreeNode* root, int k) {
    #if 1
        int count = _kthSmallest_cnt(root->left);
        if (k <= count) {
            return kthSmallest(root->left, k);
        }
        else if (k > count +1)
        {
            return kthSmallest(root->right, k - count - 1);
        }
        else
            return root->val;
    #else
        int cnt = 0;
        return _kthSmallest(root, k, cnt);
    #endif
    }

};

int main()
{
    Solution solution;
    solution.reset();
    {
        TreeNode *root = solution.push(nullptr, 3, false, false);
        TreeNode *l = solution.push(root, 1, true, false);
        TreeNode *r = solution.push(root, 4, false, true);
        //TreeNode *ll = solution.push(l, 1, true, false);
        TreeNode *lr = solution.push(l, 2, false, true);
        //TreeNode *rl = solution.push(r, 6, true, false);
        //TreeNode *rr = solution.push(r, 9, false, true);
        int k = 1;
        int ans = solution.kthSmallest(solution.get_head(), k);
        cout << ans << endl;
    }
    cout << endl;
    solution.reset();    
    {
        TreeNode *root = solution.push(nullptr, 5, false, false);
        TreeNode *l = solution.push(root, 3, true, false);
        TreeNode *r = solution.push(root, 6, false, true);
        TreeNode *ll = solution.push(l, 2, true, false);
        TreeNode *lr = solution.push(l, 4, false, true);
        TreeNode *lll = solution.push(ll, 1, true, false);
        //TreeNode *rl = solution.push(r, 6, true, false);
        //TreeNode *rr = solution.push(r, 9, false, true);
        int k = 3;
        int ans = solution.kthSmallest(solution.get_head(), k);
        cout << ans << endl;
    }
    cout << endl;


    return 0;
}
