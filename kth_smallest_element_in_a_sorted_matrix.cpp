#include <limits.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <queue>


using namespace std;


class Solution {

public:

    int kthSmallest(vector<vector<int>>& matrix, int k) {
#if 0
        vector<int> out;
        for (int i = 0; i < matrix.size(); ++i) {
            for (int j = 0; j < matrix[i].size(); ++j) {
                out.push_back(matrix[i][j]);
            }
        }
        return out[k-1];
#else
        int left = matrix[0][0];
        int right = matrix.back().back();
        
        while (left < right) {
            int mid = left + (right - left) / 2;
            int cnt = 0;
            cout << "left=" << left << " right=" << right << " mid=" << mid << endl;
            for (int i = 0; i < matrix.size(); ++i) {
                cout << "cnt=" << cnt << endl;
                cnt += upper_bound(matrix[i].begin(), matrix[i].end(), mid) - matrix[i].begin();
            }

            if (cnt < k) left = mid + 1;
            else right = mid;
        }

        return left;
#endif

    }
};

int main()
{
    Solution solution;

    {
        vector<vector<int>> matrix = { {1,5,9},{10,11,13},{12,13,15}};
        int k = 8;
        int ans = solution.kthSmallest(matrix, k);
        cout << ans << endl;
    }
cout << "next" << endl;
    {
        vector<vector<int>> matrix = { {-5}};
        int k = 1;
        int ans = solution.kthSmallest(matrix, k);
        cout << ans << endl;
    }

    return 0;
}
