#include <limits.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <queue>


using namespace std;


class Solution {

public:

    int findKthNumber(int m, int n, int k) {

        const int kth = k;
        int left = 1;
        int right = m * n;

        while (left < right) {
            int mid = left + (right - left) / 2;
            int ith = 0;
            for (int i = 1; i <= m; ++i) {
                ith += (mid > n * i) ? (n) : (mid / i);
            }
            if (ith < kth) {
                left = mid + 1;
            }
            else
                right = mid;
        }

        return left;


    }
};

int main()
{
    Solution solution;

    {
        int m = 3;
        int n = 3;
        int k = 5;
        int ans = solution.findKthNumber(m, n, k);
        cout << ans << endl;
    }
cout << "next" << endl;
    {
        int m = 2;
        int n = 3;
        int k = 6;
        int ans = solution.findKthNumber(m, n, k);
        cout << ans << endl;
    }

    return 0;
}
