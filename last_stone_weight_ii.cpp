#include <limits.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <queue>


using namespace std;


class Solution {
public:
    bool canPartition(vector<int>& nums) {
    #if 1
        int n = nums.size();
    
        int sum = 0;
        for (auto &num : nums) {
            sum += num;
        }
        int target = sum / 2;
        if (target * 2 != sum) return false;

        vector<vector<int>> dp(n, vector<int>(target + 1, 0));

        for (int j = 0; j <= target; ++j) {
            dp[0][j] = (j >= nums[0]) ? (nums[0]) : (0);
        }

        for (int i = 1; i < n; ++i) {
            int t = nums[i];
            for (int j = 0; j <= target; ++j) {
            //for (int j = target; j >=0; --j) {
                int no =           dp[i-1][j];
                int yes = j >= t ? dp[i-1][j-t] + t : 0;
                dp[i][j] = max(no, yes);
                //no : dp[i-1][j]
                //yes: dp[i-1][j-nums[i]]+nums[i]
            }
        }

        for (int i = 0; i < n; ++i) {
            for (int j = 0; j <= target; ++j) {
                cout << "dp[" << i << "]" << "[" << j << "]=" << dp[i][j] << endl;
            }
        }

        return dp[n-1][target] == target;
    #else
        int n = nums.size();
    
        int sum = 0;
        for (auto &num : nums) {
            sum += num;
        }
        int target = sum / 2;
        if (target * 2 != sum) return false;

        vector<vector<int>> dp(n, vector<int>(target + 1, 0));

        for (int j = 0; j <= target; ++j) {
            dp[0][j] = (j >= nums[0]) ? (nums[0]) : (0);
        }

        for (int i = 1; i < n; ++i) {
            int t = nums[i];
            for (int j = 0; j <= target; ++j) {
                int no = dp[i-1][j];

                int yes = j >= t ? dp[i-1][j-t] + t : 0;
                dp[i][j] = max(no, yes);
                //no : dp[i-1][j]
                //yes: dp[i-1][j-nums[i]]+nums[i]
            }
        }

        for (int i = 0; i < n; ++i) {
            for (int j = 0; j <= target; ++j) {
                cout << "dp[" << i << "]" << "[" << j << "]=" << dp[i][j] << endl;
            }
        }

        return dp[n-1][target] == target;
    #endif
    }
    int lastStoneWeightII(vector<int>& stones) {
    #if 1
        int n = stones.size();
        int sum = 0;
        for (auto &s : stones) sum += s;
        int target = sum / 2;
        vector<int> dp(target+1, 0);
        for (int i = 1; i <=n; ++i) {
            int weight = stones[i-1];
            for (int cap = target; cap >= weight; --cap) {
                int no  = dp[cap];
                int yes = dp[cap-stones[i-1]] + weight;
                dp[cap] = max(no, yes);
            }
            for (int j = 0; j <= target; ++j) {
                cout << "dp[" << j << "]=" << dp[j] << endl;
            }
        }



        return abs(sum - dp[target] - dp[target]);
    #else
        int n = stones.size();

        int sum = 0;
        for (int i = 0; i < stones.size(); ++i) sum += stones[i];

        int target = sum >> 1;
        vector<vector<int>> dp(n+1, vector<int>(target+1, 0));

        for (int i = 1; i <= n; ++i) {
            int weight = stones[i-1];
            for (int cap = 0; cap <= target; ++cap) {
                int no  = dp[i-1][cap];
                int yes = (cap >= weight) ? (dp[i-1][cap-weight]+weight) : (0);
                dp[i][cap] = max(no, yes);
            }
        }

        return abs(sum - dp[n][target] - dp[n][target]);
    #endif
    }
};

int main()
{
    Solution solution;

    {
        vector<int> nums = { 2,7,4,1,8,1};
        int ans = solution.lastStoneWeightII(nums);
        cout << "solution.lastStoneWeightII=" << ans << endl;
    }
cout << "next=" << endl;
    {
        vector<int> nums = { 31,26,33,21,40};
        int ans = solution.lastStoneWeightII(nums);
        cout << "solution.lastStoneWeightII=" << ans << endl;
    }

    return 0;
}
