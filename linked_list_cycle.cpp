#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>


using namespace std;

 //Definition for singly-linked list.
 struct ListNode {
     int val;
     struct ListNode *next;
     ListNode(int x) : val(x), next(NULL) {}
 };
 //
 //class Solution {
 //public:
 //    bool hasCycle(ListNode* head) {
 //        bool ret = false;
 //        struct ListNode* h = head;
 //        if (!h) return ret;
 //        while (h->next) {
 //            struct ListNode* h2 = h->next;
 //            while (h2) {
 //                if (h->next == h2->next)
 //                {
 //                    ret = true;
 //                    break;
 //                }
 //                h2 = h2->next;
 //            }
 //            if (ret == true)
 //                break;
 //            h = h->next;
 //        };

 //        return ret;
 //    }
 //};

 class Solution {
 public:
     bool hasCycle(ListNode* head) {
         bool ret = false;
         struct ListNode* first = head;
         if (!first) return ret;
         unordered_map<struct ListNode*, int> m;
         unordered_map<struct ListNode*, int>::iterator found;
         m[first] = 1;

         while (first->next) {
             found = m.find(first->next);
             if (found != m.end()) return true;
             m[first->next] = 1;
             first = first->next;
         };

         return ret;
     }
 };



int main()
{
    Solution s;
    {
        struct ListNode ln_0(3);
        struct ListNode ln_1(2);
        struct ListNode ln_2(0);
        struct ListNode ln_3(-4);
        ln_0.next = &ln_1;
        ln_1.next = &ln_2;
        ln_2.next = &ln_3;
        ln_3.next = &ln_1;
        cout << "ans=" << s.hasCycle(&ln_0) << endl;
    }
    {
        struct ListNode ln_0(1);
        struct ListNode ln_1(2);

        ln_0.next = &ln_1;
        ln_1.next = &ln_0;

        cout << "ans=" << s.hasCycle(&ln_0) << endl;
    }
    {
        struct ListNode ln_0(1);
        ln_0.next = &ln_0;
        cout << "ans=" << s.hasCycle(&ln_0) << endl;
    }
    {
        struct ListNode ln_0(1);

        cout << "ans=" << s.hasCycle(&ln_0) << endl;
    }
    /*
[3, 2, 0, -4]
1
[1, 2]
0
[1]
-1
[1, 2]
-1
[-1, -7, 7, -4, 19, 6, -9, -5, -2, -5]
6
    */
    {
        struct ListNode ln_0(1);
        struct ListNode ln_1(2);

        ln_0.next = &ln_1;

        cout << "ans=" << s.hasCycle(&ln_0) << endl;
    }
    return 0;
}
