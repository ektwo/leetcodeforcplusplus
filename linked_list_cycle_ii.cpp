#include <iostream>
#include <vector>
#include <string>
#include <unordered_set>
#include <queue>

using namespace std;


struct ListNode {
    int val;
    struct ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};


void print_1d_int(vector<int> nums)
{

        cout << "{";
        for (int j = 0; j < nums.size(); ++j) {
            cout << nums[j] << ",";
        }
        cout << "}," << endl;

}

void print_2d_int(vector<vector<int>> nums)
{
    cout << "{";
    for (int i = 0; i < nums.size(); ++i) {
        cout << "{";
        for (int j = 0; j < nums[0].size(); ++j) {
            cout << nums[i][j] << ",";
        }
        cout << "}," << endl;
    }
    cout << "}," << endl;
}

void print_2d_string(vector<vector<string>> nums)
{
    cout << "{";
    for (int i = 0; i < nums.size(); ++i) {
        cout << "{";
        for (int j = 0; j < nums[i].size(); ++j) {
            cout << nums[i][j] << ",";
        }
        cout << "}," << endl;
    }
    cout << "}," << endl;
}


void print_tree_preorder(TreeNode *root)
{
    if (root) {
        cout << root->val << ",";
        if (root->left) print_tree_preorder(root->left);
        if (root->right) print_tree_preorder(root->right);
    }
    else
        cout << "null,";
}

#define SOLUTION_MEMO   0
#define SOLUTION_DP_SUB 1
#define SOLUTION_BFS    3
#define SOLUTION        SOLUTION_BFS
class Solution
{

#if SOLUTION == SOLUTION_MEMO
private:
    bool check(string s, unordered_set<string> &wordset, int start, vector<int> &memo) {
        if (start >= s.length()) return true;
        if (memo[start] != -1) return memo[start];
        for (int i = start + 1; i <= s.length(); ++i) {
            if (wordset.count(s.substr(start, i - start)) && check(s, wordset, i, memo)) {
                memo[start] = 1;
                return memo[start];
            }
        }
        memo[start] = 0;
        return memo[start];
    }
public:
    bool wordBreak(string s, vector<string>& wordDict) {
        unordered_set<string> wordset(wordDict.begin(), wordDict.end());
        vector<int> memo(s.length(), -1);
        return check(s, wordset, 0, memo);
    }
#elif SOLUTION == SOLUTION_DP_SUB
public:
    bool wordBreak(string s, vector<string>& wordDict) {
        unordered_set<string> wordset(wordDict.begin(), wordDict.end());
        vector<bool> dp(s.length()+1,false);
        dp[0]=true;
        for (int i = 0; i < dp.size(); ++i) {
            for (int j = 0; j < i; ++j) {
                if (dp[j] && (0 != wordset.count(s.substr(j, i - j)))) {
                    dp[i]=true;
                    break;
                }
            }
        }
        return dp.back();
    }

#elif SOLUTION == SOLUTION_BFS
public:
    bool wordBreak(string s, vector<string>& wordDict) {
        unordered_set<string> wordset(wordDict.begin(), wordDict.end());
        vector<bool> visited(s.length(), false);
        queue<int> q {{0}};
        while (!q.empty()) {
            int start = q.front(); q.pop();
            if (!visited[start]) {
                for (int i = start; i <= s.length(); ++i) {
                    if (0 != wordset.count(s.substr(start, i-start))) {
                        q.push(i);
                        if (i == s.length()) return true;
                    }
                }
                visited[start] = 1;
            }
        }
        return false;
    }
#endif
public:
    ListNode *detectCycle(ListNode *head) {
        if (!head) return head;

        ListNode *slow = head;
        ListNode *fast = head;
        ListNode *slow2 = nullptr;
        while (slow->next && fast->next) {
            slow = slow->next;
            fast = fast->next->next;
            if (slow == fast) {
                break;
            }
        }

        if (!slow || !slow->next) return NULL;
        slow2 = head;

        while (slow->next && slow2->next) {
            if (slow == slow2) return slow;
            slow = slow->next;
            slow2 = slow2->next;
        }
        return nullptr;
    }
};

int main()
{
    Solution solution;
    {
        struct ListNode ln_0(3);
        struct ListNode ln_1(2);
        struct ListNode ln_2(0);
        struct ListNode ln_3(-4);
        ln_0.next = &ln_1;
        ln_1.next = &ln_2;
        ln_2.next = &ln_3;
        ln_3.next = &ln_1;
        ListNode* ans = solution.detectCycle(&ln_0);
    }
    cout << endl;
    {
        struct ListNode ln_0(1);
        struct ListNode ln_1(2);

        ln_0.next = &ln_1;
        ln_1.next = &ln_0;

        ListNode* ans = solution.detectCycle(&ln_0);
    }
    cout << endl;
    {
        struct ListNode ln_0(1);
        ln_0.next = &ln_0;
        ListNode* ans = solution.detectCycle(&ln_0);
    }

    return 0;
}
