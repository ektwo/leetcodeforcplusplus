#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>


using namespace std;


class Solution {
public:

    int longestCommonSubsequence(string text1, string text2) {
        int ans = 0;
		int m = text1.size();
        int n = text2.size();

        vector<vector<int>> dp(m+1, vector<int>(n+1));

        for (int i = 1; i <= m; ++i) {
            for (int j = 1; j <= n; ++j) {
                if (text1[i-1] == text2[j-1]) {
                    dp[i][j] = dp[i-1][j-1] + 1;
                }
                else
                    dp[i][j] = max(dp[i-1][j], dp[i][j-1]);
                ans = max(ans, dp[i][j]);
            }
        }

        return ans;//dp[m][n];
	}

};

int main()
{
	Solution solution;
	{
		string text1 = "abcde";
        string text2 = "ace";
		cout << solution.longestCommonSubsequence(text1, text2) << endl;
	}
	{
		string text1 = "abc";
        string text2 = "abc";
		cout << solution.longestCommonSubsequence(text1, text2) << endl;
	}
	{
		string text1 = "abc";
        string text2 = "def";
		cout << solution.longestCommonSubsequence(text1, text2) << endl;
	}
	return 0;
}