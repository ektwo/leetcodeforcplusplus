#include <iostream>
#include <vector>
#include <string>
#include <unordered_set>

using namespace std;


struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};


void print_1d_int(vector<int> nums)
{

        cout << "{";
        for (int j = 0; j < nums.size(); ++j) {
            cout << nums[j] << ",";
        }
        cout << "}," << endl;

}

void print_2d_int(vector<vector<int>> nums)
{
    cout << "{";
    for (int i = 0; i < nums.size(); ++i) {
        cout << "{";
        for (int j = 0; j < nums[0].size(); ++j) {
            cout << nums[i][j] << ",";
        }
        cout << "}," << endl;
    }
    cout << "}," << endl;
}

void print_tree_preorder(TreeNode *root)
{
    if (root) {
        cout << root->val << ",";
        if (root->left) print_tree_preorder(root->left);
        if (root->right) print_tree_preorder(root->right);
    }
    else
        cout << "null,";
}

class Solution
{
public:
    int longestConsecutive(vector<int>& nums) {
        int ans = 0;
        unordered_set<int> arr(nums.begin(), nums.end());
        for (auto &val : arr) {
            if (arr.count(val-1) == 0) {
                int curr_val = val;
                while (arr.count(curr_val)) ++curr_val;
                ans = max(ans, curr_val - val);
            }
        }
        return ans;
    }

};

int main()
{
    Solution solution;
    {
        vector<int> nums = {100,4,200,1,3,2};
        int ans = solution.longestConsecutive(nums);
        cout << ans << endl;
    }
    cout << endl;
    {
        vector<int> nums = {0,3,7,2,5,8,4,6,0,1};
        int ans = solution.longestConsecutive(nums);
        cout << ans << endl;
    }

    return 0;
}
