#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>


using namespace std;


int DP[200][200], dc[4] = { 0, 1, 0, -1 }, dr[4] = { -1, 0, 1, 0 };

class Solution {

    int rows, columns;
    vector <vector <int>>* matrix;

    int DFS(int nowRow, int nowColumn) {
        int newRow, newColumn;
        for (int i = 0; i < 4; ++i) {
            newRow = nowRow + dr[i]; newColumn = nowColumn + dc[i];
            if (0 <= newRow && newRow < rows && 0 <= newColumn && newColumn < columns && (*matrix)[newRow][newColumn] >(*matrix)[nowRow][nowColumn])
                DP[nowRow][nowColumn] = max(DP[nowRow][nowColumn], (DP[newRow][newColumn] ? DP[newRow][newColumn] : DFS(newRow, newColumn)));
        }
        return (++DP[nowRow][nowColumn]);
    }

public:
    int longestIncreasingPath(vector<vector<int>>& matrix) {
        int maximum = 0;
        this->matrix = &matrix;
        memset(DP, 0, sizeof(DP));
        rows = matrix.size(); columns = matrix[0].size();
        for (int i = 0; i < rows; ++i)
            for (int j = 0; j < columns; ++j)
                if (!DP[i][j])
                    maximum = max(maximum, DFS(i, j));
        return maximum;
    }
};

int main()
{
	Solution solution;
	{
		vector<vector<int>> prerequisites{
			{9,9,4},
			{6,6,8},
			{2,1,1},
		};
		cout << solution.longestIncreasingPath(prerequisites) << endl;
	}
	{
		vector<vector<int>> prerequisites{
			{3,4,5},
			{3,2,6},
			{2,2,1},
		};
		cout << solution.longestIncreasingPath(prerequisites) << endl;
	}

	return 0;
}
