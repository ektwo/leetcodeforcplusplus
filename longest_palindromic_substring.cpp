#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>


using namespace std;


class Solution {
public:
#if 1
	string longestPalindrome(string s) {

		int str_longest_begin = 0;
		int str_longest_len = 1;
		int n = s.length();
		bool dp[1000][1000] = { false };
		for (int i = 0; i < n; ++i) {
			int left = i;
			int right = i;
			dp[left][right] = 1;
		}

		for (int i = 0; i < n - 1; ++i) {
			int left = i;
			int right = i + 1;
			if (s[left] == s[right]) {
				dp[left][right] = true;
				str_longest_begin = left;
				str_longest_len = right - left + 1;
			}
		}

		for (int word_len = 2; word_len < n; ++word_len) {
			for (int i = 0; i < (n - word_len); ++i) {
				int left = i;
				int right = left + word_len;
				if ((s[left] == s[right]) && (dp[left+1][right-1] == true)) {
					dp[left][right] = true;
					str_longest_begin = left;
					str_longest_len = right - left + 1;
				}
			}
		}

		return s.substr(str_longest_begin, str_longest_len);
	}
#else
	void expand(string s, int left, int right, int& best_left, int& best_right) {
		while (left >= 0 && right < s.size() && s[left] == s[right]) {
			--left;
			++right;
		}
		++left;
		--right;
		if ((right - left) > (best_right - best_left)) {
			best_left = left;
			best_right = right;
		}
	}
	string longestPalindrome(string s) {
		int best_left = 0;
		int best_right = 0;

		for (int i = 0; i < s.size(); i++) {
			//odd expand
			expand(s, i, i, best_left, best_right);
			//even expand
			expand(s, i, i+1, best_left, best_right);
		}

		return s.substr(best_left, best_right - best_left + 1);
	}
#endif
};

int main()
{
	Solution solution;
	{
		string str = "babad";
		cout << solution.longestPalindrome(str) << endl;
	}
	{
		string str = "cbbd";
		cout << solution.longestPalindrome(str) << endl;
	}
	return 0;
}