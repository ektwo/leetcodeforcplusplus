#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>


using namespace std;



class Solution {
public:


    int lengthOfLongestSubstringTwoDistinct(string s) {
        int ans = 0;

        vector<int> count(128);

        int slide_window_cnt = 0;
        int slide_window_max = 2;

        int left = 0;
        for (int right = 0; right < s.length(); ++right) {

            if (++count[s[right]] == 1) ++slide_window_cnt;
            while (slide_window_cnt == (slide_window_max + 1))
                if (--count[s[left++]] == 0)
                    --slide_window_cnt;
            ans = max(ans, right - left + 1);
        }

        return ans;
    }

    int lengthOfLongestSubstringTwoDistinct2(string s) {

        int ans = 0;

        const int slide_window_max = 2;
        vector<int> count(128);

        int slide_window_cnt = 0;
        int left = 0;
        for (int right = 0; right < s.length(); ++right) {
            if (++count[s[right]] == 1)
                ++slide_window_cnt;
            while (slide_window_cnt == (slide_window_max + 1))
                if (--count[s[left++]] == 0)
                    --slide_window_cnt;
            ans = max(ans, right - left + 1);
        }
        return ans;
    }

};

int main()
{
    Solution s;
    {
        string str("eceba");
        cout << s.lengthOfLongestSubstringTwoDistinct2(str) << endl;
    }
    {
        string str("ccaabbb");
        cout << s.lengthOfLongestSubstringTwoDistinct2(str) << endl;
    }

    return 0;
}
