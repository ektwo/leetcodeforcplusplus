#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>


using namespace std;
//
//class Solution {
//public:
//    int lengthOfLongestSubstring(string str) {
//        if (str.length() == 0) return 0;
//        int max_len = 0;
//        string tmp_s("");
//
//        for (int i = 0; i < str.size(); ++i) {
//            size_t pos = tmp_s.find(str[i], 0);
//            if (pos != string::npos)
//                tmp_s.erase(0, pos + 1);
//            tmp_s += str[i];
//            if (max_len < tmp_s.length())
//                max_len = tmp_s.length();
//        }
//
//        return max_len;
//    }
//}

class Solution {
public:
    int lengthOfLongestSubstring(string str) {
        if (str.length() == 0) return 0;
        int max_len = 0;
        string tmp_s("");

        for (int i = 0; i < str.size(); ++i) {
            //for (int j = 0; i < tmp_s.size(); ++j) {
                size_t pos = tmp_s.find(str[i], 0);
                if (pos != string::npos) {
                    //cout << "str=" << str[i] << " find=" << pos << endl;
                    //cout << "1 tmp_s=" << tmp_s << endl;
                    tmp_s.erase(0, pos + 1);
                    //tmp_s.clear();
                    //cout << "2 tmp_s=" << tmp_s << endl;
                }
            //}
            tmp_s += str[i];
            if (max_len < tmp_s.length())
                max_len = tmp_s.length();
            //cout << "max_len=" << max_len << endl;
        }
        

        //cout << tmp_s.size() << endl;
        //cout << tmp_s.length() << endl;

        return max_len;
    }
};

int main()
{
    Solution s;
    {
        string str("abcabcbb");
        cout << s.lengthOfLongestSubstring(str) << endl;
    }
    {
        string str("bbbbb");
        cout << s.lengthOfLongestSubstring(str) << endl;
    }
    {
        string str("pwwkew");
        cout << s.lengthOfLongestSubstring(str) << endl;
    }
    {
        string str("dvdf");
        cout << s.lengthOfLongestSubstring(str) << endl;
    }

    return 0;
}
