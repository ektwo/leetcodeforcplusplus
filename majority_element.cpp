#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <limits>


using namespace std;



class Solution {
public:
    int majorityElement(vector<int>& nums) {
        int count1 = 0;
        int candidate1 = 0;
        for (int i = 0; i < nums.size(); ++i) {
            int num = nums[i];
            if      (num == candidate1) ++count1;
            else if (0 == count1) {candidate1 = nums[i]; ++count1; }
            else    --count1;
        }
        return candidate1;
    }
};

int main()
{
    Solution solution;
    {
        vector<int> nums = {3,2,3};
        int ans = solution.majorityElement(nums);
        cout << ans << endl;
    }
    cout << endl;
    {
        vector<int> nums = {2,2,1,1,1,2,2};
        int ans = solution.majorityElement(nums);
        cout << ans << endl;
    }
    return 0;
}