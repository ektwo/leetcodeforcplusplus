#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <limits>


using namespace std;


void print_1d_int(vector<int> nums)
{

        cout << "{";
        for (int j = 0; j < nums.size(); ++j) {
            cout << nums[j] << ",";
        }
        cout << "}," << endl;

}


class Solution {
public:
    vector<int> majorityElement(vector<int>& nums) {
        vector<int> ans;
        int n = nums.size();
        int count1 = 0, count2 = 0;
        int candidate1 = 0, candidate2 = 0;
        for (int i = 0; i < nums.size(); ++i) {
            int num = nums[i];
            if      (num == candidate1) ++count1;
            else if (num == candidate2) ++count2;
            else if (0 == count1) {candidate1 = nums[i]; ++count1; }
            else if (0 == count2) {candidate2 = nums[i]; ++count2; }
            else    { --count1; --count2; }
        }

        count1 = 0, count2 = 0;
        for (int i = 0; i < nums.size(); ++i) {
            int num = nums[i];
            if      (num == candidate1) ++count1;
            else if (num == candidate2) ++count2;
        }
        if (count1 > n/3) ans.push_back(candidate1);
        if (count2 > n/3) ans.push_back(candidate2);

        return ans;
    }
};

int main()
{
    Solution solution;
    {
        vector<int> nums = {3,2,3};
        vector<int> ans = solution.majorityElement(nums);
        print_1d_int(ans);
    }
    cout << endl;
    {
        vector<int> nums = {1};
        vector<int> ans = solution.majorityElement(nums);
        print_1d_int(ans);
    }
    cout << endl;
    {
        vector<int> nums = {1,2};
        vector<int> ans = solution.majorityElement(nums);
        print_1d_int(ans);
    }
    return 0;
}