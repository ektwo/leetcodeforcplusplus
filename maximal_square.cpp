#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>


using namespace std;


class Solution {


public:
    int maximalSquare(vector<vector<char>>& matrix) {
        int m = matrix.size(), n = matrix[0].size();
        int ans = 0;
        int pre_i_minus_1 = 0;
        vector<int> dp(m + 1, 0);
        for (int j = 0; j < n; ++j) {
            for (int i = 1; i <= m; ++i) {
                int tmp = dp[i];
                if (matrix[i-1][j] == '1') {
                    dp[i] = min(dp[i], min(dp[i-1], pre_i_minus_1)) + 1;
                    ans = max(ans, dp[i]);
                }
                else {
                    dp[i] = 0;
                }
                pre_i_minus_1 = tmp;
            }
        }


        return ans * ans;
    }
};

int main()
{
	Solution solution;
	{
		vector<vector<char>> matrix{
			{'1','0','1','0','0'},{'1','0','1','1','1'},{'1','1','1','1','1'},{'1','0','0','1','0'}
		};

		cout << solution.maximalSquare(matrix) << endl;
	}
	{
		vector<vector<char>> matrix{
			{'0','1'},{'1','0'}
		};

		cout << solution.maximalSquare(matrix) << endl;
	}
	{
		vector<vector<char>> matrix{
			{'0'}
		};

		cout << solution.maximalSquare(matrix) << endl;
	}
	return 0;
}
