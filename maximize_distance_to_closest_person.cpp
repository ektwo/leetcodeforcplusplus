#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>


using namespace std;



class Solution {

public:

    int maxDistToClosest(vector<int>& seats) {

        int ans = 1;

        int left = -1;
        int dist_max = 0;
        int right = 0;

        for (right = 0; right < seats.size(); ++right) {
            if (seats[right] == 1) {
                ans = right - 0; 
                //if (right == seats.size() - 1) return ans;
                left = right;
                ++right;
                break;
            }
        }
        //if (right == seats.size()) return ans;
cout << "A left=" << left << " right=" << right << " ans=" << ans << endl;
        for (; right < seats.size(); ++right) {
            if (seats[right] == 1) {
                if (ans < (right - left) / 2) {
                    ans = (right - left) / 2;
                }
                left = right;
                //break;
            }
        }
        --right;
cout << "B left=" << left << " right=" << right << " ans=" << ans << endl;
        if (seats[seats.size()-1] == 0)
        {
            if (ans < (right - left)) {
                ans = (right - left);
            }
        }

        return ans;
    }
};

int main()
{
    Solution s;
    {
        vector<int> seats = { 1,0,0,0,1,0,1 };
        cout << s.maxDistToClosest(seats) << endl;
    }
    {
        vector<int> seats = { 1,0,0,0 };
        cout << s.maxDistToClosest(seats) << endl;
    }
    {
        vector<int> seats = { 0,1 };
        cout << s.maxDistToClosest(seats) << endl;
    }
    {
        vector<int> seats = { 0,0,1 };
        cout << s.maxDistToClosest(seats) << endl;
    }
    {
        vector<int> seats = { 1,0,1 };
        cout << s.maxDistToClosest(seats) << endl;
    }
    {
        vector<int> seats = { 0,1,0,1,0 };
        cout << s.maxDistToClosest(seats) << endl;
    }
    return 0;
}
