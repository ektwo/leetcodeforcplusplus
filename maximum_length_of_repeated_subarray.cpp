#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>


using namespace std;


class Solution {
public:

    int findLength(vector<int>& nums1, vector<int>& nums2) {
        int ans = 0;
		int m = nums1.size();
        int n = nums2.size();

        vector<vector<int>> dp(m+1, vector<int>(n+1, 0));

        for (int i = 1; i <= m; ++i) {
            for (int j = 1; j <= n; ++j) {
                if (nums1[i-1] == nums2[j-1]) {
                    dp[i][j] = dp[i-1][j-1] + 1;
                }
                else
                    dp[i][j] = 0;//max(dp[i-1][j], dp[i][j-1]);
                ans = max(ans, dp[i][j]);
            }
        }

        return ans;//dp[m][n];
	}

};

int main()
{
	Solution solution;
	{
		vector<int> nums1 = {1,2,3,2,1};
        vector<int> nums2 = {3,2,1,4,7};
		cout << solution.findLength(nums1, nums2) << endl;
	}
	{
		vector<int> nums1 = {0,0,0,0,0};
        vector<int> nums2 = {0,0,0,0,0};
		cout << solution.findLength(nums1, nums2) << endl;
	}

	return 0;
}