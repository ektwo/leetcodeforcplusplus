#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>


using namespace std;


class Solution {
public:
	void expand(vector<int>& nums, int left, int right, int& min_product, int& max_product, int &best_product) {


		while (left >= 0 && right < nums.size()) {
		
			if (nums[right] > 0) {
				max_product = max(max_product * nums[right], nums[right]);
				min_product = min(min_product * nums[right], nums[right]);
			}
			else if (nums[right] == 0) {
				max_product = max(max_product * nums[right], nums[right]);
				min_product = min(min_product * nums[right], nums[right]);
			}
			else {
				int tmp_max_product = max_product;
				max_product = max(min_product * nums[right], nums[right]);
				min_product = min(tmp_max_product * nums[right], nums[right]);
			}
			best_product = max(best_product, max_product);

			++right;
		}

	}

	int maxProduct(vector<int>& nums) {
		int best_product = nums[0];
		int min_product = best_product;
		int max_product = best_product;
		int i = 1;
		//for (i = 1; i < nums.size(); i++) {
			expand(nums, i, i, min_product, max_product, best_product);
		//}

		return best_product;
	}
};

int main()
{
	Solution solution;
	//{
	//	vector<int> v = { 2,3,-2,4 };
	//	cout << solution.maxProduct(v) << endl;
	//}
	//{
	//	vector<int> v = { -2,0,-1 };
	//	cout << solution.maxProduct(v) << endl;
	//}
	//{
	//	vector<int> v = { -3,-1,-1 };
	//	cout << solution.maxProduct(v) << endl;
	//}
	//{
	//	vector<int> v = { -4,-3 };
	//	cout << solution.maxProduct(v) << endl;
	//}
	{
		vector<int> v = { 0,2 };
		cout << solution.maxProduct(v) << endl;
	}
	//{
	//	vector<int> v = { -2,0 };
	//	cout << solution.maxProduct(v) << endl;
	//}
	//{
	//	vector<int> v = { -2 };
	//	cout << solution.maxProduct(v) << endl;
	//}
	//{
	//	vector<int> v = { 3, -1, 4 };
	//	cout << solution.maxProduct(v) << endl;
	//}
	//{
	//	vector<int> v = { -2, 3, -4 };
	//	cout << solution.maxProduct(v) << endl;
	//}
	{
		vector<int> v = { 7, -2, -4 };
		cout << solution.maxProduct(v) << endl;
	}
	{
		vector<int> v = { -2, -3, 7 };
		cout << solution.maxProduct(v) << endl;
	}
	{
		vector<int> v = { -1, -2, -9, -6 };
		cout << solution.maxProduct(v) << endl;
	}
	{
		vector<int> v = { 2,-5,-2,-4,3 };
		cout << solution.maxProduct(v) << endl;
	}
	return 0;
}