#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>


using namespace std;


class Solution {
public:
	void expand(string s, int left, int right, int& best_left, int& best_right) {
		while (left >= 0 && right < s.size() && s[left] == s[right]) {
			--left;
			++right;
		}
		++left;
		--right;
		if ((right - left) > (best_right - best_left)) {
			best_left = left;
			best_right = right;
		}
	}
	string longestPalindrome(string s) {
		int best_left = 0;
		int best_right = 0;

		for (int i = 0; i < s.size(); i++) {
			//odd expand
			expand(s, i, i, best_left, best_right);
			//even expand
			expand(s, i, i + 1, best_left, best_right);
		}

		return s.substr(best_left, best_right - best_left + 1);
	}
	int count_total(vector<int>& nums, int left, int right, int &total_tmp)
	{
		//int total = 0;
		//for (int i = left; i <= right; ++i)
		{
			total_tmp += nums[right];
		}
		return total_tmp;
	}
	void expand2(vector<int>& nums, int left, int right, int& best_left, int& best_right, int &best_ans) {
		int total_tmp = 0;
		while (left >= 0 && right < nums.size()) {// && s[left] == s[right]) {
			count_total(nums, left, right, total_tmp);
			if (total_tmp > best_ans) {
				best_left = left;
				best_right = right;
				best_ans = total_tmp;
				cout << "best_left=" << best_left << " best_right=" << best_right << endl;
			}
			//--left;
			++right;
		}
	}
	int maxSubArray(vector<int>& nums) {
		int best_left = 0;
		int best_right = 0;
		int best_ans = -INT_MAX;

		for (int i = 0; i < nums.size(); i++) {
			expand2(nums, i, i, best_left, best_right, best_ans);
		}

		//for (int i = best_left; i <= best_right; ++i) {
		//	best_ans += nums[i];
		//}

		return best_ans;
	}
};

int main()
{
	Solution solution;
	{
		vector<int> v = { -2,1,-3,4,-1,2,1,-5,4 };
		cout << solution.maxSubArray(v) << endl;
	}
	{
		vector<int> v = { -1 };
		cout << solution.maxSubArray(v) << endl;
	}
	{
		vector<int> v = { 5,4,-1,7,8 };
		cout << solution.maxSubArray(v) << endl;
	}
	return 0;
}