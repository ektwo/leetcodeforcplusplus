#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>


using namespace std;


//class Solution {
//    //C(k-1) = max(Am1-1, Bm2 - 1)
//    //C(k) = min(Am1, Bm2)
//public:
//    double findMedianSortedArrays(vector<int>& nums1, vector<int>& nums2) {
//        if (nums1.size() == 0) return findMedianValue(nums2);
//        if (nums2.size() == 0) return findMedianValue(nums1);
//        int n1l = 0;
//        int n1r = nums1.size();
//        int n2l = 0;
//        int n2r = nums2.size();
//        int k = (n1r + n2r + 1) / 2;
//        int m1 = (n1r - n1l) / 2;
//        int m2 = k - m1;
//        int ck_1;// = max(nums1[m1 - 1], nums2[m2 - 1]);
//        int ck_2;// = min(nums1[m1], nums2[m2]);
//        int odd = ((n1r + n2r) & 1);
//        double ret = 0;
//        while (n1l < n1r) {
//            int m1 = n1l + (n1r - n1l) / 2;
//            int m2 = k - m1;
//            if (nums1[m1] < nums2[m2 - 1]) {
//                n1l = m1 + 1;
//                //m1 = (n1r - n1l) / 2;
//                //m2 = k - m1;
//            }
//            else {
//                if (nums2[m2] < nums1[m1 - 1]) {
//                    n1r = m1;
//                    //m1 = (n1r - n1l) / 2;
//                    //m2 = k - m1;
//                }
//                else {
//                    n1r = m1 - 1;
//                    //m1 = (n1r - n1l) / 2;
//                    //m2 = k - m1;
//                }
//            }
//        }
//        if (odd) {
//            ck_1 = max(nums1[m1 - 1], nums2[m2 - 1]);
//            //ck_2 = min(nums1[m1], nums2[m2]);
//            ret = ck_1;
//        }
//        else {
//            ck_1 = max(nums1[m1 - 1], nums2[m2 - 1]);
//            ck_2 = min(nums1[m1], nums2[m2]);
//            ret = ((ck_1 + ck_2) / 2.0);
//        }
//        return ret;
//    }
//    static double findMedianValue(vector<int>& num) {
//        int size = num.size();
//        if (size & 1) {
//            return (double)num[size >> 1];
//        }
//        else {
//            //cout << "1" << num[(size >> 1) - 1] << endl;
//            //cout << "2" << num[size >> 1] << endl;
//            return (double)((num[(size >> 1) - 1] + num[size >> 1]) / 2.0f);
//        }
//    }
//};

class Solution {
public:
    double findMedianSortedArrays(vector<int>& nums1, vector<int>& nums2) {

        const int n1_size = nums1.size();
        const int n2_size = nums2.size();
        if (n1_size == 0) return findMedianValue(nums2);
        if (n2_size == 0) return findMedianValue(nums1);
        if (n1_size > n2_size) return findMedianSortedArrays(nums2, nums1);

        const int k = (n1_size + n2_size + 1) / 2;
        int l = 0;
        int r = n1_size;

        while (l < r) {
            const int m1 = l + ((r - l) / 2);
            const int m2 = k - m1;
            if (nums1[m1] < nums2[m2 - 1])
                l = m1 + 1;
            else
                r = m1;
        }

        int odd = (n1_size + n2_size) & 1;
        int m1 = l;
        int m2 = k - m1;
        int ck1 = max((m1 <= 0) ? (INT_MIN) : (nums1[m1 - 1]),
            (m2 <= 0) ? (INT_MIN) : (nums2[m2 - 1]));
        if (odd) return ck1;

        int ck2 = min((m1 >= n1_size) ? (INT_MAX) : (nums1[m1]),
            (m2 >= n2_size) ? (INT_MAX) : (nums2[m2]));
        return (ck1 + ck2) * 0.5;
    }
    static double findMedianValue(vector<int>& num) {
        int size = num.size();
        if (size & 1) {
            return (double)num[size >> 1];
        }
        else {
            return (double)((num[(size >> 1) - 1] + num[size >> 1]) / 2.0f);
        }
    }
};

int main()
{
    Solution s;
    {
        vector<int> v1{ 1, 3 };
        vector<int> v2{ 2 };
        double ret = s.findMedianSortedArrays(v1, v2);
        cout << ret << endl;
    }
    {
        vector<int> v1{ 1, 2 };
        vector<int> v2{ 3, 4 };
        double ret = s.findMedianSortedArrays(v1, v2);
        cout << ret << endl;
    }
    {
        vector<int> v1{ 0, 0 };
        vector<int> v2{ 0, 0 };
        double ret = s.findMedianSortedArrays(v1, v2);
        cout << ret << endl;
    }
    {
        vector<int> v1;
        vector<int> v2{ 1 };
        double ret = s.findMedianSortedArrays(v1, v2);
        cout << ret << endl;
    }
    {
        vector<int> v1{ 2 };
        //vector<int> v1{ 1, 2, 3, 4, 5, 6 };
        vector<int> v2;
        double ret = s.findMedianSortedArrays(v1, v2);
        cout << ret << endl;
    }

    return 0;
}
