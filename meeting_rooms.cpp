#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <queue>


using namespace std;



class Solution {


public:


    bool canAttendMeetings(vector<vector<int>>& intervals) {
        // map<int, int> m;
        // for (auto a : intervals) {
        //     ++m[a[0]];
        //     --m[a[1]];
        // }
        // int rooms = 0, res = 0;
        // for (auto it : m) {
        //     res = max(res, rooms += it.second);
        // }
        // return res;

        priority_queue<int, vector<int>, greater<int>> minHeap;

        //std::sort(intervals.begin(), intervals.end(), [](const vector<int> &a, const vector<int> &b) { return a[0] < b[0]; });
        sort(begin(intervals), end(intervals));

        for (const auto& interval : intervals) {
        if (!minHeap.empty()) {
            if (interval[0] >= minHeap.top()) minHeap.pop();  // no overlap, we can reuse the same room
            else return false;
        }
        minHeap.push(interval[1]);
        }

        return true;//minHeap.size();
    }
};

int main()
{
    Solution s;
    {
        vector<vector<int>> intervals = { {0,30},{5,10},{15,20} };
        cout << s.canAttendMeetings(intervals) << endl;
    }
    {
        vector<vector<int>> intervals = { {7,10},{2,4}};
        cout << s.canAttendMeetings(intervals) << endl;
    }

    return 0;
}
