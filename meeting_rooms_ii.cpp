#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <queue>


using namespace std;



class Solution {

private:

    int partition(vector<int>& nums, int leftmostIndex, int rightmostIndex) {

        const int pivot_pos = leftmostIndex;
        const int pivot = nums[pivot_pos];

        int last_pos_that_val_less_than_pivot = leftmostIndex + 1;
        int unpartitioned_idx = leftmostIndex + 1;

        for (; unpartitioned_idx <= rightmostIndex; ++unpartitioned_idx) {
            if (pivot > nums[unpartitioned_idx]) {
                if (last_pos_that_val_less_than_pivot != unpartitioned_idx) {
                    std::swap(nums[last_pos_that_val_less_than_pivot], nums[unpartitioned_idx]);
                }
                ++last_pos_that_val_less_than_pivot;
            }
        }

        int new_pivot_pos = last_pos_that_val_less_than_pivot - 1;
        std::swap(nums[pivot_pos], nums[new_pivot_pos]);

        return new_pivot_pos;
    }

    void quick_sort(vector<int>& nums, int leftmostIndex, int rightmostIndex)
    {
        if (leftmostIndex < rightmostIndex) {
            int pivot_pos = partition(nums, leftmostIndex, rightmostIndex);   //find the pivot element such that
            quick_sort(nums, leftmostIndex, pivot_pos - 1);                  //sorts the left side of pivot.
            quick_sort(nums, pivot_pos + 1, rightmostIndex);                 //sorts the right side of pivot.
        }
    }

public:

    int findKthLargest(vector<int>& nums, int k) {
        int ans = 0;

        quick_sort(nums, 0, nums.size() - 1);

        for (int i = 0; i < nums.size(); ++i) {
            cout << " " << nums[i] << endl;
        }

        ans = nums[nums.size() - k];

        return ans;
    }

    int minMeetingRooms(vector<vector<int>>& intervals) {
        // map<int, int> m;
        // for (auto a : intervals) {
        //     ++m[a[0]];
        //     --m[a[1]];
        // }
        // int rooms = 0, res = 0;
        // for (auto it : m) {
        //     res = max(res, rooms += it.second);
        // }
        // return res;

        priority_queue<int, vector<int>, greater<int>> minHeap;

        //std::sort(intervals.begin(), intervals.end(), [](const vector<int> &a, const vector<int> &b) { return a[0] < b[0]; });
        sort(begin(intervals), end(intervals));

        for (const auto& interval : intervals) {
        if (!minHeap.empty() && interval[0] >= minHeap.top())
            minHeap.pop();  // no overlap, we can reuse the same room
        minHeap.push(interval[1]);
        }

        return minHeap.size();
    }
};

int main()
{
    Solution s;
    {
        vector<vector<int>> intervals = { {0,30},{5,10},{15,20} };
        cout << s.minMeetingRooms(intervals) << endl;
    }
    {
        vector<vector<int>> intervals = { {7,10},{2,4}};
        cout << s.minMeetingRooms(intervals) << endl;
    }

    return 0;
}
