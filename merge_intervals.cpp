#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>


using namespace std;


class Solution {
public:
    vector<vector<int>> merge(vector<vector<int>>& intervals) {
    #if 1
        vector<vector<int>> ans;
        int n = intervals.size();
        vector<int> start, end;

        for (auto &interval : intervals) {
            start.push_back(interval[0]);
            end.push_back(interval[1]);
        }

        sort(start.begin(), start.end());
        sort(end.begin(), end.end());

        for (int i = 0, j = 0; i < n; ++i) {
            if (i == n - 1) {
                ans.push_back({start[j], end[i]}); 
            }
            else if (start[i + 1] > end[i]) {
                ans.push_back({start[j], end[i]}); j = i + 1;
            }
            else {

            }
        }
        return ans;

    #else
        vector<vector<int>> ans;
        if (intervals.size() < 2) return intervals;

        sort(intervals.begin(), intervals.end(), 
        [](const vector<int>&a, const vector<int>&b) {return a[0] < b[0];});

        ans.push_back(intervals[0]);
        for (int i = 1; i < intervals.size(); ++i) {
            if (ans.back()[1]<intervals[i][0]) {
                ans.push_back(intervals[i]);
            } else {
                ans.back()[0]=min(ans.back()[0], intervals[i][0]);
                ans.back()[1]=max(ans.back()[1], intervals[i][1]);
            }
        }
        return ans;
    #endif
    }
};

int main()
{
    Solution solution;
    {
        vector<int> v11{ 1, 3 };
        vector<int> v12{ 2, 6 };
        vector<int> v13{ 8, 10 };
        vector<int> v14{ 15, 18 };
        vector<vector<int>> v1{ v11, v12, v13, v14 };
        vector<vector<int>> ret = solution.merge(v1);
        cout << "[";
        if (ret.size() > 0) {
            for (int i = 0; i < ret.size(); ++i) {
                cout << "[";
                if (ret[i].size() > 0) {
                    for (int j = 0; j < ret[i].size(); ++j) {
                        if (j < (ret[i].size() - 1))
                            cout << ret[i][j] << ",";
                        else
                            cout << ret[i][j];
                    }
                }
                if (i < (ret.size() - 1))
                    cout << "],";
                else
                    cout << "]";

            }
        }
        cout << "]" << endl;
    }
    {
        vector<int> v11{ 1, 4 };
        vector<int> v12{ 4, 5 };
        vector<vector<int>> v1{ v11, v12 };
        vector<vector<int>> ret = solution.merge(v1);
        cout << "[";
        if (ret.size() > 0) {
            for (int i = 0; i < ret.size(); ++i) {
                cout << "[";
                if (ret[i].size() > 0) {
                    for (int j = 0; j < ret[i].size(); ++j) {
                        if (j < (ret[i].size() - 1))
                            cout << ret[i][j] << ",";
                        else
                            cout << ret[i][j];
                    }
                }
                if (i < (ret.size() - 1))
                    cout << "],";
                else
                    cout << "]";

            }
        }
        cout << "]" << endl;
    }
    {
        vector<int> v11{ 1, 4 };
        vector<int> v12{ 0, 4 };
        vector<vector<int>> v1{ v11, v12 };
        vector<vector<int>> ret = solution.merge(v1);
        cout << "[";
        if (ret.size() > 0) {
            for (int i = 0; i < ret.size(); ++i) {
                cout << "[";
                if (ret[i].size() > 0) {
                    for (int j = 0; j < ret[i].size(); ++j) {
                        if (j < (ret[i].size() - 1))
                            cout << ret[i][j] << ",";
                        else
                            cout << ret[i][j];
                    }
                }
                if (i < (ret.size() - 1))
                    cout << "],";
                else
                    cout << "]";

            }
        }
        cout << "]" << endl;
    }
    {
        vector<int> v11{ 1, 4 };
        vector<int> v12{ 2, 3 };
        vector<vector<int>> v1{ v11, v12 };
        vector<vector<int>> ret = solution.merge(v1);
        cout << "[";
        if (ret.size() > 0) {
            for (int i = 0; i < ret.size(); ++i) {
                cout << "[";
                if (ret[i].size() > 0) {
                    for (int j = 0; j < ret[i].size(); ++j) {
                        if (j < (ret[i].size() - 1))
                            cout << ret[i][j] << ",";
                        else
                            cout << ret[i][j];
                    }
                }
                if (i < (ret.size() - 1))
                    cout << "],";
                else
                    cout << "]";

            }
        }
        cout << "]" << endl;
    }
    {
        vector<int> v11{ 1, 4 };
        vector<int> v12{ 0, 0 };
        vector<vector<int>> v1{ v11, v12 };
        vector<vector<int>> ret = solution.merge(v1);
        cout << "[";
        if (ret.size() > 0) {
            for (int i = 0; i < ret.size(); ++i) {
                cout << "[";
                if (ret[i].size() > 0) {
                    for (int j = 0; j < ret[i].size(); ++j) {
                        if (j < (ret[i].size() - 1))
                            cout << ret[i][j] << ",";
                        else
                            cout << ret[i][j];
                    }
                }
                if (i < (ret.size() - 1))
                    cout << "],";
                else
                    cout << "]";

            }
        }
        cout << "]" << endl;
    }
    return 0;
}
