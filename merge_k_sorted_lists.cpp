#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>


using namespace std;

//Definition for singly-linked list.
struct ListNode {
    int val;
    ListNode* next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode* next) : val(x), next(next) {}
};

class Solution {
public:
    ListNode* mergeKLists(vector<ListNode*>& lists) {
        if (lists.empty()) return NULL;
        int n = lists.size();//8(9)
        while (n > 1) {
            int k = (n + 1) / 2;//4(5),2(3)
            for (int i = 0; i < n / 2; ++i) {
                lists[i] = mergeTwoLists(lists[i], lists[i + k]);
            }
            n = k;//4(5),2(3)
        }

        return lists[0];
    }

    ListNode* mergeTwoLists(ListNode* l1, ListNode* l2) {
        struct ListNode *head = new ListNode(-101);
        struct ListNode* head_tmp = head;
        while (l1 && l2) {
            if (l1->val > l2->val)
            {
                head_tmp->next = l2;
                l2 = l2->next;
            }
            else
            {
                head_tmp->next = l1;
                l1 = l1->next;
            }
            head_tmp = head_tmp->next;
        }
        head_tmp->next = (l1) ? (l1) : (l2);
        return head->next;
    }
};



int main()
{
    Solution s;
    {
        struct ListNode ln_0_0(1);
        struct ListNode ln_0_1(4);
        struct ListNode ln_0_2(5);

        struct ListNode ln_1_0(1);
        struct ListNode ln_1_1(3);
        struct ListNode ln_1_2(4);

        struct ListNode ln_2_0(2);
        struct ListNode ln_2_1(6);

        ln_0_0.next = &ln_0_1;
        ln_0_1.next = &ln_0_2;
        ln_1_0.next = &ln_1_1;
        ln_1_1.next = &ln_1_2;
        ln_2_0.next = &ln_2_1;

        vector<ListNode*> v;
        v.push_back(&ln_0_0);
        v.push_back(&ln_1_0);
        v.push_back(&ln_2_0);
        ListNode* ret = s.mergeKLists(v);
        cout << "[";
        if (ret) {
            ListNode* ret_head = ret;
            while (ret) {
                if (ret->next)
                    cout << ret->val << ",";
                else
                    cout << ret->val;
                ret = ret->next;
            }
        }
        cout << "]" << endl;
    }
    {
        vector<ListNode*> v;
        ListNode* ret = s.mergeKLists(v);
        while (ret) {
            cout << "val=" << ret->val << endl;
            ret = ret->next;
        }
    }
    //{
    //    vector<ListNode*> v;
    //    cout << "ans=" << s.mergeKLists(v) << endl;
    //}

    return 0;
}
