#include "merge_sort.h"
#include <limits>

    void _merge(vector<int> &v, int front, int mid, int end) {
        vector<int> sub_l = vector<int>(v.begin()+front, v.begin()+mid+1);
        vector<int> sub_r = vector<int>(v.begin()+mid+1, v.begin()+end+1);
        sub_l.insert(sub_l.end(), numeric_limits<int>::max());
        sub_r.insert(sub_r.end(), numeric_limits<int>::max());
        int idx_l = 0, idx_r = 0;
        for (int i = front; i <= end; ++i) {
            if (sub_l[idx_l] < sub_r[idx_r]) v[i] = sub_l[idx_l++];
            else                             v[i] = sub_r[idx_r++];
        }
    }

    void merge_sort(vector<int> &v, int front, int end) {

        if (front >= end) return;
        int mid = front + (end - front) / 2;
        merge_sort(v, front, mid);
        merge_sort(v, mid+1, end);
        _merge(v, front, mid, end);
    }

void TestMergeSort(size_t items)
{
    CMergeSort *pMergeSort = new CMergeSort(items);
    if (pMergeSort)
    {
        pMergeSort->MergeSort();
        delete pMergeSort;
    }
}

int main()
{
    static const size_t mSize = 20;
    static const size_t nSize = 10;
    static const size_t mActaulSize = 10;
    static const size_t nActaulSize = 10;
    vector<int> vm(mSize);
    vector<int> vn(nSize);

    SolutionMergeSort s;
    //sequence of number
    if (NUMBER_OF_SEQUENCE == g_mode)
    {
        for (int i = 0; i < mActaulSize; ++i)
        {
            vm[i] = i * 2;
            cout << vm[i] << " ";
        }
        cout << endl;

        for (int i = 0; i < nActaulSize; ++i)
        {
            vn[i] = 1 + i * 2;
            cout << vn[i] << " ";
        }
        cout << endl;
        s.merge(vm, mActaulSize, vn, nActaulSize);
    }
    else if (NORMAL == g_mode)
    {
        TestMergeSort(20);
        //TestMergeSort(200);
    }

    return 0;
}