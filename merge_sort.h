#ifndef __MERGE_SORT_H__
#define __MERGE_SORT_H__

#include <iostream>
#include <vector>
#include "system_time.h"
#include "generate_array.h"

using namespace std;

enum
{
    NUMBER_OF_SEQUENCE,
    NORMAL
};
static const int g_mode = NORMAL; //NUMBER_OF_SEQUENCE;

class SolutionMergeSort
{
  public:
    //if container is <list>. vector.size() will work abnormal.
    void merge(vector<int> &nums1, int m, vector<int> &nums2, int n)
    {
        int i = m - 1;
        int j = n - 1;
        int total = m + n - 1;
        for (j; j >= 0; --j)
        {
            while ((i >= 0) && (nums1[i] > nums2[j]))
                nums1[total--] = nums1[i--];
            nums1[total--] = nums2[j];
        }
    }
    void mergeDebug(vector<int> &nums1, int m, vector<int> &nums2, int n)
    {
        int i = m - 1, j;
        int total = m + n - 1;
        for (j = n - 1; j >= 0; --j)
        {
            cout << "AA nums1[" << i << " ]=" << nums1[i] << endl;
            cout << "AA nums2[" << j << " ]=" << nums2[j] << endl;
            while ((i >= 0) && (j >= 0) && (nums1[i] > nums2[j]))
            {
                nums1[total--] = nums1[i];
                for (int k = 0; k < m + n; k++)
                {
                    cout << "BB nums1[" << k << " ]=" << nums1[k] << endl;
                }
                cout << endl;
                i--;
            }
            cout << "CC total1=" << total << endl;
            nums1[total--] = nums2[j];
            cout << "CC total2=" << total << endl;
        }
    }
    void mergeByInsert(vector<int> &nums1, int m, vector<int> &nums2, int n)
    {
        int i, newi = 0;
        for (int j = 0; j < n; ++j)
        {
            i = newi;
            for (; i < m; ++i)
            {
                if (nums1[i] < nums2[j])
                {
                    i++;
                    break;
                }
            }
            nums1.insert(nums1.begin() + i, nums2[j]);
            newi = i + 1;
            m += 1;
        }
    }
};

class CMergeSort
{
    CGenerateArray *m_pGenerateArray;
    int64_t m_startTime;
    int64_t m_endTime;

  public:
    CMergeSort(size_t arraySize) : m_pGenerateArray(NULL)
    {
        m_pGenerateArray = new CGenerateArray(arraySize);
    }
    ~CMergeSort()
    {
        if (m_pGenerateArray)
        {
            delete m_pGenerateArray;
        }
    }
#ifdef DEBUG
    template <typename T>
    void MergeSortRecursiveDebug(T inArray[], int startIdx, int endIdx, T outArray[])
    {
        //cout << "MergeSortRecursive ";
        //for (int i = startIdx; i <= endIdx; ++i)
        //    cout << inArray[i] << " ";
        //cout << endl;
        if (startIdx < endIdx)
        {
            int mid = (startIdx + endIdx) / 2;
            //cout << "MergeSortRecursive left startIdx=" << startIdx << " mid=" << mid << endl;
            MergeSortRecursive(inArray, startIdx, mid, outArray);
            //cout << "MergeSortRecursive right mid+1=" << mid + 1 << " endIdx=" << endIdx << endl;
            MergeSortRecursive(inArray, mid + 1, endIdx, outArray);
            MergeArray(inArray, startIdx, mid, endIdx, outArray);
            for (int i = startIdx; i <= endIdx; ++i)
                inArray[i] = outArray[i];
            cout << "after MergeArray ";
            for (int i = startIdx; i <= endIdx; ++i)
                cout << outArray[i] << " ";
            cout << endl;
        }
        else if (startIdx == endIdx)
        {
            outArray[endIdx] = inArray[startIdx];
        }
    }

    template <typename T>
    void MergeArrayDebug(T inArray[], int startIdx, int midIdx, int endIdx, T outArray[])
    {
        int m = midIdx - startIdx + 1;
        int n = endIdx - midIdx;
        int i = midIdx;        //m - 1; =  midIdx - startIdx + 1 -1 = > midIdx - startIdx
        int j = endIdx;        //n - 1;
        int totalIdx = endIdx; //m + n - 1;
        //cout << " MergeArray startIdx=" << startIdx << " midIdx=" << midIdx << " endIdx=" << endIdx
        //     << "  m=" << m << " n=" << n << " i=" << i << " totalIdx=" << totalIdx << endl;
        for (j; j > midIdx; --j)
        {
            //cout << " MergeArray inArray[" << i << "]=" << inArray[i] << " inArray[" << j << "]=" << inArray[j] << endl;
            while ((i >= startIdx) && (inArray[i] > inArray[j]))
            {
                //cout << " MergeArray XXXX i=" << i << " startIdx=" << startIdx << endl;
                outArray[totalIdx--] = inArray[i--];
                //cout << " MergeArray loop outArray[" << totalIdx + 1 << "]=" << outArray[totalIdx + 1] << endl;
            }
            outArray[totalIdx--] = inArray[j];
            // cout << " MergeArray last j = " << j << " outArray[" << totalIdx + 1 << "] = " << outArray[totalIdx + 1] << endl;
        }
    }
#endif
    template <typename T>
    void MergeArray(T inArray[], int startIdx, int midIdx, int endIdx, T outArray[])
    {
        int m = midIdx - startIdx + 1;
        int n = endIdx - midIdx;
        int i = midIdx;        //m - 1; =  midIdx - startIdx + 1 -1 = > midIdx - startIdx
        int j = endIdx;        //n - 1;
        int totalIdx = endIdx; //m + n - 1;
        for (j; j > midIdx; --j)
        {
            while ((i >= startIdx) && (inArray[i] > inArray[j]))
                outArray[totalIdx--] = inArray[i--];
            outArray[totalIdx--] = inArray[j];
        }
        for (int i = startIdx; i <= endIdx; ++i)
            inArray[i] = outArray[i];
    }

    template <typename T>
    void MergeSortRecursive(T inArray[], int startIdx, int endIdx, T outArray[])
    {
        if (startIdx < endIdx)
        {
            int midIdx = (startIdx + endIdx) / 2;
            MergeSortRecursive(inArray, startIdx, midIdx, outArray);
            MergeSortRecursive(inArray, midIdx + 1, endIdx, outArray);
            MergeArray(inArray, startIdx, midIdx, endIdx, outArray);
        }
        else if (startIdx == endIdx)
        {
            outArray[endIdx] = inArray[startIdx];
        }
    }

    //always O(nlogn)
    void MergeSort()
    {
        if (!m_pGenerateArray)
            return;
        {
            //int sampleArray[] = {6, 17, 6, 17, 5, 11, 3, 2, 14, 13, 2, 4, 17, 6, 8, 4, 18, 5, 1, 8};
            // int sampleArray2[20];
            int *pGArray;
            int *pOutArray = new int[m_pGenerateArray->m_arraySize];
            if (!pOutArray)
                return;

            m_pGenerateArray->GenerateRandomNumberArray();

            //pGArray = sampleArray;
            pGArray = m_pGenerateArray->m_pArray;

            m_startTime = CSystemTime::getSystemTime();

            MergeSortRecursive(pGArray, 0, m_pGenerateArray->m_itemsInArray, pOutArray);

            m_endTime = CSystemTime::getSystemTime();
            {
                std::cout << "Merge Sort Took " << (m_endTime - m_startTime) << std::endl;
                std::cout << "after" << std::endl;
                for (int i = 0; i < m_pGenerateArray->m_arraySize; ++i)
                    cout << pOutArray[i] << " ";
                cout << endl;
            }
            if (pOutArray)
                delete[] pOutArray;
        }
    }
};

#endif