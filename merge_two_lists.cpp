#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <algorithm>

using namespace std;

//*
// * Definition for singly-linked list.
 struct ListNode {
     int val;
     ListNode *next;
     ListNode() : val(0), next(nullptr) {}
     ListNode(int x) : val(x), next(nullptr) {}
     ListNode(int x, ListNode *next) : val(x), next(next) {}
 };

class Solution {
public:
    ListNode* mergeTwoLists(ListNode* l1, ListNode* l2) {
        struct ListNode head(-101);
        struct ListNode* head_tmp = &head;
        while (l1 && l2) {
            if (l1->val > l2->val)
            {
                head_tmp->next = l2;
                l2 = l2->next;
            }
            else
            {
                head_tmp->next = l1;
                l1 = l1->next;
            }
            head_tmp = head_tmp->next;
        }
        head_tmp->next = (l1) ? (l1) : (l2);
        return head.next;
    }
};

int main()
{
    Solution s;
    {
        struct ListNode *head = s.mergeTwoLists(NULL, NULL);
        int i = 0;
        cout << "[";
        struct ListNode* tmp = head;
        while (tmp) {
            cout << tmp->val;
            if (tmp->next)
                cout << ",";
            tmp = tmp->next;
        }
        cout << "]" << endl;
    }
    {
        struct ListNode l2(0, NULL);
        struct ListNode* head = s.mergeTwoLists(NULL, &l2);
        int i = 0;
        cout << "[";
        struct ListNode* tmp = head;
        while (tmp) {
            cout << tmp->val;
            if (tmp->next)
                cout << ",";
            tmp = tmp->next;
        }
        cout << "]" << endl;
    }
    {
        struct ListNode l_2_0(1);
        struct ListNode l2(0, &l_2_0);
        struct ListNode* head = s.mergeTwoLists(NULL, &l2);
        int i = 0;
        cout << "[";
        struct ListNode* tmp = head;
        while (tmp) {
            cout << tmp->val;
            if (tmp->next)
                cout << ",";
            tmp = tmp->next;
        }
        cout << "]" << endl;
    }
    {
        struct ListNode l_1_4(4);
        struct ListNode l_1_2(2, &l_1_4);
        struct ListNode l1(1, &l_1_2);
        struct ListNode l_2_4(4);
        struct ListNode l_2_3(3, &l_2_4);
        struct ListNode l2(1, &l_2_3);
        struct ListNode* head = s.mergeTwoLists(&l1, &l2);
        int i = 0;
        cout << "[";
        struct ListNode* tmp = head;
        while (tmp) {
            cout << tmp->val;
            if (tmp->next)
                cout << ",";
            tmp = tmp->next;
        }
        cout << "]" << endl;
    }
    return 0;
}
