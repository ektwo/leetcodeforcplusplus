#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <queue>


using namespace std;


//Definition for singly-linked list.
 struct ListNode {
      int val;
      ListNode *next;
      ListNode() : val(0), next(nullptr) {}
      ListNode(int x) : val(x), next(nullptr) {}
      ListNode(int x, ListNode *next) : val(x), next(next) {}
 };


class Solution {

public:

    ListNode* middleNode(ListNode* head) {
        ListNode *fast = head, *slow = head;

        while (fast->next != nullptr && fast->next->next != nullptr) {
            fast = fast->next->next;
            slow = slow->next;
        }

        return (fast->next == nullptr) ? (slow) : (slow->next);
    }

};

int main()
{
    Solution s;
    {
        vector<ListNode*> head;
        ListNode** lna = new ListNode*[5];
        lna[4] = new ListNode(5, NULL);
        lna[3] = new ListNode(4, lna[4]);
        lna[2] = new ListNode(3, lna[3]);
        lna[1] = new ListNode(2, lna[2]);
        lna[0] = new ListNode(1, lna[1]);

        ListNode *ret = s.middleNode(lna[0]);

        while (ret) {
            cout << "val=" << ret->val << endl;
            ret = ret->next;
        }
    }
    {
        vector<ListNode*> head;
        ListNode** lna = new ListNode*[6];
        lna[5] = new ListNode(6, NULL);
        lna[4] = new ListNode(5, lna[5]);
        lna[3] = new ListNode(4, lna[4]);
        lna[2] = new ListNode(3, lna[3]);
        lna[1] = new ListNode(2, lna[2]);
        lna[0] = new ListNode(1, lna[1]);

        ListNode *ret = s.middleNode(lna[0]);

        while (ret) {
            cout << "val=" << ret->val << endl;
            ret = ret->next;
        }
    }
    return 0;
}
