#include <limits.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <queue>


using namespace std;


bool isBadVersion(int version)
{
    return false;
}

class Solution {

public:

    int minCostClimbingStairs(vector<int>& cost) {
        if (cost.size() <= 1) return cost[0];
        int n = cost.size();
        vector<int> dp(n+1, 0);

        /* fibonacci */
        for (int i = 2; i < n + 1; ++i) {
            dp[i] = min(dp[i - 1]+cost[i-1], dp[i - 2] + cost[i-2]);
        }

        return dp.back();
    }
};

int main()
{
    Solution solution;

    {
        vector<int> cost = { 10, 15, 20 };
        int ans = solution.minCostClimbingStairs(cost);
        cout << "solution=" << ans << endl;
    }
cout << "next=" << endl;
    {
        vector<int> cost = { 1, 100, 1, 1, 1, 100, 1, 1, 100, 1 };
        int ans = solution.minCostClimbingStairs(cost);
        cout << "solution=" << ans << endl;
    }
    return 0;
}
