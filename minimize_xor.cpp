
#include <limits.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <queue>
#include <limits>


using namespace std;


struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};


template<class T>
void print_1d(const T& nums)
{
    cout << "{";
    for (int j = 0; j < nums.size(); ++j) {
        cout << nums[j] << ",";
    }
    cout << "}," << endl;
}
template<class T>
void print_2d(const T& nums)
{
    cout << "{";
    for (int i = 0; i < nums.size(); ++i) {
        cout << "{";
        for (int j = 0; j < nums[i].size(); ++j) {
            cout << nums[i][j] << ",";
        }
        cout << "}," << endl;
    }
    cout << "}," << endl;
}

void print_tree_preorder(TreeNode *root)
{
    if (root) {
        cout << root->val << ",";
        if (root->left) print_tree_preorder(root->left);
        if (root->right) print_tree_preorder(root->right);
    }
    else
        cout << "null,";
}

void print_tree_complete(TreeNode *root)
{
    // if (root) {
    //     cout << root->val << ",";
    //     if (root->left) print_tree_preorder(root->left);
    //     else cout << "null,";
    //     if (root->right) print_tree_preorder(root->right);
    //     else cout << "null,";
    // }
    // else
    //     cout << "null,";
    if (!root) return;
    vector<vector<int>> ans;

    queue<TreeNode*> q;
    q.push(root);
    cout << root->val <<","; 
    while (!q.empty()) {
        int size=q.size();
        for (int i = size; i > 0; --i) {
            TreeNode *tmp = q.front(); q.pop();
            if (!tmp) break;
            if (!tmp->left && !tmp->right) continue;

            if (tmp->left) {q.push(tmp->left); cout << tmp->left->val <<",";}
            else cout << "lnull,";

            if (tmp->right) {q.push(tmp->right); cout << tmp->right->val <<",";}
            else cout << "rnull,";
        }
    }
}

void print_tree_levelorder(TreeNode *root) {
    if (!root) return;
    vector<vector<int>> ans;

    queue<TreeNode*> q;
    q.push(root);
    while (!q.empty()) {
        for (int i = q.size(); i > 0; --i) {
            TreeNode *tmp = q.front(); q.pop();
            if (!tmp) break;

            if (tmp->left) q.push(tmp->left);
            if (tmp->right) q.push(tmp->right);
        }
    }
}


class CTreeNode {
private:
    TreeNode *m_head;
public:
    void reset() {
        m_head = nullptr;
    }

    TreeNode* get_head() {
        return m_head;
    }

    TreeNode* push(TreeNode *root, int val, bool left, bool right)
    {
        TreeNode* temp = new TreeNode(val);
        if (!m_head) {
            m_head = temp;
        }

        if (!root)
            root = m_head;

        if (root && left)
            root->left = temp;
        else if (root && right)
            root->right = temp;

        return temp;
    }
};

class Solution {
private:
    int calc_bits(int num) {
        unsigned char Num = 0;

        while (num)
        {
            Num++;
            num = (num - 1) & num;
        }

        return Num;
    }
public:
    int minimizeXor(int num1, int num2) {
        int count=calc_bits(num2);

        int i;
        int fres=0;
        for (int i=31;i>=0 && count>0;--i) {
            if (num1 & (1<<i)) {
                fres|=(1<<i);
                cout << "A num1=" << num1 << " 1<<i=" << (1<<i) << " count=" << count << " fres=" << fres << endl;
                --count;
            }
        }
        for (int i=0;i<=31 && count>0;++i) {
            if (!(fres&(1<<i))) {
                cout << "B num1=" << num1 << " 1<<i=" << (1<<i) << " count=" << count << " fres=" << fres << endl;
                fres|=(1<<i);
                cout << "C num1=" << num1 << " 1<<i=" << (1<<i) << " count=" << count << " fres=" << fres << endl;
                --count;
            }
        }
        return fres;
    }
};


int main()
{
    Solution solution;
    {
        int num1=3;
        int num2=5;
        int ans=solution.minimizeXor(num1,num2);
        cout << ans;
    }
    cout << endl;
    {
        int num1=1;
        int num2=12;
        int ans=solution.minimizeXor(num1,num2);
        cout << ans;
    }
    cout << endl;
    {
        int num1=64;
        int num2=40;
        int ans=solution.minimizeXor(num1,num2);
        cout << ans;
    }
    cout << endl;
    {
        int num1=25;
        int num2=72;
        int ans=solution.minimizeXor(num1,num2);
        cout << ans;
    }
    return 0;
}
