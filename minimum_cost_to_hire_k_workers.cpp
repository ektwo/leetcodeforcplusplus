#include <limits>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <queue>


using namespace std;


class Solution {

public:
    // 1.0 / cost-effective from low to high.
    static bool cmp(vector<int> &n1, vector<int> &n2) {
        return (double)n1[1]/n1[0] < (double)n2[1]/n2[0];
    }
    double mincostToHireWorkers(vector<int>& quality, vector<int>& wage, int k) {
        double ans=0x3f3f3f3f;

        vector<vector<int>> workers_cp_ratio;// a ratio sorted by cost-effective from high to low.

        for (int i = 0; i < quality.size(); ++i) {
            workers_cp_ratio.push_back({quality[i], wage[i]});
        }

        sort(workers_cp_ratio.begin(), workers_cp_ratio.end(), cmp);
        for (int i = 0; i < workers_cp_ratio.size(); ++i) {
            for (int j = 0; j < workers_cp_ratio[0].size(); ++j) {
                cout << "i=" << i << " j=" << j << " workers_cp_ratio[i][j]=" << workers_cp_ratio[i][j] << endl;
            }
        }

        int tmp_qualities = 0;
        priority_queue<int> q;
        for (auto &worker_cp : workers_cp_ratio) {

            int work_quality = worker_cp[0];
            cout << "worker_cp[0]=" << worker_cp[0] << " worker_cp[1]=" << worker_cp[1] << endl;
            if (q.size() < k)
            {
                q.push(work_quality);
                for (int j = 0; j < q.size(); ++j ) {
                    cout << "j=" << j << " q.top()=" << q.top() << endl;
                }
                tmp_qualities += work_quality;
            }
            else {
                if (work_quality >= q.top()) continue;
                tmp_qualities = tmp_qualities - q.top() + work_quality;
                q.pop();
                q.push(work_quality);
            }

            if (q.size() == k) ans = min(ans, ((double) (tmp_qualities * worker_cp[1])) / worker_cp[0]);
        }

        return ans;
    }
};

int main()
{
    Solution solution;
    {
        vector<int> quality = { 10,20,5 };
        vector<int> wage = { 70, 50 , 30 };
        cout << solution.mincostToHireWorkers(quality, wage, 2) << endl;
    }

    {
        vector<int> quality = { 3,1,10,10,1};
        vector<int> wage = { 4,8,2,2,7 };
        cout << solution.mincostToHireWorkers(quality, wage, 3) << endl;
    }

    return 0;
}
