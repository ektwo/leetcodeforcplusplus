#include <limits>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <queue>
#include <cmath>


using namespace std;


class Solution {

public:
    int minSubArrayLen(int target, vector<int>& nums) {
        int n = nums.size();
        int win_l = 0;
        int win_sum = 0;
        int min_win_size = numeric_limits<int>::max();

        for (int win_r = 0; win_r < n; ++win_r) {
            win_sum += nums[win_r];

            while (win_sum >= target) {
                min_win_size = min(min_win_size, win_r -  win_l + 1);
                win_sum -= nums[win_l];
                win_l++;
            }
        }

        return (min_win_size == numeric_limits<int>::max()) ? (0) : (min_win_size);
    }
};

int main()
{
    Solution solution;
    {
        int target = 7;
        vector<int> nums = {2,3,1,2,4,3};
        int ans = solution.minSubArrayLen(target, nums);
        cout << "ans=" << ans << endl;
        // for (int i = 0; i < ans.size(); ++i) {
        //     cout << ans[i] << ",";
        // }
        // cout << endl;
    }

    {
        int target = 4;
        vector<int> nums = {1,4,4};
        int ans = solution.minSubArrayLen(target, nums);
        cout << "ans=" << ans << endl;
        // for (int i = 0; i < ans.size(); ++i) {
        //     cout << ans[i] << ",";
        // }
        // cout << endl;
    }
    {
        int target = 11;
        vector<int> nums = {1,1,1,1,1,1,1,1};
        int ans = solution.minSubArrayLen(target, nums);
        cout << "ans=" << ans << endl;
        // for (int i = 0; i < ans.size(); ++i) {
        //     cout << ans[i] << ",";
        // }
        // cout << endl;
    }
    return 0;
}
