#include <iostream>
#include <vector>
#include <string>
#include <unordered_map>

using namespace std;

class Solution {
public:

    string minWindow(string s, string t) {
        if (t.length() > s.length()) return string("");

        string ans;

        int min = 0;

        unordered_map<char, int> str_int;
        for (char c : t)
            ++str_int[c];

        int slide_window_cnt = 0;
        int slide_window_max = INT_MAX;
        int left = 0;
        for (int right = 0; right < s.length(); ++right) {
            if (--str_int[s[right]] >= 0)
                ++slide_window_cnt;
            while (slide_window_cnt == t.length()) {

                if (slide_window_max > (right - left + 1)) {
                    slide_window_max = (right - left + 1);
                    ans = s.substr(left, slide_window_max);
                }
                if (++str_int[s[left]] > 0) --slide_window_cnt;
                ++left;
            }
        }

        return ans;
    }
};

int main()
{
    Solution solution;
    {
        string s("ADOBECODEBANC");
        string t("ABC");
        cout << solution.minWindow(s, t) << endl;
    }
    {
        string s("a");
        string t("a");
        cout << solution.minWindow(s, t) << endl;
    }
    {
        string s("a");
        string t("aa");
        cout << solution.minWindow(s, t) << endl;
    }

    return 0;
}