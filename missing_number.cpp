#include <limits.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <queue>


using namespace std;


class Solution {

public:
    int missingNumber(vector<int>& nums) {
        int res = 0 ;
        for ( int i = 0 ; i < nums.size(); ++ i) {
            res ^= i ^ nums[i];
        }
        res ^= nums.size();
        return res;
    }

};

int main()
{
    Solution solution;
    {
        vector<int> nums = { 3,0,1 };
        cout << solution.missingNumber(nums) << endl;
    }

    {
        vector<int> nums = { 0,1 };
        cout << solution.missingNumber(nums) << endl;
    }

    {
        vector<int> nums = { 9,6,4,2,3,5,7,0,1 };
        cout << solution.missingNumber(nums) << endl;
    }

    return 0;
}
