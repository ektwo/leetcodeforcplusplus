#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>


using namespace std;



class Solution {

private:
    string get_range_string(int lower, int upper) {
        if (lower == upper)
            return to_string(lower);
        return to_string(lower) + "->" + to_string(upper);
    }
public:
    vector<string> findMissingRanges(vector<int>& nums, int lower, int upper) {

        if (nums.empty()) return { get_range_string(lower, upper) };

        vector<string> ans;

        int left = lower;
        int nums_idx = 0;
        string tmp;

  
        if (nums.front() > lower)
            ans.push_back(get_range_string(lower, nums.front() - 1));

        for (nums_idx = 1; nums_idx < nums.size(); ++nums_idx) {
            if ((nums[nums_idx - 1] + 1) < nums[nums_idx]) {
                ans.push_back(get_range_string(nums[nums_idx - 1] + 1, nums[nums_idx] - 1));
            }

        }

        if (nums.back() < upper)
            ans.push_back(get_range_string(nums.back() + 1, upper));

        return ans;
    }
};

int main()
{
    Solution s;
    {
        vector<int> nums = { 0,1,3,50,75 };
        vector<string> ans = s.findMissingRanges(nums, 0, 99);
        for (int i = 0; i < ans.size(); ++i)
            cout << ans[i] << endl;
    }
    //{
    //    vector<int> nums = { 0,1,3,50,75 };
    //    vector<string> ans = s.findMissingRanges(nums, 0, 99);
    //    for (int i = 0; i < ans.size(); ++i)
    //        cout << ans[i] << endl;
    //}

    return 0;
}
