#include <limits.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <queue>


using namespace std;


class Solution {


public:
    string mostCommonWord(string paragraph, vector<string>& banned) {
        string ans = "";

        string tmp_paragraph = "";
        unordered_set<string> ban(banned.begin(), banned.end());
        unordered_map<string, int> m;
        for (auto &c : paragraph) {
            c = isalpha(c) ? tolower(c) : ' ';
            tmp_paragraph.append(1, c);
        }
        tmp_paragraph.append(1, ' ');

        int max_idx = 0;
        string tmp = "";
        for (auto &c : tmp_paragraph) {
            if (c == ' ') {
                if (!tmp.empty() && !ban.count(tmp)) {
                    m[tmp]++;
                    if (max_idx < m[tmp]) {
                        max_idx = m[tmp];
                        ans = tmp;
                    }
                }
                tmp = "";
            }
            else {
                tmp.append(1, c);
            }
        }

        return ans;
    }

};

int main()
{
    Solution solution;
    {
        string paragraph = "Bob hit a ball, the hit BALL flew far after it was hit.";
        vector<string> banned = { "hit" };
        cout << solution.mostCommonWord(paragraph, banned) << endl;
    }

    {
        string paragraph = "a.";
        vector<string> banned = { "" };
        cout << solution.mostCommonWord(paragraph, banned) << endl;
    }

    {
        string paragraph = "Bob";
        vector<string> banned = { "" };
        cout << solution.mostCommonWord(paragraph, banned) << endl;
    }
    {
        string paragraph = "Bob. hIt, baLl";
        vector<string> banned = { "bob", "hit" };
        cout << solution.mostCommonWord(paragraph, banned) << endl;
    }
    return 0;
}
