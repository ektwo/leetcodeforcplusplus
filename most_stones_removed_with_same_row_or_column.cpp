#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>


using namespace std;

//Union by rank：將rank小的樹 變成 rank大的樹 的兒子
//Path compression：將路上遇到的點都變成root的兒子


class UnionFind {
private:
	unordered_map<int, int> parent;
	unordered_map<int, int> rank;

public:
	int count;

	UnionFind(vector<vector<int>>& stones) {
		for (auto& stone : stones) {
			int row = -(stone[0] + 1);
			int col = stone[1] + 1;
			//針對每個 land 節點先將其 parent 設為自己
			parent[row] = row;
			parent[col] = col;
		}
		count = parent.size();
	}

	int find(int x) {
		if (parent[x] != x) parent[x] = find(parent[x]);
		return parent[x];
	}

	void union_(int x, int y) {
		int xset = find(x);
		int yset = find(y);
		if (xset == yset) return;
		count--;

		if (rank[xset] < rank[yset]) parent[xset] = yset;
		else if (rank[xset] > rank[yset]) parent[yset] = xset;
		else {
			parent[yset] = xset;
			rank[xset] = rank[yset] + 1;
		}
	}
};
class Solution 
{
public:
	int removeStones(vector<vector<int>>& stones) {
		UnionFind uf(stones);
		for (auto& stone : stones) {
			int row = -(stone[0] + 1);
			int col = stone[1] + 1;
			uf.union_(row, col);
		}

		return stones.size() - uf.count;
	}
#if 0
	int union_find(vector<vector<int>>& clues, vector<vector<int>>& newArray) {
		for (auto& clue : clues) {
			int leader_idx = clue[0];//1,3,2
			int member_idx = clue[1];//2,4,5
			cout << "clue[0]=" << clue[0] << " clue[1]=" << clue[1] << endl;
			int parent_group_id = -1;
			//for (auto& array : newArray) {
			for (int i = 0; i < newArray.size(); ++i) {
				int mycurr_group_id = newArray[i][0];//1,2,3,4,5,6,7,8,9
				int belong_group_id = newArray[i][1];//1,2,3,4,5,6,7,8,9
				cout << "1 mycurr_group_id=" << mycurr_group_id << " belong_group_id=" << belong_group_id << endl;
				if (mycurr_group_id == leader_idx) {
					parent_group_id = belong_group_id;
				}
				//if (parent_group_id != -1) {
				//	if (mycurr_group_id == member_idx) {
				//		newArray[i][1] = parent_group_id;
				//	}
				//}
			}
			for (int i = 0; i < newArray.size(); ++i) {
				int mycurr_group_id = newArray[i][0];//1,2,3,4,5,6,7,8,9
				int belong_group_id = newArray[i][1];//1,2,3,4,5,6,7,8,9
				cout << "2 mycurr_group_id=" << mycurr_group_id << " belong_group_id=" << belong_group_id << endl;
				//if (mycurr_group_id == leader_idx) {
				//	parent_group_id = belong_group_id;
				//}
				if (parent_group_id != -1) {
					if (mycurr_group_id == member_idx) {
						newArray[i][1] = parent_group_id;
					}
				}
			}
		}

		return 0;
	}
#endif
#if 0
	int _union_find(vector<vector<int>>& clues, unordered_map<int, pair<int, vector<int>>>& nodes) {
		return nodes.size();
	}
	int union_find(vector<vector<int>>& clues, vector<vector<int>>& newArray) {
		//id, rank, parent
		unordered_map<int, pair<int, vector<int>>> nodes;
		for (int i = 0; i < newArray.size(); ++i) {
			vector<int> parent;
			nodes[i] = make_pair(0, parent);
		}

		return _union_find(clues, nodes);
		//for (auto& clue : clues) {
		//	int leader_idx = clue[0];//1,3,2
		//	int member_idx = clue[1];//2,4,5
		//	cout << "clue[0]=" << clue[0] << " clue[1]=" << clue[1] << endl;
		//	int parent_group_id = -1;
		//	//for (auto& array : newArray) {
		//	for (int i = 0; i < newArray.size(); ++i) {
		//		int mycurr_group_id = newArray[i][0];//1,2,3,4,5,6,7,8,9
		//		int belong_group_id = newArray[i][1];//1,2,3,4,5,6,7,8,9
		//		cout << "1 mycurr_group_id=" << mycurr_group_id << " belong_group_id=" << belong_group_id << endl;
		//		if (mycurr_group_id == leader_idx) {
		//			parent_group_id = belong_group_id;
		//		}
		//		//if (parent_group_id != -1) {
		//		//	if (mycurr_group_id == member_idx) {
		//		//		newArray[i][1] = parent_group_id;
		//		//	}
		//		//}
		//	}
		//	for (int i = 0; i < newArray.size(); ++i) {
		//		int mycurr_group_id = newArray[i][0];//1,2,3,4,5,6,7,8,9
		//		int belong_group_id = newArray[i][1];//1,2,3,4,5,6,7,8,9
		//		cout << "2 mycurr_group_id=" << mycurr_group_id << " belong_group_id=" << belong_group_id << endl;
		//		//if (mycurr_group_id == leader_idx) {
		//		//	parent_group_id = belong_group_id;
		//		//}
		//		if (parent_group_id != -1) {
		//			if (mycurr_group_id == member_idx) {
		//				newArray[i][1] = parent_group_id;
		//			}
		//		}
		//	}
		//}

		//return 0;
	}
#endif
};

int main()
{
	Solution solution;
	{
		vector<vector<int>> clues = {
			{0, 0},
			{0, 1},
			{1, 0},
			{1, 2},
			{2, 1},
			{2, 2},
		};
		//solution.union_find(clues);
	}
	{
		vector<vector<int>> clues = {
			//{1, 5},
			//{2, 7},
			//{2, 5},
			{1, 2},
			{3, 4},
			{5, 2},
			//{4, 6},
			//{2, 6},
			//{8, 7},
			//{9, 7},
			//{1, 6},
			//{2, 4},
		};
		//vector<vector<int>> newArray;
		//newArray.resize(10, vector<int>(2, 0));
		//int i = 1;
		//for (auto& array : newArray) {
		//	array[0] = i;
		//	array[1] = i;
		//	i++;
		//}
		//cout << solution.union_find(clues, newArray) << endl;
		//cout << "{";
		//for (auto & a : newArray) {
		//	cout << a[0] << ", ";
		//}
		//cout << "}" << endl;
		//cout << "{";
		//for (auto& b : newArray) {
		//	cout << b[1] << ", ";
		//}
		//cout << "}" << endl;
	}
	return 0;
}