#include <iostream>
#include <vector>
#include <string>

using namespace std;


void print_1d_int(vector<int> nums)
{

        cout << "{";
        for (int j = 0; j < nums.size(); ++j) {
            cout << nums[j] << ",";
        }
        cout << "}," << endl;

}

class Solution
{

public:
    void moveZeroes(vector<int>& nums) {
        int zero_idx = 0;
        for (int non_zero_idx = 0; non_zero_idx < nums.size(); ++non_zero_idx) {
            if (nums[non_zero_idx]) {
                swap(nums[non_zero_idx], nums[zero_idx++]);
            }
        }
    }
};

int main()
{
    Solution solution;
    {
        vector<int> nums = {0,1,0,3,12};
        solution.moveZeroes(nums);
        print_1d_int(nums);
    }
    cout << endl;
    {
        vector<int> nums = {0};
        solution.moveZeroes(nums);
        print_1d_int(nums);
    }

    return 0;
}
