#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// function to swap elements
void swap(int* a, int* b) {
    int t = *a;
    *a = *b;
    *b = t;
}

// function to find the partition position
int partition(int array[], int low, int high) {

    // select the rightmost element as pivot
    int pivot = array[high];

    // pointer for greater element
    int i = (low - 1);

    // traverse each element of the array
    // compare them with the pivot
    for (int j = low; j < high; j++) {
        if (array[j] <= pivot) {

            // if element smaller than pivot is found
            // swap it with the greater element pointed by i
            i++;

            // swap element at i with element at j
            swap(&array[i], &array[j]);
        }
    }

    // swap the pivot element with the greater element at i
    swap(&array[i + 1], &array[high]);

    // return the partition point
    return (i + 1);
}

void quickSort(int array[], int low, int high) {
    if (low < high) {
        // find the pivot element such that
        // elements smaller than pivot are on left of pivot
        // elements greater than pivot are on right of pivot
        int pivot_pos = partition(array, low, high);
        quickSort(array, low, pivot_pos - 1);//sorts the left side of pivot.
        quickSort(array, pivot_pos + 1, high);//sorts the right side of pivot.
    }
}

// function to print array elements
void printArray(int array[], int size) {
    for (int i = 0; i < size; ++i) {
        printf("%d  ", array[i]);
    }
    printf("\n");
}

void printArray_before_swap(int array[], int size) {
    printf("before : ");
    for (int i = 0; i < size; ++i) {
        printf("%d  ", array[i]);
    }
    printf("\n");
}

void printArray_after_swap(int array[], int size) {
    printf("after : ");
    for (int i = 0; i < size; ++i) {
        printf("%d  ", array[i]);
    }
    printf("\n");
}
int n = 0;
int partition2(int array[], int low, int high) {

#if 0
    const int pivot_pos = low;
    int pivot = array[pivot_pos];
    int i = low + 1;

    for (int j = low + 1; j <= high; j++) {
        if (array[j] < pivot) {

            printArray_before_swap(array, n);
            swap(&array[i], &array[j]);
            printArray_after_swap(array, n);
            i++;
            printf("i become to : %d  \n", i);
        }
    }

    const int ret_pivot_pos = i - 1;
    swap(&array[pivot_pos], &array[ret_pivot_pos]); //put the pivot element in its proper place.

    return ret_pivot_pos;
#else
    const int pivot_pos = low;
    int pivot = array[pivot_pos];
    int i = low + 1;

    for (int j = low + 1; j <= high; j++) {
        if (array[j] < pivot) {

            printArray_before_swap(array, n);
            swap(&array[i], &array[j]);
            printArray_after_swap(array, n);
            i++;
            printf("i become to : %d  \n", i);
        }
    }

    const int ret_pivot_pos = i - 1;
    swap(&array[pivot_pos], &array[ret_pivot_pos]); //put the pivot element in its proper place.

    return ret_pivot_pos;
#endif
}

int partition3(int array[], int leftmostIndex, int rightmostIndex) {
    const int pivot_pos = leftmostIndex;
    const int pivot = array[pivot_pos];
    int pos_between_partitioned_left_and_right = leftmostIndex + 1;
    int pos_between_parittioned_and_unpartitioned = leftmostIndex + 1;
    for (; pos_between_parittioned_and_unpartitioned <= rightmostIndex; ++pos_between_parittioned_and_unpartitioned) {
        printArray_before_swap(array, n);
        printf("array[pos_between_parittioned_and_unpartitioned]=%d\n", array[pos_between_parittioned_and_unpartitioned]);
        if (array[pos_between_parittioned_and_unpartitioned] < pivot) {
            swap(&array[pos_between_partitioned_left_and_right], &array[pos_between_parittioned_and_unpartitioned]);
            printArray_after_swap(array, n);
            ++pos_between_partitioned_left_and_right;
        }
    }
    printf("pos_between_partitioned_left_and_right=%d\n", pos_between_partitioned_left_and_right);
    printf("pos_between_parittioned_and_unpartitioned=%d\n", pos_between_parittioned_and_unpartitioned);
    int pos_pivot_proper_place = pos_between_partitioned_left_and_right - 1;
    swap(&array[pivot_pos], &array[pos_pivot_proper_place]);

    return pos_pivot_proper_place;
}

int partition4(int array[], int leftmostIndex, int rightmostIndex) {
    const int pivot_pos = leftmostIndex;
    const int pivot = array[pivot_pos];
    int the_biggest_idx_val_small_than_pivot = leftmostIndex + 1;
    int unpartitioned_idx = leftmostIndex + 1;

    for (; unpartitioned_idx <= rightmostIndex; ++unpartitioned_idx) {

        printArray_before_swap(array, n);
        //printf("array[unpartitioned_idx]=%d\n", array[unpartitioned_idx]);

        if (array[unpartitioned_idx] < pivot) {

            if (the_biggest_idx_val_small_than_pivot != unpartitioned_idx) {
                swap(&array[the_biggest_idx_val_small_than_pivot], &array[unpartitioned_idx]);
                printArray_after_swap(array, n);
            }

            ++the_biggest_idx_val_small_than_pivot;
        }
    }

    //printf("the_biggest_idx_val_small_than_pivot=%d\n", the_biggest_idx_val_small_than_pivot);
    //printf("unpartitioned_idx=%d\n", unpartitioned_idx);

    int pos_pivot_proper_place = the_biggest_idx_val_small_than_pivot - 1;
    swap(&array[pivot_pos], &array[pos_pivot_proper_place]);

    return pos_pivot_proper_place;
}

int partition5(int array[], int leftmostIndex, int rightmostIndex) {
    const int pivot_pos = leftmostIndex;
    const int pivot = array[pivot_pos];
    int the_biggest_idx_val_small_than_pivot = leftmostIndex + 1;
    int unpartitioned_idx = leftmostIndex + 1;

    for (; unpartitioned_idx <= rightmostIndex; ++unpartitioned_idx) {

        if (array[unpartitioned_idx] < pivot) {

            if (the_biggest_idx_val_small_than_pivot != unpartitioned_idx) {
                swap(&array[the_biggest_idx_val_small_than_pivot], &array[unpartitioned_idx]);
            }

            ++the_biggest_idx_val_small_than_pivot;
        }
    }

    int pos_pivot_proper_place = the_biggest_idx_val_small_than_pivot - 1;
    swap(&array[pivot_pos], &array[pos_pivot_proper_place]);

    return pos_pivot_proper_place;
}

int partition6(int array[], int leftmostIndex, int rightmostIndex) {

    const int pivot_pos = leftmostIndex;
    const int pivot = array[pivot_pos];

    int the_biggest_idx_small_than_pivot = leftmostIndex + 1;
    int unpartitioned_idx = leftmostIndex + 1;

    for (; unpartitioned_idx <= rightmostIndex; ++unpartitioned_idx) {

        if (array[unpartitioned_idx] < pivot) {

            if (the_biggest_idx_small_than_pivot != unpartitioned_idx)
                swap(&array[the_biggest_idx_small_than_pivot], &array[unpartitioned_idx]);

            ++the_biggest_idx_small_than_pivot;
        }
    }

    int new_pivot_pos = the_biggest_idx_small_than_pivot - 1;
    swap(&array[pivot_pos], &array[new_pivot_pos]);

    return new_pivot_pos;
}

void quick_sort(int array[], int leftmostIndex, int rightmostIndex)
{
    if (leftmostIndex < rightmostIndex) {
        int pivot_pos = partition6(array, leftmostIndex, rightmostIndex);   //find the pivot element such that
        quick_sort(array, leftmostIndex, pivot_pos - 1);                  //sorts the left side of pivot.
        quick_sort(array, pivot_pos + 1, rightmostIndex);                 //sorts the right side of pivot.
    }
}


int main()
{
	int data[] = { 8, 7, 2, 1, 0, 9, 6 };
    int data2[] = { 6, 7, 2, 1, 9, 0, 8 };

	n = sizeof(data) / sizeof(data[0]);

	printf("Unsorted Array\n");
	printArray(data, n);

	// perform quicksort on data
	quickSort(data, 0, n - 1);
    quick_sort(data2, 0, n - 1);


	printf("Sorted array in ascending order: \n");
	printArray(data, n);
    printArray(data2, n);
}