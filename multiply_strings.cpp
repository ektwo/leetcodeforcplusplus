#include <iostream>
#include <vector>
#include <string>

using namespace std;

class Solution
{
public:
    string multiply(string num1, string num2) {
        if (num1[0] == '0' || num2[0] == '0') return string("0");

        vector<int> num(num1.size() + num2.size(), 0);

        for (int i = num1.size() - 1; i >= 0; --i) {
            for (int j = num2.size() - 1; j >= 0; --j) {
                num[i + j + 1] += (num1[i] - '0') * (num2[j] - '0');
                num[i + j] += num[i + j + 1] / 10;
                num[i + j + 1] %= 10;

            }
        }

        string s = "";

        int x = 0;
        while (x < num.size() && num[x] == 0) ++x;

        while (x < num.size()) {
            s.push_back(num[x++] + '0');
        }

        return s;
    }
};

int main()
{
    Solution s;

    cout << s.multiply("2", "3") << endl;
    cout << s.multiply("123", "456") << endl;

    return 0;
}