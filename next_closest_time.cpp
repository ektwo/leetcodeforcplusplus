#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>


using namespace std;



class Solution {

public:

    string nextClosestTime(string time) {

        string ans;
        int hours = stoi(time.substr(0, 2));
        //cout << "hours=" << hours << endl;
        int mins = stoi(time.substr(3));
        //cout << "mins=" << mins << endl;
        int total_time = hours * 60 + mins;
        //cout << "total_time=" << total_time << endl;

        unordered_set<int> digits;
        for (auto &c : time) {
            if (c != ':') {
                //cout << "c=" << c << endl;
                digits.insert(c - '0');
            }
        }
        // int ii = 0;
        // for (const auto &d : digits) {
        //     cout << "ii=" << ii << " digits=" << d << endl;
        //     ++ii;
        // }
        // ii = 0;
        // for (std::unordered_set<int>::iterator i = digits.begin(); i != digits.end(); ++i ) {
        //     cout << "ii=" << ii << " digits=" << *i << endl;
        //     ++ii;
        // }

        int a = 0;
        while (1) {
            total_time = (total_time + 1) % (24 * 60);
            int next_time[4] = { total_time / 60 / 10, total_time / 60 % 10, total_time % 60 / 10, total_time % 60 % 10 };
            //cout << "1=" << next_time[0] << endl;
            //cout << "2=" << next_time[1] << endl;
            //cout << "3=" << next_time[2] << endl;
            //cout << "4=" << next_time[3] << endl;
            bool is_valid = true;
            for (int i = 0; i < 4; ++i) {
                if (digits.find(next_time[i]) == digits.end()) {
                    //cout << "next_time[" << i << "]=" << next_time[i] << endl;
                    is_valid = false;
                }
            }

            if (is_valid) {
                char buf[16];
                std::snprintf(buf, 16, "%02d:%02d", total_time / 60, total_time % 60);
                ans = buf;
                return ans;
            }
            ++a;
        }

        return ans;
    }
};

int main()
{
    Solution s;
    {
        string str = "19:34";
        cout << s.nextClosestTime(str) << endl;
    }
    {
        string str = "23:59";
        cout << s.nextClosestTime(str) << endl;
    }

    return 0;
}
