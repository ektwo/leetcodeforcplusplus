#include <limits.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <queue>
#include <stack>


using namespace std;

void print_1d_int(vector<int> nums)
{
    cout << "{";
    for (int j = 0; j < nums.size(); ++j) {
        cout << nums[j] << ",";
    }
    cout << "}," << endl;
}
class Solution {
public:
    vector<int> nextGreaterElement(vector<int>& nums1, vector<int>& nums2) {

        vector<int> ans;
        unordered_map<int, int> m;
        stack<int> st;

        for (int i = 0; i < nums2.size(); ++i) {
            int num = nums2[i];
            while (!st.empty()) {
                int stacked_idx = st.top();
                int stacked_num = nums2[stacked_idx];
                if (stacked_num < num) { m[stacked_num] = num; st.pop(); continue; }
                break;
            }
            st.push(i);
        }
        for (int i = 0; i < nums1.size(); ++i) {
            int num = nums1[i];
            if (m.count(num)) {
                ans.push_back(m[num]);
            }
            else
                ans.push_back(-1);
        }
        return ans;
    }
};

int main()
{
    Solution solution;

    {
        vector<int> nums1 = { 4,1,2};
        vector<int> nums2 = { 1,3,4,2};
        vector<int> ans = solution.nextGreaterElement(nums1, nums2);
        print_1d_int(ans);
    }
cout << "next=" << endl;
    {
        vector<int> nums1 = { 2,4};
        vector<int> nums2 = { 1,2,3,4};   
        vector<int> ans = solution.nextGreaterElement(nums1, nums2);
        print_1d_int(ans);
    }
cout << "next=" << endl;
    {
        vector<int> nums1 = { 1,3,5,2,4};
        vector<int> nums2 = { 6,5,4,3,2,1,7};   
        vector<int> ans = solution.nextGreaterElement(nums1, nums2);
        print_1d_int(ans);
    }
    return 0;
}
