#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <algorithm>

using namespace std;


class Solution {
public:
    void nextPermutation(vector<int>& nums) {
        int size = nums.size();
        if (size == 1) return;
        if (size == 2) { reverse(nums.begin(), nums.end()); return; }
        int i, j, n = nums.size();
        for (i = n - 2; i >= 0; --i) {
            if (nums[i + 1] > nums[i]) {
                for (j = n - 1; j > i; --j) {
                    if (nums[j] > nums[i]) break;
                }
                swap(nums[i], nums[j]);
                reverse(nums.begin() + i + 1, nums.end());
                return;
            }
        }
        reverse(nums.begin(), nums.end());
    }
};

int main()
{
    Solution s;
    {
        vector<int> v{ 1, 2, 7, 4, 3, 1 };
        s.nextPermutation(v);
        int i = 0;
        cout << "[";
        for (int i = 0; i < v.size(); ++i) {
            if (i != (v.size() - 1))
                cout << v[i] << ",";
            else
                cout << v[i];
        }
        cout << "]" << endl;
    }

    {
        vector<int> v{ 1, 2, 3, 4 };
        s.nextPermutation(v);
        int i = 0;
        cout << "[";
        for (int i = 0; i < v.size(); ++i) {
            if (i != (v.size() - 1))
                cout << v[i] << ",";
            else
                cout << v[i];
        }
        cout << "]" << endl;
    }

    {
        vector<int> v{ 1, 2, 3 };
        s.nextPermutation(v);
        int i = 0;
        cout << "[";
        for (int i = 0; i < v.size(); ++i) {
            if (i != (v.size() - 1))
                cout << v[i] << ",";
            else
                cout << v[i];
        }
        cout << "]" << endl;
    }
    {
        vector<int> v{ 3, 2, 1 };
        s.nextPermutation(v);
        int i = 0;
        cout << "[";
        for (int i = 0; i < v.size(); ++i) {
            if (i != (v.size() - 1))
                cout << v[i] << ",";
            else
                cout << v[i];
        }
        cout << "]" << endl;
    }
    {
        vector<int> v{ 2, 3, 1 };
        s.nextPermutation(v);
        int i = 0;
        cout << "[";
        for (int i = 0; i < v.size(); ++i) {
            if (i != (v.size() - 1))
                cout << v[i] << ",";
            else
                cout << v[i];
        }
        cout << "]" << endl;
    }
    {
        vector<int> v{ 1, 1, 5 };
        s.nextPermutation(v);
        int i = 0;
        cout << "[";
        for (int i = 0; i < v.size(); ++i) {
            if (i != (v.size() - 1))
                cout << v[i] << ",";
            else
                cout << v[i];
        }
        cout << "]" << endl;
    }
    {
        vector<int> v{ 1, 2 };
        s.nextPermutation(v);
        int i = 0;
        cout << "[";
        for (int i = 0; i < v.size(); ++i) {
            if (i != (v.size() - 1))
                cout << v[i] << ",";
            else
                cout << v[i];
        }
        cout << "]" << endl;
    }
    {
        vector<int> v{ 1 };
        s.nextPermutation(v);
        int i = 0;
        cout << "[";
        for (int i = 0; i < v.size(); ++i) {
            if (i != (v.size() - 1))
                cout << v[i] << ",";
            else
                cout << v[i];
        }
        cout << "]" << endl;
    }
    return 0;
}
