#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>


using namespace std;


class Solution {
public:

    int numRollsToTarget(int n, int k, int target) {
        int mod = (int)1e9 + 7;

    #if 1
        vector<int> dp(target+1, 0);
        dp[0]=1;

        for (int i = 1; i <= n; ++i) {
            //for (int j = 0; j <= target; ++j) {
            for (int j = target; j >=0; --j) {
                dp[j] = 0;
                for (int face = 1; face <= k; ++face) {
                    if (j >= face) {
                        dp[j] = (dp[j] + dp[j-face]) % mod;
                    }
                }
            }
        }
        return dp[target];
    #else
        vector<vector<int>> dp(n+1, vector<int>(target+1, 0));
        dp[0][0]=1;

        for (int i = 1; i <= n; ++i) {
            for (int j = 0; j <= target; ++j) {
                for (int face = 1; face <= k; ++face) {
                    if (j >= face) {
                        dp[i][j] = (dp[i][j] + dp[i-1][j-face]) % mod;
                    }
                }
            }
        }
        return dp[n][target];
    #endif
	}
};

int main()
{
	Solution solution;

	{
        int n = 1;
        int k = 6;
        int target = 3;
		cout << solution.numRollsToTarget(n, k, target) << endl;
	}
	{
        int n = 2;
        int k = 6;
        int target = 7;
		cout << solution.numRollsToTarget(n, k, target) << endl;
	}
	{
        int n = 30;
        int k = 30;
        int target = 500;
		cout << solution.numRollsToTarget(n, k, target) << endl;
	}
	return 0;
}