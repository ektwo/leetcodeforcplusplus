#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>


using namespace std;


class Solution {

private:

	int DIRECTIONS[4][2] = { { -1, 0}, { 0, -1}, {1, 0}, {0, 1} };

	void bfs(vector<vector<char>>& grid, int rows, int cols, int i, int j) {
		queue<int> q;
		q.push(i * cols + j);
		grid[i][j] = '0';

		while (!q.empty()) {

			int val = q.front();
			q.pop();

			int row = val / cols;
			int col = val % cols;

			for (auto &direction : DIRECTIONS) {
				int row_idx = direction[0] + row;
				int col_idx = direction[1] + col;

				if ((row_idx > -1) && (row_idx < rows) && (col_idx > -1) && (col_idx < cols) && (grid[row_idx][col_idx] == '1'))
				{
					grid[row_idx][col_idx] = '0';
					q.push(row_idx * cols + col_idx);
				}
			}
		}
	}

public:
	int numIslands(vector<vector<char>>& grid) {

		if (grid.size() == 0) return 0;

		int nums = 0;
		int rows = grid.size();
		int cols = grid[0].size();

		for (int i = 0; i < rows; ++i) {
			for (int j = 0; j < cols; ++j) {
				if (grid[i][j] == '1') {
					++nums;
					bfs(grid, rows, cols, i, j);
				}

			}
		}

		return nums;
	}
};

int main()
{
	Solution solution;
	{
		vector<vector<char>> grid{
			{'1','0','1'},
			{'1','1','0'},
		};

		cout << solution.numIslands(grid) << endl;
	}
	{
		vector<vector<char>> grid{
			{'1','1','1','1','0'},
			{'1','1','0','1','0'},
			{'1','1','0','0','0'},
			{'0','0','0','0','0'}
		};

		cout << solution.numIslands(grid) << endl;
	}
	{
		vector<vector<char>> grid{
			{'1','1','0','0','0'},
			{'1','1','0','0','0'},
			{'0','0','1','0','0'},
			{'0','0','0','1','1'}
		};

		cout << solution.numIslands(grid) << endl;
	}
	return 0;
}
