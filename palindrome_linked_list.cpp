#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <limits>


using namespace std;

//Definition for singly-linked list.
 struct ListNode {
      int val;
      ListNode *next;
      ListNode() : val(0), next(nullptr) {}
      ListNode(int x) : val(x), next(nullptr) {}
      ListNode(int x, ListNode *next) : val(x), next(next) {}
 };
//         4
//                 8
// 0 1 2 3 4 5 6 7 8 9
class Solution {
private:
    ListNode* reverse_list(ListNode *head) {
        ListNode *prev = nullptr;
        ListNode *next = nullptr;
        ListNode *curr = head;
        while (curr) {
            ListNode *tmp = curr->next;
            next = curr->next;
            curr->next = prev;
            prev = curr;
            curr = tmp;
        }
        return prev;
    }
public:
    bool isPalindrome(ListNode* head) {
        if (!head || !head->next) return true;
        ListNode *slow = head;
        ListNode *fast = head;
        while (fast->next && fast->next->next) {
            slow = slow->next;
            fast = fast->next->next;
        }

        ListNode *reversed_slow = reverse_list(slow->next);
        ListNode *first = head;
        while (reversed_slow) {
            if (first->val != reversed_slow->val) return false;
            reversed_slow = reversed_slow->next;
            first = first->next;
        }

        return true;
    }
};

int main()
{
    Solution solution;
    {
        ListNode** lna = new ListNode * [5];
        lna[4] = new ListNode(1, nullptr);
        lna[3] = new ListNode(2, lna[4]);
        lna[2] = new ListNode(3, lna[3]);
        lna[1] = new ListNode(2, lna[2]);
        lna[0] = new ListNode(1, lna[1]);
        cout << solution.isPalindrome(lna[0]) << endl;
    }
    {
        ListNode** lna = new ListNode * [4];
        lna[3] = new ListNode(1, nullptr);
        lna[2] = new ListNode(2, lna[3]);
        lna[1] = new ListNode(2, lna[2]);
        lna[0] = new ListNode(1, lna[1]);
        cout << solution.isPalindrome(lna[0]) << endl;
    }
    cout << endl;
    {
        ListNode** lna = new ListNode * [2];
        lna[1] = new ListNode(2, nullptr);
        lna[0] = new ListNode(1, lna[1]);
        cout << solution.isPalindrome(lna[0]) << endl;
    }
}