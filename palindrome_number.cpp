#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>


using namespace std;


class Solution {
public:


	bool isPalindrome(int x) {

		bool ans = false;

        if (x < 0) return ans;

        int div = 1;
        int left = 0, right = 1;
        while ((x / div) >= 10) div *= 10;
        while (x > 0) {
            left = x / div;
            right = x % 10;
            if (left != right) return false;
            x = (x % div) / 10;
            div /= 100; //important
        }

		return true;
	}

};

int main()
{
    Solution solution;
    {
        int x = 121;
        cout << solution.isPalindrome(x) << endl;
    }
    {
        int x = -121;
        cout << solution.isPalindrome(x) << endl;
    }
    {
        int x = 10;
        cout << solution.isPalindrome(x) << endl;
    }
    return 0;
}
