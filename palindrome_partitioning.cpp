#include <iostream>
#include <vector>
#include <string>
#include <unordered_set>

using namespace std;


struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};


void print_1d_int(vector<int> nums)
{

        cout << "{";
        for (int j = 0; j < nums.size(); ++j) {
            cout << nums[j] << ",";
        }
        cout << "}," << endl;

}

void print_2d_int(vector<vector<int>> nums)
{
    cout << "{";
    for (int i = 0; i < nums.size(); ++i) {
        cout << "{";
        for (int j = 0; j < nums[0].size(); ++j) {
            cout << nums[i][j] << ",";
        }
        cout << "}," << endl;
    }
    cout << "}," << endl;
}

void print_2d_string(vector<vector<string>> nums)
{
    cout << "{";
    for (int i = 0; i < nums.size(); ++i) {
        cout << "{";
        for (int j = 0; j < nums[i].size(); ++j) {
            cout << nums[i][j] << ",";
        }
        cout << "}," << endl;
    }
    cout << "}," << endl;
}


void print_tree_preorder(TreeNode *root)
{
    if (root) {
        cout << root->val << ",";
        if (root->left) print_tree_preorder(root->left);
        if (root->right) print_tree_preorder(root->right);
    }
    else
        cout << "null,";
}

class Solution
{
private:
    bool is_alpha(char &ch) {
        if ('a' <= ch && ch <= 'z') return true;
        if ('A' <= ch && ch <= 'A') return true;
        return false;
    }
    bool is_palindrome(string str, int start, int end) {
        while (start < end) {
            if (!is_alpha(str[start])) ++start;
            else if (!is_alpha(str[end])) --end;
            else if (((str[start]+32-'a')%32) != ((str[end]+32-'a')%32)) return false;
            else {++start;--end;}
        }
        return true;
    }
    void dfs(string s, int start, vector<string> &tmp_solu, vector<vector<string>> &ans) {

        // The dfs process exit check & collect all tmp_solu
        if (start == s.size()) { 
            ans.push_back(tmp_solu); return;
        }

        for (int end = start; end < s.length(); ++end) {
            // dfs branch condition check
            if (!is_palindrome(s, start, end)) { continue; }
            tmp_solu.push_back(s.substr(start, end-start+1));
            dfs(s, end+1, tmp_solu, ans);
            tmp_solu.pop_back();
        }
    }
public:
    vector<vector<string>> partition(string s) {
        vector<vector<string>> ans;

        vector<string> tmp_solu;

        dfs(s, 0, tmp_solu, ans);

        return ans;
    }

};

int main()
{
    Solution solution;
    {
        string s = "aab";
        vector<vector<string>> ans = solution.partition(s);
        print_2d_string(ans);
    }
    cout << endl;
    {
        string s = "a";
        vector<vector<string>> ans = solution.partition(s);
        print_2d_string(ans);
    }

    return 0;
}
