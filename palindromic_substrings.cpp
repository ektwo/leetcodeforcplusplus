#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>


using namespace std;


class Solution {
public:

    int countSubstrings(string s) {

        int count = 0;
		int str_longest_begin = 0;
		int str_longest_len = 1;
		int n = s.length();
		bool dp[1000][1000] = { false };

		for (int i = 0; i < n; ++i) {
			int left = i;
			int right = i;
			dp[left][right] = true;
            ++count;
		}

		for (int i = 0; i < n - 1; ++i) {
			int left = i;
			int right = i + 1;
			if (s[left] == s[right]) {
				dp[left][right] = true;
				str_longest_begin = left;
				str_longest_len = right - left + 1;
                ++count;
			}
		}

		for (int word_len = 2; word_len < n; ++word_len) {
			for (int i = 0; i < (n - word_len); ++i) {
				int left = i;
				int right = left + word_len;
				if ((s[left] == s[right]) && (dp[left+1][right-1] == true)) {
					dp[left][right] = true;
					str_longest_begin = left;
					str_longest_len = right - left + 1;
                    ++count;
				}
			}
		}

		return count;//s.substr(str_longest_begin, str_longest_len);
	}
};

int main()
{
	Solution solution;
	{
		string str = "abc";
		cout << solution.countSubstrings(str) << endl;
	}
	{
		string str = "aaa";
		cout << solution.countSubstrings(str) << endl;
	}
	return 0;
}