#include <limits.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <queue>


using namespace std;


class Solution {
public:
    bool canPartition(vector<int>& nums) {
        int n = nums.size();
        int sum = 0;
        for (auto &num : nums) {
            sum += num;
        }
        int target = sum / 2;
        if (target * 2 != sum) return false;

        vector<vector<int>> dp(n, vector<int>(target + 1, 0));

        for (int j = 0; j <= target; ++j) {
            dp[0][j] = (j >= nums[0]) ? (nums[0]) : (0);
        }

        for (int i = 1; i < n; ++i) {
            int t = nums[i];
            for (int j = 0; j <= target; ++j) {
                int no = dp[i-1][j];

                int yes = j >= t ? dp[i-1][j-t] + t : 0;
                dp[i][j] = max(no, yes);
                //no : dp[i-1][j]
                //yes: dp[i-1][j-nums[i]]+nums[i]
            }
        }

        for (int i = 0; i < n; ++i) {
            for (int j = 0; j <= target; ++j) {
                cout << "dp[" << i << "]" << "[" << j << "]=" << dp[i][j] << endl;
            }
        }

        return dp[n-1][target] == target;
    }
};

int main()
{
    Solution solution;

    {
        vector<int> nums = { 1,5,11,5};
        int ans = solution.canPartition(nums);
        cout << "solution.canPartition=" << ans << endl;
    }
cout << "next=" << endl;
    {
        vector<int> nums = { 1, 2, 3, 5};
        int ans = solution.canPartition(nums);
        cout << "solution.canPartition=" << ans << endl;
    }

    return 0;
}
