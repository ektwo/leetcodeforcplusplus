#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <cmath>


using namespace std;


void print_1d_int(vector<int> nums)
{
    cout << "{";
    for (int j = 0; j < nums.size(); ++j) {
        cout << nums[j] << ",";
    }
    cout << "}," << endl;
}

class Solution {
public:
    vector<int> partitionLabels(string s) {
        vector<int> ans;

        unordered_map<char, int> table;

        for (int i = 0; i < s.length(); ++i)
            table[s[i]] = i;

        int left = 0;
        int right = 0;
        for (int i = 0; i < s.length(); ++i) {
            right = max(right, table[s[i]]);
            if (right == i) {
                ans.push_back(table[s[i]] -left + 1);
                left = i + 1;
            }
        }

        return ans;
    }
};

int main()
{
    Solution solution;
    {
        string s = "ababcbacadefegdehijhklij";
        vector<int> ans = solution.partitionLabels(s);
        print_1d_int(ans);
    }
    {
        string s = "eccbbbbdec";
        vector<int> ans = solution.partitionLabels(s);
        print_1d_int(ans);
    }

    return 0;
}
