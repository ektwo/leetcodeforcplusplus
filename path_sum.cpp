#include <limits.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <queue>


using namespace std;


struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};
 

class Solution {

private:
    TreeNode *m_head;
    int m_cur_target;
    int m_end_target;
    vector<vector<TreeNode*>> ans;
    bool ans_2;
    vector<vector<int>> dirs = {
        { 0, -1}, { -1, 0}, { 0, 1}, { 1, 0}
    };

    bool is_end_condition(TreeNode* root) {
        // if (!root) { //&& !root->left && !root->right) {
        //     cout << "is_end_condition YES " << endl;
        //     return true;
        // }
        // else
        //     cout << "is_end_condition NO root->val=" << root->val << endl;
        return false;
    }

    bool is_unsatisfied_condition(TreeNode* root) {
        if (!root) return true;
        return false;
    }

    bool is_branch_can_be_pruned(TreeNode* root, int candidate_start, int candiadate_end, int candidate) {
        //if (!root) return true;
        return false;
    }

    void try_collect_ans(TreeNode *root, int target, vector<TreeNode*> &tmp_solu) {
        if (root && !root->left && !root->right) {
            //if (target == m_end_target)
            {
                ans.push_back(tmp_solu);
                int total = 0;
                for (auto &tmp : tmp_solu) {
                    total += tmp->val;
                }

                //if (target == m_end_target) {
                if (total == m_end_target) {
                    ans_2 = true;
                    //return true;
                }
            }
        }
    }

    bool trune_branch_postproc(TreeNode* root, int &candidate_start, int candiadate_end, int candidate) {
        return true;
    }

    bool hasPathSum_dfs(TreeNode* root, int candidate_start, int valid, int target, vector<TreeNode*> &tmp_solu) {

        bool ret = false;

        m_cur_target = target;

        if (is_end_condition(root)) { /* find a solution with a complete ending */
            return true;
        }

        if (is_unsatisfied_condition(root)) { /* ignore possible solution at this step that do not satisfy the problem constraints */
            return false;
        }

        /* traverse all executable branch path */
        const int candidates_end = 1;
        for (int i = candidate_start; i < candidates_end; ++i)
        {
            int candidate = root->val;

            /* prune, check if it matches the current (level/target) pruning conditions */
            if (is_branch_can_be_pruned(root, i, candidates_end, candidate)) continue;

            /* update state variables */
            tmp_solu.push_back(root);

            try_collect_ans(root, target + candidate, tmp_solu);

            /* recursively execute the logic of the next step */
            bool ret1 = hasPathSum_dfs(root->left, i, (!root->left) && !(root->right), target + candidate, tmp_solu) ||
                        hasPathSum_dfs(root->right, i, (!root->left) && !(root->right), target + candidate, tmp_solu);
            ret = ret1;

            tmp_solu.pop_back();

            /* cut off the branch */
            trune_branch_postproc(root, i, candidates_end, candidate);
        }

        return ret;
    }
public:
    void probe() {
        m_cur_target = 0;
        m_end_target = 0;
        m_head = nullptr;
        ans_2 = false;
        ans.clear();
    }

    TreeNode* get_head() {
        return m_head;
    }

    TreeNode* push(TreeNode *root, int val, bool left, bool right)
    {
        TreeNode* temp = new TreeNode(val);

        if (!m_head) {
            m_head = temp;
        }

        if (!root)
            root = m_head;

        if (root && left)
            root->left = temp;
        else if (root && right)
            root->right = temp;

        return temp;
    }

    bool hasPathSum(TreeNode* root, int targetSum) {
        bool ret = false;
        vector<TreeNode*> tmp_solu;
        
        m_cur_target = 0;
        m_end_target = targetSum;
        //for (int i = 0; i < n; ++i)
        {
            //for (int j = 0; j < m; ++j)
            {
                ret = hasPathSum_dfs(root, 0, 1, 0, tmp_solu);
                //if (ans) return ans;
            }
        }

        return ans_2;
    }
};
//          1
//     2          2
//  4    5     5     4
// 8 9 10 11 11 10 9   8
int main()
{
    Solution solution;

    solution.probe();
    {
        TreeNode *root = solution.push(nullptr, 5, false, false);
        TreeNode *l = solution.push(root, 4, true, false);
        TreeNode *r = solution.push(root, 8, false, true);
        TreeNode *ll = solution.push(l, 11, true, false);
        TreeNode *rl = solution.push(r, 13, true, false);
        TreeNode *rr = solution.push(r, 4, false, true);
        TreeNode *lll = solution.push(ll, 7, true, false);
        TreeNode *llr = solution.push(ll, 2, false, true);
        TreeNode *rrr = solution.push(rr, 1, false, true);

        bool ans = solution.hasPathSum(solution.get_head(), 22);
        cout << "ans=" << ans << endl;
    }
    solution.probe();
    {
        TreeNode *root = solution.push(nullptr, 1, false, false);
        TreeNode *l = solution.push(root, 2, true, false);
        TreeNode *r = solution.push(root, 3, false, true);

        bool ans = solution.hasPathSum(solution.get_head(), 5);
        cout << "ans=" << ans << endl;
    }
    solution.probe();
    {
        TreeNode *root = solution.push(nullptr, 2, false, false);
        TreeNode *l = solution.push(root, 1, true, false);

        bool ans = solution.hasPathSum(nullptr, 0);
        cout << "ans=" << ans << endl;
    }

    return 0;
}
