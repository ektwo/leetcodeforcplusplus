#include <limits.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <queue>


using namespace std;


struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};
 

class Solution {

private:
    TreeNode *m_head;
    int m_cur_target;
    int m_end_target;
    vector<vector<int>> dirs = {
        { 0, -1}, { -1, 0}, { 0, 1}, { 1, 0}
    };

    bool is_end_condition(TreeNode* root) {
        //if (!root) return true;
        // if (!root) { //&& !root->left && !root->right) {
        //     cout << "is_end_condition YES " << endl;
        //     return true;
        // }
        // else
        //     cout << "is_end_condition NO root->val=" << root->val << endl;
        //return false;
        return false;
    }

    bool is_unsatisfied_condition(TreeNode* root) {
        if (!root) return true;
        return false;
    }

    bool is_branch_can_be_pruned(TreeNode* root, int candidate_start, int candiadate_end, int candidate) {
        //if (!root) return true;
        return false;
    }

    void try_collect_ans(TreeNode *root, long target, vector<TreeNode*> &tmp_solu, int &ans) {
        //if (root && !root->left && !root->right) {
        if (!root) return;
            target += root->val;
            //cout << "target=" << target << " m_end_target=" << m_end_target << endl;
            if (target == m_end_target)
            {
                // int total = 0;
                // for (auto &tmp : tmp_solu) {
                //     total += tmp->val;
                // }
                ans += 1;
            }
        //}
        try_collect_ans(root->left, target, tmp_solu, ans);
        try_collect_ans(root->right, target, tmp_solu, ans);
    }

    bool trune_branch_postproc(TreeNode* root, int &candidate_start, int candiadate_end, int candidate) {
        return true;
    }


public:
    void probe() {
        m_cur_target = 0;
        m_end_target = 0;
        m_head = nullptr;
    }

    TreeNode* get_head() {
        return m_head;
    }

    TreeNode* push(TreeNode *root, int val, bool left, bool right)
    {
        TreeNode* temp = new TreeNode(val);

        if (!m_head) {
            m_head = temp;
        }

        if (!root)
            root = m_head;

        if (root && left)
            root->left = temp;
        else if (root && right)
            root->right = temp;

        return temp;
    }

    bool pathSum_recursive(TreeNode *root, int candidate_start, int targetSum, long curr_sum, vector<TreeNode*> &tmp_solu, int &ans) {

        m_cur_target = curr_sum;
        if (is_end_condition(root)) { /* find a solution with a complete ending */
            return true;
        }

        if (is_unsatisfied_condition(root)) { /* ignore possible solution at this step that do not satisfy the problem constraints */
            return false;
        }

        /* traverse all executable branch path */
//         const int candidates_end = 1;
//         for (int i = candidate_start; i < candidates_end; ++i)
//         {
//             int candidate = root->val;

//             /* prune, check if it matches the current (level/target) pruning conditions */
//             if (is_branch_can_be_pruned(root, i, candidates_end, candidate)) continue;

//             /* update state variables */
//             //tmp_solu.push_back(root);
// cout << "cc curr_sum + candidate=" << curr_sum + candidate << endl;
//             try_collect_ans(root, curr_sum + candidate, tmp_solu, ans);
            try_collect_ans(root, 0, tmp_solu, ans);

//             /* recursively execute the logic of the next step */
             pathSum_recursive(root->left, 0, targetSum, 0, tmp_solu, ans);
             pathSum_recursive(root->right, 0, targetSum, 0, tmp_solu, ans);
//             pathSum_recursive(root->left, i, targetSum, 0, tmp_solu, ans) ||
//             pathSum_recursive(root->right, i, targetSum, 0, tmp_solu, ans);

//             //tmp_solu.pop_back();

//             /* cut off the branch */
//             trune_branch_postproc(root, i, candidates_end, candidate);
//         }
return true;
    }

    void sum_up(TreeNode *root, int targetSum) {

    }

    int pathSum(TreeNode* root, int targetSum) {
        int ans = 0;

        int curr_sum = 0;
        m_end_target = targetSum;
        vector<TreeNode *> tmp_solu;
        pathSum_recursive(root, 0, targetSum, curr_sum, tmp_solu, ans);

        return ans;
    }
};
//          1
//     2          2
//  4    5     5     4
// 8 9 10 11 11 10 9   8
int main()
{
    Solution solution;

    solution.probe();
    {
        TreeNode *root = solution.push(nullptr, 10, false, false);
        TreeNode *l = solution.push(root, 5, true, false);
        TreeNode *r = solution.push(root, -3, false, true);
        TreeNode *ll = solution.push(l, 3, true, false);
        TreeNode *lr = solution.push(l, 2, false, true);
        //TreeNode *rl = solution.push(r, 13, true, false);
        TreeNode *rr = solution.push(r, 11, false, true);
        TreeNode *lll = solution.push(ll, 3, true, false);
        TreeNode *llr = solution.push(ll, -2, false, true);
        TreeNode *lrr = solution.push(lr, 1, false, true);
        TreeNode *rrl = solution.push(rr, 5, true, false);
        TreeNode *rrr = solution.push(rr, 1, false, true);

        int ans = solution.pathSum(solution.get_head(), 8);
        cout << ans << endl;
    }
    solution.probe();
    {
    //             5
    //         4       8
    //     11    x   13   4
    //    7  2  x x 5  1
        TreeNode *root = solution.push(nullptr, 5, false, false);
        TreeNode *l = solution.push(root, 4, true, false);
        TreeNode *r = solution.push(root, 8, false, true);
        TreeNode *ll = solution.push(l, 11, true, false);
        //TreeNode *lr = solution.push(l, 2, false, true);
        TreeNode *rl = solution.push(r, 13, true, false);
        TreeNode *rr = solution.push(r, 4, false, true);
        TreeNode *lll = solution.push(ll, 7, true, false);
        TreeNode *llr = solution.push(ll, 2, false, true);
        //TreeNode *lrl = solution.push(lr, 1, false, true);
        //TreeNode *lrr = solution.push(lr, 1, false, true);
        TreeNode *rll = solution.push(rl, 5, true, false);
        TreeNode *rlr = solution.push(rl, 1, false, true);
        //TreeNode *rrl = solution.push(rr, 5, true, false);
        //TreeNode *rrr = solution.push(rr, 1, false, true);

        int ans = solution.pathSum(solution.get_head(), 22);
        cout << ans << endl;
    }

    return 0;
}
