#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>


using namespace std;


class Solution {
public:
    int peakIndexInMountainArray(vector<int>& arr) {
        if (arr.size() < 3) return 0;
        int left = 0;
        int right = arr.size() - 1;
        while (left < right) {
            int mid = left + (right - left) / 2;
            if (arr[mid] > arr[mid + 1]) right = mid;
            else left = mid + 1;
        }
        return right;
    }
};

int main()
{
    Solution solution;
    {
        vector<int> v{ 0, 1, 0};
        cout << solution.peakIndexInMountainArray(v) << endl;
    }
    {
        vector<int> v{ 0, 1, 0 };
        cout << solution.peakIndexInMountainArray(v) << endl;
    }
    {
        vector<int> v{ 0, 2, 1, 0 };
        cout << solution.peakIndexInMountainArray(v) << endl;
    }
    {
        vector<int> v{ 3, 4, 5, 1 };
        cout << solution.peakIndexInMountainArray(v) << endl;
    }
    {
        vector<int> v{ 24, 69, 100, 99, 79, 78, 67, 36, 26, 19 };
        cout << solution.peakIndexInMountainArray(v) << endl;
    }
    return 0;
}
