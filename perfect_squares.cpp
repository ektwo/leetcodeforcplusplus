#include <limits.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <queue>
#include <math>


using namespace std;


class Solution {
public:
    int numSquares(int n) {
    #if 1
         while (n % 4 == 0 ) n /= 4 ;
         if (n % 8 == 7 ) return  4 ;
         for ( int a = 0 ; a * a <= n; ++ a) {
             int b = sqrt(n - a * a);
             if (a * a + b * b == n) {
                 return !!a + !! b;
            }
        }
        return  3 ;
    #else
        vector<int> nums;
        int idx = 1;
        while (idx * idx <= n) {
            nums.push_back(idx * idx);
            ++idx;
        }

        int squares_num = nums.size();
        vector<int> dp(n + 1, 0);

        for (int j = 0; j <= n; ++j) {
            int t = nums[0];
            int k = j / t;
            if (k * t == j) {
                dp[j] = k;
            }
            else
                dp[j] = -1;
        }

        for (int i = 1; i < squares_num; ++i) {
            int t = nums[i];
            for (int j = t; j <= n; ++j) {
                if ((dp[j-t]) != -1) {
                    dp[j] = min(dp[j], dp[j-t] + 1);
                }
            }
        }

        return dp[n];
    #endif
    }
};

int main()
{
    Solution solution;

    {
        int ans = solution.numSquares(12);
        cout << "solution.numSquares=" << ans << endl;
    }
cout << "next=" << endl;
    {
        int ans = solution.numSquares(13);
        cout << "solution.numSquares=" << ans << endl;
    }

    return 0;
}
