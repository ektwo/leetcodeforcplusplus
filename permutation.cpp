#include <limits.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <queue>


using namespace std;


struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};
 

class Solution {

private:
    TreeNode *m_head;
    int m_cur_target;
    int m_end_target;
    vector<vector<int>> ans;
    vector<vector<int>> dirs = {
        { 0, -1}, { -1, 0}, { 0, 1}, { 1, 0}
    };

    bool is_end_condition(vector<int>& nums, int cur_target, vector<int> &solu) {
        if (cur_target == m_end_target) {
            ans.push_back(solu);
            return true;
        }
        return false;
    }

    bool is_unsatisfied_condition(vector<int>& nums) {
        return false;
    }

    bool is_branch_can_be_pruned(vector<int>& nums, int candidate_idx, int candiadate_end, vector<int>& is_visited) {
        if (is_visited[candidate_idx]) return true;
        return false;
    }

    void try_collect_ans(vector<int>& nums, int target, vector<int> &solu) {
        if (0)//root && !root->left && !root->right)
        {
            if (0)//target == m_end_target)
            {
                ans.push_back(solu);
            }
        }
    }

    bool trune_branch_postproc(vector<int>& nums, int &candidate_start, int candiadate_end, int candidate) {
        return true;
    }

    bool permute_dfs(vector<int>& nums, int candidate_start, int cur_target, vector<int>& is_visited, vector<int> &solu) {

        bool ret = false;

        if (is_end_condition(nums, cur_target, solu)) { /* find a solution with a complete ending */
            return true;
        }

        if (is_unsatisfied_condition(nums)) { /* ignore possible solution at this step that do not satisfy the problem constraints */
            return false;
        }

        /* traverse all executable branch path */
        const int candidates_end = nums.size();
        for (int i = candidate_start; i < candidates_end; ++i)
        {
            int candidate = nums[i];

            /* prune, check if it matches the current (level/target) pruning conditions */
            if (is_branch_can_be_pruned(nums, i, candidates_end, is_visited)) continue;

            /* update state variables */
            solu.push_back(candidate);
            is_visited[i] = 1;

            try_collect_ans(nums, 0, solu);

            /* recursively execute the logic of the next step */
            bool ret1 = permute_dfs(nums, 0, cur_target + 1, is_visited, solu);

            solu.pop_back();
            is_visited[i] = 0;

            /* cut off the branch */
            trune_branch_postproc(nums, i, candidates_end, candidate);
        }

        return ret;
    }
public:
    void probe() {
        m_cur_target = 0;
        m_end_target = 0;
        m_head = nullptr;
        ans.clear();
    }

    TreeNode* get_head() {
        return m_head;
    }

    TreeNode* push(TreeNode *root, int val, bool left, bool right)
    {
        TreeNode* temp = new TreeNode(val);

        if (!m_head) {
            m_head = temp;
        }

        if (!root)
            root = m_head;

        if (root && left)
            root->left = temp;
        else if (root && right)
            root->right = temp;

        return temp;
    }

    vector<vector<int>> permute(vector<int>& nums) {
        bool ret = false;
        vector<int> is_visited(nums.size(), 0);
        vector<int> solu;
        
        m_cur_target = 0;
        m_end_target = nums.size();
        //for (int i = 0; i < n; ++i)
        {
            //for (int j = 0; j < m; ++j)
            {
                ret = permute_dfs(nums, 0, 0, is_visited, solu);
                //if (ans) return ans;
            }
        }

        return ans;
    }
};

int main()
{
    Solution solution;

    solution.probe();
    {
        vector<int> nums = { 1, 2, 3};
        vector<vector<int>> ans = solution.permute(nums);
        cout << " { ";
        for (auto &a : ans) {
            cout << " { ";
            for (int j = 0; j < a.size(); ++j) {
                if (j != a.size() - 1)
                    cout << a[j] << ",";
                else
                    cout << a[j];
            }
            cout << " }, ";
        }
        cout << " } " << endl;
    }
    solution.probe();
    {
        vector<int> nums = { 0, 1};
        vector<vector<int>> ans = solution.permute(nums);
        cout << " { ";
        for (auto &a : ans) {
            cout << " { ";
            for (int j = 0; j < a.size(); ++j) {
                if (j != a.size() - 1)
                    cout << a[j] << ",";
                else
                    cout << a[j];
            }
            cout << " }, ";
        }
        cout << " } " << endl;
    }
    solution.probe();
    {
        vector<int> nums = { 1};
        vector<vector<int>> ans = solution.permute(nums);
        cout << " { ";
        for (auto &a : ans) {
            cout << " { ";
            for (int j = 0; j < a.size(); ++j) {
                if (j != a.size() - 1)
                    cout << a[j] << ",";
                else
                    cout << a[j];
            }
            cout << " }, ";
        }
        cout << " } " << endl;
    }

    return 0;
}

