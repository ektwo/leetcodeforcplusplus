#include <vector>
#include <iostream>
using namespace std;

class Solution
{
  public:
    int CalculatePlusOne(int digit, bool *lpCarry)
    {
        int ret = digit;
        if (true == *lpCarry)
            ret++;
        if (ret > 9)
        {
            ret -= 10;
            *lpCarry = true;
        }
        else
            *lpCarry = false;

        return ret;
    }

    vector<int> plusOne(vector<int> &digits)
    {
        vector<int> ret;
        bool carry = false;
        int size = digits.size();
        cout << "size=" << size << endl;
        int tmp;
        ret.reserve(size);
        cout << " ret size=" << ret.size() << endl;
        vector<int>::iterator it;

        it = digits.end() - 1;
        tmp = *it;
        carry = ((9 == tmp) ? (true) : (false));

        do
        {
            --size;

            if (it != digits.begin())
            {
                ret.insert(ret.begin(), CalculatePlusOne(tmp, &carry));
                cout << "ret.[" << size << "]=" << ret[size] << endl;
                cout << "carry=" << carry << endl;
                cout << "it=" << *it << endl;
                --it;
                tmp = *it;
            }
            else
            {
                ret.insert(ret.begin(), CalculatePlusOne(tmp, &carry));
                break;
            }
        } while (1);

        if (true == carry)
            ret.insert(ret.begin(), 1);

        cout << "ret.size()=" << ret.size() << endl;
        vector<int>::iterator it2;
        for (it2 = ret.begin(); it2 < ret.end(); it2++)
            std::cout << ' ' << *it2;

        return ret;
    }
};

int main()
{

    Solution s;
    int myints[] = {0};
    //9, 9, 9, 9

    std::vector<int> fifth(myints, myints + sizeof(myints) / sizeof(int));
    vector<int> answer(s.plusOne(fifth));

    cout << "answer.size()=" << answer.size() << endl;
    vector<int>::iterator it;
    for (it = answer.begin(); it < answer.end(); it++)
        std::cout << ' ' << *it;

    return 0;
}