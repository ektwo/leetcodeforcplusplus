#include <iostream>
#include <vector>
#include <string>

using namespace std;

class Solution
{
public:


    vector<int> plusOne(vector<int>& digits) {

        for (int j = digits.size() - 1; j >= 0; --j) {
            if (digits[j] == 9) {
                digits[j] = 0;
            }
            else {
                digits[j] += 1;
                return digits;
            }
        }

        if (digits.front() == 0) digits.insert(digits.begin(), 1);

        return digits;
    }
};

int main()
{
    Solution s;
    //{
    //    vector<int> v = { 1, 2, 3 };

    //    vector<int> ans = s.plusOne(v);

    //    for (int i = 0; i < ans.size(); ++i) {
    //        cout << ans[i] << " ";
    //    }
    //    cout << endl;
    //}
    //{
    //    vector<int> v = { 4, 3, 2, 1};

    //    vector<int> ans = s.plusOne(v);

    //    for (int i = 0; i < ans.size(); ++i) {
    //        cout << ans[i] << " " ;
    //    }
    //    cout << endl;
    //}
    {
        vector<int> v = { 0 };

        vector<int> ans = s.plusOne(v);

        for (int i = 0; i < ans.size(); ++i) {
            cout << ans[i];
        }
    }
    //{
    //    vector<int> v = { 0 };

    //    vector<int> ans = s.plusOne(v);

    //    for (int i = 0; i < ans.size(); ++i) {
    //        cout << ans[i];
    //    }
    //}
    return 0;
}