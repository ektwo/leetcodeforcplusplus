#include <limits.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <queue>


using namespace std;


class Solution {

public:
    vector<int> productExceptSelf(vector<int>& nums) {
        int nums_size = nums.size();
        vector<int> ans(nums_size, 1);
        //24,12,8,6
        // yzw, x zw, xy w, xyz
        // 1,   x   , xy  , xyz
        // 
        for (int i = 1; i < nums_size; ++i) {//x, y, z, w
            ans[i] = ans[i-1] * nums[i-1];
        }
        for (int i = 0; i < ans.size(); ++i) {
            cout << ans[i] << ",";
        }
        cout << endl;
        int tmp = 1;
        for (int i = nums_size - 1; i >= 1; --i) {
            tmp *= nums[i];
            ans[i-1] = ans[i-1] * tmp;
        }
        for (int i = 0; i < ans.size(); ++i) {
            cout << ans[i] << ",";
        }
        cout << endl;
        return ans;
    }

};

int main()
{
    Solution solution;
    {
        vector<int> nums = { 1,2,3,4 };
        vector<int> ans = solution.productExceptSelf(nums);
        for (int i = 0; i < ans.size(); ++i) {
            cout << ans[i] << ",";
        }
        cout << endl;
    }

    {
        vector<int> nums = { -1,1,0,-3,3 };
        vector<int> ans = solution.productExceptSelf(nums);
        for (int i = 0; i < ans.size(); ++i) {
            cout << ans[i] << ",";
        }
        cout << endl;
    }

    return 0;
}
