#ifndef __QUICK_SORT_H__
#define __QUICK_SORT_H__

#include <iostream>
#include <vector>
#include <iterator>
#include <type_traits>
#include "generate_array.h"
#include "base_algorithm.h"

using namespace std;

class CQuickSort : public CBaseAlgorithm
{
  public:
    CQuickSort()
    {
    }
    ~CQuickSort()
    {
    }

    template <typename T>
    int FindPivotLocation(T array[], int startIdx, int endIdx)
    {
        int left = startIdx;
        int right = endIdx - 1;
        int pivotValue = array[endIdx];
        while (true)
        {
            while ((left < endIdx - 1) && array[left] < pivotValue)
                ++left;
            while ((startIdx < right) && array[right] > pivotValue)
                --right;
            if (left >= right)
                break;
            if (array[left] > array[right])
                std::swap(array[left], array[right]);
        }

        if (array[left] > pivotValue)
            std::swap(array[left], array[endIdx]);
        else
            ++left;
        return left;
    }

    template <typename T>
    void QuickSortRecursive(T array[], int startIdx, int endIdx)
    {
        if (startIdx < endIdx)
        {
            int pivotLocation = this->FindPivotLocation(array, startIdx, endIdx);
            QuickSortRecursive(array, startIdx, pivotLocation - 1);
            QuickSortRecursive(array, pivotLocation + 1, endIdx);
        }
    }

    //avg: O(nlogn), worst: O(n^2)
    void QuickSort(CGenerateArray *pGenerateArray)
    {
        CGenerateArray *pGArray = pGenerateArray;
        if (!pGArray)
            return;
#ifdef TEST
            //int sampleArray[] = {5, 9, 4, 7, 2, 1, 8, 3, 0, 6};
            //memcpy(pArray, sampleArray, sizeof(int) * m_arraySize);
#else
        int *pArray = new int[pGArray->m_arraySize];
        memcpy(pArray, pGArray->m_pArray, sizeof(int) * pGArray->m_arraySize);
#endif
        m_startTime = CSystemTime::getSystemTime();

        QuickSortRecursive(pArray, 0, pGArray->m_itemsInArray);

        m_endTime = CSystemTime::getSystemTime();
        std::cout << "Quick sort Took " << (m_endTime - m_startTime) << std::endl;
        std::cout << "after" << std::endl;
        for (int i = 0; i < pGArray->m_arraySize; ++i)
            std::cout << " " << pArray[i];
        //std::cout << "value[" << i << "]=" << pArray[i];
        std::cout << std::endl;
        if (pArray)
            delete[] pArray;
    }

    template <typename ItRandom>
    ItRandom FindPivotLocation2(ItRandom itStart, ItRandom itEnd)
    {
        ItRandom left = itStart;
        ItRandom right = itEnd - 1;
        typename std::iterator_traits<ItRandom>::value_type pairStart = *itStart;
        typename std::iterator_traits<ItRandom>::value_type pairEnd = *itEnd;
        int pivotValue = pairEnd.first; //array[endIdx];
        //cout << "pivotValue=" << pivotValue << endl;
        //cout << "pairEnd1=" << pairEnd.second << endl;
        // while ((left < endIdx - 1) && array[left] < pivotValue)
        //     ++left;
        // while ((startIdx < right) && array[right] > pivotValue)
        //     --right;
        while (true)
        {
            //cout << 11 << endl;
            while ((left < (itEnd - 1)) && (static_cast<pair<int, int>>(*left)).first < pivotValue)
            {
                ++left;
                //cout << "aa right-left=" << right - left << endl;
                //cout << "aa *left=" << ((static_cast<pair<int, int>>(*left)).first) << endl;
            }

            while ((itStart < right) && (static_cast<pair<int, int>>(*right)).first > pivotValue)
            {
                --right;
                //cout << "12 right-left=" << right - left << endl;
                //cout << "12 *right=" << ((static_cast<pair<int, int>>(*right)).first) << endl;
            }
            //cout << "13 right-left=" << right - left << endl;
            if (left >= right)
                break;
            if (((static_cast<pair<int, int>>(*left)).first) > ((static_cast<pair<int, int>>(*right)).first))
            {
                iter_swap(left, right);
                //cout << "swap" << endl;
            }

            if ((left < (itEnd - 1)) && (static_cast<pair<int, int>>(*left)).first == pivotValue)
                ++left;
        }
        //cout << 2 << endl;
        if (((static_cast<pair<int, int>>(*left)).first) > pivotValue)
            iter_swap(left, itEnd);
        else
            ++left;
        return left;
    }

    template <typename ItRandom>
    ItRandom FindPivotLocationSecond(ItRandom itStart, ItRandom itEnd)
    {
        ItRandom left = itStart;
        ItRandom right = itEnd - 1;
        typename std::iterator_traits<ItRandom>::value_type pairStart = *itStart;
        typename std::iterator_traits<ItRandom>::value_type pairEnd = *itEnd;
        int pivotValue = pairEnd.second; //array[endIdx];
        //cout << "pivotValue=" << pivotValue << endl;
        // while ((left < endIdx - 1) && array[left] < pivotValue)
        //     ++left;
        // while ((startIdx < right) && array[right] > pivotValue)
        //     --right;
        while (true)
        {
            //cout << 11 << endl;
            while ((left < (itEnd - 1)) && (static_cast<pair<int, int>>(*left)).second < pivotValue)
            {
                ++left;
                //cout << "aa right-left=" << right - left << endl;
                //cout << "aa *left=" << ((static_cast<pair<int, int>>(*left)).second) << endl;
            }

            while ((itStart < right) && (static_cast<pair<int, int>>(*right)).second > pivotValue)
            {
                --right;
                //cout << "12 right-left=" << right - left << endl;
                //cout << "12 *right=" << ((static_cast<pair<int, int>>(*right)).second) << endl;
            }
            //cout << "13 right-left=" << right - left << endl;
            if (left >= right)
                break;
            if (((static_cast<pair<int, int>>(*left)).second) > ((static_cast<pair<int, int>>(*right)).second))
            {
                iter_swap(left, right);
                //cout << "swap" << endl;
            }

            if ((left < (itEnd - 1)) && (static_cast<pair<int, int>>(*left)).second == pivotValue)
                ++left;
        }
        //cout << 2 << endl;
        if (((static_cast<pair<int, int>>(*left)).second) > pivotValue)
            iter_swap(left, itEnd);
        else
            ++left;
        return left;
    }

    template <typename ItRandom>
    void QuickSortRecursive2(ItRandom itStart, ItRandom itEnd)
    {
        if (itStart < itEnd)
        {
            ItRandom pivotLocation = FindPivotLocation2(itStart, itEnd);
            QuickSortRecursive2(itStart, pivotLocation - 1);
            QuickSortRecursive2(pivotLocation + 1, itEnd);
        }
    }
    template <typename ItRandom>
    void QuickSortRecursiveSecond(ItRandom itStart, ItRandom itEnd)
    {
        if (itStart < itEnd)
        {
            ItRandom pivotLocation = FindPivotLocationSecond(itStart, itEnd);
            QuickSortRecursiveSecond(itStart, pivotLocation - 1);
            QuickSortRecursiveSecond(pivotLocation + 1, itEnd);
        }
    }
    //template <typename T>
    //void QuickSort(typename std::vector<T>::iterator start, typename std::vector<T>::iterator end)
    template <class ItRandom>
    void QuickSort2(ItRandom itStart, ItRandom itEnd)
    {
        //m_startTime = CSystemTime::getSystemTime();

        ItRandom itOrgEnd = itEnd;
        typename std::iterator_traits<ItRandom>::difference_type n = std::distance(itStart, itEnd);
        //cout << "yes=" << is_same<RandomIt, vector<pair<int, int>>::iterator>::value << endl;
        //QuickSortRecursive(pArray, 0, pGArray->m_itemsInArray);
        --itEnd;
        cout << "AA" << endl;
        QuickSortRecursive2(itStart, itEnd);
        cout << "AA 2" << endl;
        {
            int lastFirst = -1;
            ItRandom itTmpStart = itStart;
            ItRandom itTmpEnd;
            //typename std::iterator_traits<ItRandom>::value_type itTmpStart;
            //typename std::iterator_traits<ItRandom>::value_type itTmpEnd;
            while (itStart != itOrgEnd)
            {
                //typename std::iterator_traits<ItRandom>::value_type tmp = *itStart;
                pair<int, int> tmp2(*itStart);
                if (-1 == lastFirst)
                {
                    //cout << "-1" << endl;
                    lastFirst = tmp2.first;
                    itTmpStart = itStart; //*itStart;
                }
                else if (lastFirst != tmp2.first)
                {
                    //cout << "+1" << endl;
                    itTmpEnd = itStart - 1; //*itStart;
                    QuickSortRecursiveSecond(itTmpStart, itTmpEnd);
                    lastFirst = tmp2.first;
                    itTmpStart = itStart; //*itStart;
                }
                //cout << "first=" << tmp2.first << "second=" << tmp2.second << endl;
                ++itStart;
                if (itStart == itOrgEnd)
                    QuickSortRecursiveSecond(itTmpStart, itOrgEnd);
            }
        }
        //m_endTime = CSystemTime::getSystemTime();

        //std::cout << "Quick sort Took " << (m_endTime - m_startTime) << std::endl;
        //std::cout << "after 1" << std::endl;
        //cout << n << endl;
        --n;
        while (itStart != itOrgEnd)
        {
            typename std::iterator_traits<ItRandom>::value_type tmp = *itStart;
            pair<int, int> tmp2(*itStart);
            //cout << "first=" << tmp2.first << "second=" << tmp2.second << endl;
            ++itStart;
        }

        //for (int i = 0; i < pGArray->m_arraySize; ++i)
        ////   std::cout << " " << pArray[i];
        ////std::cout << "value[" << i << "]=" << pArray[i];
        //std::cout << std::endl;
    }
};

#endif