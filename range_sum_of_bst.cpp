#include <limits.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <queue>


using namespace std;


struct TreeNode {
    int val;
    TreeNode *parent;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};
 

class Solution {

private:
    int node_nums;
    int tree_height;
    TreeNode *m_head;
    bool _isValidBST(TreeNode* root, long min, long max) {
        if (!root) return true;
        cout << "root=" << root << endl;
        cout << "root->val=" << root->val << endl;
        cout << "root->left=" << root->left << " min=" << min << " max=" << max << endl;
        cout << "root->right=" << root->right << " min=" << min << " max=" << max << endl;
        if (root->val <= min) return false;
        if (root->val >= max) return false;
        
        return _isValidBST(root->left, min, root->val) && _isValidBST(root->right, root->val, max);
    }

    bool _isSymmetric(TreeNode *left, TreeNode *right) {
        if (!left && !right) return true;
        if (left && !right) return false;
        if (!left && right) return false;
        if (left->val != right->val) return false;
        return _isSymmetric(left->left, right->right) && _isSymmetric(left->right, right->left);
    }

public:
    void reset() {
        node_nums = 0;
        tree_height = 0;
        m_head = nullptr;
    }

    bool findp = false, findq = false;

public:
    void reset() {
        m_head = nullptr;
        findp = false;
        findq = false;
    }

    TreeNode* get_head() {
        return m_head;
    }

    TreeNode* push(TreeNode *root, int val, bool left, bool right)
    {
        TreeNode* temp = new TreeNode(val);
        //cout << "push val=" << val << " temp=" << temp << endl;
        if (!m_head) {
            m_head = temp;
        }

        if (!root)
            root = m_head;

        if (root && left)
            root->left = temp;
        else if (root && right)
            root->right = temp;

        return temp;
    }

    bool isValidBST(TreeNode* root) {
        return _isValidBST(root, LONG_MIN, LONG_MAX);

    }

    bool isSymmetric(TreeNode* root) {
        if (!root) return true;
        return _isSymmetric(root->left, root->right);
    }

    vector<vector<int>> levelOrder(TreeNode* root) {
        if (!root) return {};
        vector<vector<int>> ans;
        queue<TreeNode*> q;
        q.push(root);


        while (!q.empty()) {
            vector<int> nodes_val;
            for (int i = q.size(); i > 0; --i) {
                TreeNode *node = q.front();
                q.pop();
                nodes_val.push_back(node->val);

                if (node->left) q.push(node->left);
                if (node->right) q.push(node->right);
            }

            ans.push_back(nodes_val);
        }

        return ans;
    }
    vector<vector<int>> zigzagLevelOrder(TreeNode* root) {
        if (!root) return {};

        int level = 1;
        vector<vector<int>> ans;
        queue<TreeNode*> q;
        q.push(root);

        while (!q.empty()) {
            vector<int> nodes_val;
            for (int i = q.size(); i > 0; --i) {
                TreeNode *node = q.front();
                q.pop();
                nodes_val.push_back(node->val);
                if (node->left) q.push(node->left);
                if (node->right) q.push(node->right);
            }

            if (level % 2 == 0)
                reverse(nodes_val.begin(), nodes_val.end());
            ans.push_back(nodes_val);
            ++level;
        }

        return ans;
    }

    TreeNode* lowestCommonAncestor_binary_search(TreeNode* root, TreeNode* p, TreeNode* q) {
        if (root->val == p->val || root->val == q->val) return root;
        if (p->val < root->val && root->val < q->val) return root;
        if (q->val < root->val && root->val < p->val) return root;
        if (q->val < root->val && p->val < root->val) return lowestCommonAncestor_binary_search(root->left, p, q);
        if (root->val < p->val && root->val < q->val) return lowestCommonAncestor_binary_search(root->right, p, q);
        return nullptr;
    }

    TreeNode* lowestCommonAncestor_i(TreeNode* root, TreeNode* p, TreeNode* q) {
        if (!root || root == p || root == q) return root;
        TreeNode *left = lowestCommonAncestor_i(root->left, p, q);
        TreeNode *right = lowestCommonAncestor_i(root->right, p, q);
        if (left && right) return root;
        return (left) ? (left) : (right);
    }

    TreeNode* lowestCommonAncestor_ii_recursive(TreeNode* root, TreeNode* p, TreeNode* q) {
        if (!root) return root;
        if (root == p) {        cout << "root->val =" << root->val << " p->val =" << p->val << endl; findp = true; }
        else if (root == q) {        cout << "root->val =" << root->val << " q->val =" << q->val << endl; findq = true; }
        auto *left = lowestCommonAncestor_ii_recursive(root->left, p, q);
        auto *right = lowestCommonAncestor_ii_recursive(root->right, p, q);
        if ((root == p) || (root == q) || (left && right)) return root;
        return (left) ? (left) : (right);
    }
    TreeNode* lowestCommonAncestor_ii(TreeNode* root, TreeNode* p, TreeNode* q) {
        if (!root || !p || !q) return nullptr;
        TreeNode *ans = lowestCommonAncestor_ii_recursive(root, p, q);
        cout << findp << " " << findq << endl;
        if (findp && findq) return ans;



        return nullptr;
        // def backtrack(待搜索的集合, 递归到第几层, 状态变量 1, 状态变量 2, 结果集):
        //     # 写递归函数都是这个套路：先写递归终止条件
        //     if 可能是层数够深了:
        //         # 打印或者把当前状态添加到结果集中
        //         return

        //     for 可以执行的分支路径 do           //分支路径
                
        //         # 剪枝
        //         if 递归到第几层, 状态变量 1, 状态变量 2, 符合一定的剪枝条件:
        //             continue

        //         对状态变量状态变量 1, 状态变量 2 的操作（#）
        
        //         # 递归执行下一层的逻辑
        //         backtrack(待搜索的集合, 递归到第几层, 状态变量 1, 状态变量 2, 结果集)

        //         对状态变量状态变量 1, 状态变量 2 的操作（与标注了 # 的那一行对称，称为状态重置）
                
        //     end for
    }
    TreeNode* lowestCommonAncestor_iii(TreeNode* p, TreeNode* q) {
        
        TreeNode *tmp_p = p;
        TreeNode *tmp_q = q;

        // while (tmp_p != tmp_q) {
        //     tmp_p = (tmp_p == nullptr) ? (q) : (tmp_p->parent); // 5 -> 3 -> 9 -> nullptr(4)
        //     tmp_q = (tmp_q == nullptr) ? (p) : (tmp_q->parent); // 4 -> 2 -> 5
        // }

        return tmp_p;
        // def backtrack(待搜索的集合, 递归到第几层, 状态变量 1, 状态变量 2, 结果集):
        //     # 写递归函数都是这个套路：先写递归终止条件
        //     if 可能是层数够深了:
        //         # 打印或者把当前状态添加到结果集中
        //         return

        //     for 可以执行的分支路径 do           //分支路径
                
        //         # 剪枝
        //         if 递归到第几层, 状态变量 1, 状态变量 2, 符合一定的剪枝条件:
        //             continue

        //         对状态变量状态变量 1, 状态变量 2 的操作（#）
        
        //         # 递归执行下一层的逻辑
        //         backtrack(待搜索的集合, 递归到第几层, 状态变量 1, 状态变量 2, 结果集)

        //         对状态变量状态变量 1, 状态变量 2 的操作（与标注了 # 的那一行对称，称为状态重置）
                
        //     end for
    }
    TreeNode* lowestCommonAncestor_iv_recursive(TreeNode* root, set<TreeNode*> &nodes) {
        if (!root) return root;
        if (nodes.count(root) > 0) return root;
        TreeNode *left = lowestCommonAncestor_iv_recursive(root->left, nodes);
        TreeNode *right = lowestCommonAncestor_iv_recursive(root->right, nodes);
        if (left && right) return root;
        return (left) ? (left) : (right);
    }
    TreeNode* lowestCommonAncestor(TreeNode* root, vector<TreeNode*> &nodes) {
        set<TreeNode*> nodes_set(nodes.begin(), nodes.end());
        return lowestCommonAncestor_iv_recursive(root, nodes_set);
    }
    int rangeSumBST(TreeNode* root, int low, int high) {
        int ans = 0;

        return node_nums;
    }
};
//          1
//     2          2
//  4    5     5     4
// 8 9 10 11 11 10 9   8
int main()
{
    Solution solution;

    solution.reset();
    {
        TreeNode *root = solution.push(nullptr, 10, false, false);
        TreeNode *l = solution.push(root, 5, true, false);
        TreeNode *r = solution.push(root, 15, false, true);
        TreeNode *ll = solution.push(l, 3, true, false);
        TreeNode *lr = solution.push(l, 7, false, true);
        TreeNode *rr = solution.push(r, 18, false, true);

        int ans = solution.rangeSumBST(solution.get_head(), 7, 15);
        cout << "ans=" << ans << endl;
    }
    solution.reset();
    {
        TreeNode *root = solution.push(nullptr, 10, false, false);
        TreeNode *l = solution.push(root, 5, true, false);
        TreeNode *r = solution.push(root, 15, false, true);
        TreeNode *ll = solution.push(l, 3, true, false);
        TreeNode *lr = solution.push(l, 7, false, true);
        TreeNode *rl = solution.push(r, 13, true, false);
        TreeNode *rr = solution.push(r, 18, false, true);
        TreeNode *lll = solution.push(ll, 1, true, false);
        TreeNode *lrl = solution.push(lr, 6, true, false);

        int ans = solution.rangeSumBST(solution.get_head(), 6, 10);
        cout << "ans=" << ans << endl;
    }

    return 0;
}
