#include <limits.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <queue>


using namespace std;


class NumMatrix {
private:
    vector<vector<int>> dp;
public:
    NumMatrix(vector<vector<int>>& matrix) {
        if (matrix.empty() || matrix[0].empty()) return;
        dp.resize(matrix.size()+1, vector<int>(matrix[0].size()+1, 0));
        for (int i = 1; i <= matrix.size(); ++i) {
            for (int j = 1; j <= matrix[0].size(); ++j) {
                dp[i][j] = dp[i-1][j] + dp[i][j-1]-dp[i-1][j-1]+matrix[i-1][j-1];
            }
        }
    }
    
    int sumRegion(int row1, int col1, int row2, int col2) {
        return dp[row2+1][col2+1] - dp[row1][col2+1] - dp[row2+1][col1] + dp[row1][col1];
    }
};

int main()
{
    vector<vector<int>> matrix = {{3, 0, 1, 4, 2}, {5, 6, 3, 2, 1}, {1, 2, 0, 1, 5}, {4, 1, 0, 1, 7}, {1, 0, 3, 0, 5}};
    NumMatrix *obj = new NumMatrix(matrix);
    {
        int row1 = 2;
        int col1 = 1;
        int row2 = 4;
        int col2 = 3;
        int ans = obj->sumRegion(row1, col1, row2, col2);
        cout << "solution.sumRegion=" << ans << endl;
    }
cout << "next=" << endl;
    {
        int row1 = 1;
        int col1 = 1;
        int row2 = 2;
        int col2 = 2;
        int ans = obj->sumRegion(row1, col1, row2, col2);
        cout << "solution.sumRegion=" << ans << endl;
    }
cout << "next=" << endl;
    {
        int row1 = 1;
        int col1 = 2;
        int row2 = 2;
        int col2 = 4;
        int ans = obj->sumRegion(row1, col1, row2, col2);
        cout << "solution.sumRegion=" << ans << endl;
    }
    return 0;
}
