#include <limits.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <queue>


using namespace std;


class NumArray {
private:
    int rob_ii(vector<int>& nums, int left, int right) {
        int n = right - left;
        if (n <= 1) return nums[left];
        vector < int > dp;
        dp.push_back(nums[left]);
        dp.push_back(max(nums[ left ], nums[ left+1 ]));
        for (int i = 2; i < n; ++i) {
            dp.push_back(max(dp[i-2]+nums[left+i], dp[i-1]));
        }
        return dp.back();
    }
public:
    int rob_i(vector<int>& nums) {
        int n = nums.size();
        if (n <= 1) return nums.empty() ? 0 : nums[0];
        vector < int > dp;
        dp.push_back(nums[0]);
        dp.push_back(max(nums[ 0 ], nums[ 1 ]));
        for (int i = 2; i < n; ++i) {
            dp.push_back(max(dp[i-2]+nums[i], dp[i-1]));
        }
        return dp.back();
    }
    int rob(vector<int>& nums) {
        int n = nums.size();
        if (n <= 1) return nums.empty() ? 0 : nums[0];
        return max(rob_ii(nums, 0, n-1), rob_ii(nums, 1, n));
    }
    vector<int> dp;
    NumArray(vector<int>& nums) {
        dp = nums;
        for (int i = 1; i < nums.size(); ++i) {
            dp[i] += dp[i-1];    
            cout << "dp[" << i << "]=" << dp[i] << endl;
        }
        
    }
    
    int sumRange(int left, int right) {
        //cout << "left=" << dp[left] << " right=" << dp[right] << endl;
        return (left == 0) ? (dp[right]) : (dp[right]-dp[left-1]);
    }
};

int main()
{
    vector<int> nums = { -2, 0, 3, -5, 2, -1};
    NumArray *obj = new NumArray(nums);
    {
        int ans = obj->sumRange(0, 2);
        cout << "solution.sumRange=" << ans << endl;
    }
cout << "next=" << endl;
    {
        int ans = obj->sumRange(3, 4);
        cout << "solution.sumRange=" << ans << endl;
    }
cout << "next=" << endl;
    {
        int ans = obj->sumRange(0, 5);
        cout << "solution.sumRange=" << ans << endl;
    }
    return 0;
}
