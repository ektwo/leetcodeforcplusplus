#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>


using namespace std;


class Solution {
public:
    bool canConstruct(string ransomNote, string magazine) {
        unordered_map<char, int> m;
        for (int i = 0; i < magazine.length(); ++i) {
            m[magazine[i]]++;
        }
        for (auto &r:ransomNote) {
            if (m.find(r) == m.end() || m[r] == 0) {
                return false;
            }
            else {
                cout << r << endl;
                m[r]--;
            }
                
        }
        return true;
    }
};

int main()
{
    Solution solution;
    {
        string ransomNote("a");
        string magazine("b");
        cout << solution.canConstruct(ransomNote, magazine) << endl;
    }
    {
        string ransomNote("aa");
        string magazine("ab");
        cout << solution.canConstruct(ransomNote, magazine) << endl;
    }
    {
        string ransomNote("aa");
        string magazine("aab");
        cout << solution.canConstruct(ransomNote, magazine) << endl;
    }
    return 0;
}
