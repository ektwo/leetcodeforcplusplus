#include <iostream>
#include <vector>
#include <string>
#include <unordered_map>

using namespace std;

/**
 * The read4 API is defined in the parent class Reader4.
 *     int read4(char *buf4);
 */

class Solution {
private:
    int read_pos = 0, tmp_buf_len = 0;
    char tmp_buf[4];
public:
    /**
     * @param buf Destination buffer
     * @param n   Number of characters to read
     * @return    The number of actual characters read
     */
    int read4(char* buf) {
        return 0;
    }
    int read(char* buf, int n) {
        // total = 7
        // n = 2, tmp_buf_len = 4, read_pos = 2
        // n = 3, tmp_buf_len = 4, read_pos = 4 -> tmp_buf_len = 3 -> read_pos = 1. 
        // n = 1, tmp_buf_len = 3, read_pos = 2
        // n = 2, tmp_buf_len = 3, read_pos = 3
        for (int i = 0; i < n; ++i) {
            // if the tmp_buf is empty, call read4 once again.
            if (read_pos == tmp_buf_len) {
                tmp_buf_len = read4(tmp_buf);
                read_pos = 0;
                if (tmp_buf_len == 0) return i;
            }

            buf[i] = tmp_buf[read_pos++];
        }

        return n;
    }
};

int main()
{


    return 0;
}