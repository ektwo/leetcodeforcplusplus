#include <limits.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <queue>


using namespace std;


class Solution {


public:
    vector<string> reorderLogFiles(vector<string>& logs) {

        vector<string> ans;

        vector<pair<string, string>> let;
        vector<pair<string, string>> dig;

        for (auto &log : logs) {
            bool delimited = false;
            string identifier;
            string word;
            for (auto &c : log) {
                if ((c == ' ') && (!delimited)) {
                    delimited = true;
                }
                else if (delimited) {
                    word += c;
                }
                else {
                    identifier += c;
                }

            }
            if (isalpha(word[0])) {
                let.push_back(make_pair(word, identifier));
            }
            else {
                dig.push_back(make_pair(word, identifier));
            }
        }

        sort(let.begin(), let.end());

        for (auto &str : let) {
            ans.push_back(str.second + ' ' + str.first);
        }
        for (auto &str : dig) {
            ans.push_back(str.second + ' ' + str.first);
        }

        return ans;
    }
};

int main()
{
    Solution solution;
    {
        vector<string> logs = { "dig1 8 1 5 1","let1 art can","dig2 3 6","let2 own kit dig","let3 art zero" };
        vector<string> ans = solution.reorderLogFiles(logs);
        for (int i = 0; i < ans.size(); ++i) {
            cout << ans[i] << ",";
        }
        cout << endl;
    }

    {
        vector<string> logs = { "a1 9 2 3 1","g1 act car","zo4 4 7","ab1 off key dog","a8 act zoo" };
        vector<string> ans = solution.reorderLogFiles(logs);
        for (int i = 0; i < ans.size(); ++i) {
            cout << ans[i] << ",";
        }
        cout << endl;
    }

    return 0;
}
