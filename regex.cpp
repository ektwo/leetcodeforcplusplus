#include <iostream>
#include <regex>

using namespace std;

static void search_string(const string& str,
                          const regex& reg_ex) { // ①
  for (string::size_type i = 0; i < str.size() - 1; i++) {
    auto substr = str.substr(i, 1);
    if (regex_match(substr, reg_ex)) {
      cout << substr;
    }
  }
}
static void search_by_regex(const char* regex_s,
                            const string& s) { // ②
  regex reg_ex(regex_s);
  cout.width(12); // ③
  cout << regex_s << ": \""; // ④
  search_string(s, reg_ex);  // ⑤
  cout << "\"" << endl;
}

int main()
{
    string s1 = "abc123cdef";
    string s2 = "123456789";

    regex ex("\\d+");

    cout << "regex_match=" << regex_match(s1, ex) << endl;
    cout << "regex_match=" << regex_match(s2, ex) << endl;

    smatch match;
    regex_search(s1, match, ex);
    cout << "match=" << match[0] << endl;
    regex_search(s2, match, ex);
    cout << "match=" << match[0] << endl;

    {
        string s = R"a(bcd)a";
        cout << s << endl;
    }

    {
        std::string pattern("http(s)?://([\\w-]+\\.)+[\\w-]+(/[\\w- ./?%&=]*)?");    //匹配规则很简单，如果有疑惑，可以对照语法查看
        std::regex r(pattern);
        string link = "https://www.cnblogs.com/grandyang/p/4800552.html";
        smatch match;
        regex_search(link, match, r);
        cout << "1=" << match[0] << endl;
    }

    {
    // string s("_AaBbCcDdEeFfGg12345 \t\n!@#$%"); // ⑥

    // search_by_regex("[[:alnum:]]", s);          // ⑦
    // search_by_regex("\\w", s);                  // ⑧
    // search_by_regex(R"(\W)", s);                // ⑨
    // search_by_regex("[[:digit:]]", s);          // ⑩
    // search_by_regex("[^[:digit:]]", s);         // ⑪
    // search_by_regex("[[:space:]]", s);          // ⑫
    // search_by_regex("\\S", s);                  // ⑬
    // search_by_regex("[[:lower:]]", s);          // ⑭
    // search_by_regex("[[:upper:]]", s);
    // search_by_regex("[[:alpha:]]", s);          // ⑮
    // search_by_regex("[[:blank:]]", s);          // ⑯
    // search_by_regex("[[:graph:]]", s);          // ⑰
    // search_by_regex("[[:print:]]", s);          // ⑱
    // search_by_regex("[[:punct:]]", s);          // ⑲
    // search_by_regex("[[:xdigit:]]", s);         // ⑳
    }

    return 0;
}