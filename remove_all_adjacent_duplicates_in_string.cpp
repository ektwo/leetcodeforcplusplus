#include <iostream>
#include <stack>
#include <algorithm>
using namespace std;


class Solution
{
public:
    string removeDuplicates(string s) {
        stack<char> st;
        for (auto &c:s) {
            if (!st.empty()) {
                if (st.top()==c) {
                    st.pop();
                }
                else st.push(c);
            }
            else
                st.push(c);
        }

        string ans;
        while (!st.empty()) {
            char c=st.top(); st.pop();
            ans.push_back(c);
        }
        reverse(ans.begin(), ans.end());

        return ans;
    }
};

int main()
{
    Solution solution;
    {
        string s("abbaca");
        string ans = solution.removeDuplicates(s);
        cout << ans << endl;
    }
    {
        string s("azxxzy");
        string ans = solution.removeDuplicates(s);
        cout << ans << endl;
    }


    return 0;
}