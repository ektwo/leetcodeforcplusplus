#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <limits>


using namespace std;


void print_1d_int(vector<int> nums)
{
    cout << "{";
    for (int j = 0; j < nums.size(); ++j) {
        cout << nums[j] << ",";
    }
    cout << "}," << endl;
}


class Solution {
public:
    int removeDuplicates(vector<int>& nums) {
        if (nums.empty()) return 0;
        int ans = nums.size();
        
        int slow = 0;
        for (int fast = 0; fast < nums.size(); ++fast) {
            if (nums[slow] != nums[fast]) {
                nums[++slow]=nums[fast];
                --ans;
            }
        }

        return slow + 1;
    }
};

int main()
{
    Solution solution;
    {
        vector<int> nums = {1,1,2};
        int ans = solution.removeDuplicates(nums);
        print_1d_int(nums);
        cout << ans << endl;
    }
    cout << endl;
    {
        vector<int> nums = {0,0,1,1,1,2,2,3,3,4};
        int ans = solution.removeDuplicates(nums);
        print_1d_int(nums);
        cout << ans << endl;
    }

    return 0;
}