#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <limits>


using namespace std;


void print_1d_int(vector<int> nums)
{
    cout << "{";
    for (int j = 0; j < nums.size(); ++j) {
        cout << nums[j] << ",";
    }
    cout << "}," << endl;
}


class Solution {
public:
    int removeElement(vector<int>& nums, int val) {
    #if 1
        int slow = 0;
        int fast = 0;

        for (; fast<nums.size();++fast) {
            if (nums[fast]==val) {

            }
            else {
                nums[slow]=nums[fast];
                ++slow;
            }
        }
        return slow;
    #else
        int ans = nums.size();
        for (vector<int>::iterator it = nums.begin(); it != nums.end(); ) {
            if (*it == val) {
                it = nums.erase(it);
                --ans;
            }
            else
                ++it;
        }

        return ans;
    #endif
    }
};

int main()
{
    Solution solution;
    {
        vector<int> nums = {3,2,2,3};
        int val = 3;
        int ans = solution.removeElement(nums, val);
        print_1d_int(nums);
        cout << ans << endl;
    }
    cout << endl;
    {
        vector<int> nums = {0,1,2,2,3,0,4,2};
        int val = 2;
        int ans = solution.removeElement(nums, val);
        print_1d_int(nums);
        cout << ans << endl;
    }

    return 0;
}