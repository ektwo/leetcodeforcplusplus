#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <limits>


using namespace std;

struct ListNode {
    int val;
    ListNode *next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
};

void print_1d_int(vector<int> nums)
{
    cout << "{";
    for (int j = 0; j < nums.size(); ++j) {
        cout << nums[j] << ",";
    }
    cout << "}," << endl;
}
void print_list(ListNode *root)
{
    if (root) {
        cout << root->val << ",";
        if (root->next) print_list(root->next);
    }
    else
        cout << endl;
}

ListNode* create_list_nodes(vector<int> nums) {
    if (nums.size() <= 0) return nullptr;
    ListNode *head = new ListNode(-1);
    ListNode *prev = head, *curr = nullptr;
    for (auto &num : nums) {
        curr = new ListNode(num);
        prev->next = curr;
        prev = curr;
    }
    return head->next;
}

class Solution {

private:
    void delete_node_release(ListNode *prev, bool release, bool link_to_target_next) {
        if ((prev) && (prev->next)) {
            ListNode *node_to_release = prev->next;
            if (link_to_target_next)
                prev->next = node_to_release->next;
            node_to_release->next = nullptr;
            if (release)
                delete node_to_release;
        }
    }

public:
    ListNode* removeElements(ListNode* head, int val) {
        if (!head) return nullptr;

        ListNode *dummy = new ListNode(-1);
        dummy->next = head;
        ListNode *prev = dummy;

        while (prev->next) {
            if (prev->next->val == val) {
                ListNode *tmp = prev->next->next;
                delete_node_release(prev, true, true);
                prev->next = tmp;
            }
            else
                prev = prev->next;
        }
        return dummy->next;
    }
};

int main()
{
    Solution solution;
    {
        vector<int> nums = {1,2,6,3,4,5,6};
        ListNode *list = create_list_nodes(nums);
        int val = 6;
        ListNode *ans = solution.removeElements(list, val);
        print_list(ans);
    }
    cout << endl;
    {
        
        ListNode *list = nullptr;
        int val = 1;
        ListNode *ans = solution.removeElements(list, val);
        print_list(ans);
    }
    {
        vector<int> nums = {7,7,7,7};
        ListNode *list = create_list_nodes(nums);
        int val = 7;
        ListNode *ans = solution.removeElements(list, val);
        print_list(ans);
    }
    return 0;
}