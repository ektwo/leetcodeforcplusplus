#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>


using namespace std;


struct ListNode {
    int val;
    ListNode *next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
};
      


class Solution {
public:
    ListNode* removeNthFromEnd(ListNode* head, int n) {
        if (!head) return nullptr;

#if 1
        ListNode *dummy = new ListNode(0);
        dummy->next = head;
        ListNode *slow=head, *fast=head;
        while (n-->0&&fast!=nullptr) {
            fast=fast->next;
        }

        if (!fast) return dummy->next->next;

        while (fast->next) {
            fast=fast->next;
            slow=slow->next;
        }

        slow->next=slow->next->next;
        return dummy->next;
#else
        ListNode *last = head;

        int size = 0;

        while (last) {
            last = last->next;
            ++size;
        }

        int target = size - n;
        if (target == 0) { head = head->next; return head; }

        ListNode *prev = nullptr;
        ListNode *curr = head;

        while (target-- > 0) {
            prev = curr;
            curr = curr->next;
        }

        prev->next = curr->next;

        return head;
#endif
    }
};

int main()
{
    Solution s;
    {
        ListNode** lna = new ListNode * [5];

        lna[4] = new ListNode(5, nullptr);
        lna[3] = new ListNode(4, lna[4]);
        lna[2] = new ListNode(3, lna[3]);
        lna[1] = new ListNode(2, lna[2]);
        lna[0] = new ListNode(1, lna[1]);

        ListNode* ret = s.removeNthFromEnd(lna[0], 1);
        ListNode* ret_tmp = ret;
        cout << "[";
        while (ret_tmp) {
            if (!ret_tmp->next)
                cout << ret_tmp->val;
            else
                cout << ret_tmp->val << ",";

            ret_tmp = ret_tmp->next;
        }
        cout << "]" << endl;
    }
    {
        ListNode** lna = new ListNode * [5];

        lna[4] = new ListNode(5, nullptr);
        lna[3] = new ListNode(4, lna[4]);
        lna[2] = new ListNode(3, lna[3]);
        lna[1] = new ListNode(2, lna[2]);
        lna[0] = new ListNode(1, lna[1]);

        ListNode* ret = s.removeNthFromEnd(lna[0], 2);
        ListNode* ret_tmp = ret;
        cout << "[";
        while (ret_tmp) {
            if (!ret_tmp->next)
                cout << ret_tmp->val;
            else
                cout << ret_tmp->val << ",";

            ret_tmp = ret_tmp->next;
        }
        cout << "]" << endl;
    }
    {
        ListNode** lna = new ListNode * [5];

        lna[4] = new ListNode(5, nullptr);
        lna[3] = new ListNode(4, lna[4]);
        lna[2] = new ListNode(3, lna[3]);
        lna[1] = new ListNode(2, lna[2]);
        lna[0] = new ListNode(1, lna[1]);

        ListNode* ret = s.removeNthFromEnd(lna[0], 3);
        ListNode* ret_tmp = ret;
        cout << "[";
        while (ret_tmp) {
            if (!ret_tmp->next)
                cout << ret_tmp->val;
            else
                cout << ret_tmp->val << ",";

            ret_tmp = ret_tmp->next;
        }
        cout << "]" << endl;
    }
    {
        ListNode** lna = new ListNode * [5];

        lna[4] = new ListNode(5, nullptr);
        lna[3] = new ListNode(4, lna[4]);
        lna[2] = new ListNode(3, lna[3]);
        lna[1] = new ListNode(2, lna[2]);
        lna[0] = new ListNode(1, lna[1]);

        ListNode* ret = s.removeNthFromEnd(lna[0], 4);
        ListNode* ret_tmp = ret;
        cout << "[";
        while (ret_tmp) {
            if (!ret_tmp->next)
                cout << ret_tmp->val;
            else
                cout << ret_tmp->val << ",";

            ret_tmp = ret_tmp->next;
        }
        cout << "]" << endl;
    }
    {
        ListNode** lna = new ListNode * [5];

        lna[4] = new ListNode(5, nullptr);
        lna[3] = new ListNode(4, lna[4]);
        lna[2] = new ListNode(3, lna[3]);
        lna[1] = new ListNode(2, lna[2]);
        lna[0] = new ListNode(1, lna[1]);

        ListNode* ret = s.removeNthFromEnd(lna[0], 5);
        ListNode* ret_tmp = ret;
        cout << "[";
        while (ret_tmp) {
            if (!ret_tmp->next)
                cout << ret_tmp->val;
            else
                cout << ret_tmp->val << ",";

            ret_tmp = ret_tmp->next;
        }
        cout << "]" << endl;
    }
#if  1
    {
        ListNode** lna = new ListNode * [1];

        lna[0] = new ListNode(1, nullptr);

        ListNode* ret = s.removeNthFromEnd(lna[0], 1);
        ListNode* ret_tmp = ret;
        cout << "[";
        while (ret_tmp) {
            if (!ret_tmp->next)
                cout << ret_tmp->val;
            else
                cout << ret_tmp->val << ",";

            ret_tmp = ret_tmp->next;
        }
        cout << "]" << endl;

    }

    {
        ListNode** lna = new ListNode * [2];

        lna[1] = new ListNode(2, nullptr);
        lna[0] = new ListNode(1, lna[1]);

        ListNode* ret = s.removeNthFromEnd(lna[0], 1);
        ListNode* ret_tmp = ret;
        cout << "[";
        while (ret_tmp) {
            if (!ret_tmp->next)
                cout << ret_tmp->val;
            else
                cout << ret_tmp->val << ",";

            ret_tmp = ret_tmp->next;
        }
        cout << "]" << endl;

    }
    {
        ListNode** lna = new ListNode * [2];

        lna[1] = new ListNode(2, nullptr);
        lna[0] = new ListNode(1, lna[1]);

        ListNode* ret = s.removeNthFromEnd(lna[0], 2);
        ListNode* ret_tmp = ret;
        cout << "[";
        while (ret_tmp) {
            if (!ret_tmp->next)
                cout << ret_tmp->val;
            else
                cout << ret_tmp->val << ",";

            ret_tmp = ret_tmp->next;
        }
        cout << "]" << endl;

    }
    #endif
}