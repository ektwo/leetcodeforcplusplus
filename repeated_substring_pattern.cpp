#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>


using namespace std;

class Solution {

public:
    void get_next(const string &s, vector<int> &dp) {
        int N=s.length();
        int i=1,j=0; 
        while(i<N)
        {
            if (s[i]==s[j]) dp[++i]=++j;
            else if (j==0) ++i; //此時發現'_'和'A'不匹配，'A'已經是第一個了，不需要查表了，此時將模式串向右移動一位：
            else j=dp[j];
        }
    } 

//BBC_ABCDAB_ABCDABCDABDE
//    ABCDABD
//Maximum prefix suffix common element
//0   1   2   3   4   5   6 
//A   B   C   D   A   B   D
//0   0   0   0   1   2   0
//-1  0   0   0   0   1   2 

//0   1   2   3   4   5   6   7  
//A   B   C   D   A   B   C   E
//0   0   0   0   1   2   3   0
//-1  0   0   0   0   1   2   3
//j=6,dp[6]=k=3,dp[j]=2,dp[j+1]= ?, p[j]=C=p[k], dp[j+1]=dp[j]+1=k+1=3


//0   1   2   3   4   5   6   7   8
//D   A   B   C   D   A   B   D   E
//0   0   0   0   1   2   3   1   0
//-1  0   0   0   0   1   2   3   1
//j=7,dp[7]=k=3,p[7]=D != C=p[3], D=p[0]-->k'=0,dp[j+1]=k'+1=1
    // void get_next(const string &s, const string &t, vector<int> &dp) {
    //     int N=s.length();
    //     // int i=1,j=0; 
    //     // while(i<N)
    //     // {
    //     //     // if (s[i]==s[j]) dp[++i]=++j; //首字母x匹配上了，然後就按順序一路往下匹配，直到最後一個失配：
    //     //     // else if (j==0) ++i; //此時發現'_'和'A'不匹配，'A'已經是第一個了，不需要查表了，此時將模式串向右移動一位：
    //     //     // else j=dp[j]; //失配時，模式串向右移動的距離= 已匹配字符數     - 失配字符的上一位字符所對應的最大長度值  'D': offset=6-2
    //     //     //               //失配時，模式串向右移動的距離= 失配字符所在位置  - 失配字符對應的next值。              'D': offset=6-2=6-next[6]


    //     // }
    //     int j=0,k=-1;


    // } 

    void getNext (const string& pat, vector<int>& next){
        //prefix指向前綴末尾位置，suffix指向後綴末尾位置。
        int prefix = -1;
        int suffix = 0;
        next[suffix++]=prefix;
        //0   1   2   3   4   5   6 
        //A   B   C   D   A   B   D
        //0   0   0   0   1   2   0
        //-1  0   0   0   0   1   2 
        //-1 -1  -1  -1   0   1  -1
        for(;suffix<pat.size();++suffix){
            while(prefix>=0 && pat[prefix+1]!=pat[suffix]) { // 前后缀不相同了
                prefix=next[prefix]; // 向前回退
            }
            if(pat[prefix+1]==pat[suffix]) { // 找到相同的前后缀
                prefix++; 
            }
            next[suffix]=prefix; // 将k（前缀的长度）赋给next[suffix]
        }

        for (int a=0;a<next.size();++a) {
            cout << next[a] << " ";
        }
        cout << endl;
    }

    bool repeatedSubstringPattern (string s) {
        int N=s.length();
        //vector<int> dp(N+1,0);
        vector<int> dp(N,-1);
        getNext(s, dp);

        int n = N-1;
        //return (dp[N] && (dp[N]%(N-dp[N])==0));
        return (dp[n]!=-1 && (N%(N-(dp[n]+1))==0));
    }
};

int main()
{
    Solution solution;
    {
    string s = "abcdabd";
    bool ans = solution.repeatedSubstringPattern(s);
    cout << ans << endl;
    }
    {
    string s = "abab";
    bool ans = solution.repeatedSubstringPattern(s);
    cout << ans << endl;
    }
    {
    string s = "aba";
    bool ans = solution.repeatedSubstringPattern(s);
    cout << ans << endl;
    }
    {
    string s = "abcabcabcabc";
    bool ans = solution.repeatedSubstringPattern(s);
    cout << ans << endl;
    }
	return 0;

}