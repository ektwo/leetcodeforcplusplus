#include <limits.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <queue>


using namespace std;

template<class T>
void print_1d(const T& nums)
{
    cout << "{";
    for (int j = 0; j < nums.size(); ++j) {
        cout << nums[j] << ",";
    }
    cout << "}," << endl;
}
template<class T>
void print_2d(const T& nums)
{
    cout << "{";
    for (int i = 0; i < nums.size(); ++i) {
        cout << "{";
        for (int j = 0; j < nums[i].size(); ++j) {
            cout << nums[i][j] << ",";
        }
        cout << "}," << endl;
    }
    cout << "}," << endl;
}
class Solution {

public:
    void backtracking_findSubsequences(vector<int> &nums, int start_idx, vector<int> path, vector<vector<int>> &ans) {
        //if (start_idx == nums.size()-1) 
        {
            if (path.size()>1)
                ans.push_back(path);
            //return;
        }

        unordered_set<int> uset;

        for (int i=start_idx;i<nums.size();++i) {

            if ((path.empty()||nums[i]>=path.back()) && (uset.find(nums[i])==uset.end())) {
                uset.insert(nums[i]);
                path.push_back(nums[i]);
                backtracking_findSubsequences(nums,i+1,path,ans);
                path.pop_back();
            }
        }

    }
    vector<vector<int>> findSubsequences(vector<int>& nums) {
        vector<vector<int>> ans;
        vector<int> path;
        backtracking_findSubsequences(nums, 0, path, ans);
        return ans;
    }
};

int main()
{
    Solution solution;

    {
        vector<int> nums={4,6,7,7};
        auto ans = solution.findSubsequences(nums);
        print_2d(ans);
    }

    {
        vector<int> nums={4,4,3,2,1};
        auto ans = solution.findSubsequences(nums);
        print_2d(ans);
    }

    return 0;
}
