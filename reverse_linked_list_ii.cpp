#include <limits.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <queue>


using namespace std;


struct ListNode {
      int val;
      ListNode *next;
      ListNode() : val(0), next(nullptr) {}
      ListNode(int x) : val(x), next(nullptr) {}
      ListNode(int x, ListNode *next) : val(x), next(next) {}
 };

class Solution {

private:
    ListNode* m_head;
    ListNode* m_curr;

    ListNode* reverseList_recursive_util(ListNode* head) {
        if (!head) return nullptr;

        ListNode *prev = nullptr;
        ListNode *curr = head;
        ListNode *next = nullptr;
        while (curr) {
            next = curr->next;
            curr->next = prev;
            prev = curr;
            curr = next;
        }

        return prev;
    }
public:
    void reset() {
        m_head = nullptr;
        m_curr = nullptr;
    }

    ListNode* get_head() {
        return m_head;
    }

    void push(int data)
    {
        ListNode* temp = new ListNode(data);
        if (!m_head) {
            m_head = temp;
            m_curr = m_head;
        }
        m_curr->next = temp;
        m_curr = temp;
    }
    //recursive
    ListNode* reverseList_recursive(ListNode* head) {
        if (!head) return nullptr;

        ListNode *first = head;
        ListNode *old_head = head->next;
        ListNode *new_head = reverseList_recursive_util(old_head);

        old_head->next = first;
        first->next = nullptr;

        return new_head;
    }

    //iterative
    ListNode* reverseList(ListNode* head) {
        if (!head) return nullptr;

        ListNode *prev = nullptr;
        ListNode *curr = head;
        ListNode *next = nullptr;
        while (curr) {
            next = curr->next;
            curr->next = prev;
            prev = curr;
            curr = next;
        }

        return prev;
    }

    ListNode* reverseBetween(ListNode* head, int left, int right) {
        ListNode *dummy = new ListNode(-1), *pre = dummy;
        dummy->next = head;
        for (int i = 0; i < left - 1; ++i) pre = pre->next;
        ListNode *cur = pre->next;
        for (int i = left; i < right; ++i) {
            ListNode *tmp = cur->next;
            cur->next = tmp->next;
            tmp->next = pre->next;
            pre->next = tmp;
        }
        return dummy->next;
    }
    }
};

int main()
{
    Solution solution;
    {
        struct ListNode ln_1(1);
        struct ListNode ln_2(2);
        struct ListNode ln_3(3);
        struct ListNode ln_4(4);
        struct ListNode ln_5(5);
        ln_1.next = &ln_2;
        ln_2.next = &ln_3;
        ln_3.next = &ln_4;
        ln_4.next = &ln_5;

        ListNode *ret = solution.reverseList(&ln_1);
        ListNode* ret_tmp = ret;
        cout << "[";
        while (ret_tmp) {
            if (!ret_tmp->next)
                cout << ret_tmp->val;
            else
                cout << ret_tmp->val << ",";
            
            ret_tmp = ret_tmp->next;
        }
        cout << "]" << endl;
    }

    {
        struct ListNode ln_1(1);
        struct ListNode ln_2(2);
        ln_1.next = &ln_2;

        ListNode *ret = solution.reverseList(&ln_1);
        ListNode* ret_tmp = ret;
        cout << "[";
        while (ret_tmp) {
            if (!ret_tmp->next)
                cout << ret_tmp->val;
            else
                cout << ret_tmp->val << ",";
            
            ret_tmp = ret_tmp->next;
        }
        cout << "]" << endl;
    }

    {
        struct ListNode ln_1(1);
        struct ListNode ln_2(2);
        struct ListNode ln_3(3);
        struct ListNode ln_4(4);
        struct ListNode ln_5(5);
        ln_1.next = &ln_2;
        ln_2.next = &ln_3;
        ln_3.next = &ln_4;
        ln_4.next = &ln_5;

        ListNode *ret = solution.reverseList_recursive(&ln_1);
        ListNode* ret_tmp = ret;
        cout << "[";
        while (ret_tmp) {
            if (!ret_tmp->next)
                cout << ret_tmp->val;
            else
                cout << ret_tmp->val << ",";
            
            ret_tmp = ret_tmp->next;
        }
        cout << "]" << endl;
    }

    {
        struct ListNode ln_1(1);
        struct ListNode ln_2(2);
        ln_1.next = &ln_2;

        ListNode *ret = solution.reverseList_recursive(&ln_1);
        ListNode* ret_tmp = ret;
        cout << "[";
        while (ret_tmp) {
            if (!ret_tmp->next)
                cout << ret_tmp->val;
            else
                cout << ret_tmp->val << ",";
            
            ret_tmp = ret_tmp->next;
        }
        cout << "]" << endl;
    }

    solution.reset();
    {
        solution.push(1);
        solution.push(2);
        solution.push(3);
        solution.push(4);
        solution.push(5);

        ListNode *ret = solution.reverseList_recursive(solution.get_head());
        ListNode* ret_tmp = ret;
        cout << "[";
        while (ret_tmp) {
            if (!ret_tmp->next)
                cout << ret_tmp->val;
            else
                cout << ret_tmp->val << ",";
            
            ret_tmp = ret_tmp->next;
        }
        cout << "]" << endl;
    }
    solution.reset();
    {
        solution.push(1);
        solution.push(2);

        ListNode *ret = solution.reverseList_recursive(solution.get_head());
        ListNode* ret_tmp = ret;
        cout << "[";
        while (ret_tmp) {
            if (!ret_tmp->next)
                cout << ret_tmp->val;
            else
                cout << ret_tmp->val << ",";
            
            ret_tmp = ret_tmp->next;
        }
        cout << "]" << endl;
    }
    solution.reset();
    {
        solution.push(1);
        solution.push(2);
        solution.push(3);
        solution.push(4);
        solution.push(5);

        ListNode *ret = solution.reverseBetween(solution.get_head(), 2, 4);
        ListNode* ret_tmp = ret;
        cout << "[";
        while (ret_tmp) {
            if (!ret_tmp->next)
                cout << ret_tmp->val;
            else
                cout << ret_tmp->val << ",";
            
            ret_tmp = ret_tmp->next;
        }
        cout << "]" << endl;
    }

    // solution.reset();
    // {
    //     solution.push(5);

    //     ListNode *ret = solution.reverseBetween(solution.get_head(), 1, 1);
    //     ListNode* ret_tmp = ret;
    //     cout << "[";
    //     while (ret_tmp) {
    //         if (!ret_tmp->next)
    //             cout << ret_tmp->val;
    //         else
    //             cout << ret_tmp->val << ",";
            
    //         ret_tmp = ret_tmp->next;
    //     }
    //     cout << "]" << endl;
    // }

    return 0;
}
