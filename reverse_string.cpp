#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>


using namespace std;

void print_1d_char(vector<char> nums)
{
    cout << "{";
    for (int j = 0; j < nums.size(); ++j) {
        cout << nums[j] << ",";
    }
    cout << "}," << endl;
}
class Solution {

public:
    void reverseString(vector<char>& s) {
        int left = 0;
        int right = s.size()-1;
        while (left < right) {
            char tmp = s[right];
            s[right] = s[left];
            s[left] = tmp;
            ++left;
            --right;
        }

    }

};

int main()
{
    Solution solution;
    {
    vector<char> s = {'h','e','l','l','o'};
    solution.reverseString(s);
    print_1d_char(s);
    }
    {
    vector<char> s = {'H','a','n','n','a','h'};
    solution.reverseString(s);
    print_1d_char(s);
    }
	return 0;

}