#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>


using namespace std;


class Solution {
private:
void remove_extra_spaces(string& s) {
    int slow = 0;
    for (int fast = 0; fast < s.length(); ++fast) {
        if (s[fast]!=' ') {
            s[slow++]=s[fast];
        }
        else {
            while (fast+1 < s.length() && s[fast+1]==' ') fast++;
                if (fast+1<s.length() && slow!=0) {
                s[slow++]=' ';
                }
        }
    }
    s.resize(slow);
}

public:
    string reverseWords(string s) {
        remove_extra_spaces(s);
        reverse(s.begin(), s.end());
        int slow = 0;
        for (int fast = 0; fast < s.length(); ++fast) {
            if (s[fast]==' ') {
                reverse(s.begin()+slow,s.begin()+fast);
                slow=fast+1;
            }
            else if (fast+1 == s.length())
                reverse(s.begin()+slow,s.end());
        }

        return s;
    }
};

int main()
{
    Solution solution;
    {
    string s = "the sky is blue";

    string ans = solution.reverseWords(s);
    cout << ans << endl;
    }
    {
    string s = "  hello world  ";
    string ans = solution.reverseWords(s);
    cout << ans << endl;
    }
	return 0;

}