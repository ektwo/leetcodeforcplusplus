#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>


using namespace std;

class Robot {
public:
	     // Returns true if the cell in front is open and robot moves into the cell.
	     // Returns false if the cell in front is blocked and robot stays in the current cell.
	bool move() {}
	
		     // Robot will stay in the same cell after calling turnLeft/turnRight.
		     // Each turn will be 90 degrees.
	void turnLeft() {}
	void turnRight() {}
	
		     // Clean the current cell.
		void clean() {}
	
};

class Solution {

private:

	struct hashFunction
	{
		size_t operator()(const pair<int,int>& x) const
		{
			return x.first ^ x.second;
		}
	};

	//const int DIRECTIONS[4][2] = { { -1, 0}, { 0, -1}, {1, 0}, {0, 1} };
	pair<int, int> DIRECTIONS[4] = {
		{ -1, 0}, //up
		{ 0, 1 }, //right
		{ 1, 0 }, //down
		{ 0, -1 }, //left
	};

	void dfs_backtracking(Robot& robot) {
		robot.turnRight();
		robot.turnRight();
		robot.move();
		robot.turnRight();
		robot.turnRight();
	}

	void dfs(Robot& robot, unordered_set<pair<int, int>, hashFunction> &visited, int x, int y, int dir_idx) {
		robot.clean();

		visited.insert({ x, y });

		for (int i = 0; i < 4; ++i) {
			int new_x = x + DIRECTIONS[dir_idx].first;
			int new_y = y + DIRECTIONS[dir_idx].second;
			if (visited.count({ new_x, new_y }) == 0 && robot.move()) {
				dfs(robot, visited, new_x, new_y, dir_idx);
				dfs_backtracking(robot);
			}
			dir_idx = (dir_idx + 1) % 4;
			robot.turnRight();
		}
	}
public:
	void cleanRoom(Robot& robot) {
		unordered_set<pair<int, int>, hashFunction> visited;
		dfs(robot, visited, 0, 0, 0);
	}

};

int main()
{

	return 0;
}