#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <limits>


using namespace std;


void print_1d_int(vector<int> nums)
{

        cout << "{";
        for (int j = 0; j < nums.size(); ++j) {
            cout << nums[j] << ",";
        }
        cout << "}," << endl;

}


class Solution {
public:
    void rotate(vector<int>& nums, int k) {
        if ((k %= nums.size()) == 0) return;
        int n = nums.size();
        reverse(nums.begin(), nums.begin() + n - k);
        reverse(nums.begin() + n - k, nums.end());
        reverse(nums.begin(), nums.end());
    }
};

int main()
{
    Solution solution;
    {
        vector<int> nums = {1,2,3,4,5,6,7};
        int k = 3;
        solution.rotate(nums, k);
        print_1d_int(nums);
    }
    cout << endl;
    {
        vector<int> nums = {-1,-100,3,99};
        int k = 2;
        solution.rotate(nums, k);
        print_1d_int(nums);
    }

    return 0;
}