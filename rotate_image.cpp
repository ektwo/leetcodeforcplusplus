#include <iostream>
#include <vector>
#include <string>

using namespace std;

class Solution
{
public:
    void rotate(vector<vector<int>>& matrix) {
        const int n = matrix[0].size();
        for (int i = 0; i < n; ++i) {
            for (int j = i + 1; j < n; ++j) {
                swap(matrix[i][j], matrix[j][i]);
            }
            reverse(matrix[i].begin(), matrix[i].end());
        }
    }
};

int main()
{
    Solution s;

    vector<vector<int>> input = { {5,1,9,11},{2,4,8,10},{13,3,6,7},{15,14,12,16} };
                              //= { {15,1,9,5},{2,4,8,10},{13,3,6,7},{16,14,12,11} };
                              //= { {15,13,9,5},{2,4,8,1},{12,3,6,7},{16,14,10,11} };
                              //= { {15,13,2,5},{14,3,4,1},{12,6,8,9},{16,7,10,11} };
    
    
                               // [[15, 13, 2, 5], [14, 3, 4, 1], [12, 6, 8, 9], [16, 7, 10, 11]]


    s.rotate(input);

    cout << "[";

    for (int i = 0; i < input.size(); ++i) {
        cout << "[";
        for (int j = 0; j < input[0].size(); ++j) {
            cout << input[i][j] << ",";
        }
        cout << "]";
    }
    cout << "]" << endl;

    return 0;
}