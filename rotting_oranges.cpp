#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <queue>
#include <limits>


using namespace std;


class Solution {

private:
    vector<pair<int, int>> dirs { { 0, -1}, {-1, 0}, {0, 1}, {1, 0}};
    bool is_valid_dir(int new_x, int new_y, const int &m, const int &n) {
        return ((new_x >= 0) && (new_x < m) && (new_y >= 0) && (new_y < n));
    }
public:
    int orangesRotting(vector<vector<int>>& grid) {
        // check corner cases
        int m = grid.size();
        if (m < 1) return -1;
        int n = grid[0].size();
        if (n < 1) return -1;

        // Prepare for BFS
        const int EMPTY = 0;
        const int FRESH = 1;
        const int ROTTEN = 2;
        int fresh_num = 0;
        queue<pair<int, int>> q;

        bool have_rotten = false;
        // put all gates into queue
        for (int i = 0; i < m; ++i) {
            for (int j = 0; j < n; ++j) {
                if (grid[i][j] == FRESH) ++fresh_num;
                else if (grid[i][j] == ROTTEN) { q.push({i, j}); have_rotten = true; }
            }
        }

        //if (!have_rotten) return -1;
        if (fresh_num == 0) return 0;

        //Start BFS
        int count = 0;
        while (!q.empty() && (fresh_num > 0)) {

            for(int i = q.size(); i > 0; --i) {
                pair<int, int> cur = q.front(); q.pop();
                for (int idx = 0; idx < dirs.size(); ++idx) {
                    int new_x = cur.first + dirs[idx].first;
                    int new_y = cur.second + dirs[idx].second;
                    if (!is_valid_dir(new_x, new_y, m, n) || (grid[new_x][new_y] != FRESH)) continue;
                    grid[new_x][new_y] = ROTTEN;
                    q.push({new_x, new_y});
                    --fresh_num;
                }

            }
            ++count;
        }

        return (fresh_num == 0) ? (count) : (-1);
    }

};



int main()
{
    Solution solution;
    {
        vector<vector<int>> grid = { {2,1,1},{1,1,0},{0,1,1}};
        int ans = solution.orangesRotting(grid);
        cout << ans << endl;
    }
    {
        vector<vector<int>> grid = { {2,1,1},{0,1,1},{1,0,1}};
        int ans = solution.orangesRotting(grid);
        cout << ans << endl;
    }
    {
        vector<vector<int>> grid = {{0,2}};
        int ans = solution.orangesRotting(grid);
        cout << ans << endl;
    }
    return 0;
}
