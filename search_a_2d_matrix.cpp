#include <iostream>
#include <vector>
#include <string>

using namespace std;

class Solution
{
private:
    int M, N;
    vector<pair<int, int>> dirs = { {0, -1}, {-1, 0}, {0, 1}, {1, 0}};
    bool is_valid(int i, int j) {
        if ((i >= 0) && (i < M) && (j >=0) && (j < N)) return true;
        return false;
    }
public:
    bool searchMatrix(vector<vector<int>>& matrix, int target) {
        int left = 0;
        int right = matrix.size();

        while (left < right) {
            int mid = left + (right - left) / 2;
            if (matrix[mid][0] <= target) left = mid + 1;
            else right = mid;
        }
        if (right == 0) return false;
        //cout << "11 right=" << right << endl;
        int idx = right - 1;
        //cout << "12 right=" << right << endl;
        left = 0;
        right = matrix[idx].size();
        //cout << "2 right=" << right << endl;
        while (left < right) {
            int mid = left + (right - left) / 2;
            if (matrix[idx][mid] == target) return true;
            else if (matrix[idx][mid] < target) left = mid + 1;
            else right = mid;
        }

        //cout << "3 right=" << right << endl;
        return false; 
    }
};

void print_1d_int(vector<int> nums)
{

        cout << "{";
        for (int j = 0; j < nums.size(); ++j) {
            cout << nums[j] << ",";
        }
        cout << "}," << endl;

}

void print_2d_int(vector<vector<int>> nums)
{
    cout << "{";
    for (int i = 0; i < nums.size(); ++i) {
        cout << "{";
        for (int j = 0; j < nums[0].size(); ++j) {
            cout << nums[i][j] << ",";
        }
        cout << "}," << endl;
    }
    cout << "}," << endl;
}

int main()
{
    Solution s;
    {
        vector<vector<int>> matrix = {{1,3,5,7},{10,11,16,20},{23,30,34,60}};
        int target = 3;
        bool ans = s.searchMatrix(matrix, target);
        cout << ans << endl;
        //print_2d_int(nums);
    }
    cout << endl;
    {
        vector<vector<int>> matrix = {{1,3,5,7},{10,11,16,20},{23,30,34,60}};
        int target = 13;
        bool ans = s.searchMatrix(matrix, target);
        cout << ans << endl;
    }

    return 0;
}
