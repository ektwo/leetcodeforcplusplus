#include <iostream>
#include <vector>
#include <string>

using namespace std;

class Solution
{

public:
    bool searchMatrix(vector<vector<int>>& matrix, int target) {
        if (matrix.empty() || matrix[0].empty()) return false;
        if (target < matrix[0][0] || target > matrix.back().back()) return false;
        int x = matrix.size() - 1;
        int y = 0;
        while (1) {
            if (target < matrix[x][y]) --x;
            else if (target > matrix[x][y]) ++y;
            else return true;
            if (x < 0 || y >= matrix[0].size()) break;
        }

        return false;
    }
};

int main()
{
    Solution s;
    {
        vector<vector<int>> matrix = {{1,4,7,11,15},{2,5,8,12,19},{3,6,9,16,22},{10,13,14,17,24},{18,21,23,26,30}};
        int target = 5;
        bool ans = s.searchMatrix(matrix, target);
        cout << ans << endl;
    }
    cout << endl;
    {
        vector<vector<int>> matrix = {{1,4,7,11,15},{2,5,8,12,19},{3,6,9,16,22},{10,13,14,17,24},{18,21,23,26,30}};
        int target = 20;
        bool ans = s.searchMatrix(matrix, target);
        cout << ans << endl;
    }

    return 0;
}
