#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>


using namespace std;


class Solution {
public:
    int searchInsert(vector<int>& nums, int target) {
    #if 0
        for ( int i = 0 ; i < nums.size(); ++ i) {
            if (nums[i] >= target) return i;
        }
        return nums.size();
    #else
        int ans = 0;
        if (target <= nums[0]) return 0;
        if (target > nums.back()) return nums.size();

        int left = 0;
        int right = nums.size();// idx, not value
        while (left < right) {
            int mid = left + (right - left) / 2;
            if (nums[mid] < target) {
                left = mid + 1;
            }
            else
                right = mid;
        }

        return right;
    #endif
    }
};

int main()
{
    Solution solution;
    {
        vector<int> nums{ 1, 3, 5, 6 };
        int target = 5;
        int ans = solution.searchInsert(nums, target);
        cout << ans << endl;
    }
    {
        vector<int> nums{ 1, 3, 5, 6 };
        int target = 2;
        int ans = solution.searchInsert(nums, target);
        cout << ans << endl;
    }
    {
        vector<int> nums{ 1, 3, 5, 6 };
        int target = 7;
        int ans = solution.searchInsert(nums, target);
        cout << ans << endl;
    }
    {
        vector<int> nums{ 1, 3};
        int target = 3;
        int ans = solution.searchInsert(nums, target);
        cout << ans << endl;
    }
    return 0;
}
