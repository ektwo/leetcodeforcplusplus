#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <queue>


using namespace std;


class Solution {
public:
#if 0
    bool sequenceReconstruction(vector<int>& org, vector<vector<int>>& seqs) {
        // Init the graph
        unordered_map<int, int> inDegree;
        unordered_map<int, vector<int>> graph;
        for(auto& seq: seqs) {
            for(auto& num: seq) {
                inDegree[num] = 0;
                graph[num] = {};
            }
        }

        // Build the graph
        for(auto& seq: seqs) {
            if(seq.empty()) {
                continue;
            }

            for(int i = 0; i < seq.size() - 1; ++i) {
                int parent = seq[i];
                int child = seq[i + 1];
                ++inDegree[child];
                graph[parent].push_back(child);
            }
        }
cout << "org size=" << org.size() << endl;
cout << "inDegree size=" << inDegree.size() << endl;
cout << "graph size=" << graph.size() << endl;
        // If inDegree's size != org.size, that means there's not enough information        
        if(inDegree.size() != org.size()) {
            return false;
        }

        // Find sources
        queue<int> sources;
        for(auto& p: inDegree) {
            if(p.second == 0) {
cout << "p.first=" << p.first << endl;
                sources.push(p.first);
            } 
        }

        // Start topological sort
        // In each step, if source's size > 1 or sources.front() does not match org, it's wrong
        vector<int> order;
        while(!sources.empty()) {
            // If there are more than one choice at this time
            if(sources.size() > 1) {
                return false;
            }
cout << "order.size()=" << order.size() << endl;
cout << "org[order.size()]=" << org[order.size()] << endl;
cout << "sources.front()=" << sources.front() << endl;
            if(org[order.size()] != sources.front()) {
                return false;
            }

            int cur = sources.front();
            sources.pop();
cout << "order.push_back cur=" << cur << endl;
            order.push_back(cur);

            for(auto& child: graph[cur]) {
cout << "child=" << child << endl;
cout << "inDegree[child]=" << inDegree[child] << endl;

                if(--inDegree[child] == 0) {
                    sources.push(child);
                }
            }
        }

        return order.size() == org.size();
    }
#else
    bool sequenceReconstruction(vector<int>& nums, vector<vector<int>>& sequences) {
        // init the graph
        unordered_map<int, vector<int>> graph;
        unordered_map<int, int> in_degree;

        for (auto & seq : sequences) {
            for (auto & s : seq) {
                graph[s] = {};
                in_degree[s] = 0;
            }
        }

        // build the graph
        for (auto & seq : sequences) {

                for (int i = 0; i < seq.size() - 1; ++i) {
                    int parent = seq[i];
                    int child = seq[i+1];
                    graph[parent].push_back(child);
                    ++in_degree[child];
                }
        }

        // some conditions sufficient to determine the result
        if (in_degree.size() != nums.size()) return false;

        // find all sources
        queue<int> sources;
        for (auto &in : in_degree) {
            if (in.second == 0)
                sources.push(in.first);
        }

        vector<int> order;

        // start topological sort / build the sorted order
        while (!sources.empty()) {

            // If there are more than one choice at this time
            if (sources.size() > 1) return false;

            int cur = sources.front(); 
            if (nums[order.size()] != cur) return false;
            sources.pop();

            order.push_back(cur);

            for (auto & child: graph[cur]) {
                if (--in_degree[child] == 0)
                    sources.push(child);
            }

        }

        return order.size() == nums.size();
    }
#endif
};


int main()
{
    Solution solution;

    {
        vector<int> nums = {1,2,3};
        vector<vector<int>> sequences = { {1,2},{1,3}};
        bool ans = solution.sequenceReconstruction(nums, sequences);
        cout << "ans=" << ans << endl;
    }
    {
        vector<int> nums = {1,2,3};
        vector<vector<int>> sequences = { {1,2}};
        bool ans = solution.sequenceReconstruction(nums, sequences);
        cout << "ans=" << ans << endl;
    }
    {
        vector<int> nums = {1,2,3};
        vector<vector<int>> sequences = { {1,2},{1,3},{2,3}};
        bool ans = solution.sequenceReconstruction(nums, sequences);
        cout << "ans=" << ans << endl;
    }

    return 0;
}
