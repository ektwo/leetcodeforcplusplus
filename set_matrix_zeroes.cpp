#include <iostream>
#include <vector>
#include <string>

using namespace std;

class Solution
{
private:
    int M, N;
    vector<pair<int, int>> dirs = { {0, -1}, {-1, 0}, {0, 1}, {1, 0}};
    bool is_valid(int i, int j) {
        if ((i >= 0) && (i < M) && (j >=0) && (j < N)) return true;
        return false;
    }
public:
    void setZeroes(vector<vector<int>>& matrix) {
        M = matrix.size();
        N = matrix[0].size();

        bool first_row_zero = false;
        bool first_col_zero = false;
        for (int i = 0; i < M; ++i) {
            if (matrix[i][0] == 0) first_col_zero = true;
        }

        for (int j = 0; j < N; ++j) {
            if (matrix[0][j] == 0) first_row_zero = true;
        }

        for (int i = 1; i < M; ++i) {
            for (int j = 1; j < N; ++j) {
                if (matrix[i][j] == 0) {
                    matrix[0][j] = 0;
                    matrix[i][0] = 0;
                }
            }
        }

        for (int i = 1; i < M; ++i) {
            for (int j = 1; j < N; ++j) {
                if ((matrix[i][0] == 0) || (matrix[0][j] == 0)) {
                    matrix[i][j] = 0;
                }
            }
        }

        if (first_col_zero) {
            for (int i = 0; i < M; ++i) {
                matrix[i][0] = 0;
            }
        }

        if (first_row_zero) {
            for (int j = 0; j < N; ++j) {
                matrix[0][j] = 0;
            }
        }

        // for (int i = 0; i < M; ++i) {
        //     for (int j = 0; j < N; ++j) {
        //         if (matrix[i][j] == 0) {

        //             for (int a = 0; a < M; ++a) {
        //                 if (matrix[a][j] != 0)
        //                     matrix[a][j] = -1;
        //             }
        //             for (int b = 0; b < N; ++b) {
        //                 if (matrix[i][b] != 0)
        //                     matrix[i][b] = -1;
        //             }
                    
        //             // for (int k = 0; k < dirs.size(); ++k) {
        //             //     int new_i = i + dirs[k].first;
        //             //     int new_j = j + dirs[k].second;
        //             //     if (is_valid(new_i, new_j)) {
        //             //         matrix[new_i][new_j] = 0;
        //             //     }
        //             // }
        //         }
        //     }
        // }

        // for (int i = 0; i < M; ++i) {
        //     for (int j = 0; j < N; ++j) {
        //         if (matrix[i][j] == -1) {
        //             matrix[i][j] = 0;
        //         }
        //     }
        // }
    }
};

void print_1d_int(vector<int> nums)
{

        cout << "{";
        for (int j = 0; j < nums.size(); ++j) {
            cout << nums[j] << ",";
        }
        cout << "}," << endl;

}

void print_2d_int(vector<vector<int>> nums)
{
    cout << "{";
    for (int i = 0; i < nums.size(); ++i) {
        cout << "{";
        for (int j = 0; j < nums[0].size(); ++j) {
            cout << nums[i][j] << ",";
        }
        cout << "}," << endl;
    }
    cout << "}," << endl;
}

int main()
{
    Solution s;
    {
        vector<vector<int>> nums = {{1,1,1},{1,0,1},{1,1,1}};
        s.setZeroes(nums);
        print_2d_int(nums);
    }
    cout << endl;
    {
        vector<vector<int>> nums = {{0,1,2,0},{3,4,5,2},{1,3,1,5}};
        s.setZeroes(nums);
        print_2d_int(nums);
    }

    return 0;
}
