#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <queue>


using namespace std;


class Solution {
public:
    int singleNumber(vector<int>& nums) {
        int ans = 0;
         for (auto &num : nums) {
             ans ^= num;
             }
        return ans;
    }
};