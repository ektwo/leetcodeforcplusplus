#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <queue>


using namespace std;



class Solution {

public:
    vector<int> singleNumber(vector<int>& nums) {

        int bitmask = 0;
        for (auto &num : nums) {
            bitmask ^= num;
        }//101, 011
        //bitmask = num1 ^ num2
        //cout << "bitmask=" << bitmask << endl;


        //cout << "~bitmask =" << ~bitmask  << endl;
        int diff = (int)((long)bitmask & ((long)(~bitmask) + 1));
        //cout << "diff=" << diff << endl;

        int num1 = 0;
        for(auto &num: nums) {
            if ((num & diff) != 0)
                num1 ^= num;
        }

        return { num1, bitmask ^ num1};
    }
};

int main()
{
    Solution solution;

    {
        vector<int> nums = {1,2,1,3,2,5};
        vector<int> ans = solution.singleNumber(nums);
        for (int i = 0; i < ans.size(); ++i) {
            cout << ans[i] << ",";
        }
        cout << endl;
    }
    {
        vector<int> nums = {1,2,1,11,2,5};
        vector<int> ans = solution.singleNumber(nums);
        for (int i = 0; i < ans.size(); ++i) {
            cout << ans[i] << ",";
        }
        cout << endl;
    }

    return 0;
}
