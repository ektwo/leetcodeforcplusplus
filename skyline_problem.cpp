//#ifndef __SKYLINE_PROBLEM_H__
//#define __SKYLINE_PROBLEM_H__
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <algorithm>

using namespace std;
class Solution
{
  public:
    //A:[ [2 9 10], [3 7 15], [5 12 12], [15 20 10], [19 24 8] ]
    //A1:[[0,2,3],[2,5,3]]
    //A2:[[2,9,10],[9,12,15]]
    //ALast:[[1,2,1],[1,2,2],[1,2,3]]
    //B:[ [2 10], [3 15], [7 12], [12 0], [15 10], [20 8], [24, 0] ]
    vector<pair<int, int>> getSkyline(vector<vector<int>> &buildings)
    {
        map<int, vector<int>> m_map;
        for (vector<int> v : buildings)
        {
            m_map[v[0]].push_back(v[2]);
            m_map[v[1]].push_back(-v[2]);
        }

        //temprary high point container
        multiset<int> m_multiset;
        m_multiset.insert(0); //set a default point value : ground
        vector<pair<int, int>> res;
        for (auto itMap = m_map.begin(); itMap != m_map.end(); ++itMap)
        {
            //Q:[ [2 9 10], [3 7 15], [5 12 12], [15 20 10], [19 24 8] ]
            //A:[ [2 10], [3 15], [7 12], [12 0], [15 10], [20 8], [24, 0] ].
            cout << " first=[" << (*itMap).first << endl; //[2 [3 [5 [7 [9 [12 [15
            //", " << (*it).second << "]" << endl;
            for (int height : itMap->second)
            {
                cout << " " << height << "]" << endl; // 10] 15] 12] -15] -10] -12] 10]
                if (height > 0)
                {
                    m_multiset.insert(height); //10 15 12 . 10
                    cout << "insert" << endl;
                }
                else
                {
                    m_multiset.erase(m_multiset.find(-height)); //-15->10 X 12, -10 -> X X 12 -> X X X
                    cout << "erase" << endl;
                }
            }

            if (res.empty())
            {
                cout << "empty" << endl;
                res.push_back({itMap->first, *m_multiset.rbegin()}); //{2, 10}
            }
            else
            {
                cout << "*m_multiset.rbegin()=" << *m_multiset.rbegin() << endl; //15 15 12 12 0 10
                cout << "res.back().second=" << res.back().second << endl;       //10 15 12 12 0
                if (*m_multiset.rbegin() != res.back().second)                   // still hava a higher line on working.
                {
                    cout << "push_back {" << itMap->first << ", " << *m_multiset.rbegin() << "}" << endl;
                    //{3, 15} {7 12} {12 0}
                    res.push_back({itMap->first, *m_multiset.rbegin()});
                }
            }
        }
        return res;
    }
};
#include <list>
#include <type_traits>
template <class T, class = void>
struct is_vector : std::is_same<T, std::vector<bool>>
{
};

template <class T, class = void>
struct is_vector_iterator : std::is_same<T, std::vector<bool>::iterator>
{
};

template <class T>
struct is_vector_iterator<T,
                          decltype(*std::declval<T>(),
                                   std::enable_if_t<!std::is_same<T, std::vector<bool>::iterator>::value>())> : std::is_same<T, typename std::vector<std::decay_t<decltype(*std::declval<T>())>>::iterator>
{
};
template <class T, class U>
struct is_same2 : std::false_type
{
};

template <class T>
struct is_same2<T, T> : std::true_type
{
};

int main()
{
// std::cout << is_same2<int, int32_t>::value << '\n';   // true
// std::cout << is_same2<int, int64_t>::value << '\n';   // false
// std::cout << is_same2<float, int32_t>::value << '\n'; // false
// cout << is_same2<int, vector<int>>::value << endl;
// cout << is_vector<vector<bool>>::value << endl;
// cout << is_vector_iterator<vector<bool>>::value << endl;
// cout << is_vector_iterator<vector<bool>::iterator>::value << endl;

// static_assert(is_vector_iterator<std::vector<int>::iterator>::value, "Is not a vector iterator");
// static_assert(is_vector_iterator<std::vector<bool>::iterator>::value, "Is not a vector iterator");
// static_assert(!is_vector_iterator<std::list<int>::iterator>::value, "Is a vector iterator");
// static_assert(!is_vector_iterator<std::list<int>::iterator>::value, "Is a vector iterator");
// return 0;

//vector<vector<int>> buildings = vector<vector<int>>();
#if 1
    static const int X = 5;
    vector<vector<int>> buildings(X, vector<int>(3, 1));
    //buildings = {{0, 3, 3}, {1, 5, 3}, {2, 4, 3}, {3, 7, 3}};
    buildings = {{2, 9, 10}, {3, 7, 15}, {5, 12, 12}, {15, 20, 10}, {19, 24, 8}};
#else
    static const int X = 10000;
    vector<vector<int>> buildings(X, vector<int>(3, 1));
    //buildings[0] = {2, 9, 10};
    //[ [2 9 10], [3 7 15], [5 12 12], [15 20 10], [19 24 8] ]
    //buildings = {{2, 9, 10}, {3, 7, 15}, {5, 12, 12}, {15, 20, 10}, {19, 24, 8}};
    //[ [1 2 1], [1 2 2], [1, 2, 3] ]
    //buildings = {{1, 2, 1}, {1, 2, 2}, {1, 2, 3}};
#endif
    //for (int j = 0; j < 10000; ++j)
    //{
    //    cout << buildings[j][0] << buildings[j][1] << buildings[j][2] << endl;
    //}

    //buildings = {{1, 10001, 10000}, {2, 10001, 9999}, {3, 10001, 9998}, {4, 10001, 9997}};
    //[[1 10001 10000],[2 10001 9999],[3 10001 9998],[4 10001 9997],[9998 10001 3],[9999 10001 2],[10000 10001 1]]
    // for (int i = 0; i < 5; i++)
    // {
    //     for (int j = 0; j < 3; j++)
    //     {
    //         cout << "i=" << i << " j=" << j << " val=" << buildings[i][j] << endl;
    //     }
    // }

    Solution s;
    {

        vector<pair<int, int>> ret = s.getSkyline(buildings);
        for (int i = 0; i < ret.size(); i++)
        {
            cout << "i=" << i << " ret[i].first=" << ret[i].first << " ret[i].second=" << ret[i].second << endl;
        }
    }

    return 0;
}
//#endif