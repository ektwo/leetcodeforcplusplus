#include <iostream>
#include <stack>
#include <algorithm>
#include <vector>
#include <queue>
#include <deque>
using namespace std;

void print_1d_int(vector<int> nums)
{
    cout << "{";
    for (int j = 0; j < nums.size(); ++j) {
        cout << nums[j] << ",";
    }
    cout << "}," << endl;
}

class Solution
{
public:
    vector<int> maxSlidingWindow(vector<int>& nums, int k) {
        vector<int> ans;
        deque<int> d;
        for (int i=0;i<nums.size();++i){
            if(!d.empty()&&((i-k)==d.front())) d.pop_front();
            while (!d.empty()&&nums[d.back()]<nums[i]) d.pop_back();
            d.push_back(i);
            if ((i+1-k)>=0) ans.push_back(nums[d.front()]);
        }

        return ans;
    }
};

int main()
{
    Solution solution;
    {
        vector<int> nums={1,3,-1,-3,5,3,6,7};
        int k=3;
        vector<int> ans = solution.maxSlidingWindow(nums,k);
        print_1d_int(ans);
    }
    cout<<endl;
    {
        vector<int> nums={1};
        int k=1;
        vector<int> ans = solution.maxSlidingWindow(nums,k);
        print_1d_int(ans);
    }

    return 0;
}