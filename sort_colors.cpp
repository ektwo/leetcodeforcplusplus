#include <iostream>
#include <vector>
#include <string>

using namespace std;

void print_1d_int(vector<int> nums)
{

        cout << "{";
        for (int j = 0; j < nums.size(); ++j) {
            cout << nums[j] << ",";
        }
        cout << "}," << endl;

}

void print_2d_int(vector<vector<int>> nums)
{
    cout << "{";
    for (int i = 0; i < nums.size(); ++i) {
        cout << "{";
        for (int j = 0; j < nums[0].size(); ++j) {
            cout << nums[i][j] << ",";
        }
        cout << "}," << endl;
    }
    cout << "}," << endl;
}

class Solution
{
private:
    int M, N;
    vector<pair<int, int>> dirs = { {0, -1}, {-1, 0}, {0, 1}, {1, 0}};
    bool is_valid(int i, int j) {
        if ((i >= 0) && (i < M) && (j >=0) && (j < N)) return true;
        return false;
    }
public:
    void sortColors(vector<int>& nums) {
        const int RED = 0;
        const int WHITE = 1;
        const int BLUE = 2;
        int i = 0;
        int l = 0;
        int r = nums.size()-1;

        while (l <= r) {
            cout << "i=" << i << " l=" << l << " r=" << r << endl;
            print_1d_int(nums);

            if (i > r) break;
            if (nums[i] == RED) { swap(nums[i++], nums[l++]); }
            else if (nums[i] == BLUE) { swap(nums[i], nums[r--]); }
            else { ++i; }
            
        }
    }
};

int main()
{
    Solution s;
    {
        vector<int> nums = {2,0,2,1,1,0};
        s.sortColors(nums);
        print_1d_int(nums);
    }
    cout << endl;
    {
        vector<int> nums = {2,0,1};
        s.sortColors(nums);
        print_1d_int(nums);
    }

    return 0;
}
