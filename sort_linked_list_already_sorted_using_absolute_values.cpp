#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <limits>


using namespace std;

//Definition for singly-linked list.
 struct ListNode {
      int val;
      ListNode *next;
      ListNode() : val(0), next(nullptr) {}
      ListNode(int x) : val(x), next(nullptr) {}
      ListNode(int x, ListNode *next) : val(x), next(next) {}
 };


void print_list(ListNode *root)
{
    if (root) {
        cout << root->val << ",";
        if (root->next) print_list(root->next);
    }
    else
        cout << endl;
}

class Solution {
public:

    void insert_a_node(ListNode *root, ListNode *node) {
        if (!root || !node) return;
        ListNode *tmp = root->next;
        root->next = node;
        node->next = tmp;
    }

    void delete_node_release(ListNode *prev, bool release, bool link_to_target_next) {
        if ((prev) && (prev->next)) {
            ListNode *node_to_release = prev->next;
            if (link_to_target_next)
                prev->next = node_to_release->next;
            node_to_release->next = nullptr;
            if (release)
                delete node_to_release;
        }
    }

    ListNode* sortLinkedList(ListNode* head) {
        if ((!head) || (!head->next)) return head;
        
        ListNode *dummy = new ListNode();
        dummy->next = head;
        ListNode *prev = head;

        while (prev->next) {
            if (prev->val > prev->next->val) {
                ListNode *head_target_to_link = prev->next;
                delete_node_release(prev, false, true);
                insert_a_node(dummy, head_target_to_link);
            }
            else {
                prev=prev->next;
            }
        }

        return dummy->next;
    }
};

int main()
{
    Solution solution;
    {
        ListNode** lna = new ListNode * [6];
        lna[5] = new ListNode(-10, nullptr);
        lna[4] = new ListNode(10, lna[5]);
        lna[3] = new ListNode(5, lna[4]);
        lna[2] = new ListNode(-5, lna[3]);
        lna[1] = new ListNode(2, lna[2]);
        lna[0] = new ListNode(0, lna[1]);

        ListNode *dummy = solution.sortLinkedList(lna[0]);
        print_list(dummy);
    }
    cout << endl;
    {
        ListNode** lna = new ListNode * [3];
        lna[2] = new ListNode(2, nullptr);
        lna[1] = new ListNode(1, lna[2]);
        lna[0] = new ListNode(0, lna[1]);

        ListNode *dummy = solution.sortLinkedList(lna[0]);
        print_list(dummy);
    }
    cout << endl;
    {
        ListNode** lna = new ListNode * [4];
        lna[3] = new ListNode(-9, nullptr);
        lna[2] = new ListNode(-7, lna[3]);
        lna[1] = new ListNode(-3, lna[2]);
        lna[0] = new ListNode(-1, lna[1]);

        ListNode *dummy = solution.sortLinkedList(lna[0]);
        print_list(dummy);
    }
    cout << endl;
    {
        ListNode** lna = new ListNode * [4];
        lna[3] = new ListNode(5, nullptr);
        lna[2] = new ListNode(3, lna[3]);
        lna[1] = new ListNode(3, lna[2]);
        lna[0] = new ListNode(-1, lna[1]);

        ListNode *dummy = solution.sortLinkedList(lna[0]);
        print_list(dummy);
    }
}