#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <limits>


using namespace std;

//Definition for singly-linked list.
 struct ListNode {
      int val;
      ListNode *next;
      ListNode() : val(0), next(nullptr) {}
      ListNode(int x) : val(x), next(nullptr) {}
      ListNode(int x, ListNode *next) : val(x), next(next) {}
 };


class Solution {
public:
    void _merge(vector<int> &v, int front, int mid, int end) {
        vector<int> sub_l = vector<int>(v.begin()+front, v.begin()+mid+1);
        vector<int> sub_r = vector<int>(v.begin()+mid+1, v.begin()+end+1);
        sub_l.insert(sub_l.end(), numeric_limits<int>::max());
        sub_r.insert(sub_r.end(), numeric_limits<int>::max());
        int idx_l = 0, idx_r = 0;
        for (int i = front; i <= end; ++i) {
            if (sub_l[idx_l] < sub_r[idx_r]) v[i] = sub_l[idx_l++];
            else                             v[i] = sub_r[idx_r++];
        }
    }

    void merge_sort(vector<int> &v, int front, int end) {

        if (front >= end) return;
        int mid = front + (end - front) / 2;
        merge_sort(v, front, mid);
        merge_sort(v, mid+1, end);
        _merge(v, front, mid, end);
    }

    ListNode* merge_sortList(ListNode *l, ListNode *r) {
        ListNode *dummy = new ListNode(-1);
        ListNode *p = dummy;
        while (l && r) {
            if (l->val < r->val) {
                p->next = l;
                l = l->next;
            }
            else {
                p->next = r;
                r = r->next;
            }
            p = p->next;
        }

        while (l) { p->next = l; p = p->next; l = l->next;}
        while (r) { p->next = r; p = p->next; r = r->next;}
        return dummy->next;
    }

    ListNode* sortList(ListNode* head) {
        if ((!head) || (!head->next)) return head;
        ListNode *slow = head;
        ListNode *fast = head;
        ListNode *stop = head;
        while (fast != nullptr && fast->next != nullptr) {
            stop = slow;
            slow = slow->next;
            fast = fast->next->next;
        }
        stop->next = nullptr;
        return merge_sortList(sortList(head), sortList(slow));
    }
};

int main()
{
    Solution s;
    {
        ListNode** lna = new ListNode * [4];
        lna[3] = new ListNode(3, nullptr);
        lna[2] = new ListNode(1, lna[3]);
        lna[1] = new ListNode(2, lna[2]);
        lna[0] = new ListNode(4, lna[1]);

        ListNode *dummy = s.sortList(lna[0]);
        ListNode* ret_tmp = dummy;
        cout << "[";
        while (ret_tmp) {
            if (!ret_tmp->next)
                cout << ret_tmp->val;
            else
                cout << ret_tmp->val << ",";
            
            ret_tmp = ret_tmp->next;
        }
        cout << "]" << endl;
    }
}