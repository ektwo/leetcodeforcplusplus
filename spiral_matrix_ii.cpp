#include <iostream>
#include <vector>
#include <string>

using namespace std;

void print_2d_int(vector<vector<int>> nums)
{
    cout << "{";
    for (int i = 0; i < nums.size(); ++i) {
        cout << "{";
        for (int j = 0; j < nums[0].size(); ++j) {
            cout << nums[i][j] << ",";
        }
        cout << "}," << endl;
    }
    cout << "}," << endl;
}

class Solution
{
private:
    vector<pair<int, int>> dirs = { {0,1}, {1,0}, {0,-1}, {-1,0}};
    bool is_border_valid(int m, int n, int x, int y) {
        if ((x < 0) || (y < 0) || (x >= m) || (y >= n) || (x < 0)) return false;
        return true;
    }
public:
    vector<vector<int>> generateMatrix(int n) {
        vector<vector<int>> ans(n, vector<int>(n, 0));
        int loop = (n%2) ? (n/2 + 1) : (n/2);
        int val = 1;
        int row = 0, col = 0;

        ans[row][col]=val;
        while (--loop >= 0) 
        {
            //cout <<"loop=" << loop << endl;
            for (auto &dir : dirs) {
                //cout << "dir.first=" <<dir.first<< " dir.second=" << dir.second << endl;
                while (true) 
                {
                    int x = row+dir.first;
                    int y = col+dir.second;
                    //cout << "x=" <<x<< " y=" << y << endl;
                    if (is_border_valid(n, n, x, y) && (ans[x][y] == 0)) {
                        row=x, col=y;
                        ans[row][col] = ++val;
                    //    cout << "ans[" << row << "][" << col << "]=" << ans[row][col] << endl;
                    }
                    else {
                    //    cout << "is_border_valid(n, n, x, y)=" << is_border_valid(n, n, x, y) << endl;
                    //    if (is_border_valid(n, n, x, y))
                    //        cout << "ans[x][y]=" << ans[x][y] << endl;
                        break;
                    }
                    //if (row==2 && col==2) break;
                }
            }
        }

        return ans;
    }
};


int main()
{
    Solution solution;
    {
        int n = 3;
        vector<vector<int>> ans = solution.generateMatrix(n);
        print_2d_int(ans);
    }
    cout << endl;
    {
        int n = 1;
        vector<vector<int>> ans = solution.generateMatrix(n);
        print_2d_int(ans);
    }

    return 0;
}
