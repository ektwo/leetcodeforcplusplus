#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>


using namespace std;


class Solution {
public:
	void expand(vector<int>& nums, int left, int right, int& min_product, int& max_product, int& best_product) {


		while (left >= 0 && right < nums.size()) {

			if (nums[right] > 0) {
				max_product = max(max_product * nums[right], nums[right]);
				min_product = min(min_product * nums[right], nums[right]);
			}
			else if (nums[right] == 0) {
				max_product = max(max_product * nums[right], nums[right]);
				min_product = min(min_product * nums[right], nums[right]);
			}
			else {
				int tmp_max_product = max_product;
				max_product = max(min_product * nums[right], nums[right]);
				min_product = min(tmp_max_product * nums[right], nums[right]);
			}
			best_product = max(best_product, max_product);

			++right;
		}

	}

	int maxProduct(vector<int>& nums) {
		int best_product = nums[0];
		int min_product = best_product;
		int max_product = best_product;
		int i = 1;
		//for (i = 1; i < nums.size(); i++) {
		expand(nums, i, i, min_product, max_product, best_product);
		//}

		return best_product;
	}
	int coinChange(vector<int>& coins, int amount) {
		if (amount == 0) return 0;

		//int small_coin_price = INT_MAX;
		//for (auto& coin : coins) {
		//	if (coin < small_coin_price)
		//		small_coin_price = coin;
		//}

		//if (amount < small_coin_price) {
		//	return 0;
		//}

		int* combinations = new int[amount + 1];
		int* combination_cnt = new int[amount + 1];
		for (int i = 0; i <= amount; ++i) {
			combinations[i] = 0;
			combination_cnt[i] = amount + 1;
		}

		combinations[0] = 1;
		combination_cnt[0] = 0;

		for (auto& coin : coins) {
			for (int i = 0; i <= amount; ++i) {
				if (i >= coin) {
					combinations[i] += combinations[i - coin];
				}
			}
		}

		for (auto& coin : coins) {
			for (int i = coin; i <= amount; ++i) {
				combination_cnt[i] = min(combination_cnt[i], combination_cnt[i - coin] + 1);
			}
		}
		return (combination_cnt[amount] == amount + 1) ? (-1) : (combination_cnt[amount]);
	}


	int splitArray(vector<int>& nums, int m) {
		const int nums_size = nums.size();
		vector<int> sums = vector<int>(nums_size);
		vector<vector<int>> dp(m + 1, vector<int>(nums_size, INT_MAX));
		sums[0] = nums[0];
		for (int i = 1; i < nums_size; ++i) {
			sums[i] = sums[i - 1] + nums[i];
		}
		// dp[0][x] : 0 is impossible
		for (int i = 0; i < nums_size; ++i)
			dp[1][i] = sums[i];

		for (int i = 2; i <= m; ++i) {
			for (int j = i - 1; j < nums_size; ++j) {
				for (int k = 0; k < j; ++k) {
					dp[i][j] = min(dp[i][j], max(dp[i-1][k], sums[j] - sums[k]));
				}
			}
		}

		return dp[m][nums_size - 1];
	}


};

int main()
{
	Solution solution;

	{
		vector<int> v = { 7,2,5,10,8 };
		cout << solution.splitArray(v, 2) << endl;
	}
	{
		vector<int> v = { 1,2,3,4,5 };
		cout << solution.splitArray(v, 2) << endl;
	}
	{
		vector<int> v = { 1,4,4 };
		cout << solution.splitArray(v, 3) << endl;
	}
	return 0;
}