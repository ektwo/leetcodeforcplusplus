#include <limits.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <queue>


using namespace std;


class Solution {
private:
    int m_cur_target;
    int m_end_target;
    vector<vector<int>> ans;
    vector<vector<int>> dirs = {
        { 0, -1}, { -1, 0}, { 0, 1}, { 1, 0}
    };

    bool is_end_condition() {
        //if (m_cur_target == m_end_target) 
            return true;
        //return false;
    }

    bool is_unsatisfied_condition() {
        if (m_cur_target > m_end_target) return true;
        return false;
    }

    bool is_branch_can_be_pruned(vector<int>& nums, int candidate_start, int candiadate_end, int candidate) {

        //if ((nums[candidate_start-1] == nums[candidate_start])) {
        //    cout << "nums[" << candidate_start << "]=" << nums[candidate_start] <<  endl;
        //    return true;
        //}
        return false;
    }

    bool trune_branch_postproc(vector<int>& nums, int &candidate_start, int candiadate_end, int candidate) {
        while ((candidate_start + 1 < candiadate_end) && (nums[candidate_start+1] == nums[candidate_start])) {
            //cout << "nums[" << candidate_start << "]=" << nums[candidate_start] <<  endl;
            candidate_start++;
        }
        return true;
    }

    bool subsetsWithDup_dfs(vector<int>& nums, int candidate_start, int target, vector<int> &tmp_solu) {

        bool ret = false;

        m_cur_target = target;
        if (is_end_condition()) { /* find a solution with a complete ending */
            /* do something */
            ans.push_back(tmp_solu);
            //return true;
        }
        if (is_unsatisfied_condition()) { /* ignore possible solution at this step that do not satisfy the problem constraints */
            return false;
        }

        /* traverse all executable branch path */
        const int candidates_end = nums.size();
        for (int i = candidate_start; i < candidates_end; ++i)
        {
            int candidate = nums[i];

            /* prune, check if it matches the current (level/target) pruning conditions */
            if (is_branch_can_be_pruned(nums, i, candidates_end, candidate)) continue;

            /* update state variables */
            tmp_solu.push_back(candidate);

            /* recursively execute the logic of the next step */
            bool ret = subsetsWithDup_dfs(nums, i + 1, target, tmp_solu);

            /* reset state variables */
            tmp_solu.pop_back();

            /* cut off the branch */
            trune_branch_postproc(nums, i, candidates_end, candidate);
        }

        return false;
    }
public:

    void probe() {
        m_cur_target = 0;
        m_end_target = 0;
        ans.clear();
    }

    int findMin(vector<int>& nums) {
        
        int left = 0;
        int right = nums.size() - 1;

        //while (left <= right) {  //target
        while (left < right) { 
            int mid = left + (right - left) / 2;

            if (nums[mid] <= nums[right]) { // rotated at left side
                right = mid; // no minus 1, due to less to & equal to ?
            }
            else {
                left = mid + 1;
            }
        }

        return nums[right];//-1;
    }
    int mySqrt(int x) {
        if (x <= 1) return x;
        float tmp_x = (float)x;
        int left = 0;
        int right = x;

        while (left < right) {
            int mid = left + (right - left) / 2.0;
            int target = x/mid;

            if (mid == target) {
                left = mid + 1;
            }
            else if (mid <= target)  {
                left = mid + 1;
            }
            else if (mid > target) {
                right = mid;
            }
        }

        return right - 1;
    }
};

int main()
{
    Solution solution;

    solution.probe();
    {
        vector<int> nums = { 3,4,5,1,2};
        int ans = solution.mySqrt(4);
        cout << ans << endl;
    }
    solution.probe();
    {
        vector<int> nums = { 4,5,6,7,0,1,2};
        int ans = solution.mySqrt(8);
        cout << ans << endl;
    }
    solution.probe();
    {
        vector<int> nums = { 11,13,15,17};
        int ans = solution.mySqrt(9);
        cout << ans << endl;
    }
    solution.probe();
    {
        vector<int> nums = { 11,13,15,17};
        int ans = solution.mySqrt(0);
        cout << ans << endl;
    }
    return 0;
}
