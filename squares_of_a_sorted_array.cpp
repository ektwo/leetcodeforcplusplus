#include <limits>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <queue>
#include <cmath>


using namespace std;


class Solution {

public:
    vector<int> sortedSquares(vector<int>& nums) {
    #if 1
        int left = 0;
        int right = nums.size()-1;
        while (left < right) {
            long pow_l = pow(nums[left],2);
            long pow_r = pow(nums[right],2);
            if (pow_l<pow_r) {
                nums[right]=pow_r;
                --right;
            }
            else {
                int l=left;
                int tmp = pow_l;
                while (l<right) {
                    //nums[l]=pow(nums[l+1],2);
                    nums[l]=nums[l+1];
                    ++l;
                }
                nums[right--]=tmp;
            }
        }
        nums[0]=pow(nums[0],2);
        return nums;
    #else
        int n = nums.size();
        vector<int> squares(n);

        int l = 0;
        int r = n - 1;
        int highest_idx = r;
        while (l <= r) {
            int pow_l = pow(nums[l], 2);
            int pow_r = pow(nums[r], 2);

            if (pow_l < pow_r) {
                squares[highest_idx--] = pow_r;
                --r;
            }
            else {
                squares[highest_idx--] = pow_l;
                ++l;
            }
        }
        return squares;
    #endif
    }
};

int main()
{
    Solution solution;
    {
        vector<int> nums = {-4,-1,0,3,10};
        vector<int> ans = solution.sortedSquares(nums);
        for (int i = 0; i < ans.size(); ++i) {
            cout << ans[i] << ",";
        }
        cout << endl;
    }

    {
        vector<int> nums = {-7,-3,2,3,11};
        vector<int> ans = solution.sortedSquares(nums);
        for (int i = 0; i < ans.size(); ++i) {
            cout << ans[i] << ",";
        }
        cout << endl;
    }

    return 0;
}
