#include <stdlib.h>
#include <iostream>
#include <iomanip>
#include <vector>
#include <set>
#include <map>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>


using namespace std;


class Solution {
    //0, 1, 6, 8, 9

public:
    bool isStrobogrammatic(string num) {
        bool ret = false;
        unordered_map<char, char> mapping = {
            { '0', '0'},
            { '1', '1'},
            { '6', '9'},
            { '9', '6'},
            { '8', '8'},
        };

        int begin = 0;
        int end = num.size() - 1;
        while (begin <= end) {
            cout << 
                " num[begin]=" << num[begin] <<
                " num[end]=" << num[end] <<
                " mapping[num[begin]]=" << mapping[num[begin]] <<
                " mapping[num[end]]=" << mapping[num[end]] << endl;

            if ((mapping[num[begin]]) && (mapping[num[end]]) && (num[begin] == mapping[num[end]]) && (num[end] == mapping[num[begin]]))
            {
                begin++;
                end--;
                ret = true;
            }
            else {
                ret = false;
                break;
            }
        }

        return ret;
    }
};


int main()
{
    {
        Solution solution;
        string v = { "69" };
        cout << solution.isStrobogrammatic(v) << endl;
    }

    {
        Solution solution;
        string v = { "88" };
        cout << solution.isStrobogrammatic(v) << endl;
    }

    {
        Solution solution;
        string v = { "962" };
        cout << solution.isStrobogrammatic(v) << endl;
    }
    {
        Solution solution;
        string v = { "2" };
        cout << solution.isStrobogrammatic(v) << endl;
    }
    {
        Solution solution;
        string v = { "1" };
        cout << solution.isStrobogrammatic(v) << endl;
    }
    return 0;
}