#include <stdlib.h>
#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <set>
#include <map>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>


using namespace std;


class Solution {
public:
    //0, 1, 6, 8, 9
    vector<char> mapping_odd;
    unordered_map<char, char> mapping_even;
    vector<pair<string, string>> pair_even;


    bool isStrobogrammatic(string num) {
        bool ret = false;

        if (num.size() == 1) {
            //cout << "isStrobogrammatic ##A num=" << num << endl;
            for (auto& ch : mapping_odd) {
                if (ch == num[0]) return true;
            }
            
        }
        else {
            //cout << "isStrobogrammatic ##B num=" << num << endl;
            int begin = 0;
            int end = num.size() - 1;
            while (begin <= end) {
#if 0
                cout <<
                    " num[begin]=" << num[begin] <<
                    " num[end]=" << num[end] <<
                    " mapping[num[begin]]=" << mapping[num[begin]] <<
                    " mapping[num[end]]=" << mapping[num[end]] << endl;
#endif
                if ((mapping_even[num[begin]]) && (mapping_even[num[end]]) && (num[begin] == mapping_even[num[end]]) && (num[end] == mapping_even[num[begin]]))
                {
                    begin++;
                    end--;
                    ret = true;
                }
                else {
                    ret = false;
                    break;
                }
            }
        }
        //cout << "ret=" << ret << endl;
        return ret;
    }

    void _findStrobogrammatic(int n, vector<string> &ans) {
        if (n == 0) return;
        else if (n == 1) {
            int min = (pow(10, n - 1) == 1) ? (0) : (pow(10, n - 1));
            int max = pow(10, n);
            for (int i = min; i < max; ++i) {
                string str = to_string(i);
                if (isStrobogrammatic(str))
                    ans.push_back(str);
            }
            return;
        }

        int min = (pow(10, n - 1) == 1) ? (0) : (pow(10, n - 1));
        int max = pow(10, n);
        for (int i = min; i < max; ++i) {
            int _begin = i / min;
            int _end = i % 10;
            string str = to_string(_begin * 10 + _end);
            if (isStrobogrammatic(str))
                _findStrobogrammatic(n - 2, ans);
                //ans.push_back(str);
        }
        return;
    }

    bool _findStrobogrammatic2(string str, string str_in_calc, vector<string>& ans, bool& ret) {
        //cout << "_findStrobogrammatic2 A ret=" << ret << endl;
        if (!ret) return ret;
        bool tmp_ret = true;
        int str_len = str_in_calc.size();
        //cout << "_findStrobogrammatic2 B str_len=" << str_len << endl;
        if (str_len == 0) return tmp_ret;
        else if (str_len == 1) {
            if (isStrobogrammatic(str_in_calc)) {
                ans.push_back(str);
                return true;
            }
            else
                tmp_ret = false;
        }
        else if (str_len == 2) {
            if (isStrobogrammatic(str_in_calc)) {
                ans.push_back(str);
                return true;
            }
            else
                tmp_ret = false;
        }

        if (str_len % 2 == 0) {
            string tmp_str;
            tmp_str.push_back(str_in_calc.at(0));
            tmp_str.push_back(str_in_calc.at(str_len - 1));
            if (isStrobogrammatic(tmp_str)) {
                string new_str(str_in_calc.begin() + 1, str_in_calc.end() - 1);
                tmp_ret = _findStrobogrammatic2(str, new_str, ans, ret);
            }
            else
                tmp_ret = false;

        }
        else {
            string tmp_str;
            //cout << "_findStrobogrammatic2 C str=" << str
            //    << " str_in_calc=" << str_in_calc
            //    << endl;

            tmp_str.push_back(str_in_calc.at(0));
            tmp_str.push_back(str_in_calc.at(str_len - 1));
            if (isStrobogrammatic(tmp_str)) {
                string new_str(str_in_calc.begin() + 1, str_in_calc.end() - 1);
                //cout << "_findStrobogrammatic2 D str=" << str
                //    << " str_in_calc=" << str_in_calc
                //    << " new_str=" << new_str << endl;
                tmp_ret = _findStrobogrammatic2(str, new_str, ans, ret);
                //cout << "done" << endl;
            }
            else
                tmp_ret = false;

        }
        ret = tmp_ret;
        return tmp_ret;
    }
    vector<string> findStrobogrammatic(int n) {
        vector<string> ans;

        mapping_odd = { '0' , '1', '8' };

        mapping_even = {
                { '0', '0'},
                { '1', '1'},
                { '6', '9'},
                { '9', '6'},
                { '8', '8'},
        };
        pair_even.push_back(make_pair("0", "0"));
        pair_even.push_back(make_pair("1", "1"));
        pair_even.push_back(make_pair("6", "9"));
        pair_even.push_back(make_pair("9", "6"));
        pair_even.push_back(make_pair("8", "8"));

        vector<string> one = { "0", "1", "8" };
        vector<string> two = { "" };

        if (n % 2 == 1) ans = one;
        else ans = two;

    #if 1

        for (int i = (n % 2) + 2; i <= n; i += 2) {
            vector<string> tmp;
            for (auto& str : ans) {
                if (i != n) tmp.push_back(pair_even[0].first + str + pair_even[0].second);
                tmp.push_back(pair_even[1].first + str + pair_even[1].second);
                tmp.push_back(pair_even[2].first + str + pair_even[2].second);
                tmp.push_back(pair_even[3].first + str + pair_even[3].second);
                tmp.push_back(pair_even[4].first + str + pair_even[4].second);
            }
            ans = tmp;
        }

    #else
        int min = (pow(10, n - 1) == 1) ? (0) : (pow(10, n - 1));
        int max = pow(10, n);
        for (int i = min; i < max; ++i) {
            //cout << "i=" << i << " ch=" << std::to_string(i) << endl;
            bool ret = true;
            //cout << "@@start@@ i=" << i << endl;
            _findStrobogrammatic2(std::to_string(i), std::to_string(i), ans, ret);
        }
    #endif

        return ans;
    }
};


int main()
{
#if 0
    {
        Solution solution;
        vector<string> ans = solution.findStrobogrammatic(1);
        for (auto& str : ans) {
            cout << "\"" << str << "\",";
        }
        cout << endl;
    }

    {
        Solution solution;
        vector<string> ans = solution.findStrobogrammatic(2);
        for (auto& str : ans) {
            cout << "\"" << str << "\",";
        }
        cout << endl;
    }
#endif
    {
        Solution solution;
        vector<string> ans = solution.findStrobogrammatic(7);
        for (auto& str : ans) {
            cout << "\"" << str << "\",";
        }
        cout << endl;
    }

    return 0;
}