#include <limits.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <queue>


using namespace std;


class Solution {

public:

    int subarraySum(vector<int>& nums, int k) {
        int ans=0;

        unordered_map<int, int> dict {{0, 1}};
        int sum = 0;
        for (int i = 0; i < nums.size(); ++i) {
            sum += nums[i];
            ans += dict[sum-k];
            dict[sum]++;
        }

        return ans;
    }
};

int main()
{
    Solution solution;
    {
        vector<int> nums = { 1, 2,3,3,3,6};
        int k = 6;
        int ans = solution.subarraySum(nums, k);
        cout << "ans=" << ans << endl;
    }
    {
        vector<int> nums = { 1, 1, 1};
        int k = 2;
        int ans = solution.subarraySum(nums, k);
        cout << "ans=" << ans << endl;
    }

    {
        vector<int> nums = { 1, 1, 1};
        int k = 2;
        int ans = solution.subarraySum(nums, k);
        cout << "ans=" << ans << endl;
    }

    return 0;
}
