#include <limits.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <queue>


using namespace std;


class Solution {
private:
    int m_cur_target;
    int m_end_target;
    vector<vector<int>> ans;
    vector<vector<int>> dirs = {
        { 0, -1}, { -1, 0}, { 0, 1}, { 1, 0}
    };

    bool is_end_condition() {
        //if (m_cur_target == m_end_target) 
            return true;
        //return false;
    }

    bool is_unsatisfied_condition() {
        if (m_cur_target > m_end_target) return true;
        return false;
    }

    bool is_branch_can_be_pruned(vector<int>& nums, int candidate_start, int candiadate_end, int i) {
        //if (i > candidate_start && (candidates[i-1] == candidates[i])) return true;
        return false;
    }

    bool subsets_dfs(vector<int>& nums, int candidate_start, int target, vector<int> &tmp_solu) {

        bool ret = false;

        m_cur_target = target;
        if (is_end_condition()) { /* find a solution with a complete ending */
            /* do something */

            ans.push_back(tmp_solu);

            //return true;
        }
        if (is_unsatisfied_condition()) { /* ignore possible solution at this step that do not satisfy the problem constraints */
            return false;
        }

        /* traverse all executable branch path */
        const int candidates_end = nums.size();
        for (int i = candidate_start; i < candidates_end; ++i)
        {
            int candidate = nums[i];

            /* prune, check if it matches for pruning */
            if (is_branch_can_be_pruned(nums, candidate_start, candidates_end, candidate)) continue;

            /* update state variables */
            tmp_solu.push_back(candidate);

            /* recursively execute the logic of the next step */
            //for (auto &dir : dirs)
            {
                bool ret = subsets_dfs(nums, i + 1, target, tmp_solu);
            }

            /* reset state variables */
            tmp_solu.pop_back();
        }

        return false;
    }
public:

    void probe() {
        m_cur_target = 0;
        m_end_target = 0;
        ans.clear();
    }

    vector<vector<int>> subsets(vector<int>& nums) {
        vector<int> tmp_solu;

        m_cur_target = 0;
        m_end_target = 0;
        //for (int i = 0; i < n; ++i)
        {
            //for (int j = 0; j < m; ++j)
            {
                subsets_dfs(nums, 0, 0, tmp_solu);
                //if (ans) return ans;
            }
        }

        return ans;
    }
};

int main()
{
    Solution solution;

    solution.probe();
    {
        vector<int> nums = { 1, 2, 3};
        vector<vector<int>> ans = solution.subsets(nums);
        //cout << "ans=" << ans << endl;
        cout << " { ";
        for (int i = 0; i < ans.size(); ++i) {
            cout << " { ";
            for (int j = 0; j < ans[i].size(); ++j) {
                if (j != ans[i].size() - 1)
                    cout << ans[i][j] << ",";
                else
                    cout << ans[i][j];
            }
            cout << " }, ";
        }
        cout << " }, " << endl;
    }
    solution.probe();
    {
        vector<int> nums = { 0};
        vector<vector<int>> ans = solution.subsets(nums);
        //cout << "ans=" << ans << endl;
        cout << " { ";
        for (int i = 0; i < ans.size(); ++i) {
            cout << " { ";
            for (int j = 0; j < ans[i].size(); ++j) {
                if (j != ans[i].size() - 1)
                    cout << ans[i][j] << ",";
                else
                    cout << ans[i][j];
            }
            cout << " }, ";
        }
        cout << " }, " << endl;
    }

    return 0;
}
