#include <limits.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <queue>
#include <limits>


using namespace std;

template<class T>
void print_1d(const T& nums)
{
    cout << "{";
    for (int j = 0; j < nums.size(); ++j) {
        cout << nums[j] << ",";
    }
    cout << "}," << endl;
}
template<class T>
void print_2d(const T& nums)
{
    cout << "{";
    for (int i = 0; i < nums.size(); ++i) {
        cout << "{";
        for (int j = 0; j < nums[i].size(); ++j) {
            cout << nums[i][j] << ",";
        }
        cout << "}," << endl;
    }
    cout << "}," << endl;
}

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

class Node1 {
public:
    int val;
    vector<Node1*> children;

    Node1() {}

    Node1(int _val) {
        val = _val;
    }

    Node1(int _val, vector<Node1*> _children) {
        val = _val;
        children = _children;
    }
};
Node1* newNode(int key)
{
    Node1* temp = new Node1;
    temp->val = key;
    return temp;
}

class Node {
public:
    int val;
    Node* left;
    Node* right;
    Node* next;

    Node() : val(0), left(NULL), right(NULL), next(NULL) {}

    Node(int _val) : val(_val), left(NULL), right(NULL), next(NULL) {}

    Node(int _val, Node* _left, Node* _right, Node* _next)
        : val(_val), left(_left), right(_right), next(_next) {}
};


class Solution {

private:
    int node_nums;
    int tree_height;
    TreeNode *m_head;
    bool _isValidBST(TreeNode* root, long min, long max) {
        if (!root) return true;
        cout << "root=" << root << endl;
        cout << "root->val=" << root->val << endl;
        cout << "root->left=" << root->left << " min=" << min << " max=" << max << endl;
        cout << "root->right=" << root->right << " min=" << min << " max=" << max << endl;
        if (root->val <= min) return false;
        if (root->val >= max) return false;
        
        return _isValidBST(root->left, min, root->val) && _isValidBST(root->right, root->val, max);
    }

public:
    void reset() {
        node_nums = 0;
        tree_height = 0;
        m_head = nullptr;
    }

    TreeNode* get_head() {
        return m_head;
    }

    TreeNode* push(TreeNode *root, int val, bool left, bool right)
    {
        TreeNode* temp = new TreeNode(val);
        //cout << "push val=" << val << " m_head=" << m_head << endl;
        //cout << "push val=" << val << " root=" << root << endl;
        if (!m_head) {
            m_head = temp;
        }

        if (!root)
            root = m_head;

        if (root && left)
            root->left = temp;
        else if (root && right)
            root->right = temp;

        return temp;
    }

    vector<vector<int>> levelOrder(TreeNode* root) {
        if (!root) return {};
        vector<vector<int>> ans;

        queue<TreeNode*> q;
        q.push(root);
        while (!q.empty()) {
            vector<int> tmp_solu;
            for (int i = q.size(); i > 0; --i) {
                TreeNode *tmp = q.front(); q.pop();
                if (!tmp) break;
                tmp_solu.push_back(tmp->val);

                if (tmp->left) q.push(tmp->left);
                if (tmp->right) q.push(tmp->right);
            }
            ans.push_back(tmp_solu);
        }


        return ans;
    }

    vector<vector<int>> levelOrderBottom(TreeNode* root) {
        if (!root) return {};

        vector<vector<int>> ans;
        queue<TreeNode*> q;
        q.push(root);
        //ans.push_back({root->val});

        while (!q.empty()) {
            vector<int> tmp;
            int size=q.size();
            for (int i=0;i<size;++i) {
                TreeNode* node=q.front(); q.pop();
                if (!node) break;
                tmp.push_back(node->val);
                if (node->left) q.push(node->left);
                if (node->right) q.push(node->right);
            }
            ans.push_back(tmp);
        }
        reverse(ans.begin(), ans.end());
        return ans;
    }
    vector<double> averageOfLevels(TreeNode* root) {
        vector<double> ans;
        if (!root) return {};

        queue<TreeNode*> q;
        q.push(root);

        while (!q.empty()) {
            int size=q.size();
            double total=0,average=0;
            for (int i=size;i>0;--i) {
                TreeNode *node=q.front(); q.pop();
                total+=node->val;
                if (node->left) q.push(node->left);
                if (node->right) q.push(node->right);
            }
            ans.push_back(total/size);
        }
        return ans;
    }
    vector<vector<int>> levelOrder(Node1* root) {
        vector<vector<int> > ans;
        if (!root) return {};

        queue<Node1*> q;
        q.push(root);

        while (!q.empty()) {
            int size=q.size();
            vector<int> tmp_solu;
            for (int i=size;i>0;--i) {
                Node1 *tmp=q.front();q.pop();
                tmp_solu.push_back(tmp->val);
                for (auto &child: tmp->children) {
                    q.push(child);
                }
                // while (child) {
                    
                // }
            }
            ans.push_back(tmp_solu);
        }
 
        return ans;
    }    
    vector<int> largestValues(TreeNode* root) {
        if (!root) return {};
        vector<int> ans;

        queue<TreeNode*> q;
        q.push(root);
        while (!q.empty()) {
            int max_val=numeric_limits<int>::min();
            int size=q.size();
            for (int i=size;i>0;--i) {
                TreeNode *node=q.front();q.pop();
                if (!node) break;
                max_val=max(max_val,node->val);
                if (node->left) q.push(node->left);
                if (node->right) q.push(node->right);
            }
            ans.push_back(max_val);
        }

        return ans;
    }
    Node* connect(Node* root) {
        if (!root) return nullptr;
        queue<Node*> q;
        q.push(root);
        while (!q.empty()) {
            int size=q.size();
            Node *prev=nullptr;
            for (int i=size;i>0;--i) {
                Node *node=q.front();q.pop();
                if (i==1) node->next=nullptr;
                if (prev) prev->next=node;
                prev=node;
                if (node->left) q.push(node->left);
                if (node->right) q.push(node->right);
            }
        }

        return root;
    }
    int maxDepth(TreeNode* root) {
        if (!root) return 0;
        int ans = 0;
        queue<TreeNode*> q;
        q.push(root);

        while (!q.empty()) {
            int size=q.size();
            ++ans;
            for (int i=size;i>0;--i) {
                TreeNode *node=q.front();q.pop();
                if (node->left) q.push(node->left);
                if (node->right) q.push(node->right);
            }
        }

        return ans;
    }
    vector<int> rightSideView(TreeNode* root) {
        if (!root) return {};
        vector<int> ans;

        queue<TreeNode*> q;
        q.push(root);
        while (!q.empty()) {
            //vector<int> tmp_solu;
            for (int i = q.size(); i > 0; --i) {
                TreeNode *tmp = q.front(); q.pop();
                if (i == 1)
                ans.push_back(tmp->val);

                if (tmp->left) q.push(tmp->left);
                if (tmp->right) q.push(tmp->right);
            }
            //ans.push_back(tmp_solu);
        }

        return ans;
    }
    TreeNode* invertTree(TreeNode* root) {

        if (!root) return nullptr;
        queue<TreeNode*> q;
        q.push(root);

        while (!q.empty()) {
            int size=q.size();
            for (int i=size;i>0;--i) {
                TreeNode *node=q.front();q.pop();

                TreeNode *tmp=node->left;
                node->left=node->right;
                node->right=tmp;

                if (node->left) q.push(node->left);
                if (node->right) q.push(node->right);
            }

        }

        return root;
    }


    bool _is_symmetric(TreeNode *node1, TreeNode *node2) {
        if (!node1 && !node2) return true;
        if (node1 && !node2) return false;
        if (!node1 && node2) return false;
        if (node1->val!=node2->val) return false;
        return _is_symmetric(node1->left,node2->right) && _is_symmetric(node1->right, node2->left);
    }

    bool isSymmetric(TreeNode* root) {
        if (!root) return true;
        return _is_symmetric(root->left, root->right);
    }
    bool _is_same(TreeNode *node1, TreeNode *node2) {
        if (!node1 && !node2) return true;
        if (node1 && !node2) return false;
        if (!node1 && node2) return false;
        if (node1->val!=node2->val) return false;
        return _is_same(node1->left,node2->left) && _is_same(node1->right, node2->right);
    }
    bool isSameTree(TreeNode* p, TreeNode* q) {

        return _is_same(p, q);
    }
    bool _is_subtree(TreeNode *node1, TreeNode *node2) {
        if (!node1 && !node2) return true;
        if (node1 && !node2) return false;
        if (!node1 && node2) return false;
        if (_is_same(node1,node2)) return true;
        //if (node1->val!=node2->val) return false;
        return _is_subtree(node1->left,node2) || _is_subtree(node1->right, node2);
    }
    bool isSubtree(TreeNode* root, TreeNode* subRoot) {
        return _is_subtree(root, subRoot);
    }
};

//          1
//     2          2
//  4    5     5     4
// 8 9 10 11 11 10 9   8
int main()
{
    Solution solution;

    solution.reset();
    {
        TreeNode *root = solution.push(nullptr, 3, false, false);
        TreeNode *l = solution.push(root, 4, true, false);
        TreeNode *r = solution.push(root, 5, false, true);
        TreeNode *ll = solution.push(l, 1, true, false);
        TreeNode *lr = solution.push(l, 2, false, true);
        TreeNode *root2 = solution.push(nullptr, 4, false, false);
        TreeNode *l2 = solution.push(root2, 1, true, false);
        TreeNode *r2 = solution.push(root2, 2, false, true);
        cout << solution.isSubtree(root,root2) << endl;
    }
    solution.reset();
    {
        TreeNode *root = solution.push(nullptr, 3, false, false);
        TreeNode *l = solution.push(root, 4, true, false);
        TreeNode *r = solution.push(root, 5, false, true);
        TreeNode *ll = solution.push(l, 1, true, false);
        TreeNode *lr = solution.push(l, 2, false, true);
        TreeNode *lll = solution.push(lr, 0, true, false);
        TreeNode *root2 = solution.push(nullptr, 4, false, false);
        TreeNode *l2 = solution.push(root2, 1, true, false);
        TreeNode *r2 = solution.push(root2, 2, false, true);
        cout << solution.isSubtree(root,root2) << endl;
    }

    return 0;
}
