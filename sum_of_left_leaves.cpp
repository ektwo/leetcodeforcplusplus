#include <limits.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <queue>
#include <limits>


using namespace std;

template<class T>
void print_1d(const T& nums)
{
    cout << "{";
    for (int j = 0; j < nums.size(); ++j) {
        cout << nums[j] << ",";
    }
    cout << "}," << endl;
}
template<class T>
void print_2d(const T& nums)
{
    cout << "{";
    for (int i = 0; i < nums.size(); ++i) {
        cout << "{";
        for (int j = 0; j < nums[i].size(); ++j) {
            cout << nums[i][j] << ",";
        }
        cout << "}," << endl;
    }
    cout << "}," << endl;
}

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

class Node1 {
public:
    int val;
    vector<Node1*> children;

    Node1() {}

    Node1(int _val) {
        val = _val;
    }

    Node1(int _val, vector<Node1*> _children) {
        val = _val;
        children = _children;
    }
};
Node1* newNode(int key)
{
    Node1* temp = new Node1;
    temp->val = key;
    return temp;
}

class Node2 {
public:
    int val;
    Node2* left;
    Node2* right;
    Node2* next;

    Node2() : val(0), left(NULL), right(NULL), next(NULL) {}

    Node2(int _val) : val(_val), left(NULL), right(NULL), next(NULL) {}

    Node2(int _val, Node2* _left, Node2* _right, Node2* _next)
        : val(_val), left(_left), right(_right), next(_next) {}
};


class Solution {

private:
    int node_nums;
    int tree_height;
    TreeNode *m_head;
    bool _isValidBST(TreeNode* root, long min, long max) {
        if (!root) return true;
        cout << "root=" << root << endl;
        cout << "root->val=" << root->val << endl;
        cout << "root->left=" << root->left << " min=" << min << " max=" << max << endl;
        cout << "root->right=" << root->right << " min=" << min << " max=" << max << endl;
        if (root->val <= min) return false;
        if (root->val >= max) return false;
        
        return _isValidBST(root->left, min, root->val) && _isValidBST(root->right, root->val, max);
    }

public:
    void reset() {
        node_nums = 0;
        tree_height = 0;
        m_head = nullptr;
    }

    TreeNode* get_head() {
        return m_head;
    }

    TreeNode* push(TreeNode *root, int val, bool left, bool right)
    {
        TreeNode* temp = new TreeNode(val);
        //cout << "push val=" << val << " m_head=" << m_head << endl;
        //cout << "push val=" << val << " root=" << root << endl;
        if (!m_head) {
            m_head = temp;
        }

        if (!root)
            root = m_head;

        if (root && left)
            root->left = temp;
        else if (root && right)
            root->right = temp;

        return temp;
    }
    void _inorderTraversal(TreeNode* root, vector<int> &ans) {
        if (!root) return;
        _inorderTraversal(root->left, ans);
        ans.push_back(root->val);
        _inorderTraversal(root->right, ans);
    }
    vector<int> inorderTraversal(TreeNode* root) {

        vector<int> ans;

        _inorderTraversal(root, ans);

        return ans;
    }
    void _preorderTraversal(TreeNode* root, vector<int> &ans) {
        if (!root) return;
        ans.push_back(root->val);
        _preorderTraversal(root->left, ans);
        _preorderTraversal(root->right, ans);
    }
    vector<int> preorderTraversal(TreeNode* root) {
        vector<int> ans;

        _preorderTraversal(root, ans);

        return ans;
    }
    void _postorderTraversal(TreeNode* root, vector<int> &ans) {
        if (!root) return;
        _postorderTraversal(root->left, ans);
        _postorderTraversal(root->right, ans);
        ans.push_back(root->val);
    }

    vector<int> postorderTraversal(TreeNode* root) {
        vector<int> ans;

        _postorderTraversal(root, ans);

        return ans;
    }
    vector<vector<int>> levelOrder(TreeNode* root) {
        if (!root) return {};
        vector<vector<int>> ans;

        queue<TreeNode*> q;
        q.push(root);
        while (!q.empty()) {
            vector<int> tmp_solu;
            for (int i = q.size(); i > 0; --i) {
                TreeNode *tmp = q.front(); q.pop();
                if (!tmp) break;
                tmp_solu.push_back(tmp->val);

                if (tmp->left) q.push(tmp->left);
                if (tmp->right) q.push(tmp->right);
            }
            ans.push_back(tmp_solu);
        }


        return ans;
    }

    vector<vector<int>> levelOrderBottom(TreeNode* root) {
        if (!root) return {};

        vector<vector<int>> ans;
        queue<TreeNode*> q;
        q.push(root);
        //ans.push_back({root->val});

        while (!q.empty()) {
            vector<int> tmp;
            int size=q.size();
            for (int i=0;i<size;++i) {
                TreeNode* node=q.front(); q.pop();
                if (!node) break;
                tmp.push_back(node->val);
                if (node->left) q.push(node->left);
                if (node->right) q.push(node->right);
            }
            ans.push_back(tmp);
        }
        reverse(ans.begin(), ans.end());
        return ans;
    }

    vector<double> averageOfLevels(TreeNode* root) {
        vector<double> ans;
        if (!root) return {};

        queue<TreeNode*> q;
        q.push(root);

        while (!q.empty()) {
            int size=q.size();
            double total=0,average=0;
            for (int i=size;i>0;--i) {
                TreeNode *node=q.front(); q.pop();
                total+=node->val;
                if (node->left) q.push(node->left);
                if (node->right) q.push(node->right);
            }
            ans.push_back(total/size);
        }
        return ans;
    }

    vector<vector<int>> levelOrder(Node1* root) {
        vector<vector<int> > ans;
        if (!root) return {};

        queue<Node1*> q;
        q.push(root);

        while (!q.empty()) {
            int size=q.size();
            vector<int> tmp_solu;
            for (int i=size;i>0;--i) {
                Node1 *tmp=q.front();q.pop();
                tmp_solu.push_back(tmp->val);
                for (auto &child: tmp->children) {
                    q.push(child);
                }
                // while (child) {
                    
                // }
            }
            ans.push_back(tmp_solu);
        }
 
        return ans;
    }    
    vector<int> largestValues(TreeNode* root) {
        if (!root) return {};
        vector<int> ans;

        queue<TreeNode*> q;
        q.push(root);
        while (!q.empty()) {
            int max_val=numeric_limits<int>::min();
            int size=q.size();
            for (int i=size;i>0;--i) {
                TreeNode *node=q.front();q.pop();
                if (!node) break;
                max_val=max(max_val,node->val);
                if (node->left) q.push(node->left);
                if (node->right) q.push(node->right);
            }
            ans.push_back(max_val);
        }

        return ans;
    }
    Node2* connect(Node2* root) {
        if (!root) return nullptr;
        queue<Node2*> q;
        q.push(root);
        while (!q.empty()) {
            int size=q.size();
            Node2 *prev=nullptr;
            for (int i=size;i>0;--i) {
                Node2 *node=q.front();q.pop();
                if (i==1) node->next=nullptr;
                if (prev) prev->next=node;
                prev=node;
                if (node->left) q.push(node->left);
                if (node->right) q.push(node->right);
            }
        }

        return root;
    }
    int maxDepth1(TreeNode* root) {
        if (!root) return 0;
        int ans = 0;
        queue<TreeNode*> q;
        q.push(root);

        while (!q.empty()) {
            int size=q.size();
            ++ans;
            for (int i=size;i>0;--i) {
                TreeNode *node=q.front();q.pop();
                if (node->left) q.push(node->left);
                if (node->right) q.push(node->right);
            }
        }

        return ans;
    }
    int maxDepth(Node1* root) {
        if (!root) return 0;
        int ans=0;
        queue<Node1*> q;
        q.push(root);
        while (!q.empty()) {
            ++ans;
            int size=q.size();
            for (int i=size;i>0;--i) {
                Node1 *node=q.front();q.pop();
                for (const auto &p:node->children) {
                    q.push(p);
                }
            }
        }
        return ans;
    }
    vector<int> rightSideView(TreeNode* root) {
        if (!root) return {};
        vector<int> ans;

        queue<TreeNode*> q;
        q.push(root);
        while (!q.empty()) {
            //vector<int> tmp_solu;
            for (int i = q.size(); i > 0; --i) {
                TreeNode *tmp = q.front(); q.pop();
                if (i == 1)
                ans.push_back(tmp->val);

                if (tmp->left) q.push(tmp->left);
                if (tmp->right) q.push(tmp->right);
            }
            //ans.push_back(tmp_solu);
        }

        return ans;
    }
    TreeNode* invertTree(TreeNode* root) {

        if (!root) return nullptr;
        queue<TreeNode*> q;
        q.push(root);

        while (!q.empty()) {
            int size=q.size();
            for (int i=size;i>0;--i) {
                TreeNode *node=q.front();q.pop();

                TreeNode *tmp=node->left;
                node->left=node->right;
                node->right=tmp;

                if (node->left) q.push(node->left);
                if (node->right) q.push(node->right);
            }

        }

        return root;
    }


    bool _is_symmetric(TreeNode *node1, TreeNode *node2) {
        if (!node1 && !node2) return true;
        if (node1 && !node2) return false;
        if (!node1 && node2) return false;
        if (node1->val!=node2->val) return false;
        return _is_symmetric(node1->left,node2->right) && _is_symmetric(node1->right, node2->left);
    }

    bool isSymmetric(TreeNode* root) {
        if (!root) return true;
        return _is_symmetric(root->left, root->right);
    }
    bool _is_same(TreeNode *node1, TreeNode *node2) {
        if (!node1 && !node2) return true;
        if (node1 && !node2) return false;
        if (!node1 && node2) return false;
        if (node1->val!=node2->val) return false;
        return _is_same(node1->left,node2->left) && _is_same(node1->right, node2->right);
    }
    bool isSameTree(TreeNode* p, TreeNode* q) {

        return _is_same(p, q);
    }
    bool _is_subtree(TreeNode *node1, TreeNode *node2) {
        if (!node1 && !node2) return true;
        if (node1 && !node2) return false;
        if (!node1 && node2) return false;
        if (_is_same(node1,node2)) return true;
        return _is_subtree(node1->left,node2) || _is_subtree(node1->right, node2);
    }
    bool isSubtree(TreeNode* root, TreeNode* subRoot) {
        return _is_subtree(root, subRoot);
    }
    int get_balanced_tree_height(TreeNode *root) {
        if (!root) return 0;
        int left_height=get_balanced_tree_height(root->left);
        if (left_height==-1)return -1;
        int right_height=get_balanced_tree_height(root->right);
        if (right_height==-1)return -1;
        if (abs(left_height-right_height)>1) return -1;
        return 1+max(left_height,right_height);                                                                                                                                                                                                                          
    }
    bool isBalanced(TreeNode* root) {
        return (-1!=get_balanced_tree_height(root));
    }

    bool _sumOfLeftLeaves(TreeNode* root, int &ans) {
        if (!root) return false;

        if(root->left) { 
            if (_sumOfLeftLeaves(root->left, ans)) {
if ((!root->left->left) && (!root->left->right))
            ans += root->left->val;
            }
                
        }
        if(root->right) { _sumOfLeftLeaves(root->right, ans); }

        
        return true;

    }

    int sumOfLeftLeaves(TreeNode* root) {
        int ans=0;
        _sumOfLeftLeaves(root, ans);
        return ans;
    }
};

int main()
{
    Solution solution;

    solution.reset();
    {
        TreeNode *root = solution.push(nullptr, 3, false, false);
        TreeNode *l = solution.push(root, 9, true, false);
        TreeNode *r = solution.push(root, 20, false, true);
        TreeNode *rl = solution.push(r, 15, true, false);
        TreeNode *rr = solution.push(r, 7, false, true);
        cout << solution.sumOfLeftLeaves(root) << endl;
    }
    solution.reset();
    {
        TreeNode *root = solution.push(nullptr, 1, false, false);
        cout << solution.sumOfLeftLeaves(root) << endl;
    }

    return 0;
}
