#ifndef __SYSTEM_TIME_H__
#define __SYSTEM_TIME_H__

#include <cstdio>
#include <sys/time.h>
#include "stdint.h"

class CSystemTime
{
  public:
    static int64_t getSystemTime()
    {
        struct timeval tv;
        gettimeofday(&tv, NULL);
        int64_t t = tv.tv_sec;
        t *= 1000;
        t += tv.tv_usec / 1000;
        return t;
    }
};

#endif