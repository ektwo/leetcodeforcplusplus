#include <cmath>
#include <iostream>
#include <map>
#include <stack>

using namespace std;

class Solution
{
    uint64_t m_id;
    std::map<uint64_t, string> mymap;

  public:
    Solution() : m_id(1)
    {
    }

    stack<int> GetBase64FromID(uint64_t id)
    {
        //vector<uint64_t> digits;
        stack<int> digits;
        //uint64_t id = m_id++;
        uint64_t remainder = 0;
        while (id > 0)
        {
            remainder = id % 62;
            //digits.push_back(remainder);
            digits.push(remainder);
            id = id / 62;
        }
        return digits;
    }

    char ConvertBase62ToChar(int value)
    {
        char retCh = '0';
        if ((value >= 0) && (value < 10))
            retCh = '0' + value;
        else if ((value >= 10) && (value < 36))
            retCh = 'A' + value - 10;
        else if ((value >= 36) && (value < 62))
            retCh = 'a' + value - 36;

        return retCh;
    }

    // Encodes a URL to a shortened URL.
    string encode(string longUrl)
    {
        string shortUrl;
        mymap.insert(pair<uint64_t, string>(m_id, longUrl));

        stack<int> digits = GetBase64FromID(m_id++);

        int count = 0;
        uint64_t tmp = -1;
        uint64_t val = -1;
        for (int i = 0; i < 6; ++i)
        {
            if (!digits.empty())
            {
                cout << "digits.top()=" << digits.top() << endl;
                shortUrl.insert(0, 1, ConvertBase62ToChar(digits.top()));
                digits.pop();
            }
            else
            {
                cout << "'0'=" << '0' << endl;
                shortUrl.insert(0, 1, '0');
            }
        }

        return shortUrl;
    }

    // Decodes a shortened URL to its original URL.
    string decode(string shortUrl)
    {
        stack<int> st_int;
        bool found = false;
        int size = shortUrl.length();
        uint64_t id = 0;
        int count = 0;
        int tmp = 0;

        for (char c : shortUrl)
        {
            if (('0' <= c) && (c <= '9'))
            {
                found = true;
                tmp = c - '0';
            }
            else if (('A' <= c) && (c <= 'Z'))
            {
                found = true;
                tmp = c - 'A' + 10;
            }
            else if (('a' <= c) && (c <= 'z'))
            {
                found = true;
                tmp = c - 'a' + 36;
            }
            if (found == true)
                st_int.push(tmp);
            //cout << val << endl;
        }

        int exponent;
        int st_int_len = st_int.size();
        cout << "st_int_len=" << st_int_len << endl;
        //for (int i = 0; i < 6 - st_int.size(); i++) {
        for (int i = 0; i < 6; ++i)
        {
            //if (i > st_int.size() - 1)
            //    id += (int)(tmp * pow(62, i));
            //else {
            //st_int.push(0);
            if (!st_int.empty())
            {
                exponent = i; //
                //6 - st_int_len - i + 1;
                tmp = st_int.top();
                cout << "tmp=" << tmp << "exponent=" << exponent << endl;
                st_int.pop();
                id += (int)(tmp * pow(62, exponent));
            }
            //}
        }

        cout << "decode=" << id << endl;

        std::cout << "mymap contains:\n";
        std::map<uint64_t, string>::iterator it = mymap.begin();
        for (it = mymap.begin(); it != mymap.end(); ++it)
            std::cout << it->first << " => " << it->second << '\n';

        string str = mymap.find(id)->second;
        cout << "decode str=" << str << endl;
        return str;
    }
};

// Your Solution object will be instantiated and called as such:
// Solution solution;
// solution.decode(solution.encode(url));

int main()
{
    Solution solution;
    string url("09AZaz");
    string shortUrl = solution.encode(url);
    cout << "shortUrl=" << shortUrl << endl;
    string longUrl = solution.decode(shortUrl);
    cout << "url=" << url << endl;
    cout << "longUrl=" << longUrl << endl;
    return 0;
}