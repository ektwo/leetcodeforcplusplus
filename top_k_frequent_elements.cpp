#include <iostream>
#include <stack>
#include <algorithm>
#include <vector>
#include <queue>
using namespace std;

void print_1d_int(vector<int> nums)
{
    cout << "{";
    for (int j = 0; j < nums.size(); ++j) {
        cout << nums[j] << ",";
    }
    cout << "}," << endl;
}

class Solution
{
public:
    class mc {
    public:
        bool operator()(const pair<int,int>& lhs, const pair<int,int>& rhs) {
            return lhs.second > rhs.second;
        }
        // bool operator< (const pair<int,int>& lhs, const pair<int,int>& rhs) {
        //     return lhs.second > rhs.second;
        // }
        // bool operator> (const pair<int,int>& lhs, const pair<int,int>& rhs) {
        //     return lhs.second < rhs.second;
        // }
    };

    vector<int> topKFrequent(vector<int>& nums, int k) {
        vector<int> ans;

        unordered_map<int, int> m;
        for (auto &num:nums) {
            m[num]++;
        }

        //priority_queue<pair<int,int>, vector<pair<int, int>>, less<pair<int,int>>> pq;
        //priority_queue<pair<int,int>, vector<pair<int, int>>, less<pair<int,int>::value_type>> pq;
        priority_queue<pair<int,int>, vector<pair<int, int>>, mc> pq;

        for (unordered_map<int,int>::iterator it=m.begin();it!=m.end();it++){
            pq.push(*it);
            if (pq.size()>k) pq.pop();
        }

        for (int i=0; i< k;++i) {
            ans.push_back(pq.top().first);pq.pop();
        }

        return ans;
    }
};

int main()
{
    Solution solution;
    {
        vector<int> nums={1,1,1,2,2,3};
        int k=2;
        vector<int> ans = solution.topKFrequent(nums,k);
        print_1d_int(ans);
    }
    cout<<endl;
    {
        vector<int> nums={1};
        int k=1;
        vector<int> ans = solution.topKFrequent(nums,k);
        print_1d_int(ans);
    }

    return 0;
}