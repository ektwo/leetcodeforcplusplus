#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>


using namespace std;



class Solution {

public:
    int trap(vector<int>& height) {
        int ans = 0;

        int left = 0;
        int right = height.size() - 1;
        while (left < right) {
            int min_val = min(height[left], height[right]);
            if (min_val == height[left]) {
                ++left;
                while (left < right && height[left] < min_val) {
                    ans += min_val - height[left++];
                }
            }
            else {
                --right;
                while (left < right && height[right] < min_val) {
                    ans += min_val - height[right--];
                }
            }
        }

        return ans;
    }
};

int main()
{
    Solution s;
    {
        vector<int> height = { 0,1,0,2,1,0,1,3,2,1,2,1};
        cout << s.trap(height) << endl;
    }
    {
        vector<int> height = { 4,2,0,3,2,5 };
        cout << s.trap(height) << endl;
    }

    return 0;
}
