#include <limits>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <queue>


using namespace std;


class Solution {

public:
    vector<int> twoSum(vector<int>& nums, int target) {
        vector<int> ans;

        map<int, int> m;
        for (int i = 0; i < nums.size(); ++i) {
            if (m.find(nums[i]) != m.end()) {
                ans.push_back(m[nums[i]]);
                ans.push_back(i);
                return ans;
            }else {
                m[target - nums[i]] = i;
            }
            
        }

        // for (int right = left + 1; right < nums.size(); ++right) {
        //     cout << "left=" << nums[left] << endl;
        //     cout << "right=" << nums[right] << endl;
        //     if (nums[i] + nums[j] == target) {
        //         ans.push_back(i);
        //         ans.push_back(j);
        //         return ans;
        //     }
        // }

        // for (int i = 0; i < nums.size() - 1; ++i) {
        //     for (int j = i + 1; j < nums.size(); ++j) {
        //         cout << "i=" << nums[i] << endl;
        //         cout << "j=" << nums[j] << endl;
        //         if (nums[i] + nums[j] == target) {
        //             ans.push_back(i);
        //             ans.push_back(j);
        //             return ans;
        //         }
        //     }
        // }
        return ans;
    }
};

int main()
{
    Solution solution;
    {
        vector<int> nums = { 2, 7, 11, 15 };
        vector<int> ans = solution.twoSum(nums, 9);
        for (int i = 0; i < ans.size(); ++i) {
            cout << ans[i] << ",";
        }
        cout << endl;
    }

    {
        vector<int> nums = { 3, 2, 4 };
        vector<int> ans = solution.twoSum(nums, 6);
        for (int i = 0; i < ans.size(); ++i) {
            cout << ans[i] << ",";
        }
        cout << endl;
    }

    {
        vector<int> nums = { 3, 3 };
        vector<int> ans = solution.twoSum(nums, 6);
        for (int i = 0; i < ans.size(); ++i) {
            cout << ans[i] << ",";
        }
        cout << endl;
    }

    return 0;
}
