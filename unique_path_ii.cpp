#include <limits.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <queue>


using namespace std;


bool isBadVersion(int version)
{
    return false;
}

class Solution {

public:

    int climbStairs(int n) {
        if (n <= 1) return 1;
        vector<int> dp(n);

        /* fibonacci */
        dp[0] = 1; dp[1] = 2;
        for (int i = 2; i < n; ++i) {
            dp[i] = dp[i - 1] + dp[i - 2];
        }

        return dp.back();
        
    }
    /*  1 1 1  1  1  1  1 
        1 2 3  4  5  6  7
        1 3 6 10 15 21 28 */
    int uniquePaths(int m, int n) {
        vector<int> dp(n, 1);
        for (int i = 1; i < m; ++i) {
            for (int j = 1; j < n; ++j) {
                dp[j] += dp[j-1];
            }
        }
        return dp[n-1];
    }

    int uniquePathsWithObstacles(vector<vector<int>>& obstacleGrid) {
        if (obstacleGrid.empty() || obstacleGrid[0].empty() || obstacleGrid[0][0] == 1) return 0;
        int m = obstacleGrid.size();
        int n = obstacleGrid[0].size();
        vector<vector<long>> dp(m + 1, vector<long>(n+1, 0));
        dp[0][1] = 1;
        for (int i = 1; i <= m; ++i) {
            for (int j = 1; j <= n; ++j) {
                if (obstacleGrid[i-1][j-1] == 1) continue;
                dp[i][j] = dp[i-1][j] + dp[i][j-1];
            }
        }
        return dp[m][n];
    }
};

int main()
{
    Solution solution;

    {
        vector<vector<int>> v = { {0,0,0},{0,1,0},{0,0,0}};
        int ans = solution.uniquePathsWithObstacles(v);
        cout << "solution=" << ans << endl;
    }
cout << "next=" << endl;
    {
        vector<vector<int>> v = { {0,1},{0,0}};
        int ans = solution.uniquePathsWithObstacles(v);
        cout << "solution=" << ans << endl;
    }
    return 0;
}
