#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>


using namespace std;


class Solution {
public:
    bool isAnagram(string s, string t) {
        unordered_map<char, int> map_s, map_t;

        for (int i = 0; i < s.length(); ++i) {
            map_s[s[i]]++;
        }
        for (int i = 0; i < t.length(); ++i) {
            map_t[t[i]]++;
        }

        return (map_s == map_t);


        // if (s.length() != t.length()) return false;
        // int char_cnt[26] = { 0 };
        // int char_cnt2[26] = { 0 };
        // for (int i = 0; i < s.length(); ++i) {
        //     char_cnt[s.at(i) - 'a']++;
        //     char_cnt[t.at(i) - 'a']--;
        // }
        // if (0 == memcmp(char_cnt, char_cnt2, 26 * sizeof(int))) return true;
        // return false;
    }
};

int main()
{
    Solution solution;
    {
        string s("anagram");
        string t("nagaram");
        cout << solution.isAnagram(s, t) << endl;
    }
    {
        string s("rat");
        string t("car");
        cout << solution.isAnagram(s, t) << endl;
    }
    {
        string s("axncxlhjle");
        string t("xxconlaelh");
        cout << solution.isAnagram(s, t) << endl;
    }
    return 0;
}
