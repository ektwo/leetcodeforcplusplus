#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>


using namespace std;


class Solution {
public:
	bool is_alpha_num(char& ch) {
		if (ch >= 'a' && ch <= 'z') return true;
		if (ch >= 'A' && ch <= 'Z') return true;
		if (ch >= '0' && ch <= '9') return true;
		return false;
	}

	bool isPalindrome(string s) {

		int left = 0;
		int right = s.length() - 1;
		while (left < right) {
			if (!is_alpha_num(s[left])) ++left;
			else if (!is_alpha_num(s[right])) --right;
			else if (((s[left] + 32 - 'a') % 32) != ((s[right] + 32 - 'a') % 32)) { return false; }
			else { ++left; --right; }
		}

		return true;
	}

};

int main()
{
    Solution solution;
    {
        string s("ABA");
        cout << solution.isPalindrome(s) << endl;
    }

    return 0;
}
