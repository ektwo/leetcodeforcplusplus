#include <iostream>
#include <stack>
using namespace std;

//class Solution
//{
//  public:
//    bool isValid(string s)
//    {
//        stack<char> stack_c;
//        for (char c : s)
//        {
//            if (('(' == c) || ('[' == c) || ('{' == c))
//            {
//                cout << "if=" << c << endl;
//                stack_c.push(c);
//            }
//            else
//            {
//                cout << "else=" << c << endl;
//                if (stack_c.empty())
//                    return false;
//                if (('(' != stack_c.top()) && (')' == c))
//                    return false;
//                if (('[' != stack_c.top()) && (']' == c))
//                    return false;
//                if (('{' != stack_c.top()) && ('}' == c))
//                    return false;
//                stack_c.pop();
//            }
//        }
//        return stack_c.empty();
//    }
//};

class Solution
{
public:
    bool isValid(string s)
    {
        stack<char> stack_c;

#if 1   
        for (auto &c:s) {
            if (c == '{') stack_c.push('}');
            else if (c == '[') stack_c.push(']');
            else if (c == '(') stack_c.push(')');
            else if (!stack_c.empty()) return false;
            else if (c!=stack_c.top()) return false;
            else {stack_c.pop();}

        }

        return stack_c.empty();
#else
        for (char c : s)
        {
            if (('(' == c) || ('[' == c) || ('{' == c))
            {
                stack_c.push(c);
            }
            else
            {
                if (stack_c.empty())
                    return false;
                if (('(' != stack_c.top()) && (')' == c))
                    return false;
                if (('[' != stack_c.top()) && (']' == c))
                    return false;
                if (('{' != stack_c.top()) && ('}' == c))
                    return false;
                stack_c.pop();
            }
        }

        return stack_c.empty();
#endif
    }
};

int main()
{
    Solution s;
    string str("{{");
    cout << "Result=" << s.isValid(str) << endl;
    return 0;
}