#include <limits.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <queue>


using namespace std;


class Solution {

private:

public:

    int triangleNumber(vector<int>& nums) {
        int ans = 0;

        int diff = INT_MAX;
        int n = nums.size();
        sort(nums.begin(), nums.end());

        for (int i = n - 1; i >=2; --i) {
            int left = 0;
            int right = i - 1;
            while (left < right) {
                if ((nums[left] + nums[right]) > nums[i]) {
                    ans += right - left; //The smallest set is larger than nums[i], so every element of nums can fit the answer
                    --right;
                }
                else
                    ++left;
            }
        }
        return ans;
    }
};

int main()
{
    Solution solution;

    {
        vector<int> nums = { 2,2,3,4};
        int ans = solution.triangleNumber(nums);
        cout << "ans=" << ans << endl;
        // for (int i = 0; i < ans.size(); ++i) {
        //     cout << ans[i] << "," << endl;
        // }
    }

    {
        vector<int> nums = { 4,2,3,4};
        int ans = solution.triangleNumber(nums);
        cout << "ans=" << ans << endl;
        // for (int i = 0; i < ans.size(); ++i) {
        //     cout << ans[i] << "," << endl;
        // }
    }

    return 0;
}
