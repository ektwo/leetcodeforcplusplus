#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>

// Given an array of unique strings words, return all the word squares you can build from words. The same word from words can be used multiple times. You can return the answer in any order.

// A sequence of strings forms a valid word square if the kth row and column read the same string, where 0 <= k < max(numRows, numColumns).

// For example, the word sequence ["ball","area","lead","lady"] forms a word square because each word reads the same both horizontally and vertically.
// ball 
// area 
// lead 
// lady

// Input: words = ["area","lead","wall","lady","ball"]
// Output: [["ball","area","lead","lady"],["wall","area","lead","lady"]]
// Explanation:
// The output consists of two word squares. The order of output does not matter (just the order of words in each word square matters).

// baba 
// abat 
// baba 
// atal
// Input: words = ["abat","baba","atan","atal"]
// Output: [["baba","abat","baba","atal"],["baba","abat","baba","atan"]]
// Explanation:
// The output consists of two word squares. The order of output does not matter (just the order of words in each word square matters).

// Constraints:

// 1 <= words.length <= 1000
// 1 <= words[i].length <= 4
// All words[i] have the same length.
// words[i] consists of only lowercase English letters.
// All words[i] are unique.
using namespace std;


class Solution {
public:
    bool validWordSquare(vector<string>& words) {
        for(int i = 0; i < words.size(); ++i) {
            string word = words[i];
            for(int j = 0; j < word.length(); ++j) {
                if(j >= words.size() || i >= words[j].length()) return false;
                if(word[j] != words[j][i]) return false;
            }
        }
        return true;
    }

    bool _wordSquares(string str, char c, int j) {
        if (str[j] == c) return true;
        return false;
    }
    vector<vector<string>> wordSquares(vector<string>& words) {
        vector<vector<string>> ret;

        //for (int i= 0; i < words.size(); ++i) {
        for (auto &str : words) {
            char c1 = str[0];
            for (int j = 1; j < str.size(); ++j) {
                char c = str[j];
            }
        }
    }
};

int main()
{
	Solution solution;
	{
        vector<string> v = { "abcd","bnrt","crm","dt" };
        cout << "validWordSquare= " << solution.validWordSquare(v) << endl;
    }
	{
        vector<string> v = { "area","lead","wall","lady","ball" };
        cout << "validWordSquare= " << solution.validWordSquare(v) << endl;
    #if 0
        vector<vector<string>> ret = solution.wordSquares(v);
        cout << "[";
        for (int i = 0; i < ret.size(); ++i) {
            cout << "[";
            for (int j = 0; j < ret[i].size(); ++j) {
                if (j == ret[i].size() -1)
                    cout << ret[i][j] << ", ";
                else
                    cout << ret[i][j] << " ";
            }
            cout << "],";
        }
        cout << "]";
    #endif
	}
	{
        vector<string> v = { "abat","baba","atan","atal" };
        cout << "validWordSquare= " << solution.validWordSquare(v) << endl;
    #if 0
        vector<vector<string>> ret = solution.wordSquares(v);
        cout << "[";
        for (int i = 0; i < ret.size(); ++i) {
            cout << "[";
            for (int j = 0; j < ret[i].size(); ++j) {
                if (j == ret[i].size() -1)
                    cout << ret[i][j] << ", ";
                else
                    cout << ret[i][j] << " ";
            }
            cout << "],";
        }
        cout << "]";
    #endif
	}
	return 0;
}