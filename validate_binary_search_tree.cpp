#include <limits.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <queue>


using namespace std;


struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};
 

class Solution {

private:
    TreeNode *m_head;
    bool _isValidBST(TreeNode* root, long min, long max) {
        if (!root) return true;
        cout << "root=" << root << endl;
        cout << "root->val=" << root->val << endl;
        cout << "root->left=" << root->left << " min=" << min << " max=" << max << endl;
        cout << "root->right=" << root->right << " min=" << min << " max=" << max << endl;
        if (root->val <= min) return false;
        if (root->val >= max) return false;
        
        return _isValidBST(root->left, min, root->val) && _isValidBST(root->right, root->val, max);
    }

public:
    void reset() {
        m_head = nullptr;
    }

    TreeNode* get_head() {
        return m_head;
    }

    TreeNode* push(TreeNode *root, int val, bool left, bool right)
    {
        TreeNode* temp = new TreeNode(val);
        cout << "push val=" << val << " m_head=" << m_head << endl;
        cout << "push val=" << val << " root=" << root << endl;
        if (!m_head) {
            m_head = temp;
        }

        if (!root)
            root = m_head;

        if (root && left)
            root->left = temp;
        else if (root && right)
            root->right = temp;

        return temp;
    }

    bool isValidBST(TreeNode* root) {

        return _isValidBST(root, LONG_MIN, LONG_MAX);

    }
};
//          1
//     2          3
//  4    5     6     7
// 8 9 10 11 12 13 14 15
int main()
{
    Solution solution;

    solution.reset();
    {
        TreeNode *root = solution.push(nullptr, 2, false, false);
        solution.push(root, 1, true, false);
        solution.push(root, 3, false, true);

        cout << solution.isValidBST(solution.get_head()) << endl;
    }
    solution.reset();
    {
        TreeNode *root = solution.push(nullptr, 5, false, false);
        TreeNode *l = solution.push(root, 1, true, false);
        TreeNode *r = solution.push(root, 4, false, true);
        TreeNode *r1_l1 = solution.push(r, 4, true, false);
        TreeNode *r1_r2 = solution.push(r, 6, false, true);

        cout << solution.isValidBST(solution.get_head()) << endl;
    }

    return 0;
}
