#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <queue>
#include <limits>


using namespace std;


class Solution {

private:
    vector<pair<int, int>> dirs { { 0, -1}, {-1, 0}, {0, 1}, {1, 0}};
    bool is_valid_dir(const pair<int, int>& cur, const int &m, const int &n) {
        return ((cur.first >= 0) && (cur.first < m) && (cur.second >= 0) && (cur.second < n));
    }
public:
    void wallsAndGates(vector<vector<int>>& rooms) {
        // check corner cases
        int m = rooms.size();
        if (m < 1) return ;
        int n = rooms[0].size();
        if (n < 1) return ;

        // Prepare for BFS
        const int GATE = 0;
        const int WALL = -1;
        const int ROOM = numeric_limits<int>::max();
        queue<pair<int, int>> q;

        // put all gates into queue
        for (int i = 0; i < m; ++i) {
            for (int j = 0; j < n; ++j) {
                if (rooms[i][j] == GATE) {
                    q.push({i, j});
                }
            }
        }

        //Start BFS
        int steps = 0;
        while (!q.empty()) {

            for(int sz = q.size()-1; sz >= 0; --sz) {
                pair<int, int> cur = q.front(); q.pop();
                if (rooms[cur.first][cur.second] == ROOM) { rooms[cur.first][cur.second] = steps;}
                if (rooms[cur.first][cur.second] == WALL) continue;

                for (int idx = 0; idx < dirs.size(); ++idx) {
                    const int org_r = cur.first;
                    const int org_l = cur.second;

                    cur.first += dirs[idx].first;
                    cur.second += dirs[idx].second;
                    if (is_valid_dir(cur, m, n) && (rooms[cur.first][cur.second] == ROOM)) {
                        q.push(cur);
                    }
                    cur.first = org_r;
                    cur.second = org_l;
                }

            }
            steps++;
        }
    }

};

int main()
{
    Solution solution;
    {
        vector<vector<int>> rooms = { {2147483647,-1,0,2147483647},{2147483647,2147483647,2147483647,-1},{2147483647,-1,2147483647,-1},{0,-1,2147483647,2147483647}};
        solution.wallsAndGates(rooms);
        for (int i = 0; i < rooms.size(); ++i) {
            for (int j = 0; j < rooms[0].size(); ++j) {
                cout << rooms[i][j] << ",";
            }
            cout << endl;
        }
    }
    {
        vector<vector<int>> rooms = { {-1}};
        solution.wallsAndGates(rooms);
        for (int i = 0; i < rooms.size(); ++i) {
            for (int j = 0; j < rooms[0].size(); ++j) {
                cout << rooms[i][j] << ",";
            }
            cout << endl;
        }
    }
    return 0;
}
