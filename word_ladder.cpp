#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>


using namespace std;


class Solution {
public:

	int ladderLength(string beginWord, string endWord, vector<string>& wordList)
	{
		if (beginWord == endWord) return 0;

		unordered_set<string> os(wordList.begin(), wordList.end());
		if (!os.count(endWord)) return 0;
		//vector<string>::iterator it = find(wordList.begin(), wordList.end(), endWord);
		//if (it == wordList.end()) return 0;

		//unordered_map<string, int> words;
		//words[beginWord] = -1;

		int level = 0;
		queue<string> q;
		q.push(beginWord);

		while (!q.empty()) {
			++level;

			int q_size = q.size();
			for (int i = 0; i < q_size; ++i) {

				string s = q.front();
				q.pop();
				size_t word_len = s.length();

				for (int j = 0; j < word_len; ++j) {
					char org_c = s[j];

					for (char x = 'a'; x <= 'z'; x++) {
						s[j] = x;

						if (s == endWord) return level + 1;

						if (!os.count(s)) continue;
						//vector<string>::iterator it = find(wordList.begin(), wordList.end(), s);
						//if (it == wordList.end()) continue;
	
						q.push(s);
						//wordList.erase(it);
						os.erase(s);
					}
					s[j] = org_c;
				}
			}

		}

		return 0;
	}
};

int main()
{
	Solution solution;
	{
		string bw("hit");
		string ew("cog");
		vector<string> wl = { "hot", "dot", "dog", "lot", "log", "cog" };
		//set<string> st(wl.begin(), wl.end());
		//cout << solution.shortestChainLen(bw, ew, st) << endl;
		cout << solution.ladderLength(bw, ew, wl) << endl;
	}
#if 1
	{
		string bw("hit");
		string ew("cog");
		vector<string> wl = { "hot", "dot", "dog", "lot", "log" };
		//set<string> st(wl.begin(), wl.end());
		//cout << solution.shortestChainLen(bw, ew, st) << endl;
		cout << solution.ladderLength(bw, ew, wl) << endl;
	}
	{
		string bw("a");
		string ew("c");
		vector<string> wl = { "a", "b", "c" };
		//set<string> st(wl.begin(), wl.end());
		//cout << solution.shortestChainLen(bw, ew, st) << endl;
		cout << solution.ladderLength(bw, ew, wl) << endl;
	}
	{
		string bw("hot");
		string ew("dog");
		vector<string> wl = { "hot", "dog", "dot" };
		//set<string> st(wl.begin(), wl.end());
		//cout << solution.shortestChainLen(bw, ew, st) << endl;
		cout << solution.ladderLength(bw, ew, wl) << endl;
	}
#endif
	return 0;
}
