#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>


using namespace std;


class Solution {
private:
    void getPaths(const string& word, 
                  const string& beginWord, 
                  const unordered_map<string, vector<string>>& parents,
                  vector<string>& curr,
                  vector<vector<string>>& ans) {        
        
        if (word == beginWord) {
            ans.push_back(vector<string>(curr.rbegin(), curr.rend()));
            return;
        }
        
        for (const string& p : parents.at(word)) {
            curr.push_back(p);
            getPaths(p, beginWord, parents, curr, ans);
            curr.pop_back();
        }        
    }
public:

	int ladderLength(string beginWord, string endWord, vector<string>& wordList)
	{
		if (beginWord == endWord) return 0;

		unordered_set<string> os(wordList.begin(), wordList.end());
		if (!os.count(endWord)) return 0;

		int level = 0;
		queue<string> q;
		q.push(beginWord);

		while (!q.empty()) {
			++level;

			int q_size = q.size();
			for (int i = 0; i < q_size; ++i) {

				string s = q.front();
				q.pop();
				size_t word_len = s.length();

				for (int j = 0; j < word_len; ++j) {
					char org_c = s[j];

					for (char x = 'a'; x <= 'z'; x++) {
						s[j] = x;

						if (s == endWord) return level + 1;

						if (!os.count(s)) continue;

						q.push(s);
						os.erase(s);
					}
					s[j] = org_c;
				}
			}

		}

		return 0;
	}
	int ladderLength2(string beginWord, string endWord, vector<string>& wordList)
	{
		if (beginWord == endWord) return 0;
        vector<vector<string>> ans;

        int level = 0;
        unordered_set<string> wl(wordList.begin(), wordList.end());
        queue<string> q;

        q.push(beginWord);

        while (!q.empty()) {
            ++level;
            string s = q.front();
            q.pop();

            //for (auto &str_tmp : wl)
            {
                
                for (int i = 0; i < s.length(); ++i) {
                    char org_c = s[i];
                    for (int j = 'a'; j <= 'z'; ++j) {
                        s[i] = j;

                        if (s == endWord) { level += 1;  cout << "endWord=" << endWord << " level=" << level << endl; return level + 1;}
                        if (wl.count(s) == 0) continue;

                        wl.erase(s);
                        q.push(s);
                    }

                    s[i] = org_c;
                    
                }

            }
        }

		return 0;
	}

    vector<vector<string>> findLadders(string beginWord, string endWord, vector<string>& wordList) {
        
        unordered_set<string> dict(wordList.begin(), wordList.end());        
        if (!dict.count(endWord)) return {};
        dict.erase(beginWord);
        dict.erase(endWord);
        
        unordered_map<string, int> steps{{beginWord, 1}};
        unordered_map<string, vector<string>> parents;
        queue<string> q;
        q.push(beginWord);
        
        vector<vector<string>> ans;
        
        const int str_len = beginWord.length();
        int step = 0;        
        bool found = false;
        
        while (!q.empty() && !found) {
            ++step;            
            for (int size = q.size(); size > 0; size--) {

                const string q_str = q.front(); q.pop();
                cout << "q_str=" << q_str << endl;
                string pred_str = q_str;
                for (int i = 0; i < str_len; i++) {

                    const char org_c = pred_str[i];
                    for (int j = 'a'; j <= 'z'; j++) {
                        if (j == org_c) continue;
                        pred_str[i] = j;

                        if (pred_str == endWord) {
                            parents[pred_str].push_back(q_str);
                            found = true;
                        } else {
                            // Not a new word, but another transform
                            // with the same number of steps
                            cout << "pred_str=" << pred_str << " steps.count(pred_str)=" << steps.count(pred_str)<< endl;
                            if (steps.count(pred_str))
                            cout << "pred_str=" << pred_str << " step=" << step << " steps.at(pred_str)=" << steps.at(pred_str)<< endl;
                            if (steps.count(pred_str) && step < steps.at(pred_str)) {
                                cout << "parent" << endl;
                                parents[pred_str].push_back(q_str);
                            }
                        }
                        
                        if (!dict.count(pred_str)) continue;
                        cout << "dict.erase=" << pred_str << endl;
                        dict.erase(pred_str);
                        q.push(pred_str);
                        steps[pred_str] = steps.at(q_str) + 1;
                        cout << "q_str=" << q_str << " steps[" << pred_str << "]=" << steps[pred_str] << endl;
                        parents[pred_str].push_back(q_str);
                    }
                    pred_str[i] = org_c;

                }
            }
        }
        
        if (found) {
            vector<string> curr{endWord};
            getPaths(endWord, beginWord, parents, curr, ans);
        }
    
        return ans;
    }

};

int main()
{
	Solution solution;
	{
		string bw("hit");
		string ew("cog");
		vector<string> wl = { "hot", "dot", "dog", "lot", "log", "cog" };
        cout << "cnt=" << solution.ladderLength(bw, ew, wl) << endl;
		vector<vector<string>> ans = solution.findLadders(bw, ew, wl);
        cout << "[";
		for (int i = 0; i < ans.size(); ++i) {
            cout << "[";
            for (auto &s:ans[i]) {
                cout << s << "," << endl;
            }
            cout << "],";
        }
        cout << "]" << endl;
	}
#if 1
	{
		string bw("hit");
		string ew("cog");
		vector<string> wl = { "hot", "dot", "dog", "lot", "log" };
        cout << "cnt=" << solution.ladderLength(bw, ew, wl) << endl;
		vector<vector<string>> ans = solution.findLadders(bw, ew, wl);
        cout << "[";
		for (int i = 0; i < ans.size(); ++i) {
            cout << "[";
            for (auto &s:ans[i]) {
                cout << s << "," << endl;
            }
            cout << "],";
        }
        cout << "]" << endl;
	}
	{
		string bw("a");
		string ew("c");
		vector<string> wl = { "a", "b", "c" };
        cout << "cnt=" << solution.ladderLength(bw, ew, wl) << endl;
		vector<vector<string>> ans = solution.findLadders(bw, ew, wl);
        cout << "[";
		for (int i = 0; i < ans.size(); ++i) {
            cout << "[";
            for (auto &s:ans[i]) {
                cout << s << "," << endl;
            }
            cout << "],";
        }
        cout << "]" << endl;
	}
	{
		string bw("hot");
		string ew("dog");
		vector<string> wl = { "hot", "dog", "dot" };
        cout << "cnt=" << solution.ladderLength(bw, ew, wl) << endl;
		vector<vector<string>> ans = solution.findLadders(bw, ew, wl);
        cout << "[";
		for (int i = 0; i < ans.size(); ++i) {
            cout << "[";
            for (auto &s:ans[i]) {
                cout << s << "," << endl;
            }
            cout << "],";
        }
        cout << "]" << endl;
	}
#endif
	return 0;
}
