#include <limits.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>
#include <queue>


using namespace std;


class Solution {
#if 1
private:
    int m_cur_target;
    int m_end_target;
    string word_to_comp;
    vector<vector<int>> dirs = {
        { 0, -1}, { -1, 0}, { 0, 1}, { 1, 0}
    };

    void init() {
        m_cur_target = 0;
        m_end_target = 0;
    }

    bool is_end_condition() {
        if (word_to_comp.size() == m_cur_target) return true;
        return false;
    } 

    bool is_branch_can_be_pruned() {
        return false;
    }

    bool dfs(vector<vector<char>>& board, int row, int col, int target, bool& result) {

        bool ret = false;

        m_cur_target = target;
        if (is_end_condition()) { 
            /* do something */
            result = true;
            return true;
        }

        /* ignore possible solution at this step that do not satisfy the problem constraints */
        if (row < 0 || row >= board.size() || col < 0 || col >= board[0].size()) { return false; }
        if (board[row][col] != word_to_comp[target]) return false;
        /* traverse all executable branch path */

        const int candidate_start = 0;
        const int candidates_end = 1;
        for (int i = candidate_start; i < candidates_end; ++i)
        {
            /* prune, check if it matches for pruning */
            if (is_branch_can_be_pruned()) continue;

            /* update state variables */
            char org_char = board[row][col];
            board[row][col] = '#';

            /* recursively execute the logic of the next step */
            for (auto &dir : dirs) {
                if (dfs(board, row + dir[0], col + dir[1], target + 1, result)) return true;
            }

            /* reset state variables */
            board[row][col] = org_char;
        }

        return false;
    }
public:
    bool exist(vector<vector<char>>& board, string word) {

        bool ans = false;      
        const int n = board.size();
        const int m = board[0].size();

        init();
        word_to_comp = word;
        m_end_target = word_to_comp.size();

        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < m; ++j) {
                dfs(board, i, j, 0, ans);
                if (ans) return ans;
            }
        }

        return ans;
    }
#else
private:
    string word_to_comp;
    int m_cur_target;
    vector<vector<int>> dirs = {
        { 0, -1}, { -1, 0}, { 0, 1}, { 1, 0}
    };

    void init() {
        m_cur_target = 0;
    }

    bool is_end_condition() {
        if (word_to_comp.size() == m_cur_target) return true;
        return false;
    } 

    bool is_branch_can_be_pruned() {
        return false;
    }

    bool dfs(vector<vector<char>>& board, int row, int col, int target, bool& result) {

        bool ret = false;

        m_cur_target = target;
        if (is_end_condition()) { 
            /* do something */
            result = true;
            return true;
        }

        /* ignore possible solution at this step that do not satisfy the problem constraints */
        if (row < 0 || row >= board.size() || col < 0 || col >= board[0].size()) { cout << "invalid row=" << row << " col=" << col << endl; return false; }
        cout << "1 board[" << row <<"][" << col << "]=" << board[row][col] << " word_to_comp[" << target << "]=" << word_to_comp[target] << endl;
        if (board[row][col] != word_to_comp[target]) return false;
        cout << "2 board[" << row <<"][" << col << "]=" << board[row][col] << " word_to_comp[" << target << "]=" << word_to_comp[target] << endl;

        /* traverse all executable branch path */
        for (int i =0; i < 1; ++i)
        {
            /* prune, check if it matches for pruning */
            if (is_branch_can_be_pruned()) continue;

            /* update state variables */
            char org_char = board[row][col];
            board[row][col] = '#';

            /* recursively execute the logic of the next step */
            for (auto &dir : dirs) {
                if (dfs(board, row + dir[0], col + dir[1], target + 1, result)) return true;
            }

            /* reset state variables */
            board[row][col] = org_char;
        }

        return false;
    }
public:
    bool exist(vector<vector<char>>& board, string word) {

        bool ans = false;      
        const int n = board.size();
        const int m = board[0].size();
        cout << "m=" << m << " n=" << n << endl;
        word_to_comp = word;
        init();

        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < m; ++j) {
                cout << " board[" << i <<"][" << j << "]=" << board[i][j] << endl;
            }
        }

        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < m; ++j) {
                cout << " board[" << i <<"][" << j << "]=" << board[i][j] << endl;
                dfs(board, i, j, 0, ans);
                if (ans) return ans;
            }
        }

        return ans;
    }
#endif
};

int main()
{
    Solution solution;

    {
        vector<vector<char>> board = {{'A','B','C','E'},{'S','F','C','S'},{'A','D','E','E'}};
        string word = "ABCCED";
        auto ans = solution.exist(board, word);
        cout << "ans=" << ans << endl;
        // cout << " { ";
        // for (int i = 0; i < ans.size(); ++i) {
        //     cout << " { ";
        //     for (int j = 0; j < ans[0].size(); ++j) {
        //         //if (i != ans.size() - 1)
        //             cout << ans[i][j] << ",";
        //         //else
        //         //    cout << ans[i][j];
        //     }
        //     cout << " }, ";
        // }
        // cout << " }, " << endl;
    }
    {
        vector<vector<char>> board = {{'A','B','C','E'},{'S','F','C','S'},{'A','D','E','E'}};
        string word = "SEE";
        auto ans = solution.exist(board, word);
        cout << "ans=" << ans << endl;
        // cout << " { ";
        // for (int i = 0; i < ans.size(); ++i) {
        //     cout << " { ";
        //     for (int j = 0; j < ans[0].size(); ++j) {
        //         //if (i != ans.size() - 1)
        //             cout << ans[i][j] << ",";
        //         //else
        //         //    cout << ans[i][j];
        //     }
        //     cout << " }, ";
        // }
        // cout << " }, " << endl;
    }
    {
        vector<vector<char>> board = {{'A','B','C','E'},{'S','F','C','S'},{'A','D','E','E'}};
        string word = "ABCB";
        auto ans = solution.exist(board, word);
        cout << "ans=" << ans << endl;
        // cout << " { ";
        // for (int i = 0; i < ans.size(); ++i) {
        //     cout << " { ";
        //     for (int j = 0; j < ans[0].size(); ++j) {
        //         //if (i != ans.size() - 1)
        //             cout << ans[i][j] << ",";
        //         //else
        //         //    cout << ans[i][j];
        //     }
        //     cout << " }, ";
        // }
        // cout << " }, " << endl;
    }
    return 0;
}
