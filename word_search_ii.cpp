#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>


using namespace std;


//class Solution {
//public:
//
//    void dfs(vector<vector<char>>& board, Trie* trie, int r, int c, unordered_set<string>& result, string s) {
//
//        const int M = board.size(), N = board[0].size();
//
//        if (r < 0 || c < 0 || c == M || r == N) return;
//
//        char visited_ch = board[r][c];
//        if (visited_ch == '$') return;
//
//        Trie* child = trie->children[visited_ch - 'a'];
//        if (!child) return;
//
//        board[r][c] = '$';
//
//        string tmp_s = s + visited_ch;
//        if (child->isEndOfword) result.insert(tmp_s);
//        //if (i < board.size() - 1) 
//        dfs(board, r + 1, c, result, child, tmp_s);
//        //if (i > 0) 
//        dfs(board, r - 1, c, result, child, tmp_s);
//        //if (j < board[0].size() - 1) 
//        dfs(board, r, c + 1, result, child, tmp_s);
//        //if (j > 0) 
//        dfs(board, r, c - 1, result, child, tmp_s);
//
//        board[r][c] = visited_ch;
//    }
//
//    vector<string> findWords(vector<vector<char>>& board, vector<string>& words) {
//        if (words.size() == 0) return {};
//        Trie trie;
//        for (auto& w : words)
//            trie.insert(w);
//
//        unordered_set<string> result;
//
//        for (int r = 0; r < board.size(); ++r) {
//            for (int c = 0; c < board[0].size(); ++c) {
//                dfs(board, &trie, r, c, result, "");
//            }
//        }
//
//        vector<string> result_v(result.begin(), result.end());
//
//        return result_v;
//    }
//};

class TrieNode {
public:
    vector<TrieNode*> child;
    bool isEndOfword;
    string str;
    TrieNode() : child(26, nullptr), isEndOfword(false), str("") {};
    ~TrieNode() {
        for (auto c : child) delete c;
    }
};

class Trie {
public:
    TrieNode* root;
    Trie() : root(new TrieNode()) {};

    void insert(string word) {
        TrieNode* p = root;
        for (char c : word) {
            int i = c - 'a';
            if (!p->child[i])
                p->child[i] = new TrieNode();
            p = p->child[i];
        }
        p->isEndOfword = true;
        p->str = word;
    }
};

class Solution {
public:

    void dfs(vector<vector<char>>& board, TrieNode* p, int r, int c, vector<vector<bool>>& visited, vector<string>& result) {

        const int M = board.size(), N = board[0].size();

        if (!p->str.empty()) {
            result.push_back(p->str);
            p->str.clear();
        }

        visited[r][c] = true;
        for (auto d : dirs) {
            int next_r = r + d.first;
            int next_c = c + d.second;
            if (next_r < 0 || next_r >= M || next_c < 0 || next_c >= N || visited[next_r][next_c] || !p->child[board[next_r][next_c] - 'a'])
                continue;
            dfs(board, p->child[board[next_r][next_c] - 'a'], next_r, next_c, visited, result);
        }
        visited[r][c] = false;
    }
    vector<string> findWords(vector<vector<char>>& board, vector<string>& words) {

        const int M = board.size(), N = board[0].size();

        Trie trie;
        for (auto& word : words)
            trie.insert(word);

        vector<vector<bool>> visited(M, vector<bool>(N, false));
        vector<string> result;
        for (int r = 0; r < M; r++) {
            for (int c = 0; c < N; c++) {
                if (trie.root->child[board[r][c] - 'a']) {
                    dfs(board, trie.root->child[board[r][c] - 'a'], r, c, visited, result);
                }
            }
        }
        sort(result.begin(), result.end());

        return result;
    }
private:
    vector<pair<int, int>> dirs = { {0, 1}, {0, -1}, {1, 0}, {-1, 0} };
};

int main()
{
	Solution solution;
	{
		vector<vector<char>> boards = {
			{'o', 'a', 'a', 'n'} ,{'e', 't', 'a', 'e'},{'i', 'h', 'k', 'r'},{'i', 'f', 'l', 'v'}
		};
		vector<string> words = {"oath", "pea", "eat", "rain" };
		
		const clock_t start = clock();
		// do stuff here
		clock_t now = clock();
		vector<string> result = solution.findWords(boards, words);
		clock_t delta = now - start;
		double seconds_elapsed = static_cast<double>(delta) / CLOCKS_PER_SEC;

		cout << seconds_elapsed << endl;
		cout << "{ " << endl;
		for (auto &res : result)
			cout << res << ",";
		cout << "} " << endl;
	}

	return 0;
}
