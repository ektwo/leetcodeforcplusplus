#include <stdlib.h>
#include <iostream>
#include <iomanip>
#include <vector>
#include <set>
#include <map>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>

// Given an array of unique strings words, return all the word squares you can build from words. The same word from words can be used multiple times. You can return the answer in any order.

// A sequence of strings forms a valid word square if the kth row and column read the same string, where 0 <= k < max(numRows, numColumns).

// For example, the word sequence ["ball","area","lead","lady"] forms a word square because each word reads the same both horizontally and vertically.
// ball 
// area 
// lead 
// lady

// Input: words = ["area","lead","wall","lady","ball"]
// Output: [["ball","area","lead","lady"],["wall","area","lead","lady"]]
// Explanation:
// The output consists of two word squares. The order of output does not matter (just the order of words in each word square matters).

// baba 
// abat 
// baba 
// atal
// Input: words = ["abat","baba","atan","atal"]
// Output: [["baba","abat","baba","atal"],["baba","abat","baba","atan"]]
// Explanation:
// The output consists of two word squares. The order of output does not matter (just the order of words in each word square matters).

// Constraints:

// 1 <= words.length <= 1000
// 1 <= words[i].length <= 4
// All words[i] have the same length.
// words[i] consists of only lowercase English letters.
// All words[i] are unique.
using namespace std;

#if 0
class Trie {
#define NUMS 26
public:
    bool isEnd;
    Trie* next[NUMS];
    vector<string> startwith_word;
    vector<int> indexs;

    Trie() {
        isEnd = false;
        memset(next, 0, sizeof(next));
    }

    void insert(string word) {
        Trie* node = this;
        for (auto& ch : word) {
            int idx = ch - 'a';
            Trie* child = node->next[idx];
            if (child == 0) {
                child = new Trie();
            }
            //node->next[ch - 'a'] = child;
            child->startwith_word.push_back(word);
            node = child;
        }
        node->isEnd = true;
    }
    void insert_idx(string word, int idx) {
        Trie* node = this;
        cout << "word=" << word << " idx=" << idx << endl;
        int ch_id = 1;
        for (auto& ch : word) {
            //cout << "ch_id=" << ch_id << " ch=" << ch << " node->next[" << (char)ch << "] = " << endl;
            if (node->next[ch - 'a'] == 0) {
                node->next[ch - 'a'] = new Trie();
            }

            cout << "valu=";
            for (int i = 0; i < 26; ++i) {
                if (node->next[i])
                    cout << "[" << setw(1) << (char)('a' + i) << "]";
            }
            cout << endl;
            node->next[ch - 'a']->startwith_word.push_back(word);
            node->next[ch - 'a']->indexs.push_back(idx);
            node = node->next[ch - 'a'];

            ++ch_id;
        }
        node->isEnd = true;
    }
    bool search(string word) {
        Trie* node = this;
        for (auto& ch : word) {
            Trie* child = node->next[ch - 'a'];
            if (child) {
                node = child;
            }
            else {
                return false;
            }
        }
        return node->isEnd;
    }

    bool startsWith(string prefix) {
        bool found = true;
        Trie* node = this;
        for (auto& ch : prefix) {
            Trie* child = node->next[ch - 'a'];
            if (child) {
                node = child;
            }
            else {
                return false;
            }
        }
        return found;
    }

};

class Solution {
public:
    int word_length;
    vector<vector<string>> ans;
    Trie trie;

    Trie* get_root() { return &trie; }

    void build_trie(vector<string>& words)
    {
        for (int i = 0; i < words.size(); ++i) {
            trie.insert_idx(words[i], i);
        }

    }

    vector<string> find_by_prefix(string prefix) {
        vector<string> ret;

        Trie* node = get_root();
        for (auto& ch : prefix) {
            cout << "find_by_prefix ch=" << ch << endl;
            Trie* child = node->next[ch - 'a'];
            if (!child)
                return ret; //find nothing

            node = child;
        }
        cout << "prefix=" << prefix << " node->startwith_word.size()=" << node->startwith_word.size() << endl;
        for (int i = 0; i < node->startwith_word.size(); ++i)
            ret.push_back(node->startwith_word[i]);
        return ret;
    }

    void recursion_helper(vector<string>& words, vector<string>& ans_builder) {
        if (ans_builder.size() == words[0].size()) {
            ans.push_back(ans_builder);
            return;
        }

        int curr_idx = ans_builder.size();
        cout << "curr_idx=" << curr_idx << endl;
        string prefix_builder;
        for (auto& string_in_ans_builder : ans_builder) {
            cout << "string_in_ans_builder=" << string_in_ans_builder << " string_in_ans_builder[curr_idx]=" << string_in_ans_builder[curr_idx] << endl;
            prefix_builder.push_back(string_in_ans_builder[curr_idx]);//r
        }
        cout << "prefix_builder=" << prefix_builder << endl;
        vector<string> startWith = find_by_prefix(prefix_builder);
        for (auto& str : startWith) {
            cout << "str=" << str << endl;
            ans_builder.push_back(str);
            for (auto& str2 : ans_builder) {
                cout << "str2=" << str2 << endl;
            }
            recursion_helper(words, ans_builder);
            ans_builder.pop_back();
            for (auto& str3 : ans_builder) {
                cout << "str3=" << str3 << endl;
            }
        }
    }

    vector<vector<string>> wordSquares(vector<string>& words) {

        build_trie(words);

        vector<string> ans_builder;

        for (string word : words) {
            ans_builder.push_back(word);
            recursion_helper(words, ans_builder);
            ans_builder.pop_back(); //throw away the unproper word
        }
        return ans;
    }
};
#endif

class Trie {
#define NUMS 26
public:
    bool isEnd;
    Trie* next[NUMS];
    vector<string> startwith_word;
    vector<int> indexs;

    Trie() {
        isEnd = false;
        memset(next, 0, sizeof(next));
    }
    
    void insert(string word) {
        Trie *node = this;
        for (auto &ch : word) {
            int idx = ch - 'a';
            Trie *child = node->next[idx];
            if (child == 0) {
                child = new Trie();
            }
            //node->next[ch - 'a'] = child;
            child->startwith_word.push_back(word);
            node = child;
        }
        node->isEnd = true;
    }
    void insert_idx(string word, int idx) {
        Trie *node = this;
        int ch_id = 1;
        for (auto &ch : word) {
            if (node->next[ch - 'a'] == 0) {
                node->next[ch - 'a'] = new Trie();
            }

            node->next[ch - 'a']->startwith_word.push_back(word);
            node->next[ch - 'a']->indexs.push_back(idx);
            node = node->next[ch - 'a'];

            ++ch_id;
        }
        node->isEnd = true;
    }
    bool search(string word) {
        Trie *node = this;
        for (auto &ch : word) {
            Trie *child = node->next[ch-'a'];
            if (child) {
                node = child;
            }
            else {
                return false;
            }
        }
        return node->isEnd;
    }

    bool startsWith(string prefix) {
        bool found = true;
        Trie *node = this;
        for (auto &ch : prefix) {
            Trie *child = node->next[ch-'a'];
            if (child) {
                node = child;
            }
            else {
                return false;
            }
        }
        return found;
    }

};

class Solution {
public:
    int word_length;
    vector<vector<string>> ans;
    Trie trie;

    Trie* get_root() { return &trie; }

    void build_trie(vector<string>& words)
    {
        for (int i = 0; i < words.size(); ++i) {
            trie.insert_idx(words[i], i);
        }
        
    }

    vector<string> find_by_prefix(string prefix) {
        vector<string> ret;

        Trie* node = get_root();
        for (auto& ch : prefix) {
            Trie* child = node->next[ch - 'a'];
            if (!child)
                return ret; //find nothing

            node = child;
        }

        for (int i = 0; i < node->startwith_word.size(); ++i)
            ret.push_back(node->startwith_word[i]);
        return ret;
    }

    void recursion_helper(vector<string>& words, vector<string>& ans_builder) {
        if (ans_builder.size() == words[0].size()) {
            ans.push_back(ans_builder);
            return;
        }

        int curr_idx = ans_builder.size();

        string prefix_builder;
        for (auto& string_in_ans_builder : ans_builder) {

            prefix_builder.push_back(string_in_ans_builder[curr_idx]);//r
        }

        vector<string> startWith = find_by_prefix(prefix_builder);
        for (auto& str : startWith) {
            ans_builder.push_back(str);
            recursion_helper(words, ans_builder);
            ans_builder.pop_back();
        }
    }

    vector<vector<string>> wordSquares(vector<string>& words) {

        build_trie(words);

        vector<string> ans_builder;

        for (string word : words) {
            ans_builder.push_back(word);
            recursion_helper(words, ans_builder);
            ans_builder.pop_back(); //throw away the unproper word
        }
        return ans;
    }
};

int main()
{
	{
        Solution solution;
        vector<string> v = { "area","lead","wall","lady","ball" };

        vector<vector<string>> ret = solution.wordSquares(v);
        cout << "[";
        for (int i = 0; i < ret.size(); ++i) {
            cout << "[";
            for (int j = 0; j < ret[i].size(); ++j) {
                if (j < ret[i].size() -1)
                    cout << "\"" << ret[i][j] << "\"" << ", ";
                else
                    cout << "\"" << ret[i][j] << "\"" << " ";
            }
            if (i < ret.size() - 1)
                cout << "],";
            else
                cout << "]";
        }
        cout << "]" << endl;
	}
#if 0
	{
        Solution solution;
        vector<string> v = { "abat","baba","atan","atal" };

        vector<vector<string>> ret = solution.wordSquares(v);
        cout << "[";
        for (int i = 0; i < ret.size(); ++i) {
            cout << "[";
            for (int j = 0; j < ret[i].size(); ++j) {
                if (j < ret[i].size() - 1)
                    cout << "\"" << ret[i][j] << "\"" << ", ";
                else
                    cout << "\"" << ret[i][j] << "\"" << " ";
            }
            if (i < ret.size() - 1)
                cout << "],";
            else
                cout << "]";
        }
        cout << "]" << endl;
	}
#endif
	return 0;
}